/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
module.exports = {
  plugins: ['node'],
  extends: [
    'plugin:node/recommended',
    'airbnb-base/legacy',
    '@syntropize/eslint-config-common',
  ],
  env: {
    node: true,
  },
  rules: {
    'node/exports-style': ['error', 'module.exports', { allowBatchAssign: true }],
    'no-console': 'off',
    'no-multi-assign': 'off',
    strict: 'off',
    'comma-dangle': ['error', 'always-multiline'],
  },
};
