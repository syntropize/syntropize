/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
module.exports = {
  root: true,
  env: {
    es2021: true,
  },
  parserOptions: {
    ecmaVersion: 2021,
  },
  extends: [
    'airbnb-base',
    'angular',
    '@syntropize/eslint-config-import',
    '@syntropize/eslint-config-common',
  ],
  rules: {
    'no-param-reassign': 'off', // allow assignment to angular DI objects
    'prefer-arrow-callback': 'off', // clashes with angular controllers
    'angular/on-watch': 'off',
  },
};
