/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
module.exports = {
  plugins: [
    'json',
  ],
  parserOptions: {
    sourceType: 'module',
  },
  rules: {
    // Import Settings
    'no-multi-spaces': ['error', {
      exceptions: { ImportDeclaration: true }, // allows aligned input statements
    }],
    'import/no-extraneous-dependencies': ['error', {
      devDependencies: ['!@rushstack/eslint-patch'],
    }],
    'import/no-unresolved': ['error', {
      ignore: ['fs/promises'],
    }],
    'import/extensions': ['error', 'ignorePackages'],
  },
};
