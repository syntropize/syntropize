/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
module.exports = {
  hooks: {
    readPackage(pkg) {
      if (pkg.peerDependencies && pkg.peerDependencies.stylelint) {
        // eslint-disable-next-line no-param-reassign
        delete pkg.peerDependencies.stylelint;
      }
      return pkg;
    },
  },
};
