/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
module.exports = {
  defaultSeverity: 'warning',
  extends: 'stylelint-config-standard',
  rules: {
    'selector-type-no-unknown': [true, {
      ignore: ['custom-elements', 'default-namespace'],
    }],
    'comment-empty-line-before': ['always', {
      ignore: ['after-comment', 'stylelint-commands'],
      except: ['first-nested'],
      ignoreComments: [/\b: \b/, /end/i], // ignore spaces for commented rules
    }],
    'max-empty-lines': 2,
  },
};
