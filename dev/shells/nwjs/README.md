# Syntropize Development Tools for NW.js Shell

Syntropize Development Tools for NW.js Shell

### Usage

```sh
syntropize-nw [command] [options]
```

or

```sh
s-nw [command] [options]
```

### Commands

```
Start     Start Syntropize
Build     Build Syntropize
Download  Download NW.js to cache
```

To learn more about a command, invoke it with the help flag `--help` or `-h`.


## Start

Start the Syntropize Application. Automatically downloads the specified version of NW.js (if not in cache).

### Usage

```sh
syntropize-nw start [options] [-- <args>]
```

#### Arguments

```
[options]    Options to specify NW.js instance to be used

<args>       Arguments to the NW.js instance
                See NW.js docs for possible values
```

#### Options

```
-a, --arch          OS Architecture to target, can be:
                      32 for 32-bit
                      64 for 64-bit
                      default: Architecture of the current OS

-v, --version       Use the specified NW.js version
                      default: package.json > config > nwjs or latest

-f, --forceDownload Download a fresh copy of NW.js
                      default: false

--cacheDir          Specify an alternate NW.js cache directory
                      default: ./nwjs_cache
```


## Build

Build Syntropize from source files. Automatically downloads the specified version of NW.js (if not in cache).

### Usage

```sh
syntropize-nw download [options]
```

#### Stage Options*

```
-B  --bundle        Bundle Syntropize with NW.js
                      outputs to _bundle/

-R  --release       Create a distributable package
                      outputs to _pkg/

*Not specifying stage options will result in both operations being carried out
```

#### Build Options

```
-p, --platforms     Platforms to build, comma-sperated, can be:
                      win32, win64, osx32, osx64, linux32, linux64
                      default: all

-v, --version       Use the specified NW.js version
                      default: package.json > config > nwjs or latest

-f, --forceDownload Download a fresh copy of NW.js
                      default: false

--cacheDir          Specify an alternate NW.js cache directory
                      default: ./nwjs_cache
```

## Download

Downloads NW.js to the cache folder. It is recommended to run this in advance to allow for faster start and build.

### Usage

```sh
syntropize-nw download [options]
```

#### Options

```
-p, --platforms     Platforms to build, comma-sperated, can be:
                      win32, win64, osx64, linux32, linux64
                      default: current OS

-v, --version       Use the specified NW.js version
                      default: package.json > config > nwjs or latest

-f, --forceDownload Download a fresh copy of NW.js
                      default: false

--cacheDir          Specify an alternate NW.js cache directory
                      default: ./nwjs_cache
```
