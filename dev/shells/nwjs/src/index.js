/*!
 *  Copyright (c) 2022-2023, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import AggregateError from 'es-aggregate-error';

import bundle from './bundle/index.js';
import release from './release/index.js';
import startApp from './start/index.js';
import dl from './download/index.js';

import getCurrentPlatform from './utils/getCurrentPlatform.js';

async function build(options) {
  const {
    version,
    cacheDir,
    forceDownload,
  } = options;

  // Code to Handle OS platforms

  const platforms = [];
  const currentPlatform = getCurrentPlatform(options.arch);

  if (!options.platforms) {
    platforms.push('win32', 'win64', 'linux32', 'linux64');
    if (!currentPlatform.startsWith('win')) {
      platforms.push('osx64');
    }
  }
  else {
    options.platforms.split(',').forEach(function filterInvalidPlatforms(platform) {
      if (currentPlatform.startsWith('win') && platform.startsWith('osx')) {
        console.warn('Macintosh OS X build is not possible on Windows.');
      }
      else {
        platforms.push(platform);
      }
    });
  }

  if (platforms.length === 0) {
    throw new Error('Aborting: No valid platforms to target.');
  }

  if (options.bundle || !options.release) {
    try {
      await bundle({
        platforms,
        version,
        cacheDir,
        forceDownload,
      });
    }
    catch (error) {
      // TODO: filter out failed OS for release
      console.error('Failed to bundle');
      throw error;
    }
  }

  if (options.release || !options.bundle) {
    const results = await release({
      platforms,
    });
    const errors = results
      .filter(result => result.status === 'rejected')
      .map(result => result.reason)
    ;
    if (errors.length === 1) {
      throw errors[0];
    }
    if (errors.length > 1) {
      throw new AggregateError(errors, 'Failed to package');
    }
  }
}

async function download(options) {
  const {
    version,
    cacheDir,
    forceDownload,
  } = options;

  const platforms = [];
  const currentPlatform = getCurrentPlatform(options.arch);

  if (!options.platforms) {
    platforms.push(currentPlatform);
  }
  else if (currentPlatform.startsWith('win')) {
    options.platforms.split(',').forEach(function filterInvalidPlatforms(platform) {
      if (platform.startsWith('osx')) {
        console.warn('NWjs build for Macintosh OS X build cannot be used on Windows and will not be downloaded.');
      }
      else {
        platforms.push(platform);
      }
    });
  }

  if (platforms.length === 0) {
    throw new Error('Aborting: No valid platforms to download NW.js for specified.');
  }

  try {
    await dl({
      platforms,
      version,
      cacheDir,
      forceDownload,
    });
  }
  catch (error) {
    console.error('Failed to download NW.js');
    throw error;
  }
}

async function start(options) {
  const {
    version,
    cacheDir,
    forceDownload,
    '--': argv,
  } = options;

  await startApp({
    platforms: [getCurrentPlatform(options.arch)],
    version,
    cacheDir,
    forceDownload,
    argv,
  });
}

export {
  build,
  start,
  download,
};
