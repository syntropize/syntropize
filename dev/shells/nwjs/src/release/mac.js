/*!
 *  Copyright (c) 2023, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import libOS from 'os';

function macPkg() {
  if (libOS.platform() !== 'darwin') {
    console.log('  Distributable for Mac OSX cannot be created on another OS!');
  }
  else {
    console.log('  Distributable for Mac OS X is not implemented!');
  }
}

export default macPkg;
