/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import libPath from 'path';
import libUrl from 'url';
import fs from 'fs/promises';
import winSetup from 'innosetup-compiler';
import readPkg from 'read-pkg-up';

import defaults from '../defaults.json';

function setup(configFile, setupOptions) {
  return new Promise(function createWindowsPkg(resolve, reject) {
    winSetup(configFile, setupOptions, function winSetupCallback(error) {
      if (error) { reject(error); }
      else { resolve(); }
    });
  });
}

async function writeConfig(src, des, args) {
  const template = await fs.readFile(src, 'utf8');
  const config = template.replace(/<%(.*?)%>/gm, (match, p) => args[p]);
  await fs.writeFile(des, config);
}

async function delConfig(des) {
  await fs.unlink(des);
}

async function winPkg(arch) {
  const pkg = (await readPkg()).packageJson;

  const {
    dist,
    release,
  } = defaults.path;

  const configFile = `win-${arch}bit-installer.iss`;
  const configTemplate = libUrl.fileURLToPath(new URL(configFile, import.meta.url));
  const configPath = libPath.join(dist, configFile);

  const setupOptions = {
    gui: false,
    verbose: false,
  };

  const configArgs = {
    version: pkg.version,
    source: './',
    dest: `../${release}`,
  };

  await writeConfig(configTemplate, configPath, configArgs);
  await setup(configPath, setupOptions);
  await delConfig(configPath);
}

export default winPkg;
