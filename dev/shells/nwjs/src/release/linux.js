/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import archiver from 'archiver';
import lzma from 'lzma-native';

import { createWriteStream } from 'fs';
import { mkdir } from 'fs/promises';
import { pipeline } from 'stream';
import { promisify } from 'util';
import libPath from 'path';

import defaults from '../defaults.json';

const filters = [
  { id: lzma.FILTER_X86 },
  {
    id: lzma.FILTER_LZMA2,
    options: {
      niceLen: 256,
      /* eslint-disable no-bitwise */
      dictSize: 1 << 29,
      preset: 9 | lzma.PRESET_EXTREME,
      /* eslint-enable */
    },
  },
];

async function linuxPkg(arch) {
  const {
    dist,
    release,
    name,
  } = defaults.path;

  const dist$ = `${dist}/${name}`;

  mkdir(release, { recursive: true });
  const archive = archiver('tar');
  const compressor = lzma.createStream('streamEncoder', { filters });
  const output = createWriteStream(libPath.join(release, 'Syntropize-linux-x86-' + arch + 'bit.tar.xz'));

  const job = promisify(pipeline)(
    archive,
    compressor,
    output,
  );

  archive
    .directory(libPath.join(dist$, 'data'), 'data')
    .directory(libPath.join(dist$, 'linux' + arch), 'syntropize')
    .finalize()
  ;

  await job;
  console.log('Successfully created Linux ' + arch + 'bit bundle: ' + archive.pointer() + ' bytes');
}

export default linuxPkg;
