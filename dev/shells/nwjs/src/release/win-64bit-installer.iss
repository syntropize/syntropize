; Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
;
; This Source Code Form is subject to the terms of the Mozilla Public
; License, v. 2.0. If a copy of the MPL was not distributed with this
; file, You can obtain one at http://mozilla.org/MPL/2.0/.
;
; SPDX-License-Identifier: MPL-2.0

#define MyAppName "Syntropize"
#define MyAppVersion "<%version%>"
#define MyAppURL "https://syntropize.com/"
#define MyAppExeName "syntropize.exe"
#define MyAppSettings "config\settings.json"

[Setup]
AppId={{F1F0B046-03A8-4496-A1CF-E723ABB283E8}
AppName={#MyAppName}
AppPublisher={#MyAppName}
AppPublisherURL={#MyAppURL}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} v{#MyAppVersion}
DefaultDirName={autopf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AllowNoIcons=yes
SourceDir="<%source%>"
OutputDir="<%dest%>"
OutputBaseFilename={#MyAppName}-setup-win-x86-64bit
Compression=lzma2/ultra64
SolidCompression=yes
; "ArchitecturesAllowed=x64" specifies that Setup cannot run on
; anything but x64.
ArchitecturesAllowed=x64
; "ArchitecturesInstallIn64BitMode=x64" requests that the install be
; done in "64-bit mode" on x64, meaning it should use the native
; 64-bit Program Files directory and the 64-bit view of the registry.
ArchitecturesInstallIn64BitMode=x64

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 0,6.1

[Files]
Source: "syntropize\win64\syntropize.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "syntropize\win64\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs
Source: "syntropize\data\*"; DestDir: "{commondocs}\{#MyAppName}\"; Excludes: "\_meta.json"; Flags: ignoreversion recursesubdirs uninsneveruninstall;
Source: "syntropize\data\_meta.json"; DestDir: "{commondocs}\{#MyAppName}\"; Flags: onlyifdoesntexist uninsneveruninstall;
; dummy copy for AfterInstall
Source: "syntropize\win64\NOTICE"; DestDir: "{tmp}"; Flags: ignoreversion; AfterInstall: WriteInitPathSettings(ExpandConstant('{app}\{#MyAppSettings}'),ExpandConstant('{commondocs}\{#MyAppName}'));
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{autoappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

[Code]
var
  OldContent : AnsiString;
  NewContent: string;
procedure WriteInitPathSettings(Filename:String;ContentLocation:String);
begin
  StringChangeEx(ContentLocation,'\','/',True);
  if LoadStringFromFile(Filename, OldContent) then
  begin
    NewContent := String(OldContent);
    StringChangeEx(NewContent, '../data', 'file:///' + ContentLocation, True);
    SaveStringToFile(Filename, NewContent, False);
  end;
end;
