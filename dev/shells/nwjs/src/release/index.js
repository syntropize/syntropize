/*!
 *  Copyright (c) 2022-2023, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import NwBuilder from 'nw-builder';

import winPkg from './windows.js';
import linuxPkg from './linux.js';
import macPkg from './mac.js';

const pkg = {
  win: winPkg,
  linux: linuxPkg,
  osx: macPkg,
};

async function osPackager(platform) {
  const pName = platform.slice(0, -2);
  const pArch = platform.slice(-2);

  if (pkg[pName] && (pArch === '32' || pArch === '64')) {
    return await pkg[pName](pArch);
  }
  else {
    console.log('  Ignoring Invalid Platform:', platform);
    return undefined;
  }
}

async function release(options = {}) {
  const { platforms } = (new NwBuilder({
    files: 'package.json',
    platforms: options.platforms,
  })).options;

  return Promise.allSettled(platforms.map(platform => osPackager(platform)));
}

export default release;
