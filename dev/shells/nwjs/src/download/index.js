/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import Builder from 'nw-builder';
import readPkg from 'read-pkg-up';

import libOs from 'os';
import libPath from 'path';

import defaults from '../defaults.json';

async function download(options) {
  const pkg = (await readPkg()).packageJson;
  const nwOptions = {
    files: '*',
    version: options.version || process.env.npm_config_nwjs || pkg.config.nwjs,
    platforms: options.platforms,
    cacheDir: options.cacheDir || defaults.path.nwjsCache || libPath.resolve(libOs.homedir(), '.nwjs'),
    forceDownload: options.forceDownload || false,
  };

  const nw = new Builder(nwOptions);
  nw.on('log', console.log);
  nw.on('error', console.error);
  await nw.resolveLatestVersion();
  await nw.checkVersion();
  await nw.platformFilesForVersion();
  await nw.downloadNwjs();
}

export default download;
