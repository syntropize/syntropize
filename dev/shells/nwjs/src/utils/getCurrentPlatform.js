/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * SPDX-License-Identifier: MPL-2.0
 */
import os from 'os';

function getCurrentPlatform(optsArch) {
  const osMap = {
    win32: 'win',
    darwin: 'osx',
    linux: 'linux',
  };

  const platform = os.platform();
  if (!osMap[platform]) {
    throw new Error(`NWjs does not work on ${platform} platform!`);
  }

  if (optsArch === '32' || optsArch === '64') {
    return `${osMap[platform]}${optsArch}`;
  }
  if (optsArch) {
    console.log(`Disregarding invalid architecture ${optsArch}`);
  }
  const arch = os.arch();
  if (arch[0] !== 'x') {
    throw new Error(`NWjs does not work on ${arch} architecture!`);
  }

  return `${osMap[platform]}${arch.slice(1)}`;
}

export default getCurrentPlatform;
