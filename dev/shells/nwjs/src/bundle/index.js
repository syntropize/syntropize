/*!
 *  Copyright (c) 2022-2023, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import NwBuilder from 'nw-builder';
import { merge as _merge } from 'lodash-es';
import fs from 'fs-extra';
import libOs from 'os';
import libPath from 'path';

import readPkg from 'read-pkg-up';

import defaults from '../defaults.json';

const macHome = '../../../../';

const {
  files,
  path,
} = defaults;

const newSettings = {
  initialize: {
    home: path.data,
    debug: false,
  },
};

function eolize(os, contents) {
  return os.startsWith('win') ?
    contents.replace(/\r?\n/mg, '\r\n') :
    contents.replace(/\r?\n/mg, '\n')
  ;
}

async function writeFile(os, filename, contents) {
  const loc = os.startsWith('osx') ? `${path.name}.app/Contents/Resources/app.nw` : '';
  return fs.outputFile(
    libPath.join(path.dist, path.name, os, loc, filename),
    eolize(os, contents),
  );
}

async function copyFile(os, src, filename) {
  const loc = os.startsWith('osx') ? `${path.name}.app/Contents/Resources/app.nw` : '';
  return fs.copyFile(
    src,
    libPath.join(path.dist, path.name, os, loc, filename),
  );
}

async function writeExecutableFile(os, filename, contents) {
  const loc = os.startsWith('osx') ? `${path.name}.app/Contents/Resources/app.nw` : '';
  const filePath = libPath.join(path.dist, path.name, os, loc, filename);
  await fs.outputFile(filePath, eolize(os, contents));
  await fs.chmod(filePath, '755');
}

function writeFactory(platforms, executable) {
  return async function writeForListedPlatforms(filename, contents) {
    return await Promise.all(platforms.map(function writeContentForOS(os) {
      return (executable ? writeFile : writeExecutableFile)(os, filename, contents);
    }));
  };
}

function copyFactory(platforms) {
  return async function writeForListedPlatforms(src, filename) {
    return await Promise.all(platforms.map(function writeContentForOS(os) {
      return copyFile(os, src, filename);
    }));
  };
}

function generateLinuxDesktopFile(version) {
  return `[Desktop Entry]
Name = Syntropize
Comment = Reimagine the Desktop
Version = ${version}
Exec = bash -c "cd $(dirname %k) && ./${path.name}"
Type = Application
Terminal = false
${path.icon && path.icon.linux ? `Icon = ./${path.name}\n` : ''}`;
}

async function writeExternalFiles(options = {}, pkg = { version: '1.0.0' }) {
  // Global write
  const write = writeFactory(options.platforms);

  // Platform specific writes
  const osxPlatforms = options.platforms.filter(p => p.startsWith('osx'));
  const writeMacFiles = writeFactory(osxPlatforms);

  const linuxPlatforms = options.platforms.filter(p => p.startsWith('linux'));
  const writeLinuxFiles = writeFactory(linuxPlatforms);

  const windowsPlatforms = options.platforms.filter(p => p.startsWith('win'));
  const writeWindowsFiles = writeFactory(windowsPlatforms);

  // Write Settings File with modified settings
  const oldSettings = await fs.readJson(libPath.join(path.src, path.settings), 'utf-8');
  const settings = _merge({}, oldSettings, newSettings);
  await writeLinuxFiles(path.settings, JSON.stringify(settings, null, 2));
  await writeWindowsFiles(path.settings, JSON.stringify(settings, null, 2));
  settings.initialize.home = libPath.join(macHome, settings.initialize.home); 
  await writeMacFiles(path.settings, JSON.stringify(settings, null, 2));

  // Write Sample settings File with modified settings
  const sampleSettingsFilename = path.settings.replace(/\..*?$/, '.SAMPLE$&');
  const oldSampleSettings = await fs.readJson(libPath.join(path.src, sampleSettingsFilename), 'utf-8');
  const sampleSettings = _merge({}, oldSampleSettings, newSettings);
  await writeLinuxFiles(sampleSettingsFilename, JSON.stringify(sampleSettings, null, 2));
  await writeWindowsFiles(sampleSettingsFilename, JSON.stringify(sampleSettings, null, 2));
  sampleSettings.initialize.home = libPath.join(macHome, settings.initialize.home); 
  await writeMacFiles(sampleSettingsFilename, JSON.stringify(sampleSettings, null, 2));

  // Copy Blank Config Sample to Config
  const sampleCredFilename = path.credentials.replace(/\..*?$/, '.SAMPLE$&');
  const sampleCreds = await fs.readJson(libPath.join(path.src, sampleCredFilename), 'utf-8');
  const scrubbedCreds = {};
  if (typeof sampleCreds === 'object') {
    Object.keys(sampleCreds).forEach((origin) => { scrubbedCreds[origin] = {}; });
  }
  await write(sampleCredFilename, JSON.stringify(sampleCreds, null, 2));
  await write(path.credentials, JSON.stringify(scrubbedCreds, null, 2));

  // Create a readme file
  const intro = await fs.readFile(libPath.join(path.src, oldSettings.initialize.home, 'Syntropize', '_section.md'), 'utf8');
  const repoReadme = await fs.readFile(libPath.join(path.src, path.readme), 'utf8');
  const usage = repoReadme.match(/## Usage\r?\n\r?\n.*?\r?\n/m);
  const outro = repoReadme.match(/## Copyright[\s\S]*/mg);
  const readme = `${intro}\n${usage ? usage[0] : ''}\n${outro ? outro[0] : ''}`;
  await write(path.readme, readme);

  // Write Desktop File on Linux
  const writeLinuxExecutables = writeFactory(linuxPlatforms, true);
  const copyLinuxFiles = copyFactory(linuxPlatforms);
  await writeLinuxExecutables(`${path.name}.desktop`, generateLinuxDesktopFile(pkg.version));
  if (path.icon && path.icon.linux) {
    await copyLinuxFiles(path.icon.linux, `${path.name}${libPath.extname(path.icon.linux)}`);
  }

  // Copy Data
  const dataDes = libPath.join(path.dist, path.name, 'data');
  await fs.rmdir(dataDes, { recursive: true });
  await fs.copy(libPath.resolve(path.src, oldSettings.initialize.home), dataDes);
}


async function getOptions(options = {}, pkg = { version: '1.0.0' }) {
  const version = process.env.npm_config_nwjs || pkg.config.nwjs || 'latest';
  const appName = path.name.toLowerCase();

  const nwOptions = {
    files,
    platforms: options.platforms,
    version: options.version || version,
    cacheDir: options.cacheDir || path.nwjsCache || libPath.resolve(libOs.homedir(), '.nwjs'),
    forceDownload: options.forceDownload || false,
    buildDir: path.dist,
    appName,
    zip: false,
    winVersionString: {
      CompanyName: 'Rahul Gupta and Syntropize Contributors',
      FileDescription: 'Launch Syntropize',
      ProductName: 'Syntropize',
      ProductVersion: `${pkg.version}`,
      // FileVersion: `${pkg.version}`, // not being reset for unknown reasons
      LegalCopyright: 'Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors',
    },
  };

  if (path.icon && path.icon.win) {
    nwOptions.winIco = path.icon.win;
  }

  if (path.icon && path.icon.mac) {
    nwOptions.macIcns = path.icon.mac;
  }

  nwOptions.macPlist = {
    CFBundleDisplayName: 'Syntropize',
  }

  return nwOptions;
}

async function bundleNWJS(options) {
  const pkg = (await readPkg()).packageJson;
  const nwOptions = await getOptions(options, pkg);
  const nw = new NwBuilder(nwOptions);

  nw.on('log', console.log);

  await nw.build();

  await writeExternalFiles(nw.options, pkg);
}

export default bundleNWJS;
