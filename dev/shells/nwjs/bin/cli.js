#!/usr/bin/env node --experimental-json-modules
/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import minimist from 'minimist';

import { readFile } from 'fs/promises';
import * as tools from '../src/index.js';

const commands = Object.keys(tools);

async function cli(cmd, args) {
  if (!cmd || cmd === 'help' || cmd === '--help' || cmd === '-h') {
    const help = await readFile(new URL('./help/main.txt', import.meta.url), 'utf8');
    console.log(help);
    return;
  }

  if (!commands.includes(cmd)) {
    throw new Error('Invalid Command');
  }

  const cliOptions = {
    boolean: [
      'help',
      'forceDownload',
    ],
    string: [
      'version',
      'cacheDir',
    ],
    '--': true,
    unknown: function unknownArg(x) {
      console.warn(`Unknown argument ${x} will be ignored!\n`);
      return false;
    },
    alias: {
      v: 'version',
      f: 'forceDownload',
      h: 'help',
    },
  };

  if (cmd === 'start') {
    cliOptions['--'] = true;
    cliOptions.string.push('arch');
    cliOptions.alias.a = 'arch';
  }
  else {
    cliOptions.string.push('platforms');
    cliOptions.boolean.push('bundle', 'release');
    Object.assign(cliOptions.alias, {
      p: 'platforms',
      B: 'bundle',
      R: 'release',
    });
  }

  const options = minimist(args, cliOptions);

  if (options.h) {
    const help = await readFile(new URL(`./help/${cmd}.txt`, import.meta.url), 'utf8');
    const common = await readFile(new URL('./help/common.txt', import.meta.url), 'utf8');
    console.log(`\n${help}${common}`);
    return;
  }

  await tools[cmd](options);
}

(async function run() {
  try {
    await cli(process.argv[2], process.argv.slice(3));
  }
  catch (error) {
    console.error(error);
    process.exitCode = 1;
  }
})();
