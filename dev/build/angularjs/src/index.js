/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const options = Object.freeze({
  pluginOptions: {
    styles: {
      mode: 'extract',
      sourceMap: true,
    },
    htmlString: {
      sourcemap: true,
    },
  },
  output: {
    assetFileNames: '[name][extname]',
  },
});

if (process.env.NODE_ENV === 'production') {
  options.pluginOptions.htmlString.htmlMinifierOptions = {
    sortAttributes: true,
    collapseWhitespace: true,
    conservativeCollapse: true,
    removeComments: true,
    minifyCSS: true,
  };
  options.pluginOptions.styles.minimize = {
    preset: ['default', {
      discardComments: {
        removeAll: true,
      },
    }],
  };
}

export default options;
