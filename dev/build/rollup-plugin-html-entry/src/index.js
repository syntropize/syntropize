/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { readFileSync, copyFileSync } from 'fs';
import libPath from 'path';
import { EOL } from 'os';

function htmlEntry({ input, assetDir }) {
  let source;

  return {
    name: 'html-entry',
    generateBundle(options, bundle, isWrite) {
      if (!isWrite) {
        this.error('html-entry plugin currently only works with bundles that are written to disk');
      }

      if (typeof input !== 'string') {
        this.error('Invalid input to html-entry plugin, expect type string');
      }

      try {
        source = readFileSync(input, 'utf8');
      }
      catch (error) {
        this.error(error);
      }


      function replaceAsset(match, assetPath) {
        const assetReplace = libPath.posix.join(assetDir, libPath.basename(assetPath));
        copyFileSync(libPath.resolve(libPath.dirname(input), assetPath), libPath.join(options.dir || '.', assetReplace));
        return match.replace(assetPath, assetReplace);
      }

      source = source.replace(/<img.*? src=(?:"|')(.*?)(?:"|').*?>/mg, replaceAsset);
      source = source.replace(/<link.*? href=(?:"|')(.*?)(?:"|').*?>/mg, replaceAsset);


      const style = Object.keys(bundle)
        .filter(file => file.endsWith('.css'))
        .map(file => `  <link href="./${file}" rel="stylesheet">`)
        .join(EOL)
      ;

      const scripts = Object.values(bundle)
        .filter(a => a.isEntry)
        .map(a => a.fileName)
        .map(a => `  <script type="module" src="${a}"></script>`)
        .join(EOL)
      ;

      source = source.replace('</body>', `${style}${EOL}${scripts}${EOL}</body>`);


      this.emitFile({
        type: 'asset',
        fileName: libPath.basename(input),
        source,
      });
    },
  };
}

export default htmlEntry;
