/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import path from 'path';

// rebase as if all files originate in output folder
function rebaseToOutput(relativeSourcePath, sourcemapPath) {
  const source = Array.isArray(this.input) ? this.input[0] : this.input;
  const rebase = path.relative(path.dirname(sourcemapPath), path.dirname(source));
  return path.relative(rebase, relativeSourcePath);
  // TODO: fix for multiple inputs
}

const pluginOptions = {
  json: {},
  commonjs: {},
  nodeResolve: {},
  nodeExternals: {},
  sourcemaps: {},
};

if (process.env.NODE_ENV === 'production') {
  Object.assign(pluginOptions, {
    modify: {
      'process.env.NODE_ENV': JSON.stringify('production'),
    },
    strip: {
      debugger: true,
      functions: ['console.*', 'assert.*', 'debug.*', 'debug', 'alert'],
    },
    terser: {
      format: {
        comments: false,
        ecma: 2021,
      },
    },
  });
}

const options = {
  pluginOptions,
  output: {
    format: 'es',
    interop: 'auto',
    exports: 'auto',
    sourcemap: true,
    sourcemapPathTransform: rebaseToOutput,
  },
};

export default options;
