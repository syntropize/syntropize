/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import defaults from './defaults.js';
import plugins from './plugins.js';
import merge from './merge.js';

function resolvePlugins(pluginOptions) {
  return Object.keys(plugins)
    .filter(plugin => pluginOptions[plugin])
    .map(plugin => plugins[plugin](pluginOptions[plugin]))
  ;
}

function generateOptions(...options) {
  const mergedOptions = merge([defaults, ...options]);
  mergedOptions.plugins = [
    ...resolvePlugins(mergedOptions.pluginOptions),
    ...(mergedOptions.plugins || []),
  ];
  delete mergedOptions.pluginOptions;
  return mergedOptions;
}

export default generateOptions;
