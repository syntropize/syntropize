/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import consts from 'rollup-plugin-consts';
import modify from 'rollup-plugin-modify';
import strip from '@rollup/plugin-strip';
import shim from 'rollup-plugin-shim';
import alias from '@rollup/plugin-alias';
import legacy from '@rollup/plugin-legacy';
import json from '@rollup/plugin-json';
import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';
import nodeExternals from 'rollup-plugin-node-externals';
import externalGlobals from 'rollup-plugin-external-globals';
import styles from 'rollup-plugin-styles';
import url from '@rollup/plugin-url';
import htmlString from 'rollup-plugin-html-string';
import sourcemaps from 'rollup-plugin-sourcemaps';
import { terser } from 'rollup-plugin-terser';

export default Object.freeze({
  consts,
  modify,
  strip,
  shim,
  alias,
  legacy,
  json,
  commonjs,
  nodeResolve,
  nodeExternals,
  externalGlobals,
  styles,
  url,
  htmlString,
  sourcemaps,
  terser,
});
