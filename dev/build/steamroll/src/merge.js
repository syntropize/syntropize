/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { mergeWith as _mergeWith } from 'lodash-es';

const result = {};

function customMergeOptions(objValue, srcValue, key) {
  // Concatanate plugin arrays
  if (key === 'plugins') {
    if (Array.isArray(objValue) && Array.isArray(srcValue)) {
      return [...objValue, ...srcValue];
    }
    if (Array.isArray(objValue) && !srcValue) {
      return objValue;
    }
    if (Array.isArray(srcValue)) {
      return srcValue;
    }
  }
  if (key === 'output') {
    if (Array.isArray(srcValue) && Array.isArray(objValue)) {
      throw new TypeError('Cannot merge output arrays in different configurations!');
    }
    else if (Array.isArray(srcValue)) {
      return srcValue.map(src => _mergeWith({}, objValue, src, customMergeOptions));
    }
    else if (Array.isArray(objValue)) {
      return objValue.map(obj => _mergeWith({}, obj, srcValue, customMergeOptions));
    }
  }
  if (Array.isArray(objValue)) {
    // Discard Previous array setting
    return srcValue;
  }
  if (typeof srcValue === 'function') {
    // Bind config functions to this
    // Allows one to reuse consolidated options data in functions
    return srcValue.bind(result);
  }
  return undefined;
}

function mergeOptions(options) {
  if (typeof options === 'object') {
    if (Array.isArray(options)) {
      return _mergeWith(result, ...(options.flat(Infinity)), customMergeOptions);
    }
    else {
      return _mergeWith(result, options, customMergeOptions);
    }
  }
  else {
    throw new TypeError('Options provided are not an object or an array.');
  }
}

export default mergeOptions;
