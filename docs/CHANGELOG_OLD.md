# CHANGE LOG

All notable changes to the Syntropize project up to v0.7 are documented in this file.

## v0.7

### 0.7.0

Syntropize has been redesigned as an operating environment wherein the user experience is realized by dynamically orchestrating a mix-and-match of co-operating programs and data from multiple sources.

As part of this redesign the code base has been completely re-organized:

- Front-end and Back-end are completely independent.
- Shell has been isolated from the application. In principle, Syntropize can work with any browser based shell.
- The various functional components have be split up into their own packages.
- The code is managed using a monorepo.

#### Root

- Added: `.gitattributes` with line-break rules to be applied to repo files at checkout. This ensures line breaks are appropriate to file and operating system.
- Added: Continuous Integration to trigger a re-build of the Syntropize website when documentation is updated.
- Modified: Readme to provide an updated introduction.
- Added: Headright package to update Copyright and License headers in repo files.

##### Docs

- Modified: Usage instructions completely re-written for the new system.
- Modified: Build instructions moved to `Usage`.
- Removed: File containing Build instructions.
- Fixed: Spelling and Grammar errors in all documentation files.

**Development Dependencies**

- Added:
  - cross-env: ^7.0.3
  - rollup: <=2.54.0
  - headright: ^0.1.1

- Updated:
  - eslint: ^7.16.0
  - stylelint: ^13.8.0
  - npm-run-all: ^4.1.5

- Removed:
  - app-root-path
  - ejs
  - jspm
  - jspm-bower-endpoint
  - jspm-git
  - lint-staged
  - node-jspm-jasmine
  - vulcanize
  - husky


#### (new) Data

- Added: Preliminary documentation for Syntropize.

#### (new) Shells

Shells are completely decoupled from the application. To achieve this:

- Shells provide an environment API specific to the shell,
- Shells load the API from their packages (in case of NW.js) onto the background window object,
- Shells provide the logic for application start-up.

##### (new) NW.js

- Added: Icon for the application.

**App**

- Added: Ensure a minimum fixed interval for the splash screen.
- Fixed: Splash screen will always remain centred on screen irrespective of scaling.

**Environment**

- Removed: API for browser environment.


#### App

- subsumes the following UI components:
  - aTag
  - Common Command (from Command)
  - Execute
  - Icon
  - Launch
  - Open
  - Read Only
  - Taskbar

- Modified: (template) components renamed in the pattern `s-<component>`.

**Dependencies**

- Updated
  - angular-material: 1.1.24, prevents additional blank lines were being added to `textarea` (observed in Text Card > Input).

- Removed:
  - angular-touch

##### Navbar

- Added: Set initial Material theme as `default`.
- Fixed: Navbar address resets on empty, only when the mouse leaves. Though still not desirable, this is better than the situation where the reset was immediate.
- Modified: Uses the new *Registry* API to get the list of views.
- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.

##### Navigation

###### Error

- Modified: Replaces `errCode` with `missing` property on `AppSettings`, which only matches 404 errors.
- Refactor: Improved matching of error messages.

###### (new) Config

Abstracted from the erstwhile App config block, it initializes the navigation for the Syntropize App.

- Modified: Obtains settings from `AppSettingsProvider` instead of `InitSettings`.
- Modified: Uses `missing` property on `AppSettings` in lieu of `errCode` to determine if to load the "404" page instead of "error" page.

###### (new) Run

Abstracted from the erstwhile App run block, it initializes the logging for navigation events.


##### Routes

###### Main

- Removed: Licenses.
- Removed: "main" page and associated CSS.

###### View

- Modified: Uses the new *Registry* API to get the list of views and view-models.
- Modified: Since we use a flat registry, we no longer check for view and view-model stores.


##### Services

###### (new) Debug

Abstracted from the erstwhile App config block, it isolates Angular debug logging configuration.

###### Execute

- Modified: Uses the new *Registry* API to get the list of compile plugins.

###### Icon

- Modified: Icons are imported from `md-icons-svg-sprites-24px` library.
- Modified: Icons are directly loaded using Angular material design.

###### Launch

- Added: `http` requests are redirected immediately to the browser.

###### Read-Only

- Modified: *Read Only* service is self-contained. It now sets the read-only status on navigation.
- Modified: Does not return a `set()` function.

###### (new) Window

Abstracted from the erstwhile App run block, it controls the launch of the application window at start up.


##### Wrappers

- Modified: All API wrappers access the API from the window object.

###### Environment

- Modified: Environment uses the API provided by the shell (Previously, the API was defined locally) to allow complete decoupling of the application from the shell.

###### Resource

- Modified: Consistent with the changes in the *Resource API*, the wrapper uses the factory function to create the resource instance.

###### Settings

- Modified: Settings are initialized in a provider, so that the settings are available before the application runs.
- Modified: Initialization loads plugins location.
- Modified: Initialization allows for debug to be either a `boolean` or an `object`.


#### (new) Assets

- All Syntropize assets are now stored in a common repository:
  - Added: Splash Assets,
  - Added: A reduced subset of given fonts that contain only the necessary glyphs,
  - Added: Brand typography,
  - Added: Icons.

#### Preload

- Preloads the following dependencies:
  - angular-perfect-scrollbar-2
  - Dragula
  - ngSglclick
  - ngFilesizeFilter

- Preloads the following UI components:
  - Markdown
  - Run
  - Card Host
  - Card Factory
  - Color Picker

**Dependencies**

- Updated:
  - angular-dragula: ^1.2.8
  - angular-perfect-scrollbar-2: ^1.2.5

#### Splash

- Modified: All assets including fonts and images are loaded from the assets package and built local to the package.
- Modified: Update tag line.
- Modified: Splash image has been cropped to the right to have the screen centred at the top.


#### UI Components

- Angular modules for UI components renamed from `<category><component>` or `<category>.<component>` to `s<Category><Component>`
- Each UI component now resides in its own node package.

##### Dialog

###### File Select Dialog

- Fixed: (controller) incorrect function name.
- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.


##### File List

- Modified: Prevent selection of file-item from displacing tile contents.
- Fixed: Regression in `launch()` does not expect a parent directory name as key argument (see change in 0.6.0) as passed in the HTML template.
- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.

##### Help

###### About Content

- Modified: `notice` is read from the `about` property on global `syntropize` object.
- Fixed: License links point to the Syntropize repo.

###### About Dialog

- Modified: `title` and `version` information is read from the `about` property on global `syntropize` object.
- Modified: (template) `md-icon` loads icon image from the internal cache using `md-svg-icon`.


##### Markdown

- Refactor: (directive) Split `remark` and `rehype` configurations.
- Modified: (directive) `modifyProperties` plugin configured with an object (due to changes in the plugin).
- Modified: (service) obtains the instance of the Unified markdown engine from the `api` property on global `syntropize` object.

##### (new) Mouse Wheel Pass

Angular directive to pass mouse wheel events to underlying HTML tags; abstracted from the erstwhile **Page Linear** component.

##### Panel

- Fixed: Typo (host directive) Fix function name to `resetPanelTemplates`.
- Fixed: (template) Spurious `}` in `<panel-host>` tag.
- Modified: (structor) Injected angular panel items have an API suffix.
- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.

##### Registry

- Refactor: Registry component has been significantly refactored:
  - *RegistryService* has been replaced with the *Registry* constant which provides flat store for components.
  - *RegisterService* is now called *RegistryService* which has now been rewritten in the revealing module pattern.

**Dependencies**

- Added:
  - handle-async-await: ^1.0.0

##### Run

- Added: A `canSubmit()` function to the run API. (Provisionally, it does not test form validity and returns true).
- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.

##### (new) Toolbar

This package contains toolbar buttons that have been abstracted from erstwhile **Ribbon** component.

###### menu-button-2

- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.


#### Utils

Utilities abstracted in separated packages:

##### (new) Bulk Schema Results

This utility aggregates results of `promiseAllSettled` operations and generate proper error messages for use by schema plugins.

**Dependencies**

- Updated:
  - es-aggregate-error: ^1.0.5

##### (new) P.A.S.S.

This utility segregates the results of `promiseAllSettled` into separate arrays respectively containing fulfilled and rejected results.


#### Plugins

- Angular Modules for all plugins have been named in the pattern `<category>.<name>` for consistency.
- Plugins provide a list of files using the `exports` property in `package.json`.
- Plugins define their own configuration.

##### Card Plugins

###### Index Card

Removed: Unnecessary styling of main text.

###### Text Card

Fixed: text placement in card lens.

###### Web Card

Removed: Invocation to material Icon.
Fixed: Twitter/Github key is not a "required" entry for input template to save a card.
Fixed: `Fetch Card` button in input template not disabling correctly.


##### Compile

###### Lyx

- Refactor: `LyxScriptService` moved to its own file.


##### (new) Origin Plugins

The code required to connect to any given origin has been isolated from the Resource API into their own separate plugins. Resource API serves as a host for these plugins.

###### (new) File

**IO (real-time) interface**

- Fixed: `io.skipReconnect` is initialized as false.

**Dependencies**

- Updated:
  - chokidar: ^3.5.2
  - fs-extra: github:cxres/node-fs-extra
  - stat-mode: ^1.0.0

###### (new) Solid

**Auth**

- Fixed: Login should still be attempted when no credentials are provided.
- Fixed: Failed login/logout should result in message to the user.

**RW interface**

- Modified: `auth` object is initialized once only. Its value is inherited from the local auth module.

**IO (real-time) interface**

- Modified: Watcher uses **debug** library to log events.
- Fixed: Trailing Slash is appended on `location` only inside the watcher.
- Fixed: Spurious error/missing unlink event on folder delete.

**Location**

- Modified: default `port` setting in the parse function obtains its value from the plugin defaults.
- Modified: `resolve()` function defines its own logic since it no longer can inherit common location logic from Resource API.

**Dependencies**

- Added:
  - debug: ^4.3.1

- Updated:
  - solid-auth-cli: 1.0.14
  - ws: ^7.4.0

- Removed
  - solid-auth-client


##### (new) Schema Plugins

The various schema (data and interaction models) have been isolated from the Resource API into their own separate plugin. Resource API serves as a host for these plugins.

###### (new) Directory

**Reader**

- Fixed: Do not create empty object for every top-level entry except `'.'` in the files object.
- Fixed: Reader object returns options.
- Fixed: Proper checks for deleting file references on unlink event.

**Writer**

- Added: `create()` method.
- Fixed: Added `await` to return statements to ensure that functions show up in the error stack.
- Modified: Added punctuation to error messages.

**Dependencies**

- Added:
  - @syntropize/util-bulk-schema-results: workspace:*
  - @syntropize/util-pass: workspace:*
  - handle-async-await: ^1.0.0
- Updated:
  - stat-mode: ^1.0.0

###### (new) Board

- Modified: The plugin defines its own defaults.
- Fixed: Some or all options need not be defined when invoking the schema. All options arguments for
factory functions have empty objects as their default value.

**Reader**

- Fixed: Reader object returns options.
- Fixed: Unlink event on (deleting) empty file should remove it from record, i.e., the cards/meta object.

**Writer**

- Fixed: Added `await` to return statements to ensure that functions show up in the error stack.
- Fixed (Regression): Cards file `_card.json` was not being truncated before replacement on replace. If text was shorter than previous entry, later part of previous entry would remain, corrupting the file.
- Modified: Added punctuation to error messages.

**Dependencies**

- Added:
  - @syntropize/util-bulk-schema-results: workspace:*
  - @syntropize/util-pass: workspace:*
  - handle-async-await: ^1.0.0

###### (new) Document

- Modified: The plugin defines its own defaults.
- Fixed: Some or all options need not be defined when invoking the schema. All options arguments for factory functions have empty objects as their default value.

**Reader**

- Fixed: Reader object returns options.
- Fixed: Unlink event on (deleting) empty file should remove it from record, i.e., the sections/meta object.

**Writer**

- Refactor: Writer module has been significantly re-written in the pattern of Board Schema.
- Fixed: Added `await` to return statements to ensure that functions show up in the error stack.
- Modified: Added punctuation to error messages.

**Dependencies**

- Added:
  - handle-async-await: ^1.0.0

- Updated:
  - es-aggregate-error: ^1.0.3


##### View Plugins

View plugins are not just shells that bring together UI components but proper component programs.

Previously independent components are subsumed by the view. These components are now renamed in the pattern `<View><Component>`.

###### File-List View

- Subsumes the following UI components:
  - folder-list
  - file-properties
  - directory command (from command)
  - directory ribbon (from ribbon)
- Uses previously **File View** component as the root component.

**Root**

- Modified: Ribbon is invoked from within the root component.

**Properties**

- Modified: root component name in the stylesheet is updated to `view-file-list_properties`.
- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.

**Ribbon**

- Added: Dropdown for `New` in the `File` menu.
  - Moved: `New Folder` button to this dropdown.
  - Added: a button for `New File`.
- Modified: Controller uses the new registry API.
- Modified: Controller returns a list of card types and compile types instead of passing the registry itself to the template.
- Modified: Template populates `Run` menu using list of compile types provided by the controller.
- Added: button to clear clipboard selection.
- Moved: clipboard logic is now in the controller instead of the template.
- Modified: remove logic is now in the controller instead of the template.
- Modified: `runIcon()` obtains location for icon from the file map for the item in registry.
- Fixed: `selectionAggr()` returns a copy of the selection instead of selection itself. This prevents the selection from being accidentally modified.
- Modified: Update right-hand panel invocation with updated component names in the pattern `<View><Component>`.

**Command**

- Replaced: `write()` with `create()` which accepts name of the file to create using a dialog box.
- Added: a guard to `remove()` to check for absent selection resulting in a warning to the user. `remove()` will no longer delete the root directory in the view.

###### Board-Simple View

- Subsumes the following UI components:
  - board-simple
  - card-details
  - card-input
  - card-order
  - card-search
  - card-sort
  - card-shell
  - board command (from command)
  - board ribbon (from ribbon)
  - selection-aggregation
  - system-cards
- Uses the previously **Board Simple** component as root.

**Root**

- Modified: invokes **Ribbon**.
- Removed: Unnecessary DI of *CardOrder* in controller.

**Card Details**

- Added: `aria-label` attribute to (search) tags field.
- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.

**Card Input**

- Modified: In both the add and edit APIs, HTML tag properties updated to `board-simple-card-input` in line with `<View><Component>` pattern.
- Added: `card-input` class to the input form.
- Modified: Service to use the new registry API.
- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.

**Card Search**

- Modified: Service uses the new registry API.
- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.

**Card Shell**

- Modified: Controller to use the new registry API.
- Add: Failure to load template is logged.
- Modified: (template) `md-icon` loads icon image from internal cache using `md-svg-icon`.
- Fix (regression): last selected card has a pink (not blue) selection box.

**Card Sort**

- Modified: `md-icon` in template loads icon from internal cache using `md-svg-icon`.

**Command**

- Refactor: Command Service uses a revealing module pattern.
- Added: guard to `removeBoardItems()` to check for no selection resulting in an error. It will no longer delete the root directory in the view.
- Fixed (temp): file selections not being cleared after deletion by the command service.
- Fixed: `pasteBoardItems()` checks if the file parent exist before checking if file exists. Checking for the file directly when parent did not exist would result in an error.
- Modified: Better warning message when trying to remove root card.
- Fixed: Number of card removed is determined from the input arguments and not the selection.
- Fixed: Board is started/deleted in the first access location. `start()` and `cease()` now call the schema of the current resource at the time of invocation.

**Ribbon**

- Modified: Ribbon tab text when width is larger than `md`.
- Modified: Controller uses the new *Registry API*.
- Modified: Controller returns a list of card types and compile types instead of passing the registry itself to the template.
- Modified: Template populates list of cards in Card > Add using list of card types provided by the controller.
- Modified: Template populates Run using list of compile types provided by the controller.
- Modified: *panel* is started by the `panel()` function in controller instead of passing the `$panel` to the template.
- Modified: `runIcon()` obtains location for icon from the file map for the item in registry.
- Modified: Controller provides `saveOrder()` and `revertOrder()` instead of passing *CardOrderService* to the template.
- Modified: Controller provides various card and file operations instead of passing *BoardCommandService* to the template.
  - Added: `card.remove()`, `card.transclude()`.
  - Added: `file.create()`, `file.createDir()`.
- Fixed: Clicking on a vacant area in card cleared card selection instead of file selection in the card.
- Fixed: `file.remove()` did not remove files.
- Modified: Update right-hand panel invocation with updated component names in the pattern `<View><Component>`.

**System Cards**

*File Card*

- Modified: Controller no longer returns the interface inside a `file` object.
- Modified: Controller provides various file operations instead of passing `BoardCommandService.file` to the template.
  - Added: `remove()`, `rename()`, `create()`, `createDir()`, `link()`, `paste()`
  - Added: getter `isSelection`
- Modified: (template) File List component is invoked as `<s-file-list>`.
- Modified: (template) `md-icon` loads icon from internal cache using `md-svg-icon`.
- Added: Small margin to the left of first button on the bottom bar.

*Template Card*

- Modified: Uses the new *Registry* API to get the list of cards.


###### Page-Linear View

- Subsumes the following UI components:
  - page-linear
  - section-order
  - page-contents
  - page-list
  - document command from command
  - document ribbon from ribbon
- Uses the previously **Page Linear** component as root.

**Root**

- Modified: invokes the **Page List** and **Ribbon**.
- Fixed: Sections are correctly updated when metadata file changes.
- Modified: `markdown` attribute in template uses new component name.
- Added: Original author notice in `latex.css`.
- Added: Original author notice in `tufte.css`.
- Modified: source URLs in `tufte.css`, so that it can use fonts from the npm library.
- Modified: heading sizes for Tufte markdown for cleaner rendering across sections.
- Modified: Significant linting of external style sheet.
- Modified: Fix maximum width for page-list panel.
- Fixed: Scrolling for the page area.
- Modified: Ensure that section tops scrolls to below the top-bar.
- Removed: Repetition of `.text-space` entry in style sheet.
- Fixed (regression): Longer code blocks are full width.
- Fixed: missing parent class `.tufte` on selector for screen and webkit specific rule.
- Fixed: Sections with empty file shown as inactive due to incorrect property existence check on the sections object.

**Order**

- Fixed: Ensure sections unlisted in metadata file get shown after order is saved to disk.

**Page Contents**

- Added: Navigation to the contents of enabled sections (i.e., sections with associated markdown section files). Navigation opens section in a new page and scrolls down to selected subsection within it.

**Page List**

- Modified: `open()` method uses modified schema options to get file name.
- Modified: Controller method `create()` renamed to `start()`.
- Fixed: For the root item, `start()` method passes empty string to file.
- Modified: Controller method `isSectionActive()` renamed to `isSectionEnabled()`.
- Modified: Controller provides `reorder()` and `revertOrder()` instead of passing *DocumentCommandService* and *DocumentSectionOrderService* to the template.
- Removed: Spurious classes for non-existent selection and clipboard operations.
- Removed: Escape sequences in name of the parent section (which is derived from the URL) in the top tile of navigation pane.

**Command**

- Modified: Operations use methods of the re-written document schema.
- Modified: Operations log to the console on failure and return the error.
- Fixed: Success message on `remove()` is always singular.
- Fixed: Operations do not modify selections.
- Removed: Metadata operations (except `reorder()` which is moved to the main object).

**Ribbon**

- Removed: Unnecessary injected dependencies.
- Removed: spurious invocations to `ribbon.command` in template.
- Added: `add()`, `transclude()`, `remove()`, `removeAll()` methods.
- Replaced: `launch()` with `launchState()` which is more limited in function.
- Modified: Controller provides `saveOrder()` and `revertOrder()` instead of passing *DocumentSectionOrderService* to the template.


##### View-Model

###### Board

Fixed: detection of a new card does not trigger event handler.
Fixed: detection of a new metadata file does not trigger event handler.
Fixed: In board > detect, `removeListeners` cannot be assigned before `waitForMetadata()` is defined.

###### Document

Fixed: detection of a new section does not trigger event handler.
Fixed: detection of a new metadata file does not trigger event handler.
Fixed: In document > detect, `removeListeners` cannot be assigned before `waitForMetadata()` is defined.


#### APIs

##### Resource

- Modified: API does not include any origin and schema logic. It instead serves as a shell that load origin and schema plugins dynamically.
- Modified: API exports a factory function which is used to create a new API instance that provides the resource and logic to initialize, terminate, reconfigure the resource.
- Modified: Individual functions to modify resource configuration have been consolidated into a single reconfigure function.
- Modified: The plugin defines its own defaults.
- Modified: *Resource API* Factory returns `resource` as a named export also.
- Fixed: If resource initialization fails we always throw an error. In particular, if location is not found, we throw an error with the location included and not just the URL string.

**Settings**

- Modified: settings can merge arrays.
- Modified: settings exports a `get()` function.

**Origin**

- Modified: (fetch) origin logic is dynamically obtained from the origin plugins.
- Fixed: (location) Error message for undefined scheme.

**Schema**

- Modified: (fetch) schema logic is dynamically obtained from the schema plugins.

**Factory**

- Modified: (resource) schema are created asynchronously.
- Modified: (schema) schema name and plugin are both passed to the schema factory as arguments.
- Modified: (schema) `readOnly` is determined by per protocol setting as well as absence of `writer` on a schema.

**Dependencies**\*

- Updated:
  - lodash.mergewith: ^4.6.2

- Removed
  - bluebird

*Changes to origin and schema dependencies are tracked in their respective plugins.


##### Unified Markdown

- Added: **Footnotes** and **Code Extra** plugins.
- Fixed: `modifyProperties` requires properties names to be explicitly defined in `name: function` pairs. It does not rely on function names. This solves the issue of local images not being shown upon minification.

**Dependencies**

- Updated:
  - hast-util-has-property: ^1.0.4
  - highlight.js: ^10.1.1
  - lodash.merge: ^4.6.2
  - rehype-autolink-headings: ^4.0.0
  - rehype-document: ^5.1.0
  - rehype-highlight: ^4.0.0
  - rehype-katex: ^3.0.0
  - rehype-raw: ^4.0.2
  - rehype-stringify: ^9.0.0
  - rehype-urls: ^1.1.1
  - rehype-wrap: ^1.0.9
  - remark-abbr: ^1.4.0
  - remark-align: ^1.2.13
  - remark-attr: ^0.11.1
  - remark-behead: ^2.3.2
  - remark-bracketed-spans: ^3.0.0
  - remark-code-extra: ^1.0.1
  - remark-external-links: ^6.1.0
  - remark-frontmatter: ^2.0.0
  - remark-grid-tables: ^2.1.1
  - remark-heading-shift: ^1.1.0
  - remark-iframes: ^4.0.4
  - remark-math: ^2.0.1
  - remark-parse: ^8.0.2
  - remark-rehype: ^8.0.0
  - remark-sub-super: ^1.0.19
  - remark-textr: ^4.0.0
  - typographic-base: ^1.0.4
  - unified: ^9.0.0
  - uniqid: ^5.2.0
  - unist-util-filter: ^2.0.2
  - unist-util-visit: ^2.0.2

- Added:
  - remark-footnotes: ^2.0.0

- Removed:
  - rehype-prism
  - rehype-containers
  - rehype-emojis
  - remark-graphviz
  - remark-mermaid
  - remark-slug


#### Dev

##### Build

###### (new) AngularJS

Rollup configuration to build AngularJS components.

###### (new) Rollup plugin — html-entry

Rollup plugin to generate HTML entry pages. Used to generate the entry page for the main app from the template.

###### (new) SteamRoll

Provides baseline Rollup configuration for Syntropize packages. Individual packages need to only specify the changes to this configuration, simplifying the use of Rollup.


##### (new) Lint

Provides linting rules used by packages in this repository.

###### (new) Backend

- Added:
  - @syntropize/eslint-config-common: workspace:*
  - @syntropize/eslint-config-import: workspace:*

- Updated:
  - eslint-config-airbnb-base: ^14.2.0
  - eslint-plugin-import: ^2.22.1

###### (new) Dev

**Dependencies**

- Added:
  - @syntropize/eslint-config-common: workspace:*
  - @syntropize/eslint-config-import: workspace:*

- Updated:
  - eslint-config-airbnb-base: ^14.2.0
  - eslint-plugin-import: ^2.22.1

###### (new) Frontend

**Dependencies**

- Added:
  - @syntropize/eslint-config-common: workspace:*
  - @syntropize/eslint-config-import: workspace:*

- Updated:
  - eslint-config-airbnb-base: ^14.2.0
  - eslint-plugin-angular: ^4.0.1
  - eslint-plugin-import: ^2.22.1

###### (new) Import

**Dependencies**

- Updated:
  - eslint-plugin-import: ^2.22.0
  - eslint-plugin-json: ^2.1.1

###### (new) Legacy

**Dependencies**

- Added:
  - @syntropize/eslint-config-common: workspace:*

- Updated:
  - eslint-config-airbnb-base: ^14.2.0
  - eslint-plugin-node: ^11.1.0

###### (new) Style

**Dependencies**

- Updated:
  - stylelint-config-standard: ^20.0.0


##### (new) Unit Test

Abstracts the unit test runner.

**Dependencies**

- Updated:
  - jasmine: ^3.5.0
  - jasmine-console-reporter: ^3.1.0


##### Shells

###### NW.js

- Fixed: New App IDs for Windows installers. Previously, we were using the ID shipped in the sample file.
- Added: OSX build will not download on Windows under any circumstances. This circumvents symlink permissions required by NW Builder to extract the OSX build of NW.js. The failure to extract caused the entire build process to fail for all platforms.
- Fixed: Bundler generates clean config files and data folder on every occasion.
- Added: `.desktop` file for Linux builds.
- Modified: Compression format for Linux distributables from `.zip` to `.tar.xz`.
- Modified: Changed default NW.js cache folder to the current user's home directory as specified by the operating system.
- Fixed: Linux builds not being created when release folder does not exist.
- Fixed: Replaced `pipe()` with `pipeline()` to address memory leaks.
- Fixed: Failure to build `win32` package in absence of `win64` bundle.
- Fixed: Windows installer use auto constants.
- Added: Icon for all OS builds.
  Added: Version Information for Windows executables.

**Dependencies**

- Updated:
  - archiver: ^5.2.0
  - innosetup-compiler: ^5.6.1
  - minimist: ^1.2.5
  - nw-builder: ^3.5.7
  - replace-in-file: ^6.1.0

- Added:
  - fs-extra: ^9.1.0,
  - es-aggregate-error: ^1.0.5
  - lodash-es: ^4.17.20
  - read-pkg-up: ^7.0.1
  - lzma-native: ^8.0.6

- Removed:
  - bluebird
  - fs-extra-promise
  - lodash.mergeWith


## v0.6

### 0.6.0

#### App

- Added: Support for SOLID.
- Added: A credentials file.
- Added: cross-domain Cut, Copy, Paste Support.
- Added: NW.js starts with mixed context. This avoids many type-checking bugs.

##### Manifest

- Added: `--mixed-context` flag to `chromium-args` in package.json.


#### Shell

- Fix: (Temp) Splash screen is loaded before main to prevent missing splash. Screen shows an error when main screen gets loaded and splash screen is yet to be closed.

#### API Hooks

- Removed: NW APIs.

##### Syntropize APIs

- Added: Credentials are loaded during Syntropize initialization.

##### (new) Environment APIs

- Abstracts Environment related API functionality to a common provider.


#### Components

##### (new) A-Tag

The decorator for a-tag that was previously a part of the NW APIs has been moved to its own component, since it depends on services and is not NW specific.

##### Card-Shell

- Added: Selection for Cut and Copy of cards.
- Modified: Style service has been rewritten.
- Fixed: Missing highlight overlay.

##### Card-Input

- Modified: Board addition and removal to work with the new command service changes (removal of board > metadata).

##### (new) Clipboard

- Clipboard Service stores the result of cut and copy operations.
- Separate lists are maintained for cut and copy per view-model.

##### Command

###### Board

- Added: Common function to paste cards in lieu of move and copy.
- Modified: Remove has been rewritten to work with the new API.
- Added: Common function to paste files in lieu of move and copy.
- Fixed: (Temporary) Selections clear even when some files have been deleted.
- Modified: file > create opens a dialog box for file name.
- Modified: card > add, transclude, rename, replace have been modified to work with the new API.
- Fixed: board > file functions log errors.
- Removed: board > metadata functions.
- Added: A `reorder()` function is exposed for board operations.

###### Directory

- Added: Common function to paste files in lieu of move and copy which are not exposed.
- Fixed: (Temporary) Selections clear even when some files have been deleted.


##### File-List

- Added: Selection for Cut and Copy of files.
- Modified: `launch()` gets parent directory name from the *key* attribute on the component.
- Modified: `launch()` checks for metadata filename using options defined in the newer *Resource API*.

##### Folder-List

- Added: Clipboard Support.

##### Navbar

- Fixed: Address bar does not reset on empty, but only on the loss of focus.

##### Open

- Modified: Open Service is now shell environment agnostic. It depends on the newly defined environment API.

##### Ribbon

###### Board Ribbon

- Modified: Delete options for cards are in a drop-down.
- Modified: Activated Cut, Copy and Paste for cards.
- Modified: Activated Cut, Copy and Paste for files.

###### Directory Ribbon

- Modified: Activated Cut, Copy and Paste for files.


##### Selection Aggregation

- Removed: leading dot for root items in *selectionAggregationBoardService*. This is useful for generating cut and copy lists.

##### System Cards

###### File

- Added: Selection for Cut and Copy of files.


#### Dynamic Components

##### View-Model

###### Board VM

- Fixed: Selection structor for a card is removed only after the card has been unselected. Otherwise, there is nothing to unselect.
- Fixed: File is unselected upon delete only if a Selection object exists. This deals with the case where card file is itself deleted.

##### Cards

- Removed: **text-polymer** card. This helps simplify build. The involved components for installation are outdated and do not work with node 12 LTS.


#### Backend APIs

##### Resource [0.6.2]

- Modified: **https** origin is based on SOLID.
- Modified: **Resource** is built using Rollup to circumvent build problems when using Chokidar v3 with JSPM.
- Modified: **location** now uses node's *URL* library to get URLs from file paths.
- Added: Authorization before resource object is created.
- Added: Logic to load origins asynchronously (though the loading is still synchronous). *location* and *io* factories have been subsumed here.
- Modified: Various origins are moved to the `origins` folder to separate them from the origin loading logic.
- Added: authorization logic for all origins.


#### JSPM Dependencies

- Updated:
  - syntropize-resource: 0.6.2

- Removed:
  - html
  - polymer

#### Dependencies

- Updated:
  - NW.js: 0.34.5 (node.js must be >=10.12.0)


## v0.5

### 0.5.0

- Added: New **Page-Linear** view and **Document** view-model for presenting long form text.
- Added: File Linking and Content Transclusion Support.

#### App

##### App

- Fixed: Various scroll and overflow issues for content area. Content area is now based on `<div>` instead of `<md-content>`.

##### Preload

- Modified: Added module *markdown*.


#### API Hooks

##### NW APIs

- Modified: `<a>` tag decorator relies on *OpenService* and *LaunchService*. Removes direct dependence on **NW**.


#### Assets

- Added: Et-book font for Tufte layout.

#### Components

##### Card-Input

- Fixed: Swapped icons for Layouts section.

##### Command

###### Board

- Added: File linking.
- Added: Card transclusion.

###### Directory

- Added: File linking.

###### (new) Document

- *CommandService* module for **Document** view-model.


##### Dialog

###### (new) File-Select

- Dialog box to enable selection of files and directories (for linking and transclusion).


##### (new) Markdown

- Angular wrapper for **syntropize-unified-markdown**.

##### Nav-Error

- Added: 404 Error detection for resource.

##### Navbar

- Modified: Prevent Error Toast on 404 errors.
- Modified: Component no longer replaces the `<navbar>` tag. `replace: true` is DEPRECIATED by Angular.

##### (new) Page-Contents

- Renders a collapsible list of subsections for sections in the **Page-Linear** view.

##### (new) Page-Linear

- Primary view component for the **Page-Linear** view.


##### Ribbon

- Modified: Renamed main entry for **Ribbon** buttons as `index.js`.

###### Board

- Added: Transclude Button to Card Menu.
- Modified: File > Add disabling based on read-only status.

###### (new) Document

- Toolbar for **Page-Linear** view.


##### (new) Section-Display

- Manages view mode information for **Page-Linear** view.

##### (new) Section-Order

- Manages the order of sections for the **Document** view-model.

##### System-Cards

###### File

- Added: Symlink button to the bottom bar.


##### Taskbar

- Modified: Taskbar component is an ordinary `div` with class `.taskbar`.
- Modified: Component no longer replaces the `<taskbar>` tag. `replace: true` is DEPRECIATED by Angular.

##### (new) URL Parse

- Angular wrapper of url-parse (needed by the **markdown** component).


#### Registry

##### Card

###### Web

- Removed: `nwdisable` attribute in `<iframe>` tag as the iframe is being sanitized anyway.


#### Dynamic Components

##### View

###### (new) Page-Linear

- View to lay out a document of sequential text.


##### VM

###### (new) Document VM

- View-model for the management and display of sequential text.


#### Backend APIs

##### Resource [0.5.0]

###### Socket Factory

- Added: Detection for Error 404 Not Found.

###### (new) Document Schema

###### FileSystem Origin

- Added: `link()` method.

###### Board Schema

- Added: `transclusion()` method in *card-writer*
- Added: `link()` method in *file-writer*.

###### Directory schema

- Added: `link()` method in *writer*.


##### (new) Unified Markdown [0.1.0]

- A simple library to invoke the Unified engine for parsing markdown.


#### JSPM Dependencies

- Updated:
  - syntropize-resource: 0.5.0

- Added:
  - syntropize-unified-markdown: 0.1.0

#### Dependencies

- Updated:
  - jspm: ^0.16.55

- Added:
  - url-parse: ^1.4.7


## v0.4

### 0.4.0

#### App

- Added: Loading screen to main HTML page (in preparation of shell independence).
- Added: **preload** module to preload components that are frequently employed by **View**. This allows for faster lazy-loading of views.
- Added: `intialize > view` property to `settings.json`.

#### Routes

##### Resource

- Added: Initialization of ***view*** and ***view-model*** registry in resolve.
- Removed: *ResourceService* assignment is moved to *view-enter*. It must be done after everything is resolved in order to ensure that we do not destroy the reference to previous location in case of state rejection.

##### View

- Re-write: Allows for the dynamic invocation of ***view*** and ***view-model***:
  - Router reloads the view on change (previously view was changed in the controller).
  - **Board-Simple** and **File-View** have been removed from the **view** route module and placed in individual modules in their own registry.
  - *ViewService* determines the view status dynamically by examining the view-models corresponding to each view and presence of the files associated with the requisite view-models at the current location.

##### Main

- Added: Content license.


#### Components

##### (new) Board-Sort

- Abstracts the sorting control and logic for cards.

##### (new) Board-Search

- Abstracts the filter for cards.

##### (new) Card Order

- Control for the display order of cards is an independent module.

##### (new) Card Display

- Display of cards according to activated display mode is an independent module.

##### Card Host

- Modified: Templates are compiled after they have been appended to the element.

##### Card Input

- Modified: Dialog for discarding changes no longer swaps 'ok' and 'cancel' buttons.  `cancel()` function is re-written accordingly (corresponding changes are made in **panel**).
- Added: An internal `reset()` function in *CardInputService* to clear forms.
- Fixed: Input resets properly when a new card is loaded without reloading the component itself.
- Modified: *CardInputService* invokes only the *BoardCommandService*.
- Fixed: Contained input template is named as a property of the input form. Validation checks are accordingly fixed in the *CardInputService*.
- Modified: Separate APIs exposed for *$cardAdd* and *$cardEdit* in lieu of *$cardInput* allowing **panel** to invoke them directly without any additional logic.

##### Card Search

- Removed: Constructor for *BoardSearch* moved to a new *BoardSearchService*.

##### Card Shell

- Rewrite: **Card Shell** has been completely re-written:
  - No longer is `key` invoked for every function in the template; this responsibility has been moved to the controller and services are modified accordingly.
  - **System Cards** are components invoked directly in the template.
  - Styles are invoked per card as well.
  - Watcher looks at the card type and dynamically reloads the said card.

##### Card Sort

- Modified: Sort controls and logic moved to the **Board Sort** module. **Card Sort** is exclusively responsible for the UI component.

##### Command

- Removed: **Toast** and **Dialog** have been moved from **Command** module to their own modules.
- Modified: *CommandService* is split into independent components per view-model. *CommandService* itself only contains common commands.

###### Board

- Removed: De-selection logic for files. This is handled in the view-model.


##### (new) Dialog

- Dialogs are made into an independent module.

##### File-List

- Added: Launch will open a folder in *Default View* if its view meta content file is found.

##### (new) Toast

- Toasts are made into an independent module.

##### Help

- Added: *AboutContent* component to obtains the copyright statement from `NOTICE` file in the root directory.
- Modified: Package details are retrieved from `package.json` file in the root directory.

##### Launch

- Modified: Closing of a panel is agnostic of the UI component within the **Panel**. It looks at the `isBlocked` property of the *panelAPI* to determine its operation.
- Modified: `LaunchUrl()` and `LaunchView()` do not pass undefined params.
- Modified: `LaunchItem()` can accept views.

##### Navbar

- Removed: Error toasts. This is managed in *NavErrorService*.
- Modified: Initial values in `History2` for the updated state transitions that includes views.
- Added: `ViewButton` loads the list of available views from registry.
- Added: Status of available views is obtained from *ViewService* which determines the same dynamically for each view.
- Removed: No need to put a watcher on `$stateParams.view` as it is changed with state transitions.

##### (new) Nav-Error

- Navigation error handling and redirection are moved to their own module; removed from app module.

##### (new) Nav-Log

- Logging of navigation events is moved to its own module; removed from app module.

##### Panel

- Rewrite: UI components are loaded dynamically.
- Modified: Panel transitions are modified to reflect un-reversed (swapping back 'ok' and 'cancel' buttons) discard dialog for the hosted UI components.
- Modified: Panel API provides an `isBlocked` property to determine if it can be closed upon a user request/UI state change (such as when user wants to navigate to a new route).

##### Registry

- Added: `ensure()` in *RegisterService* which creates a registry only if one is not present.

##### Ribbon

- Modified: **Ribbon** is split into independent sub-modules per view:
  - Each sub-module only injects the relevant *\*CommandService* factories.
  - Each sub-module has its own `selectionAggr` and `clear` functions (needed for invoking **run**).

##### Run

- Modified: *RunService* is view agnostic. It receives selection and callback hooks upon invocation.

##### (new) Selection Aggregation

- **SelectionAggr** is made its own module with **SelectionAggr.board** as one of its sub-modules.

##### System Cards

- Rewrite: System cards are now components (not services feeding data into templates).

###### File

- Modified: Clicking on blank spaces deselects items.
- Added: Blank field at the end to allow user deselection.


#### Dynamic Components

##### Cards

- Modified: `formName` is bound one-way in all input components.

##### (new) View

- New register to store dynamically invoked views. Views only invoke templates (view-models are initialized independent of the view).

##### (new) View Model

- New register to store dynamically invoked view-model.

###### Board VM

- Rewrite: VM detection (viz. if a board view-model can be created for some location) has been re-written for consistency.
- Event Handlers:
  - Added: Cards and Files are deselected upon removal.
  - Removed: Card media (upon successful reading a card of new type) is not loaded in the view-model.
  - Added: Board gets its own file handler, decoupling it from the Directory VM.


#### Scripts

##### Build

###### Compile

- Added: `exclusions` property to the build configuration.
- Added: `NOTICE` and `package.json` as exclusions to the trace.


#### JSPM Dependencies

- Updated:
  - syntropize-resource: 0.4.0


## v0.3

### 0.3.0

#### App

##### Start-up

- Added: Splash Screen.
- Added: Independent shell logic for splash screen and control of window state on load. The program is launched from executable `shell/index.js`.
- Added: Preliminary styles to `index.html` to load immediately at start-up.

##### App

- Modified: Window is created once the program is loaded and the first state transition starts.


#### Routes

##### View

- Removed: `readOnly` as a URL query parameter; we provisionally consider this as a property of content.
- Fixed: Back navigation. Navigating to a previous location now allows you to go to the correct view even if the present location does not support that view.
- Fixed: read only with *stat-mode* support. **Ribbon** turns purple when a location is read-only.
- Fixed: While calculating card-order, unaccounted item in the list is added to it as the last item.
- Modified: *CardOrderService* saves the root `'.'` key in the card order.

##### Resource

- Added: Terminate Logic. Resource object for the previous location is removed on successful navigation.


#### Components

##### (New) Read Only

- Read only detection is moved to its own service.

##### (New) Taskbar

- Replaces top level footer component.

##### Card Host

- Added: Support for `metadata` attribute.

##### Card Input

- Fixed: Validations from card-type specific fields in **Card Input** are now being passed to the outer card input form.

##### FileList

- Fixed: Width of file icons in cards.

##### Card Shell

- Added: `title` attribute, so that the full card title is shown on hover (since card titles are truncated when long).
- Modified: `metadata` attribute is passed to invoked card.

##### Color Picker

- Fixed: Width to prevent wrap.

##### Execute

- Added: Error handlers to *ExecuteService* functions.

##### File-List

- Fixed: Minimum button length to prevent the button from being squashed by long file names.

##### Launch

- Fixed: Prevent navigation with an error message when Panel is open and is in a mutable state. This logic is now handled by **Launch** instead of **Panel**.
- Added: View can be changed using `view()`.

##### Navbar

- Modified: In preparation for refactoring the following have been instantiated from object factories:
  - `history2`
  - `viewMenuButton`
  - `viewButton`
  - `launchButton`
  - `progress`
  - `up`
- Added: `messages()` function to issue status message toasts.
- Fixed: Invalid disconnect message when previously connected location is accessed again.
- Fixed: Disconnect button not working.
- Added: Backslashes are automatically corrected for before launching a URL.
- Modified: `currentView` is renamed to more meaningful `viewButton`.

##### Panel

- Added: `<div>` enclosing panel body.
- Fixed: aria-labels for `Cancel` button.
- Fixed: Scrolling when content overflows.
- Removed: Logic for blocking navigation when **Panel** is open. It has been moved to **Launch**.

##### Ribbon

- Modified: `ORDER` menu is now called `BOARD`. This is to demonstrate that any view container can allow save and restore.
- Modified: read-only status is picked up from the new **read-only** module.

##### Run

- Fixed: Moved styles to CSS file.
- Added: Syntax hint for specifying selected files.

##### System Cards

###### File

- Added: Styles for overlapping icons on the mini-toolbar.

###### Status

- Fixed: Added handling for undefined `readStatus`.
- Fixed (Partial): To get around intermittent blank cards being loaded, `readStatus` is returned as `error` when `undefined`.


#### Dynamic Components

##### Cards

- Modified: `form` field to `formName` in all cards. Needed to pass back validations.
- Fixed: Moved `flag-bar` into the `card-title` to ensure that there is only one top level `<div>`
- Fixed: CSS for consistency.

###### (new) Index

- Displays a list of cards inside the said card from the metadata file.

###### Web

- Fixed: Link icon was not displayed.


#### Backend APIs

##### Resource [0.3.2]

- Added: `terminate()` to terminate access to previous location upon successful navigation, i.e., to stop listening to its events and delete the associated watcher/socket.
- Modified: Resource initialization accepts a reason for the namespace being rejected by the server (works with the updated server).
- Modified: Local file-system connector so that the emitted invalid namespace event is consistent with the server.
- Modified: Resource to work with HTTPS server.
- Fixed: Resource object re-initialization when the location was previously accessed and is connected.
- Added: Event detection for reconnection attempts and reconnection failure. Reconnection attempts are now restricted to 3.


#### Scripts

##### Build

###### Bundle

- Added: **shell** and **splash** folders to the list of bundled files.


#### JSPM Dependencies

- Updated:
  - syntropize-resource: 0.3.2


## v0.2

### 0.2.0

- Modified: Rationalized directory structure. Assets and JSPM objects moved to the top level.
- Added: User configuration for JSPM is moved to a separate file.
- Modified: Route components have been moved to a separate folder.

#### App

##### Start-up

- Modified: Removed unnecessary tags from `index.html`.
- Modified: App specific CSS moved to its own file.
- Modified: App is initialized from the `lib/` folder. This allows reuse of same start-up path for the compiled version as well.
- Modified: Separated JSPM loaders from `boostrap.js` in the compiled version (built into `lib/loaders.min*`) which is loaded before the actual app.

##### App

- Modified: For reasons of consistency, module declaration files for all components are renamed to `index.js`.
- Modified: **App** is a component. Added *app* directive with top-level markup.
- Added: Start settings are specified in the root directory.
- Modified: Initialization settings are loaded from *InitSettings* Service. *AppSettings* is a constant to hold those settings (to allow future modification of app settings without mutating start-up values).
- Modified: Set default URL in router for start-up.
- Added: Router redirects to the `/error` page in case of an error during start-up. The failure to load the default location leads to the `/404` page.
- Modified: Window is managed in the *appInstance* and not during the config phase.
- Added: Debug info can be enabled from the settings file.


#### Routes

- Modified: For consistency, module declaration files for all components are renamed to `index.js`.

##### Main

- Added: `/error` page.
- Added: `'error'` state to *MainProvider*.
- Added: `/404` page.
- Added: `'404'` state to *MainProvider*.
- Removed: Error warning box from main page.

##### Resource

- Added: ngMaterial layout to template string.
- Modified: *ResourceInitialize* moved here from View. All initialization is done from here (No separate setup).

##### View

- Modified: `ViewServiceProvider` has been replaced by `ViewService`.
- Fixed: `ViewStatus` does not need to be initialized.
- Fixed: *BoardDetect* no longer detects the `ready` event just once. Needed to support reconnection.
- Fixed: *BoardDetect* `'ready'` event is handled in a digest. This is to allow the presence of metadata to be recorded (which is also inside a digest).
- Fixed: *BoardDetect* returns immediately upon promise resolution.
- Modified: *CardSetup* and *MetadataSetup* moved to their own factories.
- Modified: *ResourceInitialize* moved over to **Resource**.
- Fixed: *\*RegistryInitialize* return an empty promise when the register already exists.
- Fixed: Saving card order saves unaccounted cards. Unfiltered list is modified to add cards unaccounted for in metadata file while using `orderBy` filtering.
- Modified: *CardOrderService* has been simplified:
  - *CardOrderService* does not watch cards and metadata file for changes. These are hooked up within the component using the card order.
  - *CardOrderService* can be re-initialized on demand. This is useful for resetting data upon state reload.
  - `updateOrder()` is responsible for object to array conversion.
  - `resetCardOrder()` only loads list from disk. It is a private function.


#### Components

##### BoardSimple

- Fixed: Deselect click works on the entire background.
- Fixed: Card order is set only after metadata file is read.

##### BoardSort

- Fixed: All properties for field based search are cleared on reset.
- Modified: Card order is initialized here instead of resolve.

##### Card Input

- Fixed: Name field allows invalid inputs. Otherwise, Angular sets ngModel to undefined when invalid (which then leads to infinite digests on ui-validate).

##### Card Shell

- Removed: Unnecessary `ng-if` condition when loading *stateCard* on read status `loading` or `error`.

##### Color Picker

- Fixed (Partial): Model value is set to lower case since color hex values are case-insensitive. A proper fix will need a color lib to be used.

##### Command

- Fixed: Bugs in rename files.
- Modified: In all renaming functions, RegExp checking for the legality of a new name is provided by *CommandService* to the input dialog.
- Fixed: `md-actions` in dialog boxes to `md-dialog-actions` (forced by upgrade of angular-material).
- Fixed: *DirectorySelection* is not declared in *CommandService*. The absence led to an error whenever file de-selections occurred.
- Fixed: Blocking behaviour of *Toasts* after a *CommandService* operation.
- Fixed: *ErrorToast* are not returned; prevents unnecessary error logging.
- Fixed: ViewStatus not being updated upon creation/destruction of board.
- Fixed: Selections are not cleared when `RUN > SCRIPT` fails.
- Fixed: *Input Dialog* was card rename specific. It is now general for any string input and accepts an arbitrary validation for RegExp.

##### FileList

- Modified: `orderFiles()` function extracted from watcher.
- Fixed: Items are highlighted on hover irrespective of their background.

##### FolderList

- Modified: *DirectorySelection* is placed in a getter.

##### Help

- Modified: *AboutDialog* dynamically gets values from the manifest.

##### Icon Set

- Modified: Path for `material-design-icons` is dynamically obtained from SystemJS.

##### Launch

- Fixed: `launchUrl()` uses absolute address calculated by the *Resource* object.
- Fixed: `launchState()` accepts `params` & `options` argument.

##### Navbar

- Fixed: Missing `break` in progress bar `'start'` and `'ready'` cases.
- Added: De-register/Destroy logic for `$stateChange*` events.
- Modified: Watching of resource status –— only the intended/current route is set by routing events. Status of the connection is watched outside routing. This ensures that the status message and progress bar are consistent with the routing as they happen (subject to delays due to `$applyAsync`).

##### (new) Open

- Opens files on the desktop via the Application Container Framework (currently only with NW.js) in an origin independent manner.

##### Panel

- Added: Provider to initialize icons.
- Added: Panel for **run**.
- Modified: `startPanelContents()` is defined before panel start logic.
- Added: De-register/Destroy logic for `$stateChange*` events in controller.
- Modified: `getCurrentView()` and `setViewChangeStatus()` are defined outside, before the watcher is set up.

##### Ribbon

- Modified: `PANEL` menu renamed to `TOOLS`.
- Fixed: `HELP > ABOUT` menu items.
- Fixed: `FILE > ADD` buttons do not show browser native UI.

##### Registry

- Modified: `initialize()` uses `System.import` instead of `$http` to fetch registers.
- Modified: Angular module name being lazy loaded using the register will have the format `${register.name}-${item}` because it is more consistent.
- Modified: `loadItem()` is made an internal function. It is not exposed on the register object.
- Added: DI for `$$animateJs`, a hack for [bug](github:ocombe/ocLazyLoad#321) when using ngAnimate.
- Modified: Register returns the `registrySetup()` function directly instead of wrapping it inside an object.
- Added: Errors from `*All()` functions are aggregated and thrown together.

##### (new) Run

- Allows users to run arbitrary command on selected files (only on local files).

##### Selection

Modified: `select()` and `unselect()` are made internal functions. That is, they are not exposed on the selection object.

##### System Cards

- Modified: Generation factory for each type of system card moved to its own service.
- Modified: Import of the system cards is made absolute; corresponding path is added to the user config.

###### File

- Fixed: `selection` property is placed in a getter because parent container is initialized before view-model (Angular did not provide lifecycle hooks in the past).

###### Status

- Fixed: `CardStatus.readStatus` is a Map and not an Object. Hence, we need to use `.get(key)` to get the status.
- Fixed: Error message is now displayed.

###### Template

- Modified: *TemplateCardService* accepts type argument.
- Fixed: Invalid type did not show a card.


#### API Hooks

##### Syntropize APIs

###### (new) Execute API

- Executes external processes (currently restricted to the users' system).

###### InitSettings API

- Modified: *InitSettings* is invoked as a constant.
- Added: *InitSettings* imports user settings from `settings.json` in the root directory.
- Modified: *InitSettings* invokes `getLocation()` from *Resource* to set the home location (in lieu of the removed **Initialize** API).

###### Resource API

- Modified: *Resource* is invoked in a separate factory.
- Added: *Resource* imports `terminate()` function and exports it as part of the API object.
- Added: *Resource* imports user settings from `settings.json` and sets them using `updateSettings()` hook provided by **Resource**.
- Added: *Resource* sets digest and logging using `setDigest()` and `setLog()` hooks respectively provided by **Resource**.


##### NW APIs

- Removed: Node Provider.


#### Dynamic Components

##### Cards

- Fixed: All `import` statements use paths relative to project root.
- Modified: Split up `index.js` so that each file has only one angular function.

###### Web Card

- Fixed: Success/Retry is reset if the address is changed.
- Fixed: Progress indicator is correctly displayed.

###### Text Polymer Card

- Fixed: Polymer library is invoked from within the component. The main application does not need to invoke Polymer.
- Modified: Split JS, HTML and CSS for the *display* component into separate files.


#### Backend APIs

- Modified: **Resource** and **Execute** are now installed using JSPM. This allows all modules to be loaded using ES2015 without any need for separate transpilation.
- Removed: **Initialize**. Initialization settings are stored in `settings.json` at the project root. **Resource** provides a `getLocation()` function to evaluate absolute initialization paths.

##### Resource [0.2.0]

- Rewrite: Large portions of **Resource** have been rewritten to implement proper location and schema independence.
- Fixed: Previously existing `Resource` object is not overwritten immediately when a new route is created. We wait till the new route is confirmed before overwriting the previous route.
- Fixed: Wrapped all `resource.status` properties in the digest, so that they update in the UI.
- Fixed: Added angular `$digest` to `add` and `ready` events in directory and board reader, so that changes to directory and board objects are immediately updated in the UI.


#### Scripts

- Added: Linting and banner checking is done only on staged files at the time of commit.

##### Build

- Modified: Moved *Build* Step to *Build/Compile*.
- Modified: *Build* now includes all the build steps.
  - *Clean*
  - *Compile* (previously called *Build*)
  - *Bundle*
  - *Package*

- Added: *Build* and *Start* scripts in `package.json`.
- Modified: Updated build to reflect the rationalized directory structure.
- Added: A minimal CLI for making builds.

###### Clean

- The clean step removes the existing `lib/` and creates a fresh `lib/` from `src/`.

###### Compile

- Renamed: *Compile* was previously called *Build*.
- Modified: Only the `src/` files are compiled to `lib/`. This allows all the other files to be reused instead of being copied over to a separate build folder.
- Modified: JSPM build options and other bundle configuration are specified in a separate file `settings.js` in the `config` directory.
- Modified: A trace of source files is made first. This is used to exclude redundant modules from registry builds.
- Modified: Registry builds exclude redundant modules from the app build.
- Fixed: Source maps are independent of minified code. This is a workaround for source maps not working in NW.js. A [bug](github:nwjs/nw.js#4730) has been filed.
- Modified: JSPM builds occur in source directory and then the results are moved over to destination. This keeps the file paths in source maps sane in the absence of the `sourceRoot` option in UglifyJS being exposed by JSPM.
- Removed: Post-Build step.
- Modified: Negative regex to determine what registry resources to copy.
- Modified: Files at registry top level are ignored.
- Added: Compile script for loaders.
- Added: Replace `system.src.js` to `system.js` in compiled `index.html`.
- Added: Include minified scripts in compiled `index.html`.
- Modified: All paths are provided in a separate file in the `config` directory.

###### Bundle

- Added: Globs to pick specific files and folders. This is necessary because we compile to `lib/`.
- Modified: NW.js version is derived from `config` property when not overridden by the CLI. This allows users to override the default value permanently using `npm config` if they so desire.
- Modified: NW.js OS platform is left unspecified by default (Together with the fixes to NW-Builder, this will pick the current OS by default).
- Added: Target OS is fed as an argument from the CLI.
- Modified: Settings overrides are loaded from a separate file in the `config` directory.
- Fixed: Settings file is created in the correct folder on OSX.

###### Package

- Modified: Simplified OS specific package invocation.
- Modified: Illegal OS strings are filtered by NW-builder.
- Modified: Target OS is fed as an argument from the CLI.
- Added: `osx*` case leads to a no-op message.
- Added: Automatic version updating in Windows packagers.
- Fixed: Missing `recursesubdirs` option for data folder in Windows packagers.


##### (new) Start

- Added: A minimal CLI for launching Syntropize.
- Added: Start scripts in `package.json`.

##### Banner

- Modified: Automatic detection and fixing of absent/outdated banners.
- Added: Banner for minified code.
- Fixed: Incorrect banner paths.
- Added: `templates` folder to hold license headers.

##### (new) Tests

- Added: ESLint and StyleLint support for linting.
- Added: Jasmine for Unit Testing.
- Added: Test scripts in `package.json`.


#### Docs

- Added: Use Instructions.
- Added: DCO Instructions.
- Moved: Build Instructions to its own file.
- Removed: Build Instructions from README.

#### JSPM Dependencies

- Updated:
  - angular: 1.5.11
  - angular-aria: 1.5.11
  - angular-dragula: 1.2.8
  - angular-material: 1.1.0
  - angular-sanitize: 1.5.11
  - angular-ui-validate: 1.2.3,
  - clean-css: 3.4.27,
  - oclazyload: 1.1.0,
  - polymer: ~1.9.2,
  - syntropize-resource: 0.2.0,
  - text: 0.0.11

- Added:
  - angular-animate: 1.5.11
  - angular-filesize-filter: 0.1.3
  - angular-messages: 1.5.11
  - angular-touch: 1.5.11,
  - json: 0.1.2
  - syntropize-execute: 0.1.0
  - syntropize-resource: 0.2.0

#### Dependencies

- Updated:
  - NW.js: 0.20.2
  - ejs: ^2.5.6
  - jspm: ^0.16.53

- Added:
  - jasmine: ^3.0.0
  - jasmine-console-reporter: ^2.0.1
  - jspm-git: ^0.6.1,
  - lodash.merge: ^4.6.1
  - minimist: ^1.2.0
  - node-jspm-jasmine: ^3.2.0
  - npm-run-all: ^4.1.2
  - nw-builder: github:CxRes/nw-builder
  - replace-in-file: ^3.4.0

- Removed:
  - syntropize-apis: gitlab:ProtoSyntropize/syntropize-apis


## v0.1

### 0.1.0

#### App

- Added: Missing dependencies.

#### Components

##### Board Simple

- Added: Drag and Drop to reorder cards.

##### Card Input

- Added: Tags.

##### Help

- Added: Items — *Welcome* and *About*.

##### Ribbon

- Fixed: Toolbar Button.

##### FileView

- Fixed: Compile.
- Fixed: File operations.

##### Command

- Fixed: Input Dialog Box.
- Added: Dialog for *Add Folder*.

##### FileCard

- Added: `ADD FOLDER` Button.

##### Panel

- Modified: Simplified **File-Properties** panel.
- Fixed: Prevent navigation on view change, if a panel that requires a discard operation is open. Otherwise, the panel is closed before changing the view. This behaviour is identical to URL navigation.


#### Docs

- Added: Build Instructions to `README.md`.

#### Build

- Modified: Rationalized the build system; each build component is placed in a separate file in the `scripts` directory.
