# Contributing

## Welcome!

We’re so glad that you’re thinking about making a contribution. At this embryonic stage, we can do with all the help we can get. Code contributions are most appreciated! However, there are many other ways in which you can help, not just code!

If you are not already using Syntropize, we urge you to give it a spin. Your feedback is extremely valuable in helping us to gain the insights required improve the design as well as the user experience. We hope that Syntropize shall become a springboard for a discussion on how to truly realize the vision of intelligence augmentation in the context of the modern computing technologies.

We also encourage you to familiarize yourself with the history of personal computing and the principles that underlie its design. These ideas are the "greatest hits" of 20th century engineering, the very foundation of the information revolution we are living through. Yet, to our utter dismay, these ideas do not even find mention in school and university curricula and are largely unknown to most industry professionals. Neither has it found any significant coverage in mainstream media. The [Background](https://syntropize.com/docs/#/Background) section on the Syntropize [Documentation](https://syntropize.com/docs) site is a great place to begin this journey.

More than anything else, if Syntropize sparks your interest and imagination, please help spread the word. Talk about it with your friends and colleagues, discuss it at home and at work. If this implementation does not excite you enough, talk about the underlying ideas -- how these ideas have already revolutionized our world and how much of their potential is yet to be tapped.

Finally, talk to us! If you’re unsure about anything, just ask! Or submit an issue or a pull request anyway. The worst that can happen is we’ll politely ask you to change something or share with you a differing perspective. We value and appreciate all friendly suggestions and contributions.

## Guidelines

Since, this is an experimental project, we feel that there is no need for strict guidelines towards making a contribution. We are open to all contributions that help us demonstrate the underlying concepts as well as build upon them to improve the user experience.

A few minor points are in order, though:

### Copyright and Licensing

As a contributor, you retain the copyright to your contributions. Your contributions are bound by the same license as the rest of the project (inbound = outbound).

### Developer Certificate of Origin

This project embraces the Developer Certificate of Origin (DCO) for contributions. Please see the [`DCO_PROCESS`](./DCO_PROCESS.md) to learn more. The Developer Certificate of Origin can be found in the [`DCO`](../DCO) file.

### Credits

If you start a new file, please ensure that the copyright statement records your name. The included [Headright](https://gitlab.com/cxres/headright) utility automatically adds copyright and license information configured with your git username. If you make significant changes to a file akin to a rewrite, add another copyright line with your name in it.

We also invite you to add your name to the list of contributors in the [CONTRIBUTORS](../CONTRIBUTORS) file along with your contribution. The purpose of the file is not to track copyright but to acknowledge all contributions to the project, not just those limited to changes to the code base (code contributions are tracked using revision control). Even those making minor contributions such as [Obvious Fixes](./DCO_PROCESS.md#the-obvious-fix-exception) can add their names under *acknowledgements*.

## Thank You

**Thank you** so much for your interest in Syntropize. We look forward to your contributions. If you still have any questions, absolutely do not hesitate to get in touch with us! Contact details can be found on the Syntropize [homepage](https://syntropize.com).
