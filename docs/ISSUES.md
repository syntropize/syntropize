# ISSUES

Here is an incomplete list of issues that plague Syntropize:

- Tick mark on cards become active when a core or property item is changed and not when content item is changed. This is because I am unable to pass validations from inside the web-components properly.
- We set `allowInvalid: true` on ng-model-options in forms such as card input because ui-validate sets resets the value of ng-model variable to undefined upon first invalidation which then leads to infinite digests.
- Navigation fails when directory contains symlinks: Chokidar does not issue ready event. A bug is already filed.
- When navigation is stopped because a panel that requires a discard operation is open, it does not reset the view correctly. This is a problem with the ui-router library with its most unhelpful developers. We prevent this issue for now by checking the panel state before navigation.
- History: Back and Forward may navigate incorrectly and may be incorrectly disabled. This is because we do not have access to the history stack and have to guess where the pointer is at any given moment.
- Cards transcluded from content within a parent folder do not show up because Chokidar does not populate contents inside link.
- Refresh on the browser side causes the program to crash since it fails to re-establish connection with backend APIs. This can be avoided by simply refreshing from the program window or from the devtools for the background page.
- For simplicity of implementation, we disable write commands on all objects in a view, even if just the primary resource is read-only. 

