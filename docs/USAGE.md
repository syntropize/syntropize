# Usage

You can run Syntropize directly from source. Alternatively, you can build it as a package to install on your system.

### Installation

We assume that you have [Node.js](https://nodejs.org/) (version 14 or above) already installed on your system.

1. If you have not already done so, install pnpm:

    ```sh
    npm install --global pnpm
    ```

2. Install dependencies for all packages. At the project root:

    ```sh
    pnpm install --recursive --filter=*
    ```

### Compilation

3. You need to build packages within the monorepo:

    + To build for development:

      ```sh
      pnpm run dev:pkg
      ```

    + To build for production:

      ```sh
      pnpm run build:pkg
      ```

    The main difference between development and production builds is that the code is minified in the latter.

### Usage

Syntropize is designed to be hosted in any browser like environment.

You need to choose an environment shell from within [`packages > shells`](../packages/shells). At present, the only choice is [NW.js](../packages/shells/nwjs).

#### Run

4. To use Syntropize in your choice of environment shell:

    ```sh
    pnpm run start:<shell>
    ```

    where you replace `<shell>` with the corresponding identifier.

    | Shell     | Identifier |
    | --------- | ---------- |
    | NW.js     | nwjs       |


#### Build

4. Alternatively, to build Syntropize for your choice of environment shell:

    ```sh
    pnpm run build:<shell>
    ```

    where you replace `<shell>` with the corresponding identifier.

    | Shell     | Identifier |
    | --------- | ---------- |
    | NW.js     | nwjs       |

    <br>

    Files are built in the respective `./package/shells/<shell>` directory. The default build targets are:

    | Object            | Directory  |
    | ----------------- | ---------- |
    | Executable Bundle | `_bundle/` |
    | Installer Package | `_pkg/`    |

    <br>

    **TIP**: Options to modify the build for a specific shell can be found using the `--help` flag:

    ```sh
    pnpm run build:<shell> --help
    ```
