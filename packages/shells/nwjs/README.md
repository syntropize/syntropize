# Syntropize <br><small><small><b>NW.js Shell</b></small></small>

This package allows users to run and build Syntropize in a NW.js Shell.

It is the only package you need if you just want to run or build Syntropize (in a NW.js shell) from source.

## Development Workflow

### Setup

We assume that you have [Node.js](https://nodejs.org/) (version 14 or above) already installed on your system.

+ If you have not already done so, install pnpm:

    ```sh
    npm install --global pnpm
    ```

+ Install dependencies:

    ```sh
    pnpm install
    ```

+ If you are building the package from the monorepo, ensure that all the dependencies have been built.

  + To build for development:

    ```sh
    pnpm run dev
    ```

  + To build for production:

    ```sh
    pnpm run build
    ```

### Usage

+ To access private online resources with Syntropize, please set up your credentials by editing the [`./config/credentials.json`](./config/credentials.json) file. A sample file [`./config/credentials.SAMPLE.json`](./config/credentials.SAMPLE.json) has been provided for guidance.

+ To run Syntropize:

    ```sh
    pnpm run start
    ```

    **TIP**: Use the `--help` flag to learn about different start options:

    ```sh
    pnpm run start -- --help
    ```

    **On Windows Powershell you need to enclose empty options in quotes like so: `"--"`


### Build

+ To create a Syntropize *executable bundle*:

    ```sh
    pnpm run bundle
    ```

+ To create a Syntropize *distributable package*:

    ```sh
    pnpm run release
    ```

+ You may combine the two operations:

    ```sh
    pnpm run build
    ```

    Builds built in the following build directories:

    | Object            | Directory  |
    | ----------------- | ---------- |
    | Executable Bundle | `_bundle/` |
    | Installer Package | `_pkg/`    |


    **TIP**: Use the `--help` flag to learn about different build options:

    ```sh
    pnpm run <build|bundle|release> -- --help
    ```

+ To remove a previously built instance of Syntropize:

    ```sh
    pnpm run clean
    ```

## Documentation

To learn more, please visit the Syntropize website at <https://syntropize.com>. Documentation can also be accessed from the Syntropize application.

## Copyright and License

Please see the [NOTICE](./NOTICE) file for Copyright and License information.

Licenses are available in the respective LICENSE* files.

## Contact Us

**Thank you** so much for your interest in Syntropize. If you still have any questions, absolutely do not hesitate to get in touch with us on [Discord](http://discord.com/syntropize).
