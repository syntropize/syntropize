/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const { builtinModules: builtins } = require('module');

function generateBridge(moduleName) {
  // eslint-disable-next-line
  const exportContents = Object.keys(require(moduleName))
    .reduce((exportList, exportItem) => `${exportList}  ${exportItem},\n`, '')
  ;

  return `const m = require('${moduleName}');

export default m;
export const {
${exportContents}} = m;
`
  ;
}

window.esmsInitOptions = {
  // skip: {
  //   test: function esmsTestUrl(url) {
  //     console.log('A', url); // TODO
  //     return false;
  //   },
  // },
  resolve: async function esmsResolveDecorator(id, parentUrl, defaultResolve) {
    if (builtins.includes(id) /* && id !== 'fs' */) {
      return `/node_builtins/${id}`;
    }
    if (id.startsWith('@syntropize/api-')) {
      return `/node_modules/${id}/lib/index.min.js`;
    }

    // Default resolve will handle the typical URL and import map resolution
    return defaultResolve(id, parentUrl);
  },
  fetch: async function esmsFetchDecorator(url) {
    if (url.startsWith('/node_builtins/')) {
      const builtin = generateBridge(url.substring(15));
      return new Response(new Blob([builtin], { type: 'application/javascript' }));
    }
    return fetch(url);
  },
};

function includeShim() {
  const shim = document.createElement('script');
  shim.setAttribute('async', '');
  shim.src = 'node_modules/es-module-shims/dist/es-module-shims.min.js';
  document.body.append(shim);
}

function includeScript(text) {
  const app = document.createElement('script');
  app.type = 'module-shim';
  app.innerText = text;
  document.body.append(app);
}

document.addEventListener('DOMContentLoaded', async function loadSyntropize() {
  includeShim();

  // You cannot use import() statement directly here as the script tag under which
  // this code is run precedes the shim. Hence, this tedious workaround.
  includeScript('import "./src/app.js"');
});
