/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
function shellEnvironment(window) {
  return function environmentService() {
    const win = window.global.nw.Window.get();

    const openWindow = function openWindow(status) {
      win.window.startup(status);
    };

    const openDevWindow = function openDevWindow() {
      win.showDevTools();
    };


    const shell = window.global.nw.Shell;

    const open = function open(address) {
      if (address.startsWith('/') || address.startsWith(':', 1)) {
        shell.openItem(address);
      }
      else if (/^\w{2,}:/.test(address)) {
        shell.openExternal(address);
      }
      else {
        throw new Error('Open Error: Invalid absolute path');
      }
    };

    return Object.freeze({
      window: Object.freeze({
        open: openWindow,
        openDev: openDevWindow,
      }),
      shell: Object.freeze({
        open,
      }),
    });
  };
}

export default shellEnvironment;
