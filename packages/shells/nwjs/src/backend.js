/*!
 *  Copyright (c) 2022-2023, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */

// This file is the brings in API modules and makes them available to
// the rest of the app.
import * as execute from '@syntropize/api-execute';
import * as resource from '@syntropize/api-resource';
import markdown from '@syntropize/api-unified-markdown';
import shellEnvironment from './environment.js';
import settings from '../config/settings.json';

const { manifest } = nw.App;

const about = {
  title: 'Syntropize (NWjs)',
  version: manifest.version,
};

const plugins = Object.keys(manifest.optionalDependencies);


const api = {
  execute,
  resource,
  markdown,
  shellEnvironment,
  settings,
};

export {
  api,
  about,
  plugins,
};
