/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import * as syntropize from './backend.js';

// syntropize.api.settings = require('./config/settings');

const startInterval = 2000;

function startupTimer() {
  return new Promise((resolve) => {
    setTimeout(resolve, startInterval);
  });
}

let splashWindow;
let splashBlur = false;

const mainOptions = {
  width: window.screen.width,
  height: window.screen.height,
  show: false,
  position: 'center',
  fullscreen: false,
  id: 'main',
};

const splashOptions = {
  fullscreen: false,
  frame: false,
  show: false,
  id: 'splash',
};


const splashHandle = function splashWindowHandle(win) {
  splashWindow = win;

  // manually set width and height to overcome screen scaling bug
  splashWindow.width = 600;
  splashWindow.height = 400;
  splashWindow.x = (window.screen.width - 600) / 2;
  splashWindow.y = (window.screen.height - 400) / 2;
  splashWindow.show();

  splashWindow.on('close', function splashWindowClose() {
    splashWindow.close(true);
    if (!splashBlur) { nw.App.quit(); }
  });
};

const mainHandle = function mainWindowHandle(win) {
  const mainWindow = win;
  mainWindow.window.syntropize = syntropize;
  const startWait = startupTimer();

  function startup(status) {
    startWait.then(function closeSplashAndOpenMain() {
      if (splashWindow) {
        mainWindow.show();
        mainWindow.focus();
        if (status.maximize) {
          mainWindow.maximize();
        }

        splashWindow.hide();
        splashBlur = true;
        splashWindow.close();
      }
    });
  }

  mainWindow.window.startup = startup;

  mainWindow.on('close', function mainWindowClose() {
    nw.App.closeAllWindows();
    nw.App.quit();
  });
};

nw.Window.open('./node_modules/@syntropize/app/lib/index.html', mainOptions, mainHandle);
nw.Window.open('./node_modules/@syntropize/splash/lib/index.html', splashOptions, splashHandle);
