/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const toolbarMenuButton2Controller = function toolbarMenuButton2Controller(
  $mdMedia,
) {
  return {
    get large() {
      return $mdMedia('gt-sm');
    },
  };
};

toolbarMenuButton2Controller.$inject = [
  '$mdMedia',
];

export default toolbarMenuButton2Controller;
