/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import '@mdi/font/css/materialdesignicons.css';
import './ribbon.css';

import './toolbar-button/toolbar-button.css';
import './toolbar-menu-button/toolbar-menu-button.css';
import './toolbar-menu-button-2/toolbar-menu-button-2.css';

import toolbarButtonDirective from './toolbar-button/toolbar-button.directive.js';
import toolbarButtonController from './toolbar-button/toolbar-button.controller.js';

import toolbarMenuButtonDirective from './toolbar-menu-button/toolbar-menu-button.directive.js';
import toolbarMenuButtonController from './toolbar-menu-button/toolbar-menu-button.controller.js';

import toolbarMenuButton2Directive from './toolbar-menu-button-2/toolbar-menu-button-2.directive.js';
import toolbarMenuButton2Controller from './toolbar-menu-button-2/toolbar-menu-button-2.controller.js';

export default angular.module('sToolbar', [])
  .directive('toolbarButton', toolbarButtonDirective)
  .controller('ToolbarButtonController', toolbarButtonController)

  .directive('toolbarMenuButton', toolbarMenuButtonDirective)
  .controller('ToolbarMenuButtonController', toolbarMenuButtonController)

  .directive('toolbarMenuButton2', toolbarMenuButton2Directive)
  .controller('ToolbarMenuButton2Controller', toolbarMenuButton2Controller)
;
