/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import toolbarButtonTemplate from './toolbar-button.html';

const toolbarButtonDirective = function toolbarButtonDirective() {
  return {
    template: toolbarButtonTemplate,
    restrict: 'E',
    transclude: true,
    controller: 'ToolbarButtonController',
    controllerAs: 'toolbtn',
    bindToController: true,
    scope: {
      name: '@',
      icon: '@',
      tooltip: '@',
      click: '&',
      disabled: '=?',
    },
  };
};

export default toolbarButtonDirective;
