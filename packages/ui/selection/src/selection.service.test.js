/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import Selection from './selection.service.js';

const selection = Selection().init();

describe('Selection Module', () => {
  it('should be initialized', () => {
    expect(selection).toBeDefined();
  });

  describe('Select Item', () => {
    beforeAll(() => {
      selection.selectItem('a');
    });

    it('should add item at position 1', () => {
      expect(selection.selectionNumber('a')).toBe(1);
    });

    it('should update list of selections', () => {
      expect(selection.list).toEqual(['a']);
    });

    it('should update hashTable of selections', () => {
      expect(Array.from(selection.hashTable.entries())).toEqual([['a', 1]]);
    });

    it('should update number of selections', () => {
      expect(selection.countSelected()).toBe(1);
    });
  });


  describe('Multiple Selections', () => {
    beforeAll(() => {
      selection.selectItem('b');
      selection.selectItem('c');
    });

    it('should update list of selections', () => {
      expect(selection.list).toEqual(['a', 'b', 'c']);
    });

    it('should update hashTable of selections', () => {
      expect(Array.from(selection.hashTable.entries())).toEqual([['a', 1], ['b', 2], ['c', 3]]);
    });

    it('should update number of selections', () => {
      expect(selection.countSelected()).toBe(3);
    });

    it('should have a first Selection', () => {
      expect(selection.firstSelection()).toBe('a');
    });

    it('should tell if an item is first selected', () => {
      expect(selection.isFirstSelected('a')).toBe(true);
    });

    it('should tell if an item is not first selected', () => {
      expect(selection.isFirstSelected('b')).toBe(false);
    });

    it('should have a last Selection', () => {
      expect(selection.lastSelection()).toBe('c');
    });

    it('should tell if an item is last selected', () => {
      expect(selection.isLastSelected('c')).toBe(true);
    });

    it('should tell if an item is not last selected', () => {
      expect(selection.isLastSelected('b')).toBe(false);
    });

    it('should tell if a selected item is at the nth position', () => {
      expect(selection.isnSelected('b', 2)).toBe(true);
    });

    it('should tell if a selected item is not at the nth position', () => {
      expect(selection.isnSelected('b', 1)).toBe(false);
    });

    it('should provide the item at the nth position when (0 < n < length)', () => {
      expect(selection.nSelection(2)).toBe('b');
    });

    it('should not be able to provide the item at the nth position when (n > length)', () => {
      expect(selection.nSelection(1000)).toBeUndefined();
    });

    it('should provide the position of an existing item', () => {
      expect(selection.selectionNumber('c')).toBe(3);
    });

    it('should not provide the position of an non-existing item', () => {
      expect(selection.selectionNumber('z')).toBeUndefined();
    });

    it('should tell if an item is selected', () => {
      expect(selection.isSelected('c')).toBe(true);
    });

    it('should tell if an item is not selected', () => {
      expect(selection.isSelected('z')).toBe(false);
    });
  });


  describe('Unselect Item', () => {
    beforeAll(() => {
      selection.unselectItem('a');
    });

    it('should remove item', () => {
      expect(selection.selectionNumber('a')).toBeUndefined();
    });

    it('should update list of selections', () => {
      expect(selection.list).toEqual(['b', 'c']);
    });

    it('should update hashTable of selections', () => {
      expect(Array.from(selection.hashTable.entries())).toEqual([['b', 1], ['c', 2]]);
    });

    it('should update number of selections', () => {
      expect(selection.countSelected()).toBe(2);
    });
  });


  describe('Toggle Item', () => {
    it('should add item that did not exist', () => {
      selection.toggle('a');
      expect(selection.isSelected('a')).toBe(true);
    });

    it('should remove item that existed', () => {
      selection.toggle('b');
      expect(selection.isSelected('b')).toBe(false);
    });
  });


  describe('Move Item', () => {
    beforeAll(() => {
      selection.toggle('d');
      selection.toggle('b');
    });

    it('should move the item forward', () => {
      selection.move('c', 2);
      expect(selection.list).toEqual(['a', 'c', 'd', 'b']);
    });

    it('should move the item backward', () => {
      selection.move('b', 2);
      expect(selection.list).toEqual(['a', 'b', 'c', 'd']);
    });
  });


  describe('Clear Selections', () => {
    it('should clear the list', () => {
      selection.clear();
      expect(selection.countSelected()).toBe(0);
    });
  });
});
