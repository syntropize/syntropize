/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const selectionService = function selectionService() {
  const Selection = function Selection() {
    this.hashTable = new Map();
    this.list = [];

    const select = function select(key) {
      /* Add Selected Card to List */
      this.list.push(key);
      /* Set Card selection to true -> assign selection number */
      this.hashTable.set(key, this.list.length);
    };

    const unselect = function unselect(key) {
      /* Remove Unselected Item from list */
      const selectionNumber = this.hashTable.get(key);
      this.list.splice(selectionNumber - 1, 1);

      /* Renumber Remaining Items */
      const numberSelected = this.list.length;
      for (let i = selectionNumber - 1; i < numberSelected; i += 1) {
        this.hashTable.set(this.list[i], i + 1);
      }
      /* Remove Item */
      this.hashTable.delete(key);
    };

    // Key Returns

    this.firstSelection = function firstSelection() {
      return (this.list.length > 0) ? this.list[0] : undefined;
    };

    this.lastSelection = function lastSelection() {
      return (this.list.length > 0) ? this.list[this.list.length - 1] : undefined;
    };

    this.nSelection = function nSelection(n) {
      return (n > 0 && n < this.list.length) ? this.list[n - 1] : undefined;
    };

    this.selection = function selection() {
      return this.list.length > 0;
    };

    this.countSelected = function countSelected() {
      return this.list.length;
    };

    this.selectionNumber = function selectionNumber(key) {
      return this.hashTable.get(key);
    };

    this.isSelected = function isSelected(key) {
      return this.hashTable.has(key);
    };

    this.isFirstSelected = function isfirstSelected(key) {
      return key === this.firstSelection();
    };

    this.isLastSelected = function isLastSelected(key) {
      return key === this.lastSelection();
    };

    this.isnSelected = function isnSelected(key, n) {
      return key === this.nSelection(n);
    };

    this.toggle = function toggle(key) {
      if (!this.hashTable.has(key)) {
        select.call(this, key);
      }
      else {
        unselect.call(this, key);
      }
      return this.hashTable.has(key);
    };

    this.set = function set(key, state) {
      return state ? this.selectItem(key) : this.unselectItem(key);
    };

    this.selectItem = function selectItem(key) {
      if (!this.hashTable.has(key)) {
        select.call(this, key);
      }
      return this.hashTable.has(key);
    };

    this.unselectItem = function unselectItem(key) {
      if (this.hashTable.has(key)) {
        unselect.call(this, key);
      }
      return this.hashTable.has(key);
    };

    this.clear = function clear() {
      let i = this.list.length;
      while (i) {
        i -= 1;
        this.hashTable.delete(this.list[i]);
        this.list.pop();
      }
    };

    this.move = function move(key, position) {
      if (this.hashTable.has(key)) {
        const originalPosition = this.hashTable.get(key);
        this.hashTable.set(key, position);
        this.list.splice(originalPosition - 1, 1);
        this.list.splice(position - 1, 0, key);

        if (originalPosition < position) {
          for (let i = originalPosition; i < position; i += 1) {
            this.hashTable[this.list[i]] -= 1;
          }
        }
        else {
          for (let i = position + 1; i <= originalPosition; i += 1) {
            this.hashTable[this.list[i]] += 1;
          }
        }
      }
    };
  };

  function init() {
    return new Selection();
  }

  return {
    init,
  };
};

export default selectionService;
