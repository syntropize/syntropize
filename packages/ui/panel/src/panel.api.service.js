/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const panelAPI = function panelAPI(
  PanelItem,
  PanelViewService,
  PanelStructor,
  $q,
  $log,
) {
  const view = PanelViewService;
  const item = PanelItem;
  const structor = PanelStructor;

  const start = function panelStart(panelMode, ...args) {
    return $q.when()
      .then(() => {
        if (view.mode !== '') {
          return item.cancel().then(structor.teardown);
        }
        return undefined;
      })
      .then(() => {
        structor.setup(panelMode);
        return item.start(...args)
          .then(() => {
            view.set(panelMode);
            view.open();
          })
          .catch(() => {
            view.reset();
            structor.teardown();
            view.close();
          })
        ;
      })
      .catch(function panelStartError(error) {
        error.message = `Panel Start Error\n${error.message}`;
        $log.warn(error);
      })
    ;
  };

  /* Cancel logic is in the controller because panel has
   * automatic cancelling when not locked-in
   */
  const cancel = function panelCancel() {
    return view.close();
  };

  const isBlocked = function panelIsBlocked() {
    if (view.open) {
      return !!(item.canSubmit && item.canSubmit());
    }
    return undefined;
  };

  return Object.freeze({
    start,
    isBlocked,
    cancel,
  });
};

panelAPI.$inject = [
  'PanelItem',
  'PanelViewService',
  'PanelStructor',
  '$q',
  '$log',
];

export default panelAPI;
