/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const panelStructor = function panelStructor(
  $injector,
  $q,
  PanelItem,
) {
  const setup = function setupPanelItem(item) {
    const base = {
      start: $q.when,
      cancel: $q.when,
      tag: item.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase(),
      name: item[0].toUpperCase() + item
        .replace(/([a-z])([A-Z])/g, '$1 $2').slice(1),
    };

    Object.assign(
      PanelItem,
      base,
      $injector.has(`${item}API`) ? $injector.get(`${item}API`) : {},
    );
  };

  const teardown = function teardownPanelItem() {
    Object.keys(PanelItem)
      .forEach((name) => { delete PanelItem[name]; })
    ;
  };

  return {
    setup,
    teardown,
  };
};

panelStructor.$inject = [
  '$injector',
  '$q',
  'PanelItem',
];

export default panelStructor;
