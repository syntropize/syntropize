/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const panelController = function panelController(
  $mdMedia,
  PanelItem,
  PanelStructor,
  PanelViewService,
  $scope,
) {
  const item = PanelItem;
  const view = PanelViewService;
  const structor = PanelStructor;

  let lockOpen;

  // Panel Close Logic
  $scope.$watch(
    function watchPanelOpen() { return view.isOpen; },
    function onPanelOpen(isOpen) {
      lockOpen = isOpen;
      if (!isOpen && view.mode !== '') {
        $scope.$applyAsync(function closePanel() {
          return item.cancel()
            .then(() => {
              view.reset();
              structor.teardown();
            })
            .catch(() => {
              view.open();
            })
          ;
        });
      }
    },
  );

  return {
    get active() {
      return view.mode && view.mode !== '';
    },
    get lockOpen() {
      return lockOpen && $mdMedia('gt-sm');
    },
    get submitExists() {
      return !!item.submit;
    },
    get submitDisabled() {
      return !item.canSubmit();
    },
    get tag() {
      return item.tag;
    },
    get name() {
      return item.name;
    },
    submit() {
      return item.submit()
        .then(view.reset)
        .then(view.close)
        .catch(() => {})
      ;
    },
    cancel() {
      return view.close();
    },
  };
};

panelController.$inject = [
  '$mdMedia',
  'PanelItem',
  'PanelStructor',
  'PanelViewService',
  '$scope',
];

export default panelController;
