/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import './panel.css';

import panelViewService  from './panel.view.service.js';
import panelStructor     from './panel.structor.js';
import panelAPI          from './panel.api.service.js';

import panelController   from './panel.controller.js';

import panelToolbarDirective from './panel-toolbar.directive.js';
import panelHostDirective    from './panel-host.directive.js';
import panelDirective        from './panel.directive.js';

export default angular.module('sPanel', [])
  .constant('PanelItem', {})

  .factory('PanelViewService', panelViewService)
  .factory('PanelStructor', panelStructor)
  .factory('$panel', panelAPI)

  .controller('PanelController', panelController)

  .directive('panelHost', panelHostDirective)
  .directive('panelToolbar', panelToolbarDirective)
  .directive('panel', panelDirective)
;
