/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const panelViewService = function panelViewService(
  $mdSidenav,
  $mdMedia,
) {
  let mode = '';

  const open = function openPanel() {
    return $mdSidenav('rpanel').open();
  };

  const close = function closePanel() {
    return $mdSidenav('rpanel').close();
  };

  const set = function setPanelMode(panelMode) {
    mode = panelMode;
  };

  const reset = function resetPanelMode() {
    mode = '';
  };

  return Object.freeze({
    open,
    close,
    set,
    reset,
    get isOpen() {
      return $mdSidenav('rpanel').isOpen();
    },
    get isLockedOpen() {
      return ($mdSidenav('rpanel').isOpen() && $mdMedia('gt-sm'));
    },
    get mode() {
      return mode;
    },
  });
};

panelViewService.$inject = [
  '$mdSidenav',
  '$mdMedia',
];

export default panelViewService;
