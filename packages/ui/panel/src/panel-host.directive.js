/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const panelHostDirective = function panelHostDirective(
  $compile,
) {
  const panelHostLinker = function panelHostLinker(scope, element, attrs) {
    const template = function panelTemplate(item) {
      return `<${item}></${item}>`;
    };

    attrs.$observe('item', function resetPanelTemplate(newValue) {
      if (newValue && newValue !== '') {
        element.html(template(newValue));
      }
      else {
        element.html('');
      }
      $compile(element.contents())(scope);
    });
  };

  return {
    restrict: 'E',
    scope: {
      item: '@',
    },
    link: panelHostLinker,
  };
};

panelHostDirective.$inject = [
  '$compile',
];

export default panelHostDirective;
