/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const fileListController = function fileListController(
  ResourceService,
  Registry,

  LaunchService,
  OpenService,

  // FileListStyleService,
  FileIconService,

  AppSettings,

  $scope,
  $filter,
) {
  const fileList = this;
  const { key } = fileList;

  const { location } = ResourceService.resource;

  let displayItems = [];

  const open = function fileOpen(file) {
    OpenService(ResourceService.resource.location.path(fileList.key, file));
  };

  const launch = function launchItem(item) {
    const { files } = ResourceService.resource.directory;
    let view;
    // FIXME: allow options for view depending on availability
    if (files[key][item].isDirectory()) {
      if (Object.prototype.hasOwnProperty.call(
        files[item],
        ResourceService.resource[
          Registry.view.items[AppSettings.view.default].models[0]
        ].options.files.metadata,
      )) {
        view = AppSettings.view.default;
      }
    }

    return LaunchService.item([key, item], view);
  };

  const orderFiles = function orderFilesByIndex() {
    displayItems = $filter('toArray')(fileList.items);
  };

  $scope.$watchCollection('fileList.items', orderFiles);

  return {
    icon: FileIconService,
    launch,
    // style:FileListStyleService,
    open,
    get displayItems() {
      return displayItems;
    },
    get clipboard() {
      let clipboardBuffer;
      let exists;
      if (clipboardBuffer !== this.xc) {
        clipboardBuffer = this.xc;
        if (!clipboardBuffer) {
          exists = false;
        }
        else {
          const { url } = clipboardBuffer.location;
          exists = url.scheme === location.url.scheme &&
            url.$root === location.url.$root &&
            url.$pathname === location.url.$pathname;
        }
      }

      return Object.freeze({
        has(item) {
          return exists && clipboardBuffer.items.has(key === '.' ?
            item : `${key}/${item}`);
        },
        get mode() {
          return exists && clipboardBuffer.mode;
        },
      });
    },
  };
};

fileListController.$inject = [
  'ResourceService',
  'Registry',

  'LaunchService',
  'OpenService',

  // 'FileListStyleService',
  'FileIconService',

  'AppSettings',

  '$scope',
  '$filter',
];

export default fileListController;
