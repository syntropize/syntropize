/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import fileIcon from '@syntropize/ui-file-icon';

import './file-list.css';
import fileListDirective  from './file-list.directive.js';
import fileListController from './file-list.controller.js';
// import fileListStyleService from './file-list.style.service';

const modules = [
  // 'ngSglclick',
  fileIcon.name,
];

export default angular.module('sFileList', modules)
  .directive('sFileList', fileListDirective)
  .controller('FileListController', fileListController)
  // .service('FileListStyleService', fileListStyleService)
;
