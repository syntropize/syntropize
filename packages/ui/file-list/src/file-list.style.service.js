/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const fileListStyleService = function fileListStyleService(
  BoardSelection,
) {
  const round = function fileListRound(value) {
    return value;
  };

  const height = function fileListHeight(/* key */) {
    return '236px';
  };

  const width = function fileListWidth(/* key */) {
    return '236px';
  };

  const color = function fileListColor(key, itemColor) {
    return (key === '.') ? 'white' : itemColor || 'lightgreen';
  };

  const borderColor = function fileListBorderColor(key) {
    let listBorderColor = 'transparent';
    // if (view.xcCards.isXCed(key)||view.sc.key === key) {
    //   listBorderColor = 'grey';
    // }
    if (BoardSelection.cards.isSelected(key)) {
      listBorderColor = '#3F51B5';
    }
    if (BoardSelection.cards.isLastSelected(key)) {
      listBorderColor = '#E91E63';
    }
    return listBorderColor;
  };

  const borderStyle = function fileListBorderStyle(key) {
    let listBorderStyle = '';

    // if (view.xcCards.isXCed(key)) {
    //   style += 'dashed ';
    // }
    // if (view.sc.key === key) {
    //   style += 'dotted';
    // }
    if (listBorderStyle === '') {
      if (BoardSelection.cards.isSelected(key)) {
        listBorderStyle += ' solid';
      }
      else {
        listBorderStyle += ' transparent';
      }
    }
    return listBorderStyle;
  };

  const highlight = function fileListHighlight(key) {
    return (
      // view.xcCards.isXCed(key)||
      // view.sc.key === key||
      BoardSelection.cards.isSelected(key)
    );
  };

  const cssClass = function fileListCssClass(index) {
    return {
      selected: BoardSelection.cards.isSelected(index),
      lastSelected: BoardSelection.cards.isLastSelected(index),
      'md-whiteframe-z5': BoardSelection.cards.isSelected(index),
    };
  };

  return Object.freeze({
    color,
    round,
    height,
    width,
    borderColor,
    borderStyle,
    highlight,
    cssClass,
  });
};

fileListStyleService.$inject = [
  'BoardSelection',
];

export default fileListStyleService;
