/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const markdownDirective = function markdownDirective(
  $sanitize,
  $compile,
  MarkdownService,
  UrlParseService,
  OpenService,
  LaunchService,
  $log,
) {
  const link = function linkMD(scope, element, attrs) {
    const remark = {};
    const rehype = {};

    scope.open = OpenService;

    scope.launch = LaunchService;

    if (attrs.heading) {
      remark.addHeading = attrs.heading;
    }

    if (attrs.behead) {
      remark.headingShift = parseInt(attrs.behead, 10) || false;
    }

    if (attrs.url) {
      rehype.modifyProperty = {
        src(prop) {
          return UrlParseService(prop, attrs.url).toString();
        },
        href(prop, node) {
          node.properties.target = '_self';
          return UrlParseService(prop, attrs.url).toString();
        },
      };
    }

    const processor = MarkdownService({ remark, rehype });

    const manageError = function manageError(error) {
      $log.error(error);
      const errorLine = `<p class="sans"><strong>Parse Error: </strong>${error.message}</p>`;
      if (attrs.heading) {
        const headTag = attrs.behead ? 'h2' : 'h1';
        return `<${headTag}>${attrs.heading}</${headTag}>${errorLine}`;
      }
      else {
        return errorLine;
      }
    };

    const renderMD = function renderMD(markdown) {
      return processor.process(markdown)
        .then((value) => {
          element.html($sanitize(value));
          scope.$applyAsync($compile(element.contents())(scope));
        })
        .catch(error => element.html(manageError(error)))
      ;
    };

    if (attrs.sMarkdown) {
      scope.$watch('sMarkdown', renderMD);
    }
    else {
      renderMD(element.text());
    }
  };

  return {
    restrict: 'A',
    scope: {
      sMarkdown: '<',
      heading: '@?',
      behead: '@?',
      url: '@?',
    },
    link,
  };
};

markdownDirective.$inject = [
  '$sanitize',
  '$compile',
  'MarkdownService',
  'UrlParseService',
  'OpenService',
  'LaunchService',
  '$log',
];

export default markdownDirective;
