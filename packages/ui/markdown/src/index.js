/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import markdownDirective from './markdown.directive.js';
import markdownService from './markdown.service.js';
import demoteMarkdownFilter from './demote.filter.js';

export default angular.module('sMarkdown', [])
  .factory('MarkdownService', markdownService)
  .directive('sMarkdown', markdownDirective)
  .filter('demoteMarkdown', demoteMarkdownFilter)
;
