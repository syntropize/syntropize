/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const demoteMarkdownFilter = function demoteMarkdownFilter() {
  return function demoteMarkdown(input, heading) {
    return input
      .replace(/^(?!# )/, heading ? `# ${heading}\n\n` : '')
      .replace(/^#+ ?/mg, '#$&')
    ;
  };
};

export default demoteMarkdownFilter;
