/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import errorToastService    from './error-toast/error-toast.service.js';
import successToastService  from './success-toast/success-toast.service.js';

export default angular.module('sToast', [])
  .factory('ErrorToastService', errorToastService)
  .factory('SuccessToastService', successToastService)
;
