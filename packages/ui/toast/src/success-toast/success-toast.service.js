/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import successToastTemplate from './success-toast.html';
import successToastController from './success-toast.controller.js';

const successToastService = function successToastService(
  $mdToast,
) {
  return function successToast(success) {
    return $mdToast.show({
      controller: successToastController,
      template: successToastTemplate,
      controllerAs: 'toast',
      bindToController: true,
      locals: { success },
      hideDelay: 3000,
    });
  };
};

successToastService.$inject = [
  '$mdToast',
];

export default successToastService;
