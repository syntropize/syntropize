/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import errorToastTemplate from './error-toast.html';
import errorToastController from './error-toast.controller.js';

const errorToastService = function errorToastService(
  $mdToast,
) {
  return function errorToast(error) {
    return $mdToast.show({
      controller: errorToastController,
      template: errorToastTemplate,
      controllerAs: 'toast',
      bindToController: true,
      locals: { error },
      hideDelay: 10000000,
    });
  };
};

errorToastService.$inject = [
  '$mdToast',
];

export default errorToastService;
