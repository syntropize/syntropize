/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import aa from 'handle-async-await';

const registryService = function registryService(
  Registry,
  AppSettings,
  $ocLazyLoad,
  // eslint-disable-next-line angular/no-private-call
  $$animateJs, // hack for bug github:ocombe/ocLazyLoad#321 with ngAnimate
  $q,
  $http,
  $log,
  $window,
) {
  /**
   * Register Factory: Create a new register to store data
   * @param {[type]} name     [description]
   * @param {[type]} location [description]
   */
  async function createRegister(name = '') {
    /**
     * Items stores the list of programs that hook to Knowledge Manager and corresponding scripts.
     * @type {Object}
     */
    const items = {};

    /**
     * The location prefix for the register
     * @type string
     */
    const location = `${AppSettings.plugins.location}/${AppSettings.plugins.prefix}-${name}`;

    /**
     * Status of individual register Items
     * @type {Object}
     */
    const status = {};

    /**
     * Path to entry files within a plugin
     * @type {Object}
     */
    const fileMap = {};

    /**
     * Load the list of files provided by a plugin
     * @param  {string} item Name of the plugin to be fetched
     * @return {[type]}      [description]
     */
    async function loadFileMap(item) {
      const pkg = await aa(
        $http.get(
          `${location}-${item}/${AppSettings.plugins.manifest.file}`,
          { responseType: 'json', cache: true },
        ),
        function manifestFetchError(error) {
          const manifestError = new Error(`Unable to load manifest file for ${name} ${item}`);
          manifestError.status = error.status;
          throw manifestError;
        },
      );

      const { map } = AppSettings.plugins.manifest;

      fileMap[item] = angular.isString(pkg.data[map]) ?
        { '.': pkg.data[map] } : pkg.data[map]
      ;
    }

    /**
     * Load plugin configuration
     * @param  {string} item Name of the plugin to be fetched
     * @return {[type]}      [description]
     */
    async function loadConfig(item) {
      const config = fileMap[item]['./config'];

      if (config) {
        items[item] = (await aa(
          $http.get(
            `${location}-${item}/${config}`,
            { responseType: 'json', cache: true },
          ),
          function configFetchError(error) {
            const configError = new Error(`Unable to load configuration file for ${name} ${item}`);
            configError.status = error.status;
            throw configError;
          },
        )).data;
      }
      else {
        items[item] = {};
      }
    }

    /**
     * Files Loader: Assumes item exists and has status 'Unloaded'
     * @param  {string} item Name of the Register Item to be fetched
     * @return {[type]}      [description]
     */
    async function loadItem(item) {
      status[item] = 'Loading';
      const main = fileMap[item]['.'] || fileMap[item]['./'];
      const css = fileMap[item]['./styles'];

      try {
        if (css) {
          await $ocLazyLoad.load({
            files: [`${location}-${item}/${css}`],
            cache: false,
          });
        }

        const module = await aa(
          import(`${location}-${item}/${main}`),
          `Unable to load package file for ${name} ${item}\n`,
        );
        await $ocLazyLoad.load({
          name: module.default.name,
          cache: false,
        });

        status[item] = 'Success';
      }
      catch (error) {
        status[item] = 'Failure';
        throw error;
      }
    }

    /**
     * Loads the given Register Item
     * @param  {string} item Register Item to be lazy loaded
     * @return {object}      Promise Object
     */
    async function load(item) {
      if (items[item] && status[item] === 'Success') {
        return $q.when(`Item ${item} in ${name} Registry is already loaded.`);
      }
      else {
        await loadFileMap(item);
        await loadConfig(item);
        return await loadItem(item);
      }
    }

    /**
     * Refreshes the given Register Item
     * @param  {string} item Register Item to be refreshed
     * @return {object}      Promise Object
     */
    async function refresh(item) {
      if (!items[item]) {
        return $q.reject(`${item} does not exist in register ${name}.`);
      }
      else if (status[item] === 'Success' || status[item] === 'Failure') {
        return await loadItem(item);
      }
      else {
        return $q.reject(`Item ${item} in ${name} Registry is still loading...
Please wait or call load() to force a reload!`);
      }
    }

    /* Load file map and configuration */
    const prefix = `${AppSettings.plugins.prefix}-${name}-`;
    await Promise.all($window.syntropize.plugins
      .filter(dep => dep.startsWith(prefix))
      .map(dep => dep.substring(prefix.length))
      .map(async (dep) => {
        await loadFileMap(dep);
        await loadConfig(dep);
      }))
    ;

    return {
      name,
      items,
      status,
      fileMap,
      location,
      load,
      refresh,
    };
  }

  /**
   * Ensure that a register exists
   * @param  {[type]} name Name for the new register
   * @return {object}      Promise Object
   */
  async function ensureRegister(register) {
    if (!Registry[register]) {
      Registry[register] = await createRegister(register);
    }
  }

  return {
    ensure: ensureRegister,
  };
};

registryService.$inject = [
  'Registry',
  'AppSettings',
  '$ocLazyLoad',
  '$$animateJs',
  '$q',
  '$http',
  '$log',
  '$window',
];

export default registryService;
