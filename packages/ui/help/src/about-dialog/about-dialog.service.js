/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import aboutDialogTemplate from './about-dialog.html';
import aboutDialogController from './about-dialog.controller.js';

const aboutDialogService = function aboutDialogService(
  $mdDialog,
) {
  return function aboutDialog() {
    return $mdDialog.show({
      controller: aboutDialogController,
      template: aboutDialogTemplate,
      controllerAs: 'dialog',
      bindToController: true,
    });
  };
};

aboutDialogService.$inject = [
  '$mdDialog',
];

export default aboutDialogService;
