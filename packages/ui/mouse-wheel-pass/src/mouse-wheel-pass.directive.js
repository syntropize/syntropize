/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const mouseWheelPassDirective = function mouseWheelPassDirective(
  $document,
  $timeout,
) {
  return function mouseWheelPassLink(scope, element, attrs) {
    element.bind('wheel', function mouseWheelPassCtrl(event) {
      $timeout(function waitForScrollEnd() {
        element.css('pointer-events', 'none');
        $document[0]
          .elementsFromPoint(event.clientX, event.clientY)
          .some(function filterByMouseWheelAttr(mouseoverElement) {
            if (mouseoverElement.classList.contains(attrs.mouseWheelPass)) {
              angular.element(mouseoverElement).triggerHandler('mouseover');
              return true;
            }
            return false;
          })
        ;
        element.css('pointer-events', 'auto');
      }, 150);
    });
  };
};

mouseWheelPassDirective.$inject = [
  '$document',
  '$timeout',
];

export default mouseWheelPassDirective;
