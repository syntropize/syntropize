# Array Filter

Angular 1.x filter to convert an object to an array.

## Installation

```sh
<npm|yarn|pnpm> add @syntropize/ui-toarray
```

## Loading

Load the main file into your web app via html after loading AngularJS:

```html
<html>
  ...
  <body>
    ...
    <script type="module" src="angular.js"></script>
    <script type="module" src="node_modules/@syntropize/to-array/src/index.js"></script>
    ...
  </body>
  ...
</html>
```

Alternatively, import it into Javascript code:

```js
import '@syntropize/ngToArray';
```

Ensure that your AngularJS application references the `ngToArray` module:

```js
angular.module('myApp', ['ngToArray']);
```

## Usage

Apply the `toArray` filter to an object that you wish to repeat over:

```html
<div ng-repeat="item in obj | toArray | orderBy : 'someProperty'">
  {{item.key}}: {{item.value}}
</div>
```

or

```js
const items = $filter('toArray')(obj);
const list = items.map(item => `${item.key}: ${item.value}`);
```
