/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const toArrayFilter = function toArrayFilter() {
  const toArrayFilterGetter = function toArrayFilterGetter(obj) {
    if (!angular.isObject(obj)) { return obj; }
    return Object.entries(obj)
      .filter(([, value]) => value)
      .map(([key, value]) => ({ key, value }))
    ;
  };

  return toArrayFilterGetter;
};

export default toArrayFilter;
