/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import './run.css';

import runDirective   from './run.directive.js';
import runController  from './run.controller.js';
import runService     from './run.service.js';
import runAPI         from './run.api.service.js';

export default angular.module('run', [])
  .directive('sRun', runDirective)
  .controller('RunController', runController)
  .factory('RunService', runService)
  .factory('RunAPI', runAPI)
;
