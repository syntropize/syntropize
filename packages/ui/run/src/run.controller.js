/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const runController = function runController(
  RunService,
) {
  const addArgument = function addArgument() {
    if (RunService.args.length === 0 || RunService.args[RunService.args.length - 1].text) {
      RunService.args.push({ text: undefined });
    }
  };

  const removeArgument = function removeArgument(index) {
    if (RunService.args.length > 0) {
      RunService.args.splice(index, 1);
    }
  };

  return {
    get form() {
      return RunService.form;
    },
    set form(value) {
      RunService.form = value;
    },
    get command() {
      return RunService.command;
    },
    set command(value) {
      RunService.command = value;
    },
    args: RunService.args,
    addArgument,
    removeArgument,
  };
};

runController.$inject = [
  'RunService',
];

export default runController;
