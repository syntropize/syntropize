/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const runService = function runService(
  $q,
  ExecuteService,
  ErrorToastService,
) {
  const form = {};
  const args = [];
  let command;
  let selectionFn;
  let callbackFn;

  const clear = function clearArgs() {
    command = undefined;
    args.length = 0;
    selectionFn = undefined;
  };

  const start = function initializeRun(selection, callback) {
    selectionFn = selection || function noop() {};
    callbackFn = callback || function noop() {};
    return $q.when();
  };

  const submit = function runCommand() {
    const cleanArgs = args.map(arg => arg.text);
    const selection = selectionFn();

    return (selection ?
      ExecuteService.runCommand({ cmdPath: command, args: cleanArgs }, selection) :
      $q.reject('Undefined Selection')
    )
      .then(function clearSelectionOnExecuteSuccess() { // TODO: logging stdout
        clear();
        callbackFn();
      })
      .catch(function notifyExecuteFailure(error) {
        ErrorToastService(error);
      })
    ;
  };

  const canSubmit = function isFormValid() {
    // return form.name && (form.name.$dirty && form.name.$valid);
    return true;
  };

  const cancel = function submitCancel() {
    clear();
    return $q.when();
  };

  return {
    start,
    submit,
    cancel,
    canSubmit,
    get command() {
      return command;
    },
    set command(value) {
      command = value;
    },
    form,
    args,
  };
};

runService.$inject = [
  '$q',
  'ExecuteService',
  'ErrorToastService',
];

export default runService;
