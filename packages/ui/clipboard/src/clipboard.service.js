/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const clipboardService = function clipboardService() {
  const store = {};

  function clear(model) {
    delete store[model];
  }

  function set(mode, model, location, list) {
    if (list.length > 0) {
      store[model] = {
        mode,
        location: { ...location }, // Copy location not reference
        items: new Set(list),
      };
    }
    else {
      clear(model);
    }
  }

  function cut(...args) {
    set('X', ...args);
  }

  function copy(...args) {
    set('C', ...args);
  }

  function get(model) {
    return store[model];
  }

  function active(model) {
    return !!store[model];
  }

  return Object.freeze({
    cut,
    copy,
    clear,
    get,
    active,
  });
};

export default clipboardService;
