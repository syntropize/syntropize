/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardDisplayService = function cardDisplayService(
  CardDisplay,
) {
  const modeList = ['card', 'file'];
  const { mode } = CardDisplay;
  let globalMode = 0;

  const change = function changeCardDisplayModeIndividual(key, left) {
    if (mode[key] !== 0 && left) { mode[key] -= 1; }
    if (mode[key] !== 1 && !left) { mode[key] += 1; }
  };

  const namedMode = function changeCardDisplayModeAll(key) {
    if (globalMode === 0) {
      return (modeList[mode[key]] || modeList[0]);
    }
    else {
      return modeList[globalMode];
    }
  };

  return Object.freeze({
    change,
    mode: namedMode,
    get global() {
      return modeList[globalMode];
    },
    set global(value) {
      globalMode = modeList.indexOf(value);
    },
  });
};

cardDisplayService.$inject = [
  'CardDisplay',
];

export default cardDisplayService;
