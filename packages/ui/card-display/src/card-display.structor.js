/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardDisplayStructor = function cardDisplayStructorService(
  CardDisplay,
) {
  const setup = function setupCardDisplay() {
    CardDisplay.mode = {};
  };

  const add = function addCardDisplay(key) {
    CardDisplay.mode[key] = 0;
  };

  const remove = function removeCardDisplay(key) {
    delete CardDisplay.mode[key];
  };

  const teardown = function teardownCardDisplay() {
    CardDisplay.mode = undefined;
  };

  return Object.freeze({
    add,
    remove,
    setup,
    teardown,
  });
};

cardDisplayStructor.$inject = [
  'CardDisplay',
];

export default cardDisplayStructor;
