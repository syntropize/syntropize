/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import cardDisplayService   from './card-display.service.js';
import cardDisplayStructor  from './card-display.structor.js';

export default angular.module('sCardDisplay', [])
  .value('CardDisplay', {})
  .factory('CardDisplayService', cardDisplayService)
  .factory('CardDisplayStructor', cardDisplayStructor)
;
