/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const fileSelectDialogController = function fileSelectDialogController(
  $mdDialog,
  $scope,
) {
  const { fileSelect } = this;

  fileSelect.cancel = fileSelect.cancel || 'Cancel';
  fileSelect.continue = fileSelect.continue || 'OK';

  function setArray(blacklist) {
    return blacklist ? [blacklist] : [];
  }

  Object.entries(fileSelect.properties).forEach(([pname, property]) => {
    if (Object.prototype.hasOwnProperty.call(fileSelect.properties, pname)) {
      property.legal = property.legal || /.*/;
      if (!angular.isArray(property.blacklist)) {
        property.blacklist = setArray(property.blacklist);
      }
      fileSelect.properties[pname].isUnique = function isUnique() {
        return property.blacklist.every(item => item !== property.value);
      };
      fileSelect.properties[pname].isLegal = function isLegal() {
        return property.legal.test(property.value);
      };
    }
  });

  fileSelect.setPath = function setPath(event) {
    const path = event.target.files && event.target.files[0].path;
    $scope.$apply(() => {
      fileSelect.properties.path.value = path;
    });
  };

  fileSelect.cancelDialog = function cancelInputDialog() {
    return $mdDialog.cancel();
  };

  fileSelect.continueDialog = function closeInputDialog() {
    return $mdDialog.hide({
      name: fileSelect.properties.name.value,
      path: fileSelect.properties.path.value,
    });
  };

  return fileSelect;
};

fileSelectDialogController.$inject = [
  '$mdDialog',
  '$scope',
];

export default fileSelectDialogController;
