/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import fileSelectDialogTemplate from './file-select-dialog.html';
import fileSelectDialogController from './file-select-dialog.controller.js';

const fileSelectDialogService = function fileSelectDialogService(
  $mdDialog,
) {
  return function fileSelectDialog(fileSelect) {
    return $mdDialog.show({
      controller: fileSelectDialogController,
      template: fileSelectDialogTemplate,
      controllerAs: 'dialog',
      bindToController: true,
      locals: { fileSelect },
    });
  };
};

fileSelectDialogService.$inject = [
  '$mdDialog',
];

export default fileSelectDialogService;
