/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import confirmDialogTemplate from './confirm-dialog.html';
import confirmDialogController from './confirm-dialog.controller.js';

const confirmDialogService = function confirmDialogService(
  $mdDialog,
) {
  return function confirmDialog(confirm) {
    return $mdDialog.show({
      controller: confirmDialogController,
      template: confirmDialogTemplate,
      controllerAs: 'dialog',
      bindToController: true,
      locals: { confirm },
    });
  };
};

confirmDialogService.$inject = [
  '$mdDialog',
];

export default confirmDialogService;
