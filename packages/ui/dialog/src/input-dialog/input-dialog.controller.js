/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const inputDialogController = function inputDialogController(
  $mdDialog,
) {
  let name = this.input.value;

  this.input.cancel = this.input.cancel || 'Cancel';
  this.input.continue = this.input.continue || 'OK';

  this.input.legal = this.input.legal || /.*/;
  this.input.blacklist = this.input.blacklist || [];

  const isUnique = input => Object.keys(this.input.blacklist).every(item => item !== input);

  const isLegal = input => this.input.legal.test(input);

  return {
    input: this.input,
    cancel: function cancelInputDialog() {
      return $mdDialog.cancel();
    },
    continue: function closeInputDialog(value) {
      return $mdDialog.hide(value);
    },
    valid: {
      isUnique,
      isLegal,
    },
    get name() {
      return name;
    },
    set name(newName) {
      name = newName;
    },
  };
};

inputDialogController.$inject = [
  '$mdDialog',
];

export default inputDialogController;
