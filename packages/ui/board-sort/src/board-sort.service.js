/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardSortService = function boardSortService(
  BoardSort,
) {
  const sort = BoardSort;

  const toggleDirection = function toggleSortFieldDirection(field) {
    if (sort.field.isSelected(field)) {
      sort.direction.delete(field);
    }
    else {
      sort.direction.set(field, true);
    }
  };

  const toggleField = function toggleSortField(field) {
    if (sort.field.isSelected(field)) {
      sort.order.splice(sort.order.length - sort.field.selectionNumber(field), 1);
    }
    else {
      sort.order.unshift(`+${field}`);
    }
    toggleDirection(field);
    sort.field.toggle(field);
  };

  const changeDirection = function changeSortFieldDirection(field) {
    const direction = !sort.direction.get(field);
    sort.direction.set(field, direction);
    sort.order[sort.order.length - sort.field.selectionNumber(field)] =
      direction ? `+${field}` : `-${field}`;
  };

  const clear = function clearSort() {
    sort.field.clear();
    sort.direction.clear();
    sort.order = [];
    sort.index.direction = true;
  };

  return {
    changeDirection,
    toggleField,
    toggleDirection,
    clear,
  };
};

boardSortService.$inject = [
  'BoardSort',
];

export default boardSortService;
