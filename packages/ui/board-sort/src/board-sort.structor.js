/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardSortStructor = function boardSortStructor(
  BoardSort,
  SelectionService,
) {
  const setup = function setupCardSort() {
    BoardSort.direction = new Map();
    BoardSort.field = SelectionService.init();
    BoardSort.global = '';
    BoardSort.order = [];
    BoardSort.index = {
      direction: true,
    };
  };

  return Object.freeze({
    setup,
  });
};

boardSortStructor.$inject = [
  'BoardSort',
  'SelectionService',
];

export default boardSortStructor;
