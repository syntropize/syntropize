/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const fileIconService = function fileIconService(
  ResourceService,
) {
  const icon = {
    file: 'editor:insert_drive_file',
    folder: 'file:folder',
    board: 'action:view_module',
    nothing: 'image:lens',
    card: 'image:crop_16_9',
  };

  const fetch = function fetchFileIcon(key, name, item) {
    if (!item) { return icon.nothing; }

    if (item.isDirectory()) {
      if (key === '.') {
        const meta = Object.prototype.hasOwnProperty.call(
          ResourceService.resource.board.metadata,
          name,
        );
        const card = Object.prototype.hasOwnProperty.call(
          ResourceService.resource.board.cards,
          name,
        );
        if (meta) {
          return icon.board;
        }
        else if (card) {
          return icon.card;
        }
        else {
          return icon.folder;
        }
      }
      else {
        return icon.folder;
      }
    }

    if (item.isFile()) {
      return icon.file;
    }
    else {
      return icon.nothing;
    }
  };

  return { fetch };
};

fileIconService.$inject = [
  'ResourceService',
];

export default fileIconService;
