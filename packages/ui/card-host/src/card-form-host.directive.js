/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardFormHostDirective = function cardFormHostDirective(
  $compile,
) {
  const cardFormHostLinker = function cardFormHostLinker(scope, element, attrs) {
    const template = function cardFormTemplate(type) {
      return `<${type}-card-input
          contents = "host.contents"
          form-name = "host.formName">
        </${type}-card-input>`
      ;
    };

    attrs.$observe('type', function recompileFormOnCardTypeChange(newValue) {
      const el = angular.element(template(newValue));
      element.empty();
      element.append(el);
      $compile(el)(scope);
    });
  };

  return {
    restrict: 'E',
    scope: {
      contents: '=?',
      formName: '=?',
    },
    bindToController: true,
    controller: function noop() {},
    controllerAs: 'host',
    link: cardFormHostLinker,
  };
};

cardFormHostDirective.$inject = [
  '$compile',
];

export default cardFormHostDirective;
