/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardDisplayHostDirective = function cardDisplayHostDirective(
  $compile,
) {
  const cardDisplayHostLinker = function cardDisplayHostLinker(scope, element, attrs) {
    const template = function cardDisplayTemplate(type) {
      return `<${type}-card-display
        metadata="metadata"
        contents="contents"
        key="{{key}}"
        index="{{index}}">
        </${type}-card-display>`
      ;
    };

    const getElement = function getElement(type) {
      return angular.element(template(type));
    };

    attrs.$observe('type', function resetCardTemplate(newValue) {
      const el = getElement(newValue);
      element.empty();
      element.append(el);
      $compile(el)(scope);
    });
  };

  return {
    restrict: 'E',
    scope: {
      metadata: '=?',
      contents: '=?',
      key: '@',
      index: '@',
    },
    link: cardDisplayHostLinker,
  };
};

cardDisplayHostDirective.$inject = [
  '$compile',
];

export default cardDisplayHostDirective;
