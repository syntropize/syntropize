/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import colorPickerCheckboxTemplate from './color-picker-checkbox.html';

const colorPickerCheckboxDirective = function colorPickerCheckboxDirective() {
  return {
    template: colorPickerCheckboxTemplate,
    restrict: 'E',
    scope: {
      model: '=',
      colors: '=',
      name: '@?',
    },
    controller: function noop() {},
    controllerAs: 'cp',
    bindToController: true,
  };
};

export default colorPickerCheckboxDirective;
