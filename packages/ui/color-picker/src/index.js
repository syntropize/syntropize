/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import './color-picker-radio/color-picker-radio.css';
import './color-picker-checkbox/color-picker-checkbox.css';

import colorPickerRadioDirective    from './color-picker-radio/color-picker-radio.directive.js';
import colorPickerCheckboxDirective from './color-picker-checkbox/color-picker-checkbox.directive.js';

export default angular.module('sColorPicker', [])
  .directive('colorPickerRadio', colorPickerRadioDirective)
  .directive('colorPickerCheckbox', colorPickerCheckboxDirective)
;
