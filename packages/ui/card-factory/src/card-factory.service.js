/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardFactoryService = function cardFactoryService() {
  const cardFactory = function cardFactory(prototype) {
    const {
      core:
        {
          flags = [],
          type = 'text',
          tags = [],
          color = '#80D8FF',
        } = {},
      contents = {},
      properties: {
        cardHeight = 4,
        cardWidth = 4,
      } = {},
    } = prototype || {};

    return {
      core: {
        type,
        color,
        tags,
        flags,
      },
      contents,
      properties: {
        cardHeight,
        cardWidth,
      },
    };
  };

  return cardFactory;
};

export default cardFactoryService;
