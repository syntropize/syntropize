/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardSearchStructor = function boardSearchStructor(
  BoardSearch,
) {
  const setup = function setupBoardSearch() {
    BoardSearch.filter = {};
  };

  return Object.freeze({
    setup,
  });
};

boardSearchStructor.$inject = [
  'BoardSearch',
];

export default boardSearchStructor;
