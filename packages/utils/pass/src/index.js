/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const segregate = function promiseAllSettledSegregator(results) {
  const values = {};
  const reasons = [];

  function fulfilled({ value }, index) {
    values[index] = value;
  }

  function rejected({ reason }, index) {
    if (Array.isArray(reason)) {
      reason.forEach(r => Array.isArray(r.index) && r.index.unshift(index));
      reasons.push(...reason);
    }
    else {
      reasons.push(Object.assign(reason, { index: [index] }));
    }
  }

  function partial(result, params) {
    fulfilled(result, params);
    rejected(result, params);
  }

  const handlers = {
    fulfilled,
    rejected,
    partial,
  };

  results.forEach((result, index) => {
    handlers[result.status](result, index);
  });


  if (reasons.length === 0) {
    return {
      status: 'fulfilled',
      value: values,
    };
  }
  if (Object.keys(values).length === 0) {
    return {
      status: 'rejected',
      reason: reasons,
    };
  }

  return Object.freeze({
    status: 'partial',
    value: values,
    reason: reasons,
  });
};

export default segregate;
