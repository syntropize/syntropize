# Promise All Settled Segregator

This utility segregates the results of `promiseAllSettled` into separate arrays respectively containing fulfilled and rejected results.

## Usage
```js
import segregate from '@syntropize/util-pass';

const segregatedResults = segregate(
  await promiseAllSettled([/* Array of promises*/])
);
```
Where `segregatedResults` is an object wih properties:
+ `status`: Status of `promiseAllSettled` operation. Can be:
  + `fulfilled`: if all promises are fulfilled,
  + `rejected`: if all promises are rejected,
  + `partial`: if some promises are fulfilled and others are rejected.
+ `value`: An array of fulfilled promise results. Available only when atleast one promise has been fulfilled, i.e. when status is `fulfilled` or `partial`.
+ `reason`: An array containing reasons for promise rejection. Available only when atleast one promise has been rejected, i.e. when status is `rejected` or `partial`.
