/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import AggregateError from 'es-aggregate-error';

function getCountStatus(count) {
  if (count === Infinity) {
    return 'ALL';
  }
  else if (count === 0) {
    return 'ZERO';
  }
  else if (count === 1) {
    return 'ONE';
  }
  else {
    return 'MANY';
  }
}

async function bulkSchemaResults(messages, mainResults = {}, metaResults = {}) {
  const mainErrors = mainResults.reason || [];
  const metaErrors = metaResults.reason || [];

  const value = {
    main: mainResults.value,
    metadata: metaResults.value,
  };

  const mainErrorStatus = getCountStatus(mainResults.status === 'rejected' ? Infinity : mainErrors.length);
  const metaErrorStatus = getCountStatus(metaErrors.length);

  const message = messages[metaErrorStatus][mainErrorStatus];

  const totalErrors = mainErrors.length + metaErrors.length;

  if (totalErrors === 0) {
    return value;
  }
  else if (totalErrors === 1) {
    const error = mainErrors.length === 1 ? mainErrors[0] : metaErrors[0];
    error.success = value;
    error.message = `${message}, ${error.message}`;
    throw error;
  }
  else {
    const error = new AggregateError([...mainErrors, ...metaErrors], `${message} multiple errors!`);
    error.success = value;
    throw error;
  }
}

export default bulkSchemaResults;
