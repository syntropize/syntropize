# Bulk Schema Results

This utility aggregates results of `promiseAllSettled` operations and generate proper error messages.

It is used by Syntropize schema plugins to aggregate results of write operations involving multiple entities such as copy, move and remove.

## Usage
```js
import segregate from '@syntropize/util-pass';
import bulkResults from '@syntropize/util-bulk-schema-results';

const errorMessages = {
  ZERO: undefined,
  ONE: 'One operation failed',
  MANY: 'Some operations failed',
  ALL: 'All operations failed',
};

let results;

try {
  results = bulkResults(
    errorMessages,
    segregate(await promiseAllSettled([/* Array of promises for primary operations*/])),
    /* optional */ segregate(await promiseAllSettled([/* Array of promises for secondary operations*/])),
  );
}
catch (error) {
  results = error.success;
  console.log(error.message);
}
```
