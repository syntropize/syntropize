/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
// Cheat module to ensure shared components are loaded once only;
// Since Angular puts everything in a global space.

import 'angular-perfect-scrollbar-2';
import 'angular-dragula/dist/dragula.css';
import dragula from 'angular-dragula';
import 'angular-filesize-filter';
import 'ng-sglclick';

import fileList from '@syntropize/ui-file-list';
import markdown from '@syntropize/ui-markdown';
import run from '@syntropize/ui-run';
import cardHost from '@syntropize/ui-card-host';
import cardFactory from '@syntropize/ui-card-factory';
import colorPicker from '@syntropize/ui-color-picker';

const ngDependencies = [
  'angular-perfect-scrollbar-2',
  dragula(angular),
  'ngSglclick',
  'ngFilesizeFilter',
];

const appDependencies = [
  fileList,
  markdown,
  run,

  cardHost,
  cardFactory,

  colorPicker,
];


export default angular.module('sPreload', [
  ...ngDependencies,
  ...appDependencies.map(module => module.name),
]);
