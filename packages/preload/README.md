# Syntropize Preload

A wrapper to preload the most commonly shared components between Syntropize and its plugins. This allows us to make smaller plugins that load faster.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
