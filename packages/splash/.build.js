/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import fs from 'fs/promises';
import postcss from 'postcss';
import url from 'postcss-url';

const options = {
  url: 'copy',
  assetsPath: 'assets',
  basePath: ['.', '../node_modules'],
  useHash: true,
  hashOptions: {
    shrink: 6,
    append: true,
  }
};

async function processCss() {
  const css = await fs.readFile('src/splash.css');

  const result = await postcss()
    .use(url(options))
    .process(css, {
      from: "src/splash.css",
      to: "lib/splash.css"
    })
  ;

  await fs.writeFile('lib/splash.css', result.css, () => true);
}

async function copyHtml() {
  await fs.copyFile('./src/index.html', './lib/index.html');
}

async function buildSplash() {
  try {
    await processCss();
  }
  catch (error) {
    console.error(`Unable to build Splash Screen: Could not process CSS!
${error.message}`);
    process.exitCode = 1;
  }

  try {
    await copyHtml();
  }
  catch (error) {
    console.error(`Unable to build Splash Screen: Could not copy HTML!
${error.message}`);
    process.exitCode = 1;
  }
}

buildSplash();
