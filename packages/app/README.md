# Syntropize App

This is the main Syntropize application.

## Usage

This package is not meant to be used on its own but within a browser based environment which hosts the application as well as provides configuration and APIs.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
