/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const navbarController = function navbarController(
  ResourceAPI,
  ReadOnlyService,
  Registry,

  ViewStatus,

  LaunchService,
  ErrorToastService,
  SuccessToastService,

  $stateParams,
  $mdMedia,
  $rootScope,
  $scope,
  $location,
  $window,
) {
  const { resource } = ResourceAPI;

  const { history } = $window;

  // A temporary hack to determine if back/forward are disabled
  // because one cannot access history stack
  // A known issue with nwjs - pointed out on the mailing list
  // https://groups.google.com/forum/#!searchin/nwjs-general/back$20forward/nwjs-general/DuMTsU8TwhM/yzwszREL9kcJ
  const history2 = (function history2Factory() {
    let position = 0; // assumes application is started fresh

    const back = function back() {
      history.back();
      position -= 2;
    };

    const forward = function forward() {
      history.forward();
    };

    const increment = function incrementHistory() {
      position += 1;
    };

    const update = function updateHistoryPosition(length) {
      if (position > length) { position = length; }
    };

    return Object.freeze({
      get disableBack() {
        return position <= 1;
      },
      get disableForward() {
        return position === 0 || position === history.length;
      },
      back,
      forward,
      increment,
      update,
    });
  })();

  $scope.$watch(
    'history.length',
    history2.update,
  );


  const address = {
    text: '',
    searchItems: [],
  };
  let location;
  let currentView;

  let state;
  let ifPrevDisconnect;


  const viewButton = (function viewButtonFactory() {
    let name;
    let icon;
    let viewList = {};

    const reset = function resetViewButton() {
      name = 'Container';
      icon = 'navigation:arrow_drop_down';
    };

    const set = function setViewButton(viewName) {
      if (viewName) {
        viewList = Registry.view.items;
        if (viewList[viewName]) {
          name = viewName;
          ({ icon } = viewList[viewName]);
          return;
        }
      }
      reset();
    };

    reset();

    return Object.freeze({
      get name() {
        return name;
      },
      get icon() {
        return icon;
      },
      get list() {
        return viewList;
      },
      set,
    });
  })();

  const viewMenuButton = (function viewMenuButtonFactory() {
    let isLocation = false;
    const urlRegexp = /^.+:\/\/.*$/;

    const setViewMenuButton = function setViewMenuButton(destination) {
      isLocation = urlRegexp.test(destination);
    };

    return Object.freeze({
      set: setViewMenuButton,
      get active() {
        return isLocation;
      },
    });
  })();

  const launchButton = (function launchButtonFactory() {
    let message;
    let icon;
    let enabled;

    const set = function setLaunchButton(status) {
      switch (status) {
        case 'reload':
          enabled = true;
          message = 'Reload';
          icon = 'navigation:refresh';
          break;
        case 'cancel':
          enabled = true;
          message = 'Cancel';
          icon = 'navigation:cancel';
          break;
        case 'launch':
          enabled = true;
          message = 'Launch';
          icon = 'action:launch';
          break;
        default:
          enabled = false;
      }
    };
    set();

    return Object.freeze({
      get message() { return message; },
      get icon() { return icon; },
      get enabled() { return enabled; },
      set,
    });
  })();

  const progress = (function progressFactory() {
    let status;
    let color;
    let value;

    const set = function setNavigationProgress(ioStatus) {
      switch (ioStatus) {
        case 'connecting':
          status = 'query';
          color = 'md-primary';
          value = undefined;
          break;
        case 'reconnecting':
          status = 'query';
          color = 'md-warn';
          value = undefined;
          break;
        case 'valid':
          status = 'intermediate';
          color = 'md-primary';
          break;
        case 'ready':
          status = 'determinate';
          color = 'md-primary';
          value = value || 80;
          break;
        default:
          status = 'determinate';
          color = 'md-primary';
          value = 100;
      }
    };

    // Initialize Progress Bar Values with default
    set();

    return {
      get status() { return status; },
      get color() { return color; },
      get value() { return value; },
      set,
    };
  })();

  const up = (function upFactory() {
    let disabled = true;

    const go = function goUp() {
      LaunchService.item('..');
    };

    const setDisabled = function setUpDisabled(activeLocation) {
      if (activeLocation && !!resource[activeLocation].location) {
        const base = resource[activeLocation].location.url.$pathname;
        disabled = ((base[1] === ':' && base.length === 2) || base === '/' || base === '');
      }
      else {
        disabled = true;
      }
    };

    return Object.freeze({
      get disabled() {
        return disabled;
      },
      go,
      setDisabled,
    });
  })();

  const messages = function messages(status, params) {
    let reconnectMessage;
    switch (status) {
      case 'reconnecting':
        reconnectMessage = `${resource[location].socket.error ?
          `${resource[location].socket.error} | ` : ''}
          Reconnecting (${params.toString()})...`;
        SuccessToastService({ message: reconnectMessage });
        break;
      case 'disconnected':
        ifPrevDisconnect = true;
        ErrorToastService(new Error('Disconnected: Refresh to Reconnect'));
        break;
      case 'valid':
        if (ifPrevDisconnect === true) {
          SuccessToastService({ message: 'Reconnected' });
        }
        break;
      case 'error':
      case 'connect_fail':
      case 'fs_error':
        if (resource[location].socket.error.message !== 'Invalid namespace: file' &&
        resource[location].socket.error.description !== 404) {
          ErrorToastService(resource[location].socket.error);
        }
        break;
      default:
    }
  };

  const watchResourceStatus = function watchResourceStatus() {
    try {
      return location && resource[location].socket.status;
    }
    catch (e) {
      return undefined;
    }
  };

  const setNavbarFromStatus = function setNavbarFromStatus(status) {
    $scope.$applyAsync(function _setNavbarFromStatus() {
      if (angular.isNumber(status)) {
        progress.set('reconnecting');
        messages('reconnecting', status);
      }
      else {
        progress.set(status);
        messages(status);
      }
    });
  };

  $scope.$watch(watchResourceStatus, setNavbarFromStatus);


  const deregisterSetNavigationStart = $rootScope.$on(
    '$stateChangeStart',
    function setNavigationStart(
      startEvent, toState, toParams,
    ) {
      ifPrevDisconnect = false;
      // event = startEvent;
      state = toState.name;
      up.setDisabled();
      progress.set('connecting');
      if (toState.name === 'resource.view') {
        launchButton.set('cancel');
        ({ location } = toParams);
        // address.text = location;
      }
      else {
        location = undefined;
        // address.text = state;
      }
    },
  );

  const deregisterSetNavigationError = $rootScope.$on(
    '$stateChangeError',
    function setNavigationError(
      errorEvent, toState, toParams, fromState, fromParams,
    ) {
      progress.set();
      // theme = 'default';
      launchButton.set('reload');
      state = fromState.name;
      if (state === 'resource.view') {
        ({ location } = fromParams);
        up.setDisabled(location);
        address.text = location;
        currentView = fromParams.view;
      }
      else {
        address.text = fromState.name;
      }
      // event = errorEvent;
      // ErrorToastService(error);
    },
  );

  const deregisterSetNavigationSuccess = $rootScope.$on(
    '$stateChangeSuccess',
    function setNavigationSuccess(
      successEvent, toState, toParams,
    ) {
      // theme = 'default';
      launchButton.set('reload');
      state = toState.name;
      if (state === 'resource.view') {
        ({ location } = toParams); // TODO: case sensitivity with windows
        address.text = location;
        currentView = toParams.view;
        up.setDisabled(location);
        viewButton.set(currentView);
      }
      else {
        location = undefined;
        currentView = undefined;
        address.text = toState.name;
      }
      history2.increment();
      progress.set();
      // event = successEvent;
    },
  );

  $scope.$on('$destroy', deregisterSetNavigationStart);
  $scope.$on('$destroy', deregisterSetNavigationError);
  $scope.$on('$destroy', deregisterSetNavigationSuccess);


  /** Address Autocomplete */
  address.searchItems = []; // Needed to Stop ng-material from screaming!
  // nav.address.onSearchTextChange = function onAddressSearchTextChange() {
  //   if (nav.location[nav.location.length - 1] === '/') {
  //     let location = nav.location;
  //     if (resource[location].directory.files) {
  //       nav.address.searchItems = resource[location].directory.files;
  //     }
  //     else {
  //       fetch or
  //       nav.address.searchItems = undefined;
  //       nav.address.invalid = true;
  //     }
  //   }
  //   else {
  //   }
  // };

  // nav.address.onSelectedItemChange = function onSelectedItemChange(item) {
  //   nav.address.text = nav.address.text + item;
  // };


  /** Reset Location to current url on Cancel/Esc */
  const setAddressBar = function setAddressBar(destination) {
    // temporary fix to reset address bar: ng-blur/ng-focus does not
    // work with md-autocomplete. See github:angular/material#3906
    if (launchButton.message !== 'Cancel') {
      if (destination === location || destination === state) {
        launchButton.set('reload');
      }
      else if (viewMenuButton.active) {
        launchButton.set('launch');
      }
      else if (address.text === '' || !address.text) {
        launchButton.set();
      }
    }
  };


  const resetAddress = function resetAddress() {
    address.text = location || state;
  };

  $scope.$watch(
    function watchAddressText() { return address.text; },
    function updateAddressBar(destination) {
      $scope.$applyAsync(function updateAddressBarItems() {
        setAddressBar(destination);
        viewMenuButton.set(destination);
      });
    },
  );

  const launchView = function launchView(viewId) {
    /** add a layer of logic to allow for non address launches */
    if (address.text === location) {
      /** Same Location and Same View Constitutes a Reload */
      if (currentView === viewId) {
        LaunchService.url();
      }
      /** Same Location and Different View leads to a view change */
      else {
        LaunchService.view(viewId);
      }
    }
    else {
      /** Normalize path to follow Posix Conventions */
      const [protocol, path] = address.text.split('://');
      const cleanedPath = path
        .replace(/\\/g, '/')
        .replace(/\/\.\//g, '/')
        .replace(/\/\.\.\/[^/]*/g, '')
        .replace(/\/$/, '');
      address.text = `${protocol}://${cleanedPath}`;
      LaunchService.url(address.text, viewId);
    }
  };

  // container should be disabled for state launches

  const launchAddress = function launchAddress() {
    if (location && launchButton.message === 'Cancel') {
      // Done in absence of lifecycle events in old ui-router
      if (angular.isDefined(resource[location].socket.ready)) {
        resource[location].socket.disconnect();
      }
    }

    else if (viewMenuButton.active) {
      launchView(currentView || 'boardSimple');
    }
    else {
      LaunchService.state(address.text);
    }
  };

  const keyIsEnter = function navbarKeyIsEnter({ which, keyCode }) {
    return ((which || keyCode) === 13);
  };

  return {
    get address() {
      return address;
    },
    resetAddress,
    get progress() {
      return progress;
    },
    get media() {
      return $mdMedia;
    },
    history,
    history2,
    up,
    viewButton,
    viewMenuButton,
    launchButton,
    get view() {
      return currentView;
    },
    get readOnly() {
      return ReadOnlyService.status;
    },
    get viewStatus() {
      return ViewStatus;
    },

    launchView,
    launchAddress,
    keyIsEnter,
  };
};

navbarController.$inject = [
  'ResourceAPI',
  'ReadOnlyService',
  'Registry',

  'ViewStatus',

  'LaunchService',
  'ErrorToastService',
  'SuccessToastService',

  '$stateParams',
  '$mdMedia',
  '$rootScope',
  '$scope',
  '$location',
  '$window',
];

export default navbarController;
