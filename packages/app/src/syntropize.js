/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import 'angular-material/angular-material.css';

import 'angular';
import 'angular-messages';
import 'angular-sanitize';
import 'angular-animate';
import 'angular-aria';

import 'angular-material';
import 'angular-ui-router';
import 'angular-ui-validate';

// Shared Components
import fileChange from '@syntropize/ui-file-change';
import mwPass     from '@syntropize/ui-mouse-wheel-pass';
import toArray    from '@syntropize/ui-to-array';
import urlParse   from '@syntropize/ui-url-parse';

import clipboard  from '@syntropize/ui-clipboard';
import dialog     from '@syntropize/ui-dialog';
import help       from '@syntropize/ui-help';
import panel      from '@syntropize/ui-panel';
import registry   from '@syntropize/ui-registry';
import selection  from '@syntropize/ui-selection';
import toast      from '@syntropize/ui-toast';
import toolbar    from '@syntropize/ui-toolbar';

import preload from '@syntropize/app-preload';

// Settings
import appSettingsProvider from './wrappers/settings.provider.js';

import environmentProvider from './wrappers/environment.provider.js';
import executeAPI          from './wrappers/execute.service.js';
import resourceAPI         from './wrappers/resource.service.js';


import './fonts.css';

// Initialization
import debugProvider      from './services/debug.config.js';
import iconProvider       from './services/icon.config.js';
import navigationProvider from './navigation/navigation.config.js';
import navigationInstance from './navigation/navigation.run.js';
import windowInstance     from './services/window.run.js';

// Services
import navigationErrorService from './navigation/error.service.js';
import navigationLogService   from './navigation/log.service.js';

import htmlAnchorDecorator  from './services/a.decorator.js';
import commonCommandService from './services/command.service.js';
import executeService       from './services/execute.service.js';
import openService          from './services/open.service.js';
import launchService        from './services/launch.service.js';
import readOnlyService      from './services/read-only.service.js';

// Components
import './app/app.css';
import appDirective from './app/app.directive.js';

import './navbar/navbar.css';
import navbarController from './navbar/navbar.controller.js';
import navbarDirective from './navbar/navbar.directive.js';

import './taskbar/taskbar.css';
import taskbarDirective from './taskbar/taskbar.directive.js';

// Routes
import './routes/main/main.css';
import mainProvider from './routes/main/main.config.js';

import resourceProvider from './routes/resource/resource.config.js';
import resourceInstance from './routes/resource/resource.run.js';
// import resourceController from './routes/resource/resource.controller'; // Test

import viewProvider from './routes/view/view.config.js';
import viewStatusService from './routes/view/view.status.service.js';


const ngDependencies = [
  'ngMessages',
  'ngSanitize',
  'ngAnimate',
  'ngAria',
  'ngMaterial',
  'ui.router',
  'ui.validate',
];

const appDependencies = [
  fileChange,
  mwPass,
  toArray,
  urlParse,

  clipboard,
  dialog,
  help,
  panel,
  registry,
  selection,
  toast,
  toolbar,

  // Things that Plugins should eventually lazy-load
  preload,
];


export default angular.module('Syntropize', [
  ...ngDependencies,
  ...appDependencies.map(module => module.name),
])

  // Settings
  .provider('AppSettings', appSettingsProvider)

  // Initialization
  .config(debugProvider)
  .config(iconProvider)
  .config(navigationProvider)
  .run(navigationInstance)
  .run(windowInstance)

  // Environment
  .provider('Environment', environmentProvider)
  // API
  .factory('ExecuteAPI', executeAPI)
  .factory('ResourceAPI', resourceAPI)

  // Components
  .directive('sApp', appDirective)

  .controller('NavbarController', navbarController)
  .directive('sNavbar', navbarDirective)

  .directive('sTaskbar', taskbarDirective)

  // Services
  .factory('NavigationErrorService', navigationErrorService)
  .factory('NavigationLogService', navigationLogService)

  .decorator('aDirective', htmlAnchorDecorator)
  .factory('CommandService', commonCommandService)
  .factory('ExecuteService', executeService)
  .factory('LaunchService', launchService)
  .factory('ReadOnlyService', readOnlyService)
  .factory('OpenService', openService)

  // Routes
  .config(mainProvider)

  .value('ResourceService', { resource: undefined })
  .config(resourceProvider)
  .run(resourceInstance)
  // .controller('ResourceController', resourceController) // Test

  .config(viewProvider)
  .factory('ViewStatus', viewStatusService)
;
