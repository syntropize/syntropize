/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const htmlAnchorDecorator = function htmlAnchorDecorator(
  $delegate,
  // OpenService,
  LaunchService,
  // UrlParseService,
  // NW,
) {
  const a = $delegate[0];

  a.compile = function recompileLinkTag(tEl, tAttrs) {
    if (tAttrs.href || tAttrs.ngHref) {
      return function detectClickOnLink(scope, element, attrs) {
        if (element[0].nodeName.toLowerCase() === 'a') {
          element.on('click', function openHrefInExternal(event) {
            if (attrs.href) {
              event.preventDefault();
              // The failure of LaunchService automatically precipitates OpenService
              LaunchService.url(attrs.href);
            }
          });
        }
      };
    }
    else {
      return () => {};
    }
  };

  return $delegate;
};

htmlAnchorDecorator.$inject = [
  '$delegate',
  // 'OpenService',
  'LaunchService',
  // 'UrlParseService',
  // 'NW',
];

export default htmlAnchorDecorator;
