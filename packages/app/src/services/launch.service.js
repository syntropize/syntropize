/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const launchService = function launchService(
  AppSettings,
  $panel,
  ResourceService,
  OpenService,
  $state,
  $location,
  $mdToast,
) {
  const panel = $panel;

  const stopEventToast = function stopEventToast() {
    return $mdToast.show($mdToast.simple()
      .textContent('Close Side Panel First to Navigate!')
      .highlightClass('md-warn')
      .highlightAction(true)
      .hideDelay(3000)
      .position('top'));
  };

  const checkPanel = function checkPanel() {
    const status = panel.isBlocked();
    if (angular.isDefined(status) && !status) {
      panel.cancel();
    }
    return !status;
  };


  const launchState = function launchState(state, params, options) {
    // All http: requests are sent to the browser
    // It is possible to have a https style real-time service, solid redirects to https anyway,
    // supporting it with a separate server system as was originally done is just a hassle.
    if (params && params.location && params.location.startsWith('http:')) {
      return OpenService(params.location);
    }
    else {
      if (checkPanel()) {
        return $state.go(state, params, options);
      }
      else {
        return stopEventToast();
      }
    }
  };

  const launchView = function launchView(view) {
    const params = {};
    if (angular.isDefined(view)) {
      params.view = view;
    }

    if ($state.is('resource.view')) {
      return launchState(
        'resource.view',
        params,
        { reload: 'resource.view', location: true },
      );
    }

    return undefined;
  };

  const launchUrl = function launchUrl(location, view) {
    // if (!angular.isArray(location)) {
    //   location = [location];
    // }
    // location = ResourceService.resource.location.address(...location);


    if (!$state.is('resource.view') && !view) {
      view = AppSettings.view.default; // eventually will be a recommended view
    }

    const params = {};
    if (angular.isDefined(location)) {
      params.location = location;
    }
    if (angular.isDefined(view)) {
      params.view = view;
    }

    if ($state.is('resource.view') || location) {
      return launchState(
        'resource.view',
        params,
        { reload: true, location: true },
      );
    }

    return undefined;
  };

  const launchItem = function launchItem(item, view) {
    if (angular.isArray(item)) {
      return launchUrl(ResourceService.resource.location.address(...item), view);
    }
    else if (angular.isUndefined(item)) {
      return launchUrl(item, view);
    }
    else {
      return launchUrl(ResourceService.resource.location.address(item), view);
    }
  };


  return {
    state: launchState,
    view: launchView,
    url: launchUrl,
    item: launchItem,
  };
};

launchService.$inject = [
  'AppSettings',
  '$panel',
  'ResourceService',
  'OpenService',
  '$state',
  '$location',
  '$mdToast',
];

export default launchService;
