/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const executeService = function executeService(
  ResourceService,

  Registry,
  ExecuteAPI,

  $q,
  $injector,
) {
  const script = {};
  const compileRegistry = Registry.compile;
  const execute = ExecuteAPI;

  const checkProtocol = function checkProtocol() {
    if (ResourceService.resource.location.url.scheme === 'file') {
      return $q.when();
    }
    else {
      return $q.reject('Scripts work only on local resources');
    }
  };

  const runScript = function runScript(scriptName, fileList, targetFile) {
    const location = ResourceService.resource.location.path();
    return checkProtocol()
      .then(function loadScriptOnCheckProtocolSuccess() {
        /** Load the script asked for and if successful inject and run */
        if (Object.prototype.hasOwnProperty.call(script, scriptName)) {
          return $q.when();
        }
        else {
          return compileRegistry.load(scriptName)
            .then(function compileOnScriptLoadSuccess() {
              script[scriptName] = $injector.get(`${scriptName}Script`);
            })
          ;
        }
      })
      .then(function compileOnScriptLoadSuccess() {
        return execute.script(
          script[scriptName],
          compileRegistry.items[scriptName],
          { location, fileList, targetFile },
        )
          .catch(function noteExecuteFailure(error) {
            error.message = `Run Error: Unable to execute Script: ${error.message}`;
            throw error;
          })
        ;
      })
    ;
  };

  const runCommand = function runCommand(cmd, fileList, targetFile) {
    const location = ResourceService.resource.location.path();
    return checkProtocol()
      .then(function runCommandOnCheckProtocolSuccess() {
        return execute.command(cmd, { location, fileList, targetFile });
      })
      .catch(function noteExecuteFailure(error) {
        error.message = `Run Error: Unable to execute Command: ${error.message}`;
        throw error;
      })
    ;
  };

  return {
    runCommand,
    runScript,
  };
};

executeService.$inject = [
  'ResourceService',

  'Registry',
  'ExecuteAPI',

  '$q',
  '$injector',
];

export default executeService;
