/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const windowInstance = function windowInstance(
  $rootScope,
  AppSettings,
  Environment,
) {
  const { window: win } = Environment;

  /* eslint-disable no-use-before-define */

  const removeFirstStartListener = $rootScope.$on('$stateChangeStart', function loadWindowOnFirstStateStart() {
    win.open({
      maximize: AppSettings.window && AppSettings.window === 'maximize',
    });
    removeFirstStartListener();
  });

  const removeFirstErrorListener = $rootScope.$on('$stateChangeError', function logOnLocationChangeError() {
    win.openDev();
    removeFirstErrorListener();
    removeFirstSuccessListener();
  });

  const removeFirstSuccessListener = $rootScope.$on('$stateChangeSuccess', function loadWindowOnFirstStateStart() {
    removeFirstErrorListener();
    removeFirstSuccessListener();
  });
};

windowInstance.$inject = [
  '$rootScope',
  'AppSettings',
  'Environment',
];

export default windowInstance;
