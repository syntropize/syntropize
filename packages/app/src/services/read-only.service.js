/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const readOnlyService = function readOnlyService(
  ResourceService,
  $rootScope,
  $log,
) {
  let status;
  let active = false;

  const setReadOnly = function setReadOnly(scheme, { mode2 }) {
    switch (scheme) {
      case 'http':
        return true;
      case 'https':
      case 'file':
        return mode2 && !mode2.others.write;
      default:
        $log.warn('ViewController: Unable to determine mode: Assuming ReadOnly');
        return true;
    }
  };

  const extractReadOnly = function exttractReadOnly(loc, name) {
    if (loc === '.' && name === '') {
      status = setReadOnly(
        ResourceService.resource.location.url.scheme,
        ResourceService.resource.directory.files['.'][''],
      );
    }
  };

  $rootScope.$on('$stateChangeSuccess', (event, toState) => {
    status = undefined;
    if (ResourceService.resource.directory.options.readOnly) {
      status = true;
      return;
    }
    if (toState === 'resource.view' && !active) {
      ResourceService.Resource.directory.event.on('add', extractReadOnly);
      active = true;
    }
    if (toState !== 'resource.view' && active) {
      ResourceService.Resource.directory.event.on('add', extractReadOnly);
      active = false;
    }
  });

  $rootScope.$on('$stateChangeError', () => {
    status = undefined;
  });

  return {
    get status() {
      return status;
    },
  };
};

readOnlyService.$inject = [
  'ResourceService',
  '$rootScope',
  '$log',
];

export default readOnlyService;
