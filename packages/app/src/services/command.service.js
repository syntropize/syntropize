/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const commandService = function commandService(
  ResourceService,
  ExecuteService,

  ErrorToastService,
  SuccessToastService,
  InputDialogService,
) {
  const filenameRE = /^\w{1}[ \w-.,;]*$/;

  const run = {
    script: function runScript(name, selection, callback) {
      return InputDialogService({
        text: 'Enter name for output file: ',
        title: 'Output File',
        cancel: 'Skip',
        continue: 'Run',
        value: undefined,
        legal: filenameRE,
        blacklist: ResourceService.resource.directory.files['.'],
      })
        .then(function onValidScriptArguments(value) {
          return ExecuteService.runScript(name, selection(), value);
        })
        .then(function onRunScriptSuccess() {
          callback();
          SuccessToastService({ message: 'Success: Compiled' });
        })
        .catch(function onRunScriptFailure(error) {
          if (error) {
            ErrorToastService(error);
          }
          return error;
        })
      ;
    },

    command: function noop() {},
  };

  return Object.freeze({
    run,
  });
};

commandService.$inject = [
  'ResourceService',
  'ExecuteService',

  'ErrorToastService',
  'SuccessToastService',
  'InputDialogService',
];

export default commandService;
