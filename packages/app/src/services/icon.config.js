/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import action     from 'md-icons-svg-sprites-24px/svg-sprite-action.svg';
import alert      from 'md-icons-svg-sprites-24px/svg-sprite-alert.svg';
import content    from 'md-icons-svg-sprites-24px/svg-sprite-content.svg';
import editor     from 'md-icons-svg-sprites-24px/svg-sprite-editor.svg';
import file       from 'md-icons-svg-sprites-24px/svg-sprite-file.svg';
import image      from 'md-icons-svg-sprites-24px/svg-sprite-image.svg';
import navigation from 'md-icons-svg-sprites-24px/svg-sprite-navigation.svg';
import toggle     from 'md-icons-svg-sprites-24px/svg-sprite-toggle.svg';

const iconProvider = function iconProvider(
  $mdIconProvider,
) {
  $mdIconProvider
    .iconSet('action', action)
    .iconSet('alert', alert)
    .iconSet('content', content)
    .iconSet('editor', editor)
    .iconSet('file', file)
    .iconSet('image', image)
    .iconSet('navigation', navigation)
    .iconSet('toggle', toggle)
  ;
};

iconProvider.$inject = [
  '$mdIconProvider',
];

export default iconProvider;
