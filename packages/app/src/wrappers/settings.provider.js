/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const settingsProvider = function settingsProvider(
  $windowProvider,
) {
  const {
    settings,
    resource,
  } = $windowProvider.$get().syntropize.api;

  const initializeSettings = (settings && settings.initialize) || {};

  initializeSettings.plugins = settings.plugins;
  initializeSettings.home = resource.getLocation(initializeSettings.home);

  const { debug } = initializeSettings;
  if (angular.isObject(debug)) {
    initializeSettings.debug = { compile: false, log: false, ...debug };
  }
  else if (typeof debug === 'boolean') {
    initializeSettings.debug = { compile: !!debug, log: !!debug };
  }
  else {
    initializeSettings.debug = { compile: false, log: false };
  }

  return {
    $get: function SettingsService() {
      return initializeSettings;
    },
  };
};

settingsProvider.$inject = [
  '$windowProvider',
];

export default settingsProvider;
