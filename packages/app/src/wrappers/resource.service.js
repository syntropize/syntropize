/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const resourceAPI = function resourceAPI(
  $rootScope,
  $log,
  $window,
) {
  const {
    settings,
    resource,
  } = $window.syntropize.api;
  const resourceSettings = (settings && settings.resource) || {};
  return resource.createResource({
    log: $log,
    digest: $rootScope.$applyAsync.bind($rootScope),
    settings: {
      ...resourceSettings,
      plugins: settings.plugins,
    },
  });
};

resourceAPI.$inject = [
  '$rootScope',
  '$log',
  '$window',
];

export default resourceAPI;
