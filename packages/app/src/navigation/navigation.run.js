/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const navigationInstance = function navigationInstance(
  $rootScope,
  NavigationErrorService,
  NavigationLogService,
) {
  const log = NavigationLogService;

  $rootScope.$on('$viewContentLoading', log.view.loading);
  $rootScope.$on('$viewContentLoaded', log.view.loaded);

  $rootScope.$on('$stateChangeStart', log.state.start);
  $rootScope.$on('$stateChangeSuccess', log.state.success);
  $rootScope.$on('$stateChangeError', log.state.error);

  $rootScope.$on('$stateChangeError', NavigationErrorService);
};

navigationInstance.$inject = [
  '$rootScope',
  'NavigationErrorService',
  'NavigationLogService',
];

export default navigationInstance;
