/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const navigationProvider = function navigationProvider(
  $locationProvider,
  $urlRouterProvider,
  // InitSettings,
  AppSettingsProvider,
) {
  // Object.assign(AppSettings, InitSettings);
  const AppSettings = AppSettingsProvider.$get();

  // Routing
  $locationProvider.html5Mode(false);

  $urlRouterProvider.when('', function setDefaultPathOnStartup() {
    // Make sure all URLs end with '/' for ui-router
    return `${AppSettings.home}/` +
      `${AppSettings.view.default && `?view=${AppSettings.view.default}`}`
    ;
  });

  $urlRouterProvider.otherwise(function setDefaultPathOnFailedRoute() {
    return AppSettings.missing ? '/404' : '/error';
  });
};

navigationProvider.$inject = [
  '$locationProvider',
  '$urlRouterProvider',
  // 'InitSettings',
  'AppSettingsProvider',
];

export default navigationProvider;
