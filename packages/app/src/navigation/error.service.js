/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const navigationErrorService = function navigationErrorService(
  AppSettings,
  ErrorToastService,
  $state,
  OpenService,
) {
  const isError = function isStateChangeErrorMessage(error, errorMessageToMatch) {
    return error ? error.message.startsWith(errorMessageToMatch) : false;
  };

  const resourceErrorMsg = 'Resource Resolve Failure';
  const viewErrorMsg = 'View Resolve Failure';
  const connectionFailedMsg = 'Failed to connect';

  const navErrorHandle = function navErrorHandle(
    event, toState, toParams, fromState, fromParams, error,
  ) {
    AppSettings.missing = isError(error, `${resourceErrorMsg}: ${connectionFailedMsg}: Resource does not exist`);

    if (toState.name === 'resource.view') {
      if (fromState.name === 'resource.view' && toParams.location === fromParams.location) {
        if (toState.view === AppSettings.failSafe) {
          ErrorToastService(error);
        }
        else {
          // TODO: Warning toast for view failure
        }
      }
      // toParams.location !== fromParams.location || fromState.name !== 'resource.view'
      else {
        if (isError(error, resourceErrorMsg)) {
          const prefix = resourceErrorMsg;
          const subPrefix = connectionFailedMsg;
          if (
            (isError(error, `${prefix}: ${subPrefix}: Resource is a file`)) ||
            (isError(error, `${prefix}: ${subPrefix}: Updates not avaiable`)) ||
            (isError(error, `${prefix}: ${subPrefix}: Invalid Syntropize resource`))
          ) {
            OpenService(toParams.location);
            // TODO: Warning toasts for each error
          }
          else {
            // (isError(error, '${prefix}: Could not establish location') ||
            // (isError(error, '${prefix}: Could not construct resource') ||
            // (isError(error, `${prefix}: ${subPrefix}: Resource does not exist`)) ||
            // (isError(error, `${prefix}: ${subPrefix}: Access not authorized`)) ||
            // (isError(error, `${prefix}: ${subPrefix}: Invalid resource type`))
            ErrorToastService(error);
          }
        }
        else if (isError(error, viewErrorMsg)) {
          if (toParams.view === AppSettings.view.failSafe) {
            ErrorToastService(error);
          }
          else if (toParams.view === AppSettings.view.default) {
            event.preventDefault();
            $state.go(
              'resource.view',
              { location: toParams.location, view: AppSettings.view.failSafe },
            );
            // TODO: Warning toast for opening file in external appl
          }
          else {
            event.preventDefault();
            // Unfrotunately reloads parent resolves github:angular-ui/ui-router#2274
            $state.go(
              'resource.view',
              { location: toParams.location, view: AppSettings.view.default },
            );
            // TODO: Warning toast for Invalid to Default Redirection
          }
        }
        else {
          ErrorToastService(error);
        }
      }
    }
    else {
      ErrorToastService(error);
    }
  };

  return navErrorHandle;
};

navigationErrorService.$inject = [
  'AppSettings',
  'ErrorToastService',
  '$state',
  'OpenService',
];

export default navigationErrorService;
