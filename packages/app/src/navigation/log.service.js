/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const navigationLogService = function navigationLogService(
  $log,
) {
  const viewLoading = function logOnViewContentLoading(event, viewConfig) {
    $log.debug('$viewContentLoading', viewConfig);
  };

  const viewLoaded = function logOnViewContentLoaded(event) {
    $log.debug('$viewContentLoaded', event);
  };

  const stateStart = function logOnStateChangeStart(
    event, toState, toParams, fromState, fromParams,
  ) {
    $log.debug(
      'Loading State...',
      '\n===\nTo State:', toState, '\nTo Params:', toParams,
      '\n===\nFrom State:', fromState, '\nFrom Params:', fromParams,
    );
  };

  const stateSuccess = function logOnStateChangeSuccess(
    event, toState, toParams, fromState, fromParams,
  ) {
    $log.debug(
      'Successfully Changed State :)',
      '\n===\nTo State:', toState, '\nTo Params:', toParams,
      '\n===\nFrom State:', fromState, '\nFrom Params:', fromParams,
    );
  };

  const stateError = function logOnStateChangeError(
    event, toState, toParams, fromState, fromParams, error,
  ) {
    $log.error(
      'Failed to Change State :(',
      '\n===\nTo State:', toState, '\nTo Params:', toParams,
      '\n===\nFrom State:', fromState, '\nFrom Params:', fromParams,
    );
    if (error) { $log.error(error); }
  };


  const view = Object.freeze({
    loading: viewLoading,
    loaded: viewLoaded,
  });

  const state = Object.freeze({
    start: stateStart,
    success: stateSuccess,
    error: stateError,
  });

  return Object.freeze({
    view,
    state,
  });
};

navigationLogService.$inject = [
  '$log',
];

export default navigationLogService;
