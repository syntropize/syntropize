/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import syntropize from './syntropize.js';

/* eslint-env browser */
const $document = angular.element(document);

$document.ready(function bootstrapAngular() {
  angular.bootstrap(
    $document[0].getElementById('syntropize'),
    [syntropize.name],
    { strictDi: true },
  );
});
