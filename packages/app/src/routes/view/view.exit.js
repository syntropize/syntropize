/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const viewExit = function viewExit(
  Resource,
  Registry,
  $stateParams,
  $injector,
  $log,
) {
  // Perhaps one day we may do a fancy per location view-model state save here;

  const {
    view,
    location,
  } = $stateParams;

  Registry.view.items[view].models.forEach(
    vm => $injector.get(`VM$${vm}`).teardown(Resource),
  );

  $log.debug(`View Exit: ${location} @ ${view}`);
};

viewExit.$inject = [
  'Resource',
  'Registry',
  '$stateParams',
  '$injector',
  '$log',
];

export default viewExit;
