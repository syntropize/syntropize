/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const viewStatusService = function viewStatusService(
  ResourceService,
  Registry,
  $rootScope,
) {
  const viewStatus = {};

  const modelStatus = {};
  const modelWatch = {};


  const clearViewStatus = function clearViewStatus() {
    Object.keys(viewStatus).forEach(view => delete viewStatus[view]);
  };

  const unwatchViewStatus = function unwatchViewStatus() {
    Object.entries(modelWatch).forEach(([model, unwatchRequires]) => {
      unwatchRequires();
      delete modelWatch[model];
    });
  };

  const resetViewStatus = function resetViewStatus() {
    Object.keys(Registry.view.items)
      .forEach((view) => { viewStatus[view] = true; })
    ;
  };


  const watchMatcher = function watchMatcher(requires) {
    return requires.map(require => () => Object.prototype.hasOwnProperty.call(
      ResourceService.resource.directory.files['.'],
      require,
    ));
  };

  const updateModelStatus = function updateModelStatus(model, requireStatus) {
    modelStatus[model] = requireStatus.every(status => status);
  };

  const updateViewStatus = function updateViewStatus(updatedModel) {
    Object.entries(Registry.view.items)
      .forEach(([view, { models }]) => {
        if (models.includes(updatedModel)) {
          viewStatus[view] =
            models.length === 0 || models.every(model => modelStatus[model]);
        }
      })
    ;
  };

  const watchViewStatus = function watchViewStatus() {
    Object.entries(Registry.vm.items)
      .forEach(([model, { requires = [] }]) => {
        modelWatch[model] = $rootScope.$watchGroup(
          watchMatcher(requires),
          (requireStatus) => {
            updateModelStatus(model, requireStatus);
            updateViewStatus(model);
          },
        );
      })
    ;
  };


  $rootScope.$on(
    '$stateChangeSuccess',
    (successEvent, toState) => {
      unwatchViewStatus();
      clearViewStatus();
      if (toState.name === 'resource.view') {
        resetViewStatus();
        watchViewStatus();
      }
    },
  );

  return viewStatus;
};

viewStatusService.$inject = [
  'ResourceService',
  'Registry',
  '$rootScope',
];

export default viewStatusService;
