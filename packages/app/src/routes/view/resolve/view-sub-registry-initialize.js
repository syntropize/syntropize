/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const viewSubRegistryInitialize = function viewSubRegistryInitialize(
  RegistryService,
  Registry,
  /* eslint-disable no-unused-vars */
  ViewRegistryInitialize, // Timing
  ViewFetch, // Timing
  /* eslint-enable no-unused-vars */
  $stateParams,
  $q,
  $log,
) {
  return $q.all(Registry.view.items[$stateParams.view].registers
    .map(item => RegistryService.ensure(item)))
    .then(function logSubRegistryInitializeSuccess(result) {
      $log.debug(`View Resolve Success: Initialize Sub-Registries: ${$stateParams.view}`);
      return result;
    })
    .catch(function noteSubRegistryInitializeFailure(error) {
      error.message = `View Resolve Failure: Initialize Sub-Registries: ${error.message}`;
      throw error;
    })
  ;
};

viewSubRegistryInitialize.$inject = [
  'RegistryService',
  'Registry',
  'ViewRegistryInitialize',
  'ViewFetch',
  '$stateParams',
  '$q',
  '$log',
];

export default viewSubRegistryInitialize;
