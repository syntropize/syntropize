/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const viewModelFetch = function viewModelFetch(
  Registry,
  /* eslint-disable no-unused-vars */
  ViewRegistryInitialize, // Timing
  ViewModelRegistryInitialize, // Timing
  ViewFetch, // Timing
  /* eslint-enable no-unused-vars */
  $stateParams,
  $q,
  $log,
) {
  return $q
    .all(Registry.view.items[$stateParams.view].models
      .map(viewModel => Registry.vm.load(viewModel)))
    .then(function logVMFetchSuccess(result) {
      $log.debug(`View Resolve Success: Fetch View Models: ${$stateParams.view}`);
      return result;
    })
    .catch(function noteVMFetchFailure(error) {
      error.message = `View Resolve Failure: Fetch View Models: ${error.message}`;
      throw error;
    })
  ;
};

viewModelFetch.$inject = [
  'Registry',
  'ViewRegistryInitialize',
  'ViewModelRegistryInitialize',
  'ViewFetch',
  '$stateParams',
  '$q',
  '$log',
];

export default viewModelFetch;
