/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const viewModelDetect = function viewModelDetect(
  Registry,
  Resource,
  /* eslint-disable-next-line no-unused-vars */
  ViewModelFetch, // Timing
  $injector,
  $stateParams,
  $q,
  $log,
) {
  return $q
    .all(Registry.view.items[$stateParams.view].models
      .map(vm => ($injector.get(`VM$${vm}`).detect || $q.when)(Resource)))
    .then(function logViewDetectSuccess(result) {
      $log.debug(`View Resolve Success: Detect View Model: ${$stateParams.view}`);
      return result;
    })
    .catch(function noteViewDetectFailure(error) {
      error.message = `View Resolve Failure: Detect View Model: ${error.message}`;
      throw error;
    })
  ;
};

viewModelDetect.$inject = [
  'Registry',
  'Resource',
  'ViewModelFetch',
  '$injector',
  '$stateParams',
  '$q',
  '$log',
];

export default viewModelDetect;
