/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const viewFetch = function viewFetch(
  Registry,
  /* eslint-disable-next-line no-unused-vars */
  ViewRegistryInitialize, // Timing
  $stateParams,
  $log,
) {
  return Registry.view.load($stateParams.view)
    .then(function logViewFetchSuccess(result) {
      $log.debug(`View Resolve Success: Fetch View: ${$stateParams.view}`);
      return result;
    })
    .catch(function noteViewFetchFailure(error) {
      error.message = `View Resolve Failure: Fetch View: ${error.message}`;
      throw error;
    })
  ;
};

viewFetch.$inject = [
  'Registry',
  'ViewRegistryInitialize',
  '$stateParams',
  '$log',
];

export default viewFetch;
