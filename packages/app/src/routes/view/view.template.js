/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const viewTemplateProvider = function viewTemplateProvider(
  $stateParams,
) {
  return `
<div flex layout="row" layout-fill>
  <view-${$stateParams.view} class="view-shell" flex layout="column" layout-fill></view-${$stateParams.view}>
  <panel class="md-whiteframe-z2"></panel>
</div>`
  ;
};

viewTemplateProvider.$inject = [
  '$stateParams',
];

export default viewTemplateProvider;
