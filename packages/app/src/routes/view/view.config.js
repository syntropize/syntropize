/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import viewTemplateProvider from './view.template.js';


import viewFetch      from './resolve/view-fetch.js';
import viewModelFetch from './resolve/vm-fetch.js';

import viewSubRegistryInitialize from './resolve/view-sub-registry-initialize.js';

import viewModelDetect from './resolve/vm-detect.js';

import viewEnter from './view.enter.js';
import viewExit  from './view.exit.js';

const viewProvider = function cardViewProvider(
  $stateProvider,
) {
  $stateProvider
    .state('resource.view', {
      url: '/?view',
      templateProvider: viewTemplateProvider,
      resolve: {
        ViewFetch: viewFetch,
        ViewModelFetch: viewModelFetch,
        ViewSubRegistryInitialize: viewSubRegistryInitialize,
        ViewModelDetect: viewModelDetect,
      },
      onEnter: viewEnter,
      onExit: viewExit,
    });
};

viewProvider.$inject = [
  '$stateProvider',
];

export default viewProvider;
