/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const viewEnter = function viewEnter(
  Resource,
  ResourceService,
  Registry,
  $stateParams,
  $injector,
  $rootScope,
  $log,
) {
  // Perhaps one day we may do a fancy per location view-model state retrieval here;

  const {
    view,
    location,
  } = $stateParams;

  $log.debug(`View Enter: ${location} @ ${view}`);

  ResourceService.resource = Resource;

  const vmServices = Registry.view.items[view].models.map(
    vm => $injector.get(`VM$${vm}`),
  );

  vmServices.forEach(vmService => vmService.setup());

  if (Resource.socket.ready) {
    vmServices.forEach(vmService => vmService.initialize(Resource));
  }
  else {
    Resource.board.event.on('ready', function vmInitializeOnReady() {
      vmServices.forEach(vmService => vmService.initialize(Resource));
    });
  }
};

viewEnter.$inject = [
  'Resource',
  'ResourceService',
  'Registry',
  '$stateParams',
  '$injector',
  '$rootScope',
  '$log',
];

export default viewEnter;
