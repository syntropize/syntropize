/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import errorTemplate    from './error.html';
import _404Template     from './404.html';

const mainProvider = function mainProvider(
  $stateProvider,
) {
  $stateProvider
    .state('error', {
      url: '/error',
      template: errorTemplate,
    })
    .state('404', {
      url: '/404',
      template: _404Template,
    })
  ;
};

mainProvider.$inject = [
  '$stateProvider',
];

export default mainProvider;
