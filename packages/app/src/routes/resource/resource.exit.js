/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const resourceExit = function resourceExit(
  $stateParams,
  $log,
) {
  // Perhaps one day we may do a fancy per location model save here

  const { location } = $stateParams;
  $log.debug(`Resource Exit: ${location}`);
};

resourceExit.$inject = [
  '$stateParams',
  '$log',
];

export default resourceExit;
