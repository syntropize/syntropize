/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const resourceSetup = function resourceSetup(
  $stateParams,
  ResourceAPI,
) {
  const { location } = $stateParams;

  return ResourceAPI.initialize(location)
    .then(function resourceInitializationSuccess() {
      return ResourceAPI.resource[location];
    })
    .catch(function noteResourceInitializationFailure(error) {
      error.message = `Resource Resolve Failure: ${error.message}`;
      throw error;
    })
  ;
};

resourceSetup.$inject = [
  '$stateParams',
  'ResourceAPI',
];

export default resourceSetup;
