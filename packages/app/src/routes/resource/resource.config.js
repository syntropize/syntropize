/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import resourceSetup from './resolve/resource.setup.js';
import viewRegistryInitialize from './resolve/view-registry-initialize.js';
import viewModelRegistryInitialize from './resolve/vm-registry-initialize.js';

import resourceEnter from './resource.enter.js';
import resourceExit  from './resource.exit.js';

const resourceProvider = function ResourceProvider(
  $stateProvider,
  $urlMatcherFactoryProvider,
) {
  function valToString(val) {
    return (val !== null) ? val.toString() : val;
  }

  $urlMatcherFactoryProvider.type('nonURIEncoded', {
    performsAllNeededUriEncoding: true,
    encode: valToString,
    decode: valToString,
    is: function testUrlForProtocolPattern(val) {
      return this.pattern.test(val);
    },
    pattern: /[a-zA-Z]{2,}:.+/,
  });

  $stateProvider
    .state('resource', {
      url: '/{location:nonURIEncoded}',
      abstract: true,
      template: '<div ui-view flex layout="column" layout-fill></div>',
      // templateUrl: 'app/resource/resource.html', // Test
      // controller: 'ResourceController as resource', // Test
      resolve: {
        Resource: resourceSetup,
        ViewRegistryInitialize: viewRegistryInitialize,
        ViewModelRegistryInitialize: viewModelRegistryInitialize,
      },
      onEnter: resourceEnter,
      onExit: resourceExit,
    })
  ;
};

resourceProvider.$inject = [
  '$stateProvider',
  '$urlMatcherFactoryProvider',
];

export default resourceProvider;
