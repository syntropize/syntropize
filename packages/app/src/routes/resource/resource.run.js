/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const resourceInstance = function resourceInstance(
  $rootScope,
  ResourceAPI,
) {
  $rootScope.$on(
    '$stateChangeSuccess',
    function cleanOnStateChangeSuccess(
      event, toState, toParams, fromState, fromParams,
    ) {
      if (fromState.name === 'resource.view') {
        // Not on refresh
        if (toParams.location !== fromParams.location) {
          ResourceAPI.terminate(fromParams.location);
        }
      }
    },
  );
};

resourceInstance.$inject = [
  '$rootScope',
  'ResourceAPI',
];

export default resourceInstance;
