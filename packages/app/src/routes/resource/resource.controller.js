/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const resourceController = function resourceController(
  Resource,
  ResourceAPI,
  Registry,
  $stateParams,
  // $scope,
) {
  // ========================== //
  // Controller Initializations //
  // ========================== //

  /**
   * Location of the board
   * @type {Object}
   */
  this.location = $stateParams.location;

  this.directory = Resource.directory;

  // $scope.$apply(function updateBoard() {
  //   this.board = Resource.board;
  // });
  this.board = ResourceAPI.resource[this.location].board;

  /**
   * Loads the schema which specifies format common to all cards
   * @type {Object}
   */
  // this.cardRegistry = Registry.card;

  /**
   * Loads the schema which specifies format common to all cards
   * @type {Object}
   */
  // this.compileRegistry = Registry.compile;

  /**
   * Loads the schema which specifies format common to all cards
   * @type {Object}
   */
  // this.masterSchema = CardSchemaService;

  /**
   * Whether cards metadata has been loaded
   * @type {boolean}
   */
  // this.cardsMetadataStatus = BoardSetup.cardsMetadataStatus;

  /**
   * Determines the Board's Primary Display View
   * @type {string}
   */
  // this.viewMain = $state.current.name.split('.')[1];

  /**
   * Implements the Switch between Board's Primary Display Views
   * Switches the UI-Router's State to access the view
   * @param  {string} viewType The View to be switched to
   * @return {[type]}          [description]
   */
  // this.switchView = function switchView(viewType) {
  //   switch(viewType) {
  //     case 'cards':
  //       if (resource.cardsMetadataStatus) {
  //         $state.go(
  //           'board.cards',
  //           {
  //             location: $stateParams.location,
  //             platform: $stateParams.platform,
  //           },
  //         );
  //       }
  //       else {
  //         $log.warn('Cannot Change View');
  //       }
  //       break;
  //     // case 'files' :
  //     //   $state.go(
  //     //     'board.files',
  //     //     {
  //     //       location: $stateParams.location,
  //     //       platform:$stateParams.platform,
  //     //     },
  //     //   );
  //     //   break;
  //     default:
  //       $state.go(
  //         'board.dummy',
  //         {
  //           location: $stateParams.location,
  //           platform: $stateParams.platform,
  //         },
  //       );
  //       break;
  //   }
  // };

  /**
   * The board object at the specific location
   * @type {Object}
   */
  // this.board = BoardService.boards[this.location];

  /**
   * Loads the menu for the board
   * The function will eventually load commands based on the
   * privelidge level of a given user once that is implemented
   * @type {Object}
   */
  // this.menu = MenuService.menu[this.location];

  /**
   * Navigation
   */
  // this.nav = {
  //   location:resource.location,
  //   launchIcon:'navigation:refresh',
  //   launchMessage:'Refresh'
  // };

  // $scope.$watch('resource.nav.location', function changeLaunchIconOnAddress(newLoc) {
  //   console.log(newLoc);
  //   if (newLoc === resource.location) {
  //     resource.nav.launchIcon = 'navigation:refresh';
  //     resource.nav.launchMessage = 'Refresh';
  //   }
  //   else {
  //     resource.nav.launchIcon = 'content:forward';
  //     resource.nav.launchMessage = 'Go';
  //   }
  // });

  // this.nav.go = function go(view,params) {
  //   console.log(view.params);
  //   if (params === undefined) {params = {};}
  //   $state.go('board.'+ view , params, { reload: true });
  // };

  // this.nav.goChild = function goChild(child) {
  //   $state.go(
  //     'board.'+ resource.viewMain,
  //     {
  //       location: path.join($stateParams.location,child),
  //       platform: $stateParams.platform
  //     },
  //   );
  // };

  // this.nav.refresh = function refresh() {
  //   $state.transitionTo(
  //     'board.'+ resource.viewMain,
  //     {
  //       location: $stateParams.location,
  //       platform: $stateParams.platform
  //     },
  //     {reload: true},
  //   );
  // };

  // this.nav.change = function change() {
  //   console.log('VALUE CHANGED');
  // };

  // return Object.freeze({
  //   location,
  //   directory,
  //   socket,
  //   board,
  // });
};

resourceController.$inject = [
  'Resource',
  'ResourceAPI',
  'Registry',
  '$stateParams',
  // '$scope',
];

export default resourceController;
