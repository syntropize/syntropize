/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { readFileSync } from 'fs';

import config from '@syntropize/steamroll';
import ng     from '@syntropize/rollup-preset-angularjs';

import htmlEntry from '@syntropize/rollup-plugin-html-entry';

const pkg = JSON.parse(readFileSync('./package.json'));

const outputDir = 'lib';
const assets = 'assets';

const options = {
  input: pkg.entry,
  pluginOptions: {
    modify: {
      'obs_opts = {': 'var obs_opts = {',
      'obs = new MutationObserver': 'var obs = new MutationObserver',
    },
    url: {
      limit: 0,
      destDir: `${outputDir}/${assets}`,
      publicPath: `${assets}/`,
      include: [/\.svg$/, /\.gif$/, /\.png$/, /\.jpe?g$/],
    },
    htmlString: {
      include: '../../**/*.html',
      exclude: '**/*.htm',
    },
    styles: {
      url: {
        inline: false,
        publicPath: `${assets}/`,
      },
    },
  },
  plugins: [
    htmlEntry({
      input: pkg.source,
      assetDir: assets,
    }),
  ],
  output: {
    dir: outputDir,
    assetFileNames: '[name]-[hash][extname]',
    entryFileNames: '[name]-[hash].min.js',
    manualChunks(id) {
      if (id.includes('node_modules')) {
        return 'vendor';
      }
      return undefined;
    },
  },
};

export default config(ng, options);
