# Unified Markdown API

Unified Markdown API provides a comprehensive markdown system for Syntropize.

## Description

Unified Markdown API is a wrapper around the (old) Remark markdown parser and various Unified project components.

We use the old Remark parser in order to support Tufte markdown plugins, which have not been upgraded to work with latest parser.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
