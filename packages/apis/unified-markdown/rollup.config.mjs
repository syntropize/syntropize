/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { readFileSync } from 'fs';
import config from '@syntropize/steamroll';

const pkg = JSON.parse(readFileSync('./package.json'));

const circularErrorPackages = [
  // 'mdast-util-to-hast',
  'hast-util-to-html',
  'hast-util-select',
];

function onwarn(warning, rollupWarn) {
  // supress circular dependency error for known modules
  if (warning.code === 'CIRCULAR_DEPENDENCY') {
    if (circularErrorPackages.some(p => !warning.importer.includes(p))) {
      return;
    }
  }
  // supress undefined this error for known modules
  if (warning.code === 'THIS_IS_UNDEFINED') {
    if ((/(?:\/|\\)remark-code-extra(?:\/|\\)/).test(warning.id)) {
      return;
    }
  }
  rollupWarn(warning);
}

const options = {
  input: pkg.source,
  onwarn,
  output: {
    file: pkg.main,
  },
};

export default config(options);
