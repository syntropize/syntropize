/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
export default function attacher(heading) {
  return function transformer(root) {
    const first = root.children && root.children[0];
    if (!(first && first.type === 'heading') && typeof heading === 'string') {
      root.children.unshift({
        type: 'heading',
        depth: 1,
        children: [{ type: 'text', value: heading }],
      });
    }
  };
}
