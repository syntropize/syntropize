/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import has from 'hast-util-has-property';
import visit from 'unist-util-visit';

export default function attacher(modifiers = {}) {
  return function transformer(tree) {
    const modifierList = new Map();
    Object.keys(modifiers).forEach(function filterModifierList(modifier) {
      if (typeof modifiers[modifier] === 'function') {
        modifierList.set(modifier, modifiers[modifier]);
      }
    });

    if (modifierList.size > 0) {
      visit(tree, 'element', (node) => {
        modifierList.forEach((modifier, name) => {
          if (has(node, name)) {
            // eslint-disable-next-line no-param-reassign
            node.properties[name] = modifier(node.properties[name], node);
          }
        });
      });
    }
  };
}
