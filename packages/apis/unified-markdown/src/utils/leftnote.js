/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */

// A very hacky left note implementation
import visit from 'unist-util-visit';
import uniqid from 'uniqid';

const labelSymbol = '&#8857';

const preLeftnote = () => {
  const input = uniqid('ln-');
  return `<label for=${input} class="margin-toggle">${labelSymbol}</label><input type="checkbox" id="${input}" class="margin-toggle"/>`;
};

export default function attacher() {
  return function transformer(tree) {
    visit(tree, 'html', (node) => {
      if (
        node.value &&
        (typeof node.value === 'string') &&
        node.value.startsWith('<span') &&
        node.value.indexOf('class="leftnote') !== -1) {
        // eslint-disable-next-line no-param-reassign
        node.value = `${preLeftnote()}${node.value}`;
      }
    });
  };
}
