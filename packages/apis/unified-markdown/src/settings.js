/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import textrBase from 'typographic-base';
// import 'highlight.js/styles/github.css';

export default {
  syntaxes: [
    'remark',
    'retext',
    'rehype',
  ],
  plugins: {
    remark: [
      'parse',
      'footnotes',
      'frontmatter',
      'codeExtra',
      'removeFrontmatter',
      'addHeading',
      'headingShift',
      'attr',
      'align',
      'behead',
      // 'bracketedSpans',
      'emoji',
      'subSuper',
      'mermaid',
      // 'graphviz',
      'abbr',
      'math',
      'externalLinks',
      'textr',

      'tufteSidenotes',
      // 'tufteWrapInSection',
      'tufteFigureParser',
      'tufteFigureTransformer',
      'leftnote',

      'rehype',
    ],
    retext: [

    ],
    rehype: [
      'stringify',
      'raw',
      'highlight',
      'katex',
      // 'wrap',
      // 'autolinkHeadings',
      // 'document',
      // 'modifySrc',
      'modifyProperty',
      // 'urls',
    ],
  },
  config: {
    remark: {
      parse: {
        commonmark: true,
        footnotes: true,
      },
      footnotes: {
        inlineNotes: true,
      },
      frontmatter: [
        'yaml',
        'toml',
      ],
      align: {
        left: 'align-left',
        center: 'align-center',
        right: 'align-right',
      },
      codeExtra: {
        transform: {},
      },
      textr: {
        plugins: [textrBase],
      },
      rehype: {
        allowDangerousHTML: true,
      },
      headingShift: false,
    },
    retext: {},
    rehype: {},
  },
};
