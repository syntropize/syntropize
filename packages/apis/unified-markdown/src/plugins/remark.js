/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */

/* eslint-disable no-multi-spaces */
export { default as parse }             from 'remark-parse';
export { default as footnotes }         from 'remark-footnotes';
export { default as frontmatter }       from 'remark-frontmatter';
export { default as headingShift }      from 'remark-heading-shift';
export { default as attr }              from 'remark-attr';
export { default as align }             from 'remark-align';
export { default as behead }            from 'remark-behead';
export { default as bracketedSpans }    from 'remark-bracketed-spans';
export { default as codeExtra }         from 'remark-code-extra';
// export { default as containers }        from 'remark-containers'; //Buggy
// export { default as emoji }             from 'remark-emoji';
export { default as subSuper }          from 'remark-sub-super';
// export { default as mermaid }           from 'remark-mermaid';
// export { default as graphviz }          from 'remark-graphviz'; // Buggy with JSPM
export { default as abbr }              from 'remark-abbr';
export { default as math }              from 'remark-math';
export { default as externalLinks }     from 'remark-external-links';
// export { default as slug }              from 'remark-slug'; // Buggy with JSPM
export { default as textr }             from 'remark-textr';

export { default as removeFrontmatter } from '../utils/remove-frontmatter.js';
export { default as addHeading }        from '../utils/add-heading.js';
export { default as leftnote }          from '../utils/leftnote.js';

export { default as tufteSidenotes }          from '@tufte-markdown/remark-sidenotes';
export { default as tufteWrapInSection }      from '@tufte-markdown/remark-wrap-in-section';
export { default as tufteFigureParser }       from '@tufte-markdown/remark-figure-parser';
export { default as tufteFigureTransformer }  from '@tufte-markdown/remark-figure-transformer';

export { default as rehype } from 'remark-rehype';
