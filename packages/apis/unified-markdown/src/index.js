/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import unified from 'unified';
import _merge from 'lodash.merge';

import * as remark from './plugins/remark.js';
import * as retext from './plugins/retext.js';
import * as rehype from './plugins/rehype.js';

import defaults from './settings.js';

const plugins = { remark, retext, rehype };

export default (config = {}) => {
  const settings = {};
  _merge(settings, defaults);
  _merge(settings.config, config);

  return settings.syntaxes.reduce(
    (processor, syntax) => settings.plugins[syntax].reduce(
      (chain, plugin) => {
        if (plugins[syntax][plugin]) {
          return chain.use(plugins[syntax][plugin], settings.config[syntax][plugin]);
        }
        else {
          return chain;
        }
      },
      processor,
    ),
    unified(),
  );
};
