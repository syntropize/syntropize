# Resource API

**Resource API** allows Syntropize to orchestrate the interaction between presentation media (front-end) and data resources (back-end).

## Description

Based on the URL specified, the Resource API loads the requisite origin plugin and establishes a connection to the given origin. To interact with data and events at that URL, it loads various schema plugins that provide the presentation media with necessary data models to generate requested view.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
