/* eslint-disable no-param-reassign */
const preXCChecks = function preXCChecks(from, to) {
  if (!from) {
    from = [''];
  }
  else if (typeof from === 'string') {
    from = [from];
  }
  else if (!Array.isArray(from)) {
    throw new TypeError('Source is not a string or array');
  }
  else if (!from.every(item => (
    typeof item === 'string' || (Array.isArray(item) && (!item.every(sub => (
      typeof sub === 'string'))))
  ))) {
    throw new TypeError('Source are not an array/array^2 of strings');
  }

  if (!to) {
    to = from.slice();
  }
  else if (typeof to === 'string') {
    to = [to];
  }
  else if (!Array.isArray(to)) {
    throw new TypeError('Destination is not a string or array');
  }
  else if (!to.every(item => (
    typeof item === 'string' || (Array.isArray(item) && (!item.every(sub => (
      typeof sub === 'string'))))
  ))) {
    throw new TypeError('Destinations are not an array/array^2 of strings');
  }

  if (to.length !== from.length) {
    throw new RangeError('Mismatched number of Source and Destination items');
  }

  return from.map((src, i) => [src, to[i]]);
};

export default preXCChecks;

