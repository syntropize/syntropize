const errorPrefix = '';

function checkLocation(fromLocation, toLocation) {
  switch (typeof fromLocation) {
    case 'string':
      if (fromLocation === toLocation) {
        throw new Error(`${errorPrefix} Identical source and destination`);
      }
      break;
    case 'object':
      if (fromLocation.path() === toLocation.path) {
        throw new Error(`${errorPrefix} Identical source and destination`);
      }
      break;
    default:
      throw new Error(`${errorPrefix} Invalid Source`);
  }
}

export default checkLocation;
