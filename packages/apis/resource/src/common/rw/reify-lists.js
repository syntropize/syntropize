/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
function reifyList(items) {
  if (!items || items === '.') {
    // directory at location
    return Object.freeze(['']);
  }
  else if (typeof item === 'string') {
    return Object.freeze([items]);
  }
  else if (Array.isArray(items)) {
    if (items.length === 0) {
      throw new Error('Invalid parameters: empty Array');
    }
    else if (!items.every(item => (
      typeof item === 'string' || (Array.isArray(item) && (!item.every(sub => (
        typeof sub === 'string'))))
    ))) {
      throw new TypeError('Invalid parameters: not an Array/Array^2 of strings');
    }
    else {
      return Object.freeze(items);
    }
  }
  else {
    throw new Error('Invalid parameters: not a string or Array');
  }
}


function reifyXCList(from, to) {
  const list = {};

  try {
    list.from = reifyList(from);
  }
  catch (error) {
    error.message = `Invalid source: ${error.message}`;
    throw error;
  }

  if (!to) {
    list.to = list.from;
  }
  else {
    try {
      list.to = reifyList(to);
    }
    catch (error) {
      error.message = `Invalid destination: ${error.message}`;
      throw error;
    }
  }

  if (list.to.length !== list.from.length) {
    throw new RangeError('Mismatched number of source and destination items');
  }

  return Object.freeze(list);
}

export default Object.freeze({
  list: reifyList,
  xcList: reifyXCList,
});
