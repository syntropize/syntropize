/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
function arrifyPath(...args) {
  return [
    ...(args.slice(0, -1).filter(arg => arg !== '' && arg !== '.')),
    // last argument can be a empty string (folder case)
    ...(args.slice(-1).filter(arg => arg !== '.')),
  ];
}

export default arrifyPath;
