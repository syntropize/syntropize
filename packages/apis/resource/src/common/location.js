/*! Copyright (c) 2016-2017, Rahul Gupta and ProtoSyntropize Developers
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import libUrl from 'url';
import libPath from 'path';

const parse = function parse(address) {
  const urlObj = libUrl.parse(address);

  const origin = libUrl.format({
    protocol: urlObj.protocol,
    slashes: urlObj.slashes,
    hostname: urlObj.hostname,
    port: urlObj.port,
  });

  return Object.freeze({
    origin,
    scheme: urlObj.protocol.split(':', 1)[0],
    $pathname: decodeURI(urlObj.pathname),
    $root: origin,
    ...urlObj,
  });
};

const resolve = function resolvePath(root, ...paths) {
  return libUrl.resolve(root, libPath.posix.join(...paths));
};

export default {
  parse,
  resolve,
};
