/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import settings from '../settings.js';

const cache = new Map();

async function fetchSchema(model) {
  // This function will be made dynamic at a later date
  if (cache.has(model)) {
    return cache.get(model);
  }
  try {
    const pluginSettings = settings.plugins;
    const location = `${pluginSettings.location}/${pluginSettings.prefix}-schema-${model}`;
    const manifest = `${location}/${pluginSettings.manifest.file}`;
    const pkg = await import(manifest/* rollupUncomment , { assert:{ type:'json' } } */);
    const mainEntry = pkg.default[pluginSettings.manifest.map];
    const main = typeof mainEntry === 'string' ?
      mainEntry : mainEntry['./'];
    const schema = await import(`${location}/${main}`);
    cache.set(model, schema.default);
    return schema.default;
    // if (!origin[model]) throw new Error();
    // return origin[model];
  }
  catch (error) {
    error.message = `Unable to fetch Schema or Invalid Schema "${model}": ${error.message}`;
    throw error;
  }
}

export default fetchSchema;
