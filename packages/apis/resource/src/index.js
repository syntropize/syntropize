/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { resource, initialize, terminate } from './resource.js';
import { get as getLocation } from './location.js';
import { set as setDigest } from './digest.js';
import { set as setLog } from './log.js';
import { update as updateSettings } from './settings.js';
import { load as loadCredentials } from './credentials.js';

const createResource = function createResource(config = {}) {
  const reconfigure = function reconfigureResource({
    log,
    digest,
    settings,
  }) {
    if (digest) {
      setDigest(digest);
    }
    if (log) {
      setLog(log);
    }
    if (settings) {
      updateSettings(settings);
      if (settings.credentials_file_location) {
        loadCredentials(settings.credentials_file_location);
      }
    }
  };

  reconfigure(config);

  // In the future resource will not be a singleton
  return Object.freeze({
    resource,
    initialize,
    terminate,
    reconfigure,
  });
};

export {
  createResource,
  getLocation,
};
