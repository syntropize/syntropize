/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import log from './log.js';

import resourceFactory from './factory/resource.js';
import locationFactory from './origin/location.js';

import settings from './settings.js';

const resource = {};

const initialize = async function initializeResource(address) {
  /** Parse Location:
   *  !Do this first important because different protocol schemes have
   *  different url requirements
   */
  let location;
  try {
    location = await locationFactory(address, settings.uri);
  }
  catch (error) {
    // log.error(`Could not establish location ${error}`);
    // throw location;
    error.message = `Could not establish location: ${error.message}`;
    throw error;
  }

  const temp = resource[address];

  try {
    resource[address] = await resourceFactory(location);
  }
  catch (error) {
    error.message = `Could not construct resource: ${error.message}`;
    throw error;
  }

  try {
    await resource[address].socket.connect();
    log.debug(`Connected to ${address}`, resource[address]);
    if (temp) { temp.socket.disconnect(); }
  }
  catch (error) {
    if (temp) { resource[address] = temp; }
    else { delete resource[address]; }
    error.message = `Failed to connect: ${error.message}`;
    throw error;
  }
};

const terminate = async function terminateResource(address) {
  if (Object.prototype.hasOwnProperty.call(resource, address)) {
    try {
      await resource[address].socket.disconnect();
    }
    catch (error) {
      log.debug(`Unable to Disconnect ${address}`);
    }
    delete resource[address];
  }
};

export default resource;
export { initialize, terminate, resource };
