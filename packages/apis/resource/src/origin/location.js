/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import common from '../common/location.js';
import fetchOrigin from './fetch.js';

async function fetchLocation(address) {
  const scheme = address.split(':', 1)[0].toLowerCase();
  try {
    return (await fetchOrigin(scheme)).location;
  }
  catch (error) {
    error.message = `Undefined Scheme "${scheme}"
${error.message}`;
    throw error;
  }
}

async function locationFactory(address, options) {
  const location = await fetchLocation(address);

  const parsedLocation = location.parse(address, options);

  function resolvePath(...paths) {
    return location.resolve(
      parsedLocation.$root, parsedLocation.$pathname, ...paths,
    );
  }

  function resolveAddress(...paths) {
    return common.resolve(
      parsedLocation.origin, parsedLocation.pathname, ...paths,
    );
  }

  return Object.freeze({
    url: parsedLocation,
    path: resolvePath,
    address: resolveAddress,
  });
}

export default locationFactory;
