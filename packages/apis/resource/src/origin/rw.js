/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import utils from '../common/rw/index.js';
import fetchOrigin from './fetch.js';

async function fetchRW(location) {
  const { scheme } = location.url;
  return (await fetchOrigin(scheme)).rw;
}

function makeFlatArray(item) {
  return Array.isArray(item) ? item.flat() : [item];
}

const traps = {
  async obj(o, location) {
    return (await fetchRW(location))[o];
  },
  async args1(fn, location, item, ...args) {
    const path = location.path(...makeFlatArray(item));
    return (await fetchRW(location))[fn](path, ...args);
  },
  async args2(fn, from, fromItem, to, toItem, ...args) {
    const fromScheme = from.url.scheme;
    const toScheme = to.url.scheme;

    // A hacky intercept to ensure suitable methods are used for cross-platform copy
    if (fromScheme === 'file' && toScheme === 'file') {
      const fromPath = from.path(...makeFlatArray(fromItem));
      const toPath = to.path(...makeFlatArray(toItem));
      return (await fetchOrigin('file')).rw[fn](fromPath, toPath, ...args);
    }
    else if (
      (fromScheme === 'file' && (toScheme === 'http' || toScheme === 'https')) ||
      ((fromScheme === 'http' || fromScheme === 'https') && toScheme === 'file')
    ) {
      const fromAddress = from.address(...makeFlatArray(fromItem));
      const toAddress = to.address(...makeFlatArray(toItem));
      const writeOptions = {
        withAcl: false,
        withMeta: false,
      };
      const options = typeof args[0] === 'object' ?
        { ...args[0], ...writeOptions } : writeOptions;
      return (await fetchOrigin('https')).rw[fn](fromAddress, toAddress, options, ...args.slice(1));
    }
    else if (
      (fromScheme === 'http' || fromScheme === 'https') &&
      (toScheme === 'http' || toScheme === 'https')
    ) {
      const fromAddress = from.address(...makeFlatArray(fromItem));
      const toAddress = to.address(...makeFlatArray(toItem));
      return (await fetchOrigin('https')).rw[fn](fromAddress, toAddress, ...args);
    }
    else {
      throw new Error(`${fn} between specified locations has not been enabled`);
    }
  },
};

const methodTable = {
  obj: ['path'],
  args1: ['read', 'readDir', 'write', 'createDir', 'link', 'remove', 'delete'],
  args2: ['rename', 'move', 'copy'],
};

const rw = { utils };
/* Map traps to method names */
Object.entries(methodTable).forEach(([actual, methods]) => {
  methods.forEach((method) => {
    rw[method] = async function interceptMethod(...args) {
      return traps[actual](method, ...args);
    };
  });
});

export default rw;
