/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import fetchOrigin from './fetch.js';

async function fetchIO(location) {
  const { scheme } = location.url;
  try {
    return (await fetchOrigin(scheme)).io;
  }
  catch (error) {
    error.message = `Resource Error: Unable to create IO object: ${error.message}`;
    return error;
  }
}

async function ioFactory(location, options) {
  const io = await fetchIO(location);
  return io(location.path(), options);
}

export default ioFactory;
