/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import settings from '../settings.js';

const cache = new Map();

const originMap = {
  https: 'solid',
  file: 'file',
};

async function fetchOrigin(scheme) {
  // This function will be made dynamic at a later date
  if (cache.has(scheme)) {
    return cache.get(scheme);
  }
  try {
    const pluginSettings = settings.plugins;
    const location = `${pluginSettings.location}/${pluginSettings.prefix}-origin-${originMap[scheme]}`;
    const manifest = `${location}/${pluginSettings.manifest.file}`;
    const pkg = await import(manifest/* rollupUncomment , { assert: { type:'json' } } */);
    const mainEntry = pkg.default[pluginSettings.manifest.map];
    const main = typeof mainEntry === 'string' ?
      mainEntry : mainEntry['./'];
    const origin = await import(`${location}/${main}`);
    return origin.default;
    // if (!origin[scheme]) throw new Error();
    // return origin[scheme];
  }
  catch (error) {
    error.message = `Unable to fetch Origin or Invalid Origin "${scheme}": ${error.message}`;
    throw error;
  }
}

export default fetchOrigin;
