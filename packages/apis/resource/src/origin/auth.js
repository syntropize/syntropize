/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import fetchOrigin from './fetch.js';

async function authFactory(location) {
  const { scheme } = location.url;
  try {
    return (await fetchOrigin(scheme)).auth;
  }
  catch (error) {
    error.message = `No Authorization Function Defined: ${error.message}`;
    return error;
  }
}

export default authFactory;
