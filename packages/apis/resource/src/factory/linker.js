/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import settings from '../settings.js';
// import log from 'src/log';

const linker = function schemaLinker(reader, io, events = settings.linker.events) {
  events.forEach((event) => {
    io.on(event, function socketCb(...args) {
      return reader.on[event](...args);
    });
  });

  // socket.on('raw', function (event, path, details) {
  //   log.debug('Raw:', event, path, details);
  // });
};

export default linker;
