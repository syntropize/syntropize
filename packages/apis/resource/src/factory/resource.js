/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import settings from '../settings.js';
import log from '../log.js';
import { get as getCredentials } from '../credentials.js';
import fetchSchema from '../schema/fetch.js';
import { io as ioFactory, rw, auth } from '../origin/index.js';
import socketFactory  from './socket.js';
import schemaFactory  from './schema.js';


async function resourceFactory(location, schemaList /* = Object.keys(schemata) */) {
  const resource = {};
  const options = {};

  resource.location = location;

  resource.auth = await auth(location);

  try {
    await resource.auth.login(getCredentials(location.url.scheme) || {});
  }
  catch (error) {
    error.message = `Unable to log in: ${error.message}`;
    error.url = location.address();
    log.debug(error);
    options.readOnly = true;
  }

  const io = await ioFactory(location);

  resource.socket = socketFactory(location, io);

  const mySchemaList = (Array.isArray(schemaList) && schemaList) || settings.schema.list;

  // TODO: Do you have to create all schema right now?
  async function makeSchema(schemaName) {
    try {
      const schema = await fetchSchema(schemaName);
      resource[schemaName] = schemaFactory(schemaName, schema, location, rw, io, options);
    }
    catch (error) {
      if (!error.message.startsWith('Unable to fetch Schema or Invalid Schema')) {
        throw error;
      }
    }
  }

  try {
    await Promise.all(mySchemaList.map(makeSchema));
  }
  catch (error) {
    error.message = `Failed to create Schemas
${error.message}`;
    throw error;
  }

  return resource;
}

export default resourceFactory;
