/*! Copyright (c) 2016-2017, Rahul Gupta and ProtoSyntropize Developers
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import origin from '../origin/index.js';

const ioFactory = function ioFactory(location, options) {
  const { scheme } = location.url;

  try {
    return origin[scheme].io(location.path(), options);
  }
  catch (error) {
    error.message = `Resource Error: Unable to create IO object: ${error.message}`;
    return error;
  }
};

export default ioFactory;
