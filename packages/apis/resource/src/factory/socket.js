/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import digest from '../digest.js';

const socketFactory = function socketFactory(location, io) {
  let status;
  let ready;
  let lastError;

  let disconnect = function noop() {
    return Promise.resolve();
  };

  const setStatus = function setStatus(value) {
    digest(function setSocketStatus() {
      status = value;
    });
  };


  const addEventHandlers = function addEventHandlers(eventList) {
    Object.keys(eventList).forEach(evt => io.on(evt, eventList[evt]));
  };

  const removeEventHandlers = function removeEventHandlers(eventList) {
    Object.keys(eventList).forEach(evt => io.removeListener(evt, eventList[evt]));
  };


  const connectHandlers = {
    ready: function rejectResourceOnDisconnect() {
      ready = true;
      setStatus('ready');
    },
    disconnect: function rejectResourceOnDisconnect() {
      setStatus('disconnected');
      ready = undefined;
      removeEventHandlers(connectHandlers);
    },
    fs_error: function rejectResourceOnDisconnect(error) {
      lastError = error;
      setStatus('fs_error');
    },
    error: function rejectResourceOnDisconnect(error) {
      lastError = error;
      setStatus('error');
    },
  };


  let housekeeping;

  const initHandlers = {
    connect: function InitSuccessHandle() {
      setStatus('valid');

      disconnect = function disconnectSocket() {
        if (ready === undefined) {
          return Promise.resolve();
        }
        return new Promise(function resourceDisconnect(resolve, reject) {
          io.once('disconnect', resolve);
          setTimeout(function fiveSecondTimerToDisconnect() {
            reject(new Error('Failed to disconnect in 5 seconds'));
          }, 5000);
          io.disconnect();
        });
      };

      addEventHandlers(connectHandlers);

      housekeeping();
    },
    error: function errorEventHandler(error) {
      if (typeof error === 'string') {
        // eslint-disable-next-line no-param-reassign
        error = new Error(error);
      }
      housekeeping(error);
    },
    reconnecting: function rejectResourceOnReconnecting(number) {
      setStatus(number);
    },
    connect_error: function connectErrorHandler(error) {
      if (typeof error === 'string') {
        // eslint-disable-next-line no-param-reassign
        error = new Error(error);
      }
      if (
        (error.message === 'Resource is a file') ||
        (error.message.startsWith('Resource does not exist')) ||
        (error.message.startsWith('Access not authorized')) ||
        (error.message.startsWith('Request Error')) ||
        (error.message.startsWith('Resource Error')) ||
        (error.message === 'Invalid Syntropize resource') ||
        (error.message === 'Invalid resource type') ||
        (error.message === 'Updates not avaiable')
      ) {
        // eslint-disable-next-line no-param-reassign
        io.io.skipReconnect = true;
        housekeeping(error);
      }
      else {
        // eslint-disable-next-line no-param-reassign
        error.message = `Unable to Connect: ${error.message}`;
      }
      lastError = error;
    },
    reconnect_failed: function reconnectFailedHandler() {
      housekeeping(new Error('All Reconnect Attempts Failed'));
    },
  };

  const connect = function connect() {
    return new Promise(function resourceConnect(resolve, reject) {
      disconnect = function disconnectInitial() {
        removeEventHandlers(initHandlers);
        io.disconnect();
        ready = undefined;
        reject(new Error('Connection Interrupted by User'));
      };

      housekeeping = function connectionHousekeeping(error) {
        removeEventHandlers(initHandlers);
        return error ? reject(error) : resolve();
      };

      setStatus('connecting');
      ready = false;
      addEventHandlers(initHandlers);
      io.open();
    });
  };

  return Object.freeze({
    connect,
    disconnect() {
      return disconnect();
    },
    get status() {
      return status;
    },
    get ready() {
      return ready;
    },
    get error() {
      return lastError;
    },
  });
};

export default socketFactory;
