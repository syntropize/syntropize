/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import digest from '../digest.js';
import settings from '../settings.js';

import eventLinker from './linker.js';

const schemaFactory = function schemaFactory(name, schema, location, rw, io, schemaOptions = {}) {
  const { scheme } = location.url;

  const {
    readOnly = (
      settings.protocol && settings.protocol[scheme] && settings.protocol[scheme].readOnly
    ) || !schema.writer,
  } = schemaOptions || {};

  const schemaSettings = (settings.schema && settings.schema[name]) || {};

  const options = { ...schemaSettings, ...schemaOptions, ...{ readOnly } };

  const reader = schema.reader({ location, rw, digest, options });
  const writer =
    (!readOnly /* && rw.write */) ?
      schema.writer({ location, reader, rw, options }) :
      {};

  const finalSchema = { ...reader, writer };
  if (Object.prototype.hasOwnProperty.call(finalSchema, 'on')) {
    eventLinker(reader, io);
    delete finalSchema.on;
  }

  return Object.freeze(finalSchema);
};

export default schemaFactory;
