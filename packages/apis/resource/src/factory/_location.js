/*! Copyright (c) 2016-2017, Rahul Gupta and ProtoSyntropize Developers
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import origin from '../origin/index.js';
import common from '../common/location.js';

const locationFactory = function locationFactory(address, options) {
  const scheme = address.split(':', 1)[0].toLowerCase();

  if (!origin[scheme]) {
    return new Error(`Resource Error: Undefined Scheme "${scheme}"`);
  }

  const parsedLocation = origin[scheme].location.parse(address, options);

  function resolvePath(...paths) {
    return origin[scheme].location.resolve(
      parsedLocation.$root, parsedLocation.$pathname, ...paths,
    );
  }

  function resolveAddress(...paths) {
    return common.resolve(
      parsedLocation.origin, parsedLocation.pathname, ...paths,
    );
  }

  return Object.freeze({
    url: parsedLocation,
    path: resolvePath,
    address: resolveAddress,
  });
};

export default locationFactory;
