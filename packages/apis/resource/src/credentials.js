/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { readFileSync } from 'fs';
import log from './log.js';

let credentials = {};

function load(location) {
  try {
    credentials = JSON.parse(readFileSync(location, 'utf8'));
  }
  catch (error) {
    log.debug(`Unable to load Credentials. ${error.message}`);
  }
}

function get(origin) {
  return credentials[origin];
}

export {
  load,
  get,
};
