/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import merge from 'lodash.mergewith';
import defaults from './defaults.json';

const settings = {};

const customizer = function customizeResourceSettingsMerge(objValue, srcValue) {
  if (Array.isArray(objValue) && Array.isArray(srcValue)) {
    return [...objValue, ...srcValue];
  }
  return undefined;
};

const updateSettings = function updateResourceSettings(newSettings) {
  if (typeof newSettings === 'object') {
    merge(settings, newSettings, customizer);
  }
};

const getSettings = function getResourceSettings() {
  return settings;
};

updateSettings(defaults);

export default settings;
export {
  updateSettings as update,
  getSettings as get,
};
