/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import libUrl from 'url';
// import libPath from 'path';

function getLocation(address = '.') {
  if (!/^\w{2,}:/.test(address)) {
    return libUrl.pathToFileURL(address);
    // return `file:///${libPath.posix.resolve(address || '.')}`;
  }
  else {
    return address;
  }
}

// eslint-disable-next-line import/prefer-default-export
export { getLocation as get };
