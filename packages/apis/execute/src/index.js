/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import child from 'child_process';
import libPath from 'path';

const execute = function execute(cmd, args) {
  return new Promise((resolve, reject) => {
    const cp = child.spawn(
      cmd,
      args,
      { detached: true },
    );
    cp.unref();
    cp.on('error', err => reject(err));
    cp.on('exit', () => resolve());
  });
};

const execScript = function execScript(
  script,
  registry,
  { location, fileList, targetFile },
) {
  const target = libPath.join(location, `${targetFile}.${registry.ext0}`);
  const files = fileList.map(
    file => libPath.join(location, file),
  );

  return execute(
    registry.path,
    script.args(target, files),
  );
};

const execCommand = function execCommand(
  { cmdPath, args, sep = ' ' },
  { location, fileList, targetFile },
) {
  const fileListStr = fileList.map(
    file => libPath.join(location, file),
  ).join(sep);

  const targetStr = (targetFile && libPath.join(location, targetFile));

  const newArgs = args.map(arg => arg
    .replace(/<files>/gi, fileListStr)
    .replace(/<target>/gi, targetStr));

  return execute(cmdPath, newArgs);
};

export {
  execCommand as command,
  execScript as script,
};
