# Execute API

**Execute API** allows Syntropize to execute external programs.

## Description

Execute API provides two methods to process cli arguments before invoking the required program:

+ It can call a script from the Syntropize registry that can processes arguments and associated filenames according to the requirements of the external program.
+ It can process the arguments and filenames according to a regular expression replacing:
  + `<files>` with a list of files passed to it
  + `<target>` with the name of a target file

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
