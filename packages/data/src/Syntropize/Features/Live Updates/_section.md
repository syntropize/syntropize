# Live Updates

When changes are made to a resource, Syntropize dynamically updates all views that are connected to that resource. Live updates ensure that the information on the screens of different users is always synchronized.
