# Application Hooks

Application Hooks leverage compute services offered by some origin to integrate external functionality into a Syntropize workspace. Most often this is functionality from your own computer, but hooks can just as well be run on a remote system. Hooks allow users to perform tasks without the distraction of having to navigate away from their content or opening new application windows.

Application Hooks in Syntropize can run scripts or trigger external applications using information from within a workspace. This facility can be used, for example, to incorporate scripts that build only the content selected by a user on screen (say, instead of typing out arguments on a command line).

The ability to include third-party hooks (including self-authored hooks) provide unprecedented opportunities for users extend the capabilities of their workspaces. Some ideas for application hooks might include bots, semantic processing, build tools etc. to name just a very few. Syntropize can examine the available hooks, the content in view and user settings to selectively expose features to a user.
