# Clean Data

With the ability to generate representations on-the-fly, Syntropize only needs to store the contents (aside from semantic information such as author intent) without any media information, such as mark-up, styles or code. Data stored in this way is much more intelligible and meaningful, and thus more usable, by humans and machines alike. The absence of media information is essential to the ability to mix-and-match content. Other benefits include:

+ Such pure data can be stored transparently using much simpler file formats.
+ Users can actually examine the information stored in their own private store using their own tools.
+ Search engines can not only index public information more easily, but also understand semantic context.
