# Universal Access

Syntropize Workspaces can bring together information and services simultaneously from a number of different sources, both local and remote. Syntropize treats all sources at par, that is, it does not make artificial distinctions between origins based on location and/or transport.

In particular, this ties together the capabilities of a desktop and the web into one seamless user-experience, bridging the online/offline divide.

Another upshot of universal access is that Syntropize works seamlessly with decentralized storage and compute facilities. Users have the freedom and flexibility to not just choose between, but also mix-and-match store and/or service providers at an extremely granular level.
