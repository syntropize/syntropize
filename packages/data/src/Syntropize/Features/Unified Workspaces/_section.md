# Unified Workspaces

Syntropize creates a computing experience that brings together interrelated information on a topic of interest from multiple sources and the relevant tools needed to work with that information into unified workspace.

Workspaces serve as windows that provide a cohesive view into some tiny portion of information landscape of interest to a user. Users are free to explore this information without necessarily having to keep track of the origin and state of each and every fragment of information. In particular, file (or more generally resource) divisions are invisible to users, unless a user explicitly demands to see them.

Providing users with the right information, in the right form, at the right time in one central space extends their capacity to engage with complex material. Unburdened from the task of setting up and managing their information environment, users can instead focus on their actual work. The ability to combine information and services thus creates unprecedented opportunities to tailor the experience to user requirements and expectations, in ways that are simply impossible in the prevailing desktop paradigm.
