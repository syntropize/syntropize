# Explicit Structure

A corollary of Data Atomicity is that Syntropize tracks the structure of information within a resource independent of its contents. The ability to create multiple, arbitrary structures allows for the creative organization of information by authors and editors. Syntropize uses these explicit structures to generate representations of data at run-time. This has many benefits:

+ **Multidimensionality**: We can have rich models of structure that can simultaneously track relationships along multiple dimensions.
+ **Selectivity**: Syntropize can provide a selective view of the resource, focusing on some specific part of the structure. In doing so, Syntropize only needs to selectively fetch contents, improving performance.
+ **Reuse**: Syntropize can reuse the same structure to create different views of the resource. Again, the need to fetch less data improves performance.
+ **Experimentation**: Users can experiment with alternate structures during the course of their work (with the possibility to revert to the original arrangement at the click of a button),
+ **Choice**: Authors can publish multiple structures, giving user the option to choose between different arrangements.
+ **Composition**: Multiple structures can even be tied together to produce composite structures.

The separation of content from structure is another way in which Syntropize makes data storage more transparent, minimizing, in most cases eliminating, the need to use complex, opaque file formats.
