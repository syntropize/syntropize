# Transclude to Extend

Syntropize allows users to extend a resource by simply transcluding data from another source. Whenever Syntropize generates a representation for a transcluding resource, it will show the transcluded data as if it were a part of that resource. With transclusion, users can organically grow their content, building on the hard work of the entire community.
