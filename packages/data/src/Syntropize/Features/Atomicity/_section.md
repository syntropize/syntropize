# Data Atomicity and Federation

The ability to dynamically recombine information in a workspace at run-time means that the underlying data can be broken-up and its storage federated amongst multiple providers. The Syntropize storage architecture allows for data to be disaggregated in the smallest semantically meaningful units, maximizing the possibilities for re-use. Not only can the same data be used in different contexts, it can also be combined and visualized in multiple ways to create different points of view.

The federation of data also makes storage more transparent:
+ Each chunk is transparently visible to the operating environment.
+ Each chunk is associated with its own metadata.
+ Each chunk has its own set of permissions.
+ Contents of a chunk can be stored using simpler file formats.

The software to work on the data chunks is much simpler. Users are also more likely to find third-party tools to work on these chunks (should they ever wish to step out of Syntropize).
