# View Customization

While Syntropize automatically generates suitable representations for information and services at a resource, users have the flexibility to modify its form, structure and contents of the view according to their requirements:

## Form

Users can choose from one of many views, each providing a different way to visualize the content. Users can further modify the visualization of any sub-structure or content within a view (within the constraints of that view). This might involve, say, visualizing the same data in different media (e.g., numerical data being represented either as a table or a graph).

With the entire library of (sub-)views available to the operating environment, Syntropize can offer a degree of customization that far surpasses the capabilities of any standalone application.

## Structure

Within any view, users can also modify how the contents or some part of it is structured. Simple example might include different ordering and filtering of content based on some attribute.

With the possibility of multiple as well as multidimensional structures, again, users have an unprecedented choice in organizing workspace content.

## Content

Finally, users can modify the content shown for any (sub-)concept. For example, one can selectively choose between different levels of expositions such as preferring a summary or an image instead of seeing the text in full.
