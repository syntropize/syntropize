# Copy to Share/Publish

Publishing is as simple as copying your work to a Syntropize supported public origin. When users access the shared resource, Syntropize regenerates the view identical to one seen by the author (subject to user preferences). Those with permissions to write can directly make changes to the shared content. Live updates ensure that changes are instantly communicated to all other users.

In effect, Syntropize provides users with their own personal interactive space to engage with the rest of the world.
