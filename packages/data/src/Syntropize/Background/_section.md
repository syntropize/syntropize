# Background

*This section is yet to be written because a proper discussion requires us to cover material sufficient for at least a book or a Ph.D. thesis. For now, we provide a plan for this section and a list of references.*

## The Plan

We shall motivate the need for Syntropize from three different but complementary perspectives:

+ **Media**: which provides a philosophical foundation for the design of information systems.
+ **Intelligence Augmentation**: the research programme that led to the advent of the modern personal computer.
+ **Complexity**: A theoretical framework to reason about the design of systems; more specifically for our limited purpose, the design of computers and its software.

Our aim here is to tie together the knowledge from these tracks to understand how computers can augment our individual and collective abilities, to tackle the “complex, urgent problems” of humanity and inform the design of computers systems that can help us advance towards our highest goals.

## References

Here is an (incomplete) list of essential texts to get you started on your journey through the world of personal computing:

+ [As We May Think](http://www.theatlantic.com/magazine/archive/1945/07/as-we-may-think/303881/) \
Bush, Vannevar. The Atlantic Monthly 176.1 (1945):101-108.

+ [Augmenting Human Intellect: A Conceptual Framework](https://www.dougengelbart.org/pubs/augment-3906.html) \
Engelbart, Douglas C. 1962.

+ [Computer Lib/Dream Machines](https://computerlibbook.com/) \
Nelson, Theodor H., 1974

+ [Understanding Media: The Extensions of Man](https://www.worldcat.org/title/understanding-media-the-extensions-of-man/oclc/1031984262) \
McLuhan, Marshall, 1964.

+ [Personal Dynamic Media](https://doi.org/10/cdmm6c) \
Kay, A., and A. Goldberg. 1977

+ Memex Revisited \
Bush, Vannevar. In from [Memex to Hypertext: Vannevar Bush and the mind's machine](https://www.worldcat.org/title/from-memex-to-hypertext-vannevar-bush-and-the-minds-machine/oclc/24870981), 197–216. 1991.

+ [An Experimental System for Creating and Presenting Interactive Graphical Documents](https://doi.org/10.1145/357290.357296) \
Feiner, Steven, Sandor Nagy, and Andries Van Dam.
ACM Transactions on Graphics (TOG) 1, no. 1 (1982): 59–77.


+ [Knowledge-Domain Interoperability and an Open Hyperdocument System](https://www.dougengelbart.org/content/view/164/126/#) \
Engelbart, Douglas C.
In Proceedings of the 1990 ACM Conference on Computer-Supported Cooperative Work, 143–56. ACM, 1990.

+ [Toward High-Performance Organizations: A Strategic Role for Groupware](http://www.dougengelbart.org/pubs/augment-132811.html) \
Engelbart, Douglas C.
In Proceedings of the GroupWare, 92:3–5, 1992.

+ [Toward a Deep Electronic Literature: The Generalization of Documents and Media](http://xanadu.com/XanaduSpace/xuGzn.htm) \
Nelson, Theodor Holm. 2019.

+ [The Humane Interface: New directions for designing interactive systems](https://www.worldcat.org/title/humane-interface-new-directions-for-designing-interactive-systems/oclc/797157885) \
Raskin, Jef. 2000.

+ [Vision and Reality of Hypertext and Graphical User Interfaces](http://edoc.sub.uni-hamburg.de/informatik/volltexte/2009/52/pdf/B_237.pdf) \
Müller-Prove, Matthias. Universität Hamburg, 2002.

+ [Building the Memex Sixty Years Later: Trends and Directions in Personal Knowledge Bases](http://scholar.colorado.edu/csci_techreports/931) \
Davies, Stephen, Javier Velez-Morales, and Roger King.
Technical Report. Boulder, Colorado: University of Colorado at Boulder, 2005.
