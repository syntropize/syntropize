# Resource Layer

The resource layer provides a unified mechanism to access resources and services on different computers (including your own). In order to access a resource, the resource layer invokes the appropriate programs to convert the interface presented by the resource provider to a standardized format for consumption by the domain layer. In effect, the resource layer abstracts over various origins and transport protocols to implement a **universal resource interface** for consuming decentralized storage and compute services.


###### Open Questions:

+ Should the *Resource Layer* also provide data caching, especially since browsers/shells already implement caching for remote protocols?
+ If we implement such a cache, how should we be invalidating it (e.g., should the resource layer be listening in to incoming notifications)?
