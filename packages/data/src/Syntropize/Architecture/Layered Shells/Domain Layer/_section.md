# Domain Layer

The domain layer brings together programs that implement the business logic and/or data models needed to realize the desired application functionality ([Evans 2004][evans2004]).

[evans2004]: https://www.academia.edu/download/62918913/Domain-Driven_Design__Tackling_-_Eric_Evans_1420200411-23993-hv1uem.pdf "Evans, Eric, and Evans, Eric J.  Domain-driven design: tackling complexity in the heart of software. Addison-Wesley Professional, 2004."

In bring together data and logic, Syntropize can create a number of independent domain models, each serving a different purpose. This is yet another strategy to separate functional concerns. Again, a view can (and typically does) consume multiple such domain models simultaneously.

These models, though independent, might reuse the same data and logic. Therefore, the domain layer maintains a global model to keep track of these instances. This allows views (consumers of these domain models) to expose shared contexts between them.


###### Open Questions:

+ How much flexibility should we provide developers to separate data models and business logic? Should, for example, these two concerns be put in two separate layers as is done in many 4 layered architectures (If so, should these layers be open)?
