# Layered Shells

Syntropize uses a layered architecture to separate the primary functional concerns within the program infrastructure. Each layer of the infrastructure itself acts a shell for interchangeable programs that implement the functional concerns of the layer for a given scenario. Syntropize dynamically invokes programs within each layer at runtime to realize the desired functionality.

The use of layers is a well understood design strategy to organize software with its benefits documented by multiple authors ([Fowler 2012][fowler2012], [Microsoft 2009][microsoft2009]). The ability to switch functionality within a layer has been proposed by Engelbart ([Engelbart 1988][engelbart1988]) but I could not find a detailed exposition and/or implementation of this idea in literature.

[fowler2012]: https://martinfowler.com/books/eaa.html "Fowler, Martin. Patterns of Enterprise Application Architecture, Addison-Wesley, 2012."

[microsoft2009]: https://docs.microsoft.com/en-us/previous-versions/msp-n-p/ee658109(v=pandp.10) "Chapter 5: Layered Application Guidelines, Microsoft Application Architecture Guide, 2nd Edition, 2009"

[engelbart1988]: https://www.dougengelbart.org/pubs/papers/scanned-original/1986-augment-101931-Workstation-History-and-the-Augmented-Knowledge-Workshop.pdf "Engelbart, Douglas C. The augmented knowledge workshop. In A history of personal workstations, pp. 185-248. 1988"

The use of **layered shells** simplifies the interaction between component programs while promoting maximal code reuse. At the outset, we propose the following layers:

