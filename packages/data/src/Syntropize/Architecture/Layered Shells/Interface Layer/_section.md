# Interface Layer

The interface layer allows agents to interact with a computer through some media. This description is intentionally vague; whilst the most common agent is a human end user, it can very well be another entity, such as another organism or machine. Even when we narrow down to humans, the interaction media could exploit any combination of sensory-motor apparatus to capture and convey information. Examples of such media include objects in a space (tracked by, say, cameras and other sensors), lights and switches on a control board, a voice assistant etc. The benefit of separating out the interface layer is that our interaction with application logic is agnostic to our choice of interaction media. The most common such media, however, remains the screen (augmented by keyboard, mouse and/or touch inputs); for the sake of clarity, practicality and concreteness we shall restrict our discussion to a visual interface for the remainder of this section. As and when they are supported, the same concepts can be extended to other media.

The user environment in Syntropize is built by combining hypermedia views. Somewhere between an app and a template, a view provides a visual representation of content and associated tools.

## Properties

+ Views enable Mashups: Views can seamlessly blend information from different sources (models) into a common graphical environment.

+ Views are dynamic: Syntropize dynamically selects views based on the content. The automatic generation of the interface may be modified by the author or the reader to suit their needs.

+ Views are modular: Views can successively transclude other views to represent content of arbitrary complexity and depth.

+ Views are interchangeable: Users may employ different views to visualize the same data from different perspectives.

+ Views create perspectives: When practical, the same content can be visualized using multiple views with each representation providing a different perspective on content. Similarly, within a view, content can be shared amongst sub-views.

+ Views are synchronized: Since these different views derive their content from the same data models, any change made through one of the views automatically updates the representations in all other views.

## View-Models

Associated with each view is a view-model, which keeps track of the state of the domain models in the view. This not just allows for shared views but is also useful for keeping track of a user's view preferences.

When different views load common content (derived from a shared domain model), we track this through a global view-model. Similarly, view-models within a view can track common content within that view. This information allows views to explicitly show the shared context to the users.


###### Comparison to Apps

+ Views obviously don't include business logic and data access.
+ Views can, in effect, bring together information from multiple sources and of multiple formats unlike apps.

###### Comparison to Templates

+ Whilst a template can only bring in data, a view can bring in other views as well.
+ Unlike templates, views also carry within them the means of interacting with the data.
