# Storage Model

Storage in Syntropize is organized around **Concepts**^[Probably resource would have been more suitable but that term has already been appropriated].
A concept is made up of the following components:
+ Data micro-representations^[not to be confused with presentation/screen representations or representations in the REST sense, data micro-representations are meant to be consumed by models, not views], each of which may carry a different part, aspect or version of the concept.
  + Associated with any data micro-representation may be additional information regarding state (e.g., initial state) and/or presentation (e.g., CSS).
+ Sub-concepts, that are also concepts in its own right, together with information about how a given sub-concept relate to the parent concept.
  + In particular, this includes information about how data micro-representations and associated part/aspect/version of a sub-concept relate to a data micro-representation and associated part/aspect/version of the parent concept.

A model can consume one or more concept micro-representations (as well as micro-representations of associated sub-concepts) to construct a representation of the concept.

This atomic approach to storage allows for fine-grained control over how models mix-and-match data to produce complex representations. Such representations do a better job providing users with a cohesive picture suitable to comprehend and reason about a given concept in relation to a problem domain without overwhelming them with unnecessary details. It also allows for progressive discovery of information within the information infrastructure.

## Inheritance

One way in which a hierarchical storage model is insufficient is that a parent must include all of its contents. It would be much more preferable if instead we could reuse the contents of one concept in another without the need to manually keep their contents in sync.

We can implement such reuse through inheritance, that is, by allowing concepts to inherit sub-concepts from other concepts, in a vein similar to object-oriented programming.

The inheritance feature, while powerful, brings with it a number of issues:

  1. It might be the case that one may want to inherit most but not all the sub-concepts within a concept. Thus, one needs to provide a mechanism for selection and/or elimination of sub-concepts inherited. I prefer the latter for the following reasons:
      + Selection is the same as building the concept by adding sub-concepts to it one-by-one.
      + The use of inheritance, in all likelihood, implies that we want to include most of the sub-concepts. Eliminations, thus, will typically involve less work.
      + It is more likely that we would want to include within our concept any sub-concepts that are subsequently added to the inherited concept.

  1. The inheriting concept also has access to micro-representations of the inherited concept.^[I am not yet sure what would be the best way to handle these micro-representations. I am especially concerned about how local micro-representations that use micro-representations inherited from another concept must be handled in response to representational changes in inherited concept].

While the benefits of inheritance are more than obvious, the complexity introduced to the storage model has the potential create much confusion in the minds all but the most sophisticated users. If we choose to implement such inheritance, the burden of proof shall be on us, the implementers, to find mechanisms that present concept inheritance to users in a manner that is intuitive.

## File System as Implementation Media

It is incidental that Syntropize uses directories/folders as containers for concepts, and files to store data, its representation, states, presentation and other associated meta-information. These are, however, implementation details, merely meant to organize data within the constraints of a file-system^[This situation is in no way ideal as file-systems are not designed to interact with web. Implementing sub-concepts that exist online require implementation of what are in effect quasi-folders using special format files which the file-system itself does not understand]. It might just be the case that on a server we may prefer to use a database instead as our data store.

We expect that file systems and databases will evolve into proper concept stores as this storage paradigm (whether in the proposed form or some other) catches on.
