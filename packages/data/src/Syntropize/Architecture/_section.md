# Architecture

Application functionality in Syntropize is realized through a co-operative interaction of programs. Each program is responsible for only one single function. By seamlessly bringing these programs into a unified workspace, Syntropize creates intricate hypermedia experiences tailored, in each instance, to the content that is being viewed and the context it is being viewed in.

Syntropize workspaces are, in effect, **Grand Unified Applications**! Underpinning the user experience is an infrastructure of programs; For this reason, we call this (proposed replacement of the desktop paradigm) the infrastructure paradigm.

This ability to mix-and-match functionality results in software that is more flexible and yet more performant than traditional software. At the same time, the pervasive reuse of functional components economizes the development effort.
