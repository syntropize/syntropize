# Package Manager

The package manager maintains a local cache of programs that Syntropize can immediately access. In case a program is not available locally, the package manager can download it from CDN/app-store and optionally cache it locally. The package manager thus provides a secure mechanism to manage programs, independent of the remainder of the operating environment.
