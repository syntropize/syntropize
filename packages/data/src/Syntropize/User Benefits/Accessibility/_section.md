# Accessibility

Since the representation of information is independent of data/service models, it is now possible to create front-ends that cater to specific needs of differently abled individuals without the need to rewrite an entire application and/or make significant modifications. Such solutions will always be superior to generalized additions/changes to user interface to improve accessibility in applications. Interface experts and specialists in ergonomics can design interfaces suited to particular disabilities independent of application developers.
