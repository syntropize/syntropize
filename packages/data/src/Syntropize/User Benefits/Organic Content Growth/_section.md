# Organic Content Growth

Any significant work builds upon the work of many individuals and groups. Syntropize encourages users to leverage the work of their peers by making transclusion of external resources an integral part of the authoring experience.

Together with frictionless publishing, this creates a virtuous cycle of content growth where people continually build upon the each-others work. The economization of intellectual effort and transparency of material sources will increase the quality of published work and reduce disinformation.
