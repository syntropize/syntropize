# Rich Hypermedia Environments

Syntropize workspaces have access to all the views, models and access methods available to the operating environment, with the possibility to further fetch capabilities on demand. The extensive choice of co-operating programs allows Syntropize to create the perfect blend of media elements to represent information and their interrelations. It is as if Syntropize starts applications within an application except that all these applications integrate seamlessly into one super-application.

Contrast this with monolithic applications which are limited by the functionality and presentation capabilities baked into them. The rigid containerization of logic into application packages has meant, clumsy efforts notwithstanding, that capabilities between applications cannot be shared. The user is forced switch between multiple applications to access different capabilities even when working on the same or related data.

The web in its attempt to tackle this problem has been transformed from a document exchange system into an application platform. Loading different applications for each and every website (even when they share functionality) upon every visit is not only extremely inefficient but is beset with other problems such as centralization of data and security vulnerabilities.
