# No Lock-in

Since data is stored transparently using simple formats in stores of their choosing, users are not locked into Syntropize. They are free to use another software if they so desire.

This is a departure from the use of complex, typically proprietary file-formats used by standalone applications to store data. This practice effectively results in user lock-in, unless a community engages in an extremely expensive and time-consuming standardization process. Web-applications go a step even further to completely lock-in users by exercising total control over their data.
