# Perspective

Syntropize allows users to visualize and interact with the same information using different views. Each view provides a different perspective and means of engagement with information in the workspace. Users can modify any element of how the information is represented, allowing for subtle shifts in point of view.
