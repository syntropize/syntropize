# Synchronized Workspaces

With the hypermedia environment in Syntropize being live, changes to data are communicated (near-)instantaneously. Thus, all workspaces accessing a particular resource are always synchronized. Changes to a resource published instantly allowing for collaboration in real-time.
