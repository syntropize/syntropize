# Frictionless Publishing

To publish on the web with full control and ownership over your data requires significant technical knowledge. This creates a barrier to entry for most people who just wish to express themselves. Even with services to simplify the process, publishing is often costly, with limited options and yet it presents a challenge for the lay user. It is this inability to quickly and comfortably publish, bolstered by with network effects, that has been responsible for the rise of privacy invading, poorly moderated, misinformation and disinformation breeding cesspools that we call social-media.

Syntropize takes away this friction by making the process of publishing as simple as copying it over to an online store. For example, users do not have to write their own HTML markup. User retain full ownership of their content. They are not constrained by the facilities offered by a web service when they put information on to the web.
