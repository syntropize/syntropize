# Reuse of Information

An invaluable benefit of data disaggregation is that each and every piece of information can now be reused independent of all others. Data reuse primarily takes two different forms:

+ Syntropize reuses data to create different views of a concept providing different perspectives on the data.
+ Syntropize reuses data in when different concepts shown in different workspaces build upon a common sub-concept.

Reuse ensures that data is not unnecessarily duplicated, since these copies will inevitably drift out of sync. Changes made to data in one view are automatically reflected in another view, without the need to manually update the contents of the view. Finally, if data is reused by different workspaces on the same device, it needs to be fetched only once from storage, improving performance.
