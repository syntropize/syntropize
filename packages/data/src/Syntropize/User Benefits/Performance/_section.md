# Performance

Minimal functional redundancy and maximal re-use of software components allows for efficient use of hardware resources:

+ Computations that tend to be repeated when running multiple applications (or even multiple instances of same program) need to be executed only once, resulting in leaner software.

+ Monolithic applications include a lot of functionality that might be unnecessary or irrelevant to a user. Due to its ability to mix-and-match programs, Syntropize can avoid loading such features into the workspace at launch and/or selectively load them on use.

+ The deduplication of code arising from such pervasive sharing and reuse results in a significantly smaller codebase.

It follows from the efficient use of hardware resources that programs will run faster. Performance, in fact, scales-up with complexity of the work environment, due to increased sharing and reuse of functionality.

Contrast this with the situation today wherein multiple applications are running separate instances of similar, sometimes even identical, functionality at the same time. Consider, for example, the case of OS native versions of web applications implemented in browser based shells such as Electron, where every application runs its own instance of chromium and v8. This duplication of functionality is simply a waste of system resources and can impose a significant performance penalty on the users' machine (upwards of 1 GB of RAM per application in some cases).
