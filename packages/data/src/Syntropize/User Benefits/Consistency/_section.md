# Consistency

The reuse of components throughout the application infrastructure, especially views, means that users have a more consistent interaction experience across different workspaces. This consistency of experience is another way in which Syntropize minimizes complexity and thus cognitive load during the course of information work.

The infrastructure paradigm further opens up the possibility to define user specific experience grammar. Such an experience grammar would ensure that, irrespective of the nature of content on display, the experience for a particular user is consistent across workspaces and even on different terminals. The visual grammar, for example, can be used to select (sub-)views based on proficiency and purpose.

In comparison, monolithic applications come with a fixed set of interfaces each with their own idiosyncrasies. If customization is even possible, it must be done separately for each application. Since users must users switch between applications to perform different tasks, the inconsistency of interfaces inevitably becomes a source of cognitive dissonance. Especially awkward are the cases where competing applications that provide the same functionality use inconsistent interface elements, just to make it difficult for users to switch.
