# Customizability

Because Syntropize creates workspaces by bringing together a number of different information and functional artefacts, it is possible to individually customize these components. Users can change form, structure and content of the information as well as the functionality available within a workspace. This allows users to finely-tune their experience exactly to their requirements.

In particular, users can select from the entire library of (sub-)views to modify the representation of on-screen information, leading an unprecedented degree of customization, far surpassing the capabilities of any standalone program. Further, the users can choose between multiple and/or multidimensional informational structures as well as modify the information contents within them. Syntropize, thus, gives users an unprecedented choice in organizing information and workspaces, incomparable with existing software.
