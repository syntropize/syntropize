# Search Optimized

The storage architecture makes it simpler for search engines to index your content and make inference about its relationships with other related information. Separation of content and structure allows machines to examine the two concerns independently. The absence of markup means that there is significantly less irrelevant information for web crawlers to trawl through. Based on the analysis of structure and shared usage of content, search engines can actually provide more relevant results based on context and not just similarity.
