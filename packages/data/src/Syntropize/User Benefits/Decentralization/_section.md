# Decentralization

Since Syntropize can bring together information and services simultaneously from a number of different sources, users have the freedom and flexibility to choose between service providers (or use their own machine). Users, not applications, decide where their content is stored and which services they would like to use.

This choice between services can even be granular; users pick which part of their content they would like to store at a specific origin. This, for example, can enable progressive disclosure of information where users can selectively move parts of their work into a public store, while continuing to work on the entire content as if it were at one place.

The benefits of decentralization include:
+ Scalability: services are not determined by the capacity of a private entity but work at internet scale, that is, take advantage of the capacities of individual nodes spread across the entire network,
+ Robustness: there is no single point of failure as the data is spread across multiple stores,
+ Self-Ownership: users are in control of their data and can decide what to do with it.
+ Choice: users decide where they want to host their data and how to pay for the service,
+ Privacy: users decide what they share.
