# Extensibility

Syntropize Workspaces are extensible, that is, they can be suitably adapted or extended by integrating features in response to changes in data and/or user requirements. Depending on the structure and contents of information and services as well as the context in which it is being shown, Syntropize can seamlessly integrate new functionality into any existing workspace.

Contrast this with standalone applications which are essentially limited by the capabilities baked into the particular application with, at best, limited possibilities of being extended. Whilst some applications can be extended using macros, it is almost always the case that macros written for one application will not work in another.
