# Security

Syntropize orthogonalizes code from content, that is, the content never contains embedded code. Based on the type of the content, Syntropize selects appropriate component programs, typically cached in trusted source, to generate the workspace. Provided these programs are themselves secure, users might safely access data from untrusted sources.

Further, the Syntropize architecture allows for the possibility of fine-grained and context specific sandboxing. Multiple sandboxes might, for example, be created around component programs, a subset of programs and/or layers as well as around concurrent user environments depending on the security requirements.
