# Package Manager

Syntropize prototype uses the npm manifest file a.k.a. `package.json` in lieu of a package manager to manage plugins. The inclusion of Syntropize specific package manager is essentially an engineering concern with little novelty. Also, newer JavaScript engines like [deno](https://deno.land) ship with native package management capabilities.
