# Implementation

We have endeavoured to create an experience relatively familiar to a modern personal computer user in an effort to not distract from the underlying methods and techniques that have been faithfully implemented.

Syntropize is primarily built on web-technologies. This choice is but natural, given the remarkable ubiquity and scalability of standards and technologies that underlie the web. Moreover, the very recent advent of browser based desktop application frameworks has made it possible use web-technologies to provide a (near-)native experience on all major operating systems with a development experience is nearly identical to creating web-applications. Though still in its infancy, web-assembly further holds the promise that environments built in Syntropize shall be as performant as their native counterparts.

Syntropize is designed to run as a standalone program hosted in any browser like environment. At present, we ship Syntropize using [nwjs](http://nwjs.io/) (Chromium + nodejs). We also hope to port it soon to the browser (but with reduced capabilities on account of security restrictions).
