# Prototype

Syntropize is both a means and an end -- research and engineering in equal measure. It aims to revive the Intelligence Augmentation programme in the context of present and future computing technologies.

The Syntropize prototype serves as a proof of concept implementation meant to:

+ Demonstrate feasibility,
+ Establish basic principles and exhibit core capabilities,
+ Explore a preliminary architecture.

At the same time, we aim to deliver functional software that can serve the immediate computing needs of users.

The focus of this prototype is on features and capabilities that to the best of our knowledge are **not** well-established or widely implemented, at least in the context of modern software applications. This choice has meant that more than a few proposed features are yet to be implemented.

I had initially designed Syntropize as an information management application and then as meta-information environment meant to replace the desktop. These solutions, in retrospect, were clearly inadequate to solve the Intelligence Augmentation problem. The experimentation and evolution has involved many missteps, vestiges of which, unfortunately, still haunt the code.
