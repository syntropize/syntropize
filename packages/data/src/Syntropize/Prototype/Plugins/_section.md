# Plugins

The Syntropize prototype uses plugins in lieu of interchangeable programs (as discussed in the [Architecture](../../Architecture) section) to provide specific functionality. The main difference being that programs can be written in any language (plugins, on the other hand, target JavaScript or web-assembly) and run as independent operating system processes (plugins run on the same thread as the operating environment). This is only meant to be a stop-gap solution on account of ease of implementation.

