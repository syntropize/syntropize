# Domain Layer

The domain layer calls upon schema plugins to provide data models for an information resource (and the set of linked resources). Schema plugins expose:
+ The data model,
+ An events interface to changes in the data model and
+ A way to modify data in the data model.

Syntropize ships with the following schema plugins:
