# Document Schema

**Document Schema** creates a data-model for Markdown documents within a resource and sub-resources that it contains and/or links to.

## Formats

The Document Schema uses the following formats to populate its data-model:

### Section File

A section of text is stored as a markdown, typically in a resource named `_section.md`. The format used extends markdown syntax to support Tufte style web articles. For syntax guidance please refer to the [Tufte Markdown documentation](https://github.com/luhmann/tufte-markdown/blob/master/examples/md/tufte.md).

### Meta File

The Document Schema reuses the meta file defined for the Board Schema. Thus, the order of sections is same as that of cards. See Board Schema documentation for more.

