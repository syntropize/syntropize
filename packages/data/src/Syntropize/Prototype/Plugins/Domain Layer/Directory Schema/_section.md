# Directory Schema

**Directory Schema** allows users to directly interact with the contents at a resource, that is, the resources and containers that it contains and/or links to.
