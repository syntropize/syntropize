# Interface Layer

The interface layer uses view plugins to create the environment where users interact with information obtained from different resources. This has conventionally meant the representation of the information on a screen; however, the upshot of separation of view from data model is that it allows us to generalize the interface to any media.

A view plugin obtains its information by connecting to one or more models from the domain layer. Corresponding to each view is a view-model that keeps track of state of data as well as user interaction within the view.

View plugins are implemented as [angular.js](https://angularjs.org) components and depend on the angular.js instance provided by Syntropize. A better implementation shall be framework-agnostic. Alternatively, for native environments, one might consider shipping the most common front-end frameworks as part of the operating environment itself.

Syntropize ships with the following view plugins:
