# Board-Simple View

The **Board-Simple View** is based on a Board and Card metaphor. It represents a resource as tiled cards placed on a virtual corkboard, in a manner similar to applications such as Evernote and Scriviner. The view can dynamically load different types of cards based on the content.

Syntropize ships three types of cards:
+ Text Card: Like a post-it note.
+ List Card: Lists contents inside the resource.
+ Web Card: Loads web media using [iframly](https://iframely.com).
