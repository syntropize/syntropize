# File-List View

The **File-List View** presents the contents of a resource, that is, the resources and container (typically files and directories) in a resource. It is the fall back view when no other view can be used to represent a given resource.
