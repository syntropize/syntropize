# Page-Linear View

The **Page-Linear View** presents sections of text at the resource and its linked resources as a document.

The current implementation shows the contents only two levels deep. This is because the page-linear view uses the same data model structures as the one used by the Board-Simple view. We intend to extend it to an arbitrary depth in a future revision.
