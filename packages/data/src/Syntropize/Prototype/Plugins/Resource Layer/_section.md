# Resource Layer

The resource layer depends upon origin plugins to access resources at some origin. Origin plugins implement the following four interfaces:

+ Authorization: A way to authorize user before they can use the resource.
+ Location: A way to calculate paths and URLs specific to the resource.
+ Read-Write: A way to interact with data on the resource. At present, this interface mirrors file system operations.
+ Events: A way to exchange notifications about resource events in real-time.

In principle, origin plugins must also implement an interface to access computational capabilities available at that origin. This is implemented instead as a separate interface solely for the local system using the Execute API. This was the easiest (though technically incorrect) route to take, in the absence of standardized RESTful protocols for computational exchange.

One possible candidate for future implementations of a baseline resource layer is the [Solid-Rest](https://github.com/solid/solid-rest) library which extends the [Solid](https://solidproject.org/) standard to multiple origins.

At present, Syntropize ships with the following origin plugins:

