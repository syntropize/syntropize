# Solid Origin

The **Solid Origin** plugin allow Syntropize to access resources on a [Solid](https://solidproject.org/) server over HTTPS. We are currently relying on the [Node Solid Server](https://github.com/solid/node-solid-server) for hosting and use a customized version of [Solid File Client](https://github.com/CxRes/solid-file-client) to implement the file-system like interface.

