# File Origin

The **File Origin** plugin allows Syntropize to access data resources on the local file-system.