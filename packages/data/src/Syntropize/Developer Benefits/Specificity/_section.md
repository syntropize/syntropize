# Specificity

Developers today are responsible for delivering an entire application. That is, they are simultaneously responsible for the concerns of access, model and view that make up all of the application package.

In Syntropize, on the other hand, all functionality is realized through infrastructure of interoperable programs. Developers need to develop or modify only the component programs necessary to implement the unique feature they wish to offer.
