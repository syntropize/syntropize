# Reduced Complexity

Syntropize constrains programmers through the separation of concerns and interoperability requirements. But well-designed constraints can actually be liberating [The Paradox of Choice, Schwartz (2004)](https://www.worldcat.org/title/paradox-of-choice-why-more-is-less/oclc/1050224531). With fewer decisions to make, programmers can concentrate on the functionality they aim to deliver. The resulting programs are small and focused. This will paradoxically allow developers to deliver more complex functionality with greater ease.
