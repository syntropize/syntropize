# Separation of Concerns

<!-- Check Horizontal and Vertical -->

The Syntropize architecture allows for the orthogonalization of concerns along the following axes:

+ Horizontal: concerns of access, model and view are handled independently.
+ Vertical: concerns of individual components within access, model and view layers are handled independently of others.

The separation allows developers to focus on their area of expertise without being encumbered by extraneous concerns and side effects.
