# Scale

Syntropize allows new functionality to be built upon an existing infrastructure. Only the novel part of any feature needs to be implemented. This shall allow software development to scale with our emerging needs.

