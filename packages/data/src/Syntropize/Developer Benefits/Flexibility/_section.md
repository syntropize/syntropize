# Flexibility

Since changes to the user experience are made per functional component, developer have greater flexibility to experiment as well as to make more frequent changes.
