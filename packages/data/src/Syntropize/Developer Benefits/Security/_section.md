# Security

Developers can reduce surface area for vulnerabilities with:

+ Compartmentalization of data,
+ Separation of concerns,
+ Separation of data from executable code.

