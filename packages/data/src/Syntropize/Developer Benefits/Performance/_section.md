# Performance

Minimal functional redundancy and maximal re-use of components allows for frugal and efficient use of hardware resources.

