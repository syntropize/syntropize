# Program Diversity

Since the functional components are compartmentalized, the development of competing solutions to a particular software does not require a re-write of the entire application but only the development of the novel and/or competing functionality. The considerable reduction in development effort shall encourage developers to create a greater diversity of features and implementations.
