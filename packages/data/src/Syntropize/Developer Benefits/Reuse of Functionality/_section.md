# Reuse of Functionality

With application functionality realized through a program infrastructure with clear separation of concerns, it follows that component programs are reused to create the varied functionality. The deduplication of effort due to massive reuse will make software development more efficient and developers more productive.
