# Extensibility

The plugin architecture allows for the functionality of any workspace to be extended by adding new features. Being workspace agnostic, features developed for one situation can just as easily be deployed in a different workspace.

Contrast that with applications that might allow for their functionality to be extended via plugins, macros etc. but these solutions are specific to that specific application limiting their usefulness.
