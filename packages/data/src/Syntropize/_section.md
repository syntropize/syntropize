# Syntropize

Syntropize is a proposal to reclaim the original vision of the personal computer as the means for intelligence augmentation and seamless collaboration.

Through a comprehensive redesign of the operating environment of a personal computer using the latest computing and web technologies, we aim to radically increase the effectiveness of information workers to solve the complex, urgent problems of society.
