# Welcome

Welcome to Syntropize!

Let us take this opportunity to look at some quirks and features of the Syntropize Operating Environment.

## Navigation

The Navigation Bar behaves just like the one you might find in a regular file or web browser:

+ Type any address and hit enter to navigate. If the resource at this address is not Syntropize compatible, it shall be opened using the system specified application, for example, a regular website shall open in the web browser.

+ Navigation buttons to help you move between pages (forward and back and up one level) are on the left on large screens and in the unified menu button on the right on smaller screens.

+ The large rectangular button on the right, when on large screens, shows the current view. The button leads to a drop-down that contains a list of views that the user can navigate to. See the View section below for more.

NB: The address in the navigation bar resets as soon as the mouse leaves it; so if you wish to navigate to a new address, make sure you hit enter or select a view while the mouse is still on the navigation bar. A better implementation in the future would fix this issue by ensuring that the address resets only when an event occurs outside navigation bar.

## Views

You can change the view using the button in the top right corner. Choosing the active view causes the view to refresh the data. This is the same as clicking the reload button beside the dropdown.

### Board-Simple View

The Board-Simple view imitates cards on a corkboard or post-it notes on a wall. It is intended to serve as a meta-information layer over the resource, that is, a layer that contains information about the information contained within the given resource^[We distinguish meta-information from metadata, which unfortunately, in the context of file systems, has come to used to refer to data about the data container instead of the contents within a resource]:

+ The main organizing unit of this view is the **card**, which can carry a short text, small image or another media describing the contents of the resource.

+ A **board** contains multiple cards representing the multiple resource contained inside a parent resource.

+ Cards can be selected by clicking on their titles. The last selected card, showing up in pink, is the active card. This will be the card loaded for editing or whose metadata will be shown in the `Details` menu.

+ Cards can be reordered by dragging them from their title bar. Remember to save the new card order from the `Board` menu.

+ Card themselves can expand to their own boards. If this is the case, the card will reveal a board icon on its top right corner when the user hovers on the title. The board can be opened by double-clicking the card title. Clicking on the revealed right corner icon when hovering on the card title will instead open the resource in system specified manner, allowing you to easily access the contents of the resource using other programs.

+ Cards are also directories. Clicking on the `File` menu in the ribbon bar reveals the file contents inside the cards. Files can be selected by clicking on them. The `Run` command on the ribbon allows you to act directly on selected files. A toolbar on the bottom of the card allows the user to perform the most common file operations.

### Page-Linear View

The Page-Linear view (i.e., this view), is a whimsical implementation of long form articles that are styled using the ideas demonstrated by Edward Tufte’s books and handouts. The document produced combines information from the markdown files stored alongside the cards in Board-Simple view.

+ Buttons on the toolbar change depending on the active section. The active section is the one under the mouse and thus under the toolbar when performing any operation. It is also highlighted on the navigation panel on the left (see below).

+ As this view is read-only at present, the contents of these files must be edited using the native editor on your machine. The active section can be opened in the system editor from the toolbar or the navigation-panel. Other sections can be opened from the navigation panel also.

+ A navigation panel is available to browse through the document. It can be accessed from the toolbar using the "burger" menu on the left:

  + Scroll to a section by clicking on its tile.
  + Navigate to the section (and see its contents) by double-clicking on the tile.
  + If a section is inactive, click on the pencil button to start the section, that is, create the underlying markdown file.
  + If a section is active, clicking on it allows you to edit *local* files corresponding to the section in the editor or download *online* files.
  + Double-clicking allows you to see the files in file-browser/web-browser, depending on their location.
  + Drag and drop section tiles to reorder contents. If you reorder contents using the navigation panel, don't forget to hit save.

### File-List View

A view to see the contents, typically files and containers, of some resource, just like a file browser. This is the fallback view in case a resource has no board or document associated with it.
