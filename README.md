# Syntropize

Syntropize is a proposal to reclaim the original vision of the personal computer as the means for intelligence augmentation and seamless collaboration.

Through a comprehensive redesign of the operating environment of a personal computer, we aim to radically increase the effectiveness of information workers to solve the complex, urgent problems of society.

To learn more, head-on over to the Syntropize website at <https://syntropize.com>.

## Contributions

Please see the [CONTRIBUTORS](./CONTRIBUTORS) file for the list of contributors. If you make a contribution (any contribution, not just code), please consider adding your name to the contributors list.

## Copyright and License

Please see the [NOTICE](./NOTICE) file for Copyright and License information.

Licenses are available in the respective LICENSE* files.

