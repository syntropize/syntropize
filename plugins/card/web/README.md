# Web Card

**Web Cards** allows you to associate a rich-media generated using [iframe.ly](https://iframely.com/) to a resource.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
