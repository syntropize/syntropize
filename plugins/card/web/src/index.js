/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import './display/display.css';
import './input/input.css';

import webDynamicDirective      from './display/dynamic.directive.js';
import webCardDisplayDirective  from './display/display.directive.js';
import webCardDisplayController from './display/display.controller.js';
import webCardInputDirective    from './input/input.directive.js';
import webCardInputController   from './input/input.controller.js';

export default angular.module('card.web', [])
  .controller('WebCardInputController', webCardInputController)
  .controller('WebCardDisplayController', webCardDisplayController)
  .directive('webCardDisplay', webCardDisplayDirective)
  .directive('webCardInput', webCardInputDirective)
  .directive('webDynamic', webDynamicDirective)
;
