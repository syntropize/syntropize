/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const webCardInputController = function webCardInputController(
  $http,
  $log,
) {
  const template = this;
  let success = 0;
  let key;
  let { address } = template.contents;
  let fetchAddress = address;

  // $scope.$watch(
  //   'address',
  //   function resetSuccessOnNewAddress() {
  //     success = 0;
  //   },
  // );

  const fetch = function fetchWebCard() {
    fetchAddress = address;
    success = 1;
    return $http.get(
      `http://open.iframe.ly/api/oembed?url=${address}&maxwidth=232&maxheight=200&origin=${key}&omit_script=1`,
      { responseType: 'json' },
    )
      .then(function onFetchWebCardSuccess({ data }) {
        template.contents.data = data;
        template.contents.address = address;
        success = 2;
      })
      .catch(function onFetchWebCardFailure(error) {
        $log.error(error);
        success = -1;
      })
    ;
  };

  const disableButton = function disableButton() {
    return success;
  };

  return {
    fetch,
    get contents() {
      return template.contents;
    },
    set contents(item) {
      template.contents = item;
    },
    disableButton,
    get address() {
      if (address !== fetchAddress) success = 0;
      return address;
    },
    set address(url) {
      address = url;
    },
    get key() {
      return key;
    },
    set key(username) {
      key = username;
    },
    get success() {
      return success;
    },
  };
};


webCardInputController.$inject = [
  '$http',
  '$log',
];

export default webCardInputController;
