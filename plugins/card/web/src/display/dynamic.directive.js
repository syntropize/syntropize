/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const webDynamicDirective = function webDynamicDirective(
  $compile,
  $sce,
) {
  return {
    restrict: 'EA',
    replace: true,
    link: function linkWebCard(scope, element, attrs) {
      scope.$watch(attrs.webDynamic, function replaceWebCardIFrame(html) {
        if (html) {
          html = html.replace(/"\/\//g, '"http://');
          html = html.replace(/<iframe/, '/<iframe /');
          // html = html.replace(/<\/a>/, )
        }
        else {
          html = '';
        }
        element.html($sce.trustAsHtml(html));
        $compile(element.contents())(scope);
      });
    },
  };
};

webDynamicDirective.$inject = [
  '$compile',
  '$sce',
];

export default webDynamicDirective;
