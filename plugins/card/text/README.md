# Text Card

**Text Cards** allow you to describe a resource with plain text.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
