# Index Card

**Index Cards** keep track of the list of cards contained within a resource.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
