# Card Plugins

This directory contains **card plugins**, view sub-components to display different types of cards and to edit their contents. These plugins are invoked by parent views that support cards, such as the Board-Simple view.

## Design

Each card plugin provide two components (presently AngularJS directives) to:
+ Display the cards in a Board-Simple view or similar, and
+ Input or edit the card within the side panel.
