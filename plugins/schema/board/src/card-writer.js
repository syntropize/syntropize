/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardCardWriter = function cardWriter({
  location,
  rw,
  options = {},
}) {
  const cardFile = options.file;

  const {
    arrifyPath: arrify,
  } = rw.utils;

  const add = async function addCard(key, card) {
    return await rw.write(location, arrify(key, cardFile), JSON.stringify(card, null, 2), { flag: 'wx' });
  };

  const replace = async function replaceCard(key, card) { // TODO: Create a proper rw.replace
    return await rw.write(location, arrify(key, cardFile), JSON.stringify(card, null, 2), { flag: 'w' });
  };

  const remove = async function removeCard(card) {
    return await rw.remove(
      location,
      arrify(card, cardFile),
    );
  };

  const copy = async function copyCard(fromLocation, from, toCard = '.', to) {
    return await rw.copy(
      fromLocation,
      arrify(from, cardFile),
      location,
      arrify(toCard, to, cardFile),
    );
  };

  const move = async function moveCard(fromLocation, from, toCard = '.', to) {
    return await rw.move(
      fromLocation,
      arrify(from, cardFile),
      location,
      arrify(toCard, to, cardFile),
    );
  };


  return Object.freeze({
    add,
    replace,
    remove,
    copy,
    move,
  });
};

export default boardCardWriter;
