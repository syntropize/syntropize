/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import EventEmitter from 'events';
import libPath from 'path';
import settings from './defaults.json';

const boardReader = function boardReader({
  location,
  rw,
  digest,
  options = {},
}) {
  const self = {
    card: {},
    metadata: {},
  };

  const files = options.files || settings.files;

  const watcher = new EventEmitter();

  async function readItem(item) {
    try {
      const result = await rw.read(location, item);
      return JSON.parse(result);
    }
    catch (error) {
      if (error instanceof SyntaxError) {
        error.message = `Board: Unable to parse "${item}"
${error.message}`;
      }
      else {
        error.message = `Board: Unable to access "${item}"
${error.message}`;
      }
      throw error;
    }
  }

  async function destructurePath(item) {
    // To be activated once proper 'ready' logic is implemented
    // const libPath = await rw.path(location);
    return [libPath.dirname(item), libPath.basename(item)];
  }

  async function addItem(type, parent, item) {
    if (!Object.prototype.hasOwnProperty.call(self[type], parent)) {
      digest(function applyItemAddStart() {
        self[type][parent] = undefined;
      });
      watcher.emit(`${type} add`, parent);
    }
    else {
      watcher.emit(`${type} edit`, parent);
    }

    try {
      const result = await readItem(item);
      digest(function applyRecordItemAdd() {
        self[type][parent] = result;
      });
      watcher.emit(`${type} readSuccess`, parent);
    }
    catch (error) {
      digest(function applyRecordItemAddFailure() {
        self[type][parent] = error;
      });
      watcher.emit(`${type} readError`, parent);
    }
  }

  const add = async function onBoardItemAdd(item) {
    const [parent, base] = await destructurePath(item);

    Object.keys(self).some((type) => {
      if (files[type] === base) {
        addItem(type, parent, item);
        return true;
      }
      return false;
    });
  };

  async function unlinkItem(type, parent) {
    if (Object.prototype.hasOwnProperty.call(self[type], parent)) {
      digest(function applyRecordItemDelete() {
        delete self[type][parent];
      });
      watcher.emit(`${type} delete`, parent);
    }
  }

  const unlink = async function onBoardItemUnlink(item) {
    const [parent, base] = await destructurePath(item);

    Object.keys(self).some((type) => {
      if (files[type] === base) {
        unlinkItem(type, parent);
        return true;
      }
      return false;
    });
  };

  const ready = function onBoardReady() {
    watcher.emit('ready');
  };

  const clear = function clearBoard() {
    digest(function applyClearBoardObjects() {
      Object.values(self).forEach(obj => (
        // eslint-disable-next-line no-param-reassign
        Object.keys(obj).forEach(key => delete obj[key])
      ));
    });
  };

  return {
    get cards() {
      return self.card;
    },
    get metadata() {
      return self.metadata;
    },
    on: Object.freeze({
      add,
      change: add,
      unlink,
      addDir: add,
      unlinkDir: unlink,
      ready,
    }),
    event: watcher,
    clear,
    options: {
      ...options,
      files,
    },
  };
};

export default boardReader;
