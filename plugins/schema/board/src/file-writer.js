/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardFileWriter = function boardFileWriter({ location, rw }) {
  const arrify = rw.utils.arrifyPath;

  const write = async function writeBoardFile(card, file, contents, options) {
    return await rw.write(location, arrify(card, file), contents, options);
  };

  const createDir = async function createBoardDir(card, dir) {
    return await rw.createDir(location, arrify(card, dir));
  };

  const link = async function linkBoardFile(card, file, path) {
    return await rw.link(location, arrify(card, file), path);
  };

  const rename = async function renameBoardFile(oldCard, oldFile, newCard, newFile) {
    return await rw.rename(location, arrify(oldCard, oldFile), location, arrify(newCard, newFile));
  };

  const remove = async function removeBoardFile(card, item) {
    return await rw.remove(location, arrify(card, item));
  };

  const copy = async function copyBoardItem(from, fromItem, toCard, toItem) {
    return await rw.copy(from, fromItem, location, arrify(toCard, toItem));
  };

  const move = async function moveBoardItem(from, fromItem, toCard, toItem) {
    return await rw.move(from, fromItem, location, arrify(toCard, toItem));
  };

  return Object.freeze({
    write,
    createDir,
    link,
    remove,
    rename,
    move,
    copy,
  });
};

export default boardFileWriter;
