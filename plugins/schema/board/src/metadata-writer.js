/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardMetadataWriter = function boardMetadataWriter({
  location,
  reader,
  rw,
  options = {},
}) {
  const metadataFile = options.file;

  // Helpers

  const {
    arrifyPath: arrify,
  } = rw.utils;

  function getWriteArguments(...args) {
    return args[1] ? args : ['.', args[0] || []];
  }

  const write = async function writeMetadata(key, metadata, flag) {
    return await rw.write(
      location,
      arrify(key, metadataFile),
      JSON.stringify(metadata, null, 2),
      { flag },
    );
  };

  const read = async function writeMetadata(key) {
    const result = await rw.read(
      location,
      arrify(key, metadataFile),
    );
    return JSON.parse(result);
  };

  const getKeys = async function getKeys(key) {
    if (reader && reader.metadata && Array.isArray(reader.metadata[key])) {
      return reader.metadata[key].slice();
    }
    else {
      return await read(key);
    }
  };


  // Board Methods

  const create = async function createMetadata(...args) {
    const [key, metadata] = getWriteArguments(...args);
    return await write(key, metadata, 'wx');
  };

  const update = async function updateMetadata(...args) {
    const [key, metadata] = getWriteArguments(...args);
    return await write(key, metadata, 'w');
  };

  const remove = async function removeMetadata(key = '.') {
    return await rw.remove(
      location,
      arrify(key, metadataFile),
    );
  };

  const copy = async function copyMetadata(fromLocation, from, toCard = '.', to) {
    return await rw.copy(
      fromLocation,
      arrify(from, metadataFile),
      location,
      arrify(toCard, to, metadataFile),
    );
  };

  const move = async function moveMetadata(fromLocation, from, toCard = '.', to) {
    return await rw.move(
      fromLocation,
      arrify(from, metadataFile),
      location,
      arrify(toCard, to, metadataFile),
    );
  };


  // Key Methods

  const addKeys = async function addBoardMetadataKeys(...args) {
    const [card, keys] = getWriteArguments(...args);
    const modifiedKeys = await getKeys(card);
    keys.forEach((key) => {
      const index = modifiedKeys.indexOf(key);
      if (index === -1) {
        modifiedKeys.push(key);
      }
    });
    return await update(card, modifiedKeys);
  };

  const removeKeys = async function removeBoardMetadataKeys(...args) {
    const [card, keys] = getWriteArguments(...args);
    const modifiedKeys = await getKeys(card);
    keys.forEach((key) => {
      const index = modifiedKeys.indexOf(key);
      if (index >= 0) {
        modifiedKeys.splice(index, 1);
      }
    });
    return await update(card, modifiedKeys);
  };

  const replaceKeys = async function replaceBoardMetadataKeys(...args) {
    const [card, keys] = getWriteArguments(...args);
    const modifiedKeys = await getKeys(card);
    Object.entries(keys).forEach(([oldkey, newKey]) => {
      const index = modifiedKeys.indexOf(oldkey);
      if (index >= 0) {
        modifiedKeys.splice(index, 1, newKey);
      }
      else {
        modifiedKeys.push(newKey);
      }
    });
    return await update(card, modifiedKeys);
  };


  return Object.freeze({
    create, // TODO: changed API in commandService
    update,
    remove,
    copy,
    move,
    addKeys,
    removeKeys,
    replaceKeys,
  });
};

export default boardMetadataWriter;
