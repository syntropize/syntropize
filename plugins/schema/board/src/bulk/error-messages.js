/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
function bulkCardErrorMessages(ops) {
  const cardErrorsOnly = Object.freeze({
    ZERO: undefined,
    ONE: `Board: Failed to ${ops} a card`,
    MANY: `Board: Failed to ${ops} some cards`,
    ALL: `Board: Failed to ${ops} all cards`,
  });

  function pluralize(plural) {
    return plural ? 's' : '';
  }

  function cardAndMetaErrors(plural) {
    return Object.freeze({
      ZERO: `Board: Unable to update board${pluralize(plural)} after ${ops}`,
      ONE: `Board: Failed to ${ops} a card and update board${pluralize(plural)}`,
      MANY: `Board: Failed to ${ops} some cards and update board${pluralize(plural)}`,
      ALL: undefined,
    });
  }

  return Object.freeze({
    ZERO: cardErrorsOnly,
    ONE: cardAndMetaErrors(false),
    MANY: cardAndMetaErrors(true),
  });
}

function bulkFileErrorMessages(ops) {
  const fileErrors = Object.freeze({
    ZERO: undefined,
    ONE: `Board: Failed to ${ops} an item`,
    MANY: `Board: Failed to ${ops} some items`,
    ALL: `Board: Failed to ${ops} all items`,
  });

  return Object.freeze({
    ZERO: fileErrors,
  });
}

const bulkErrorMessages = Object.freeze({
  card: bulkCardErrorMessages,
  file: bulkFileErrorMessages,
});

export default bulkErrorMessages;
