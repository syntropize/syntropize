/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import segregate from '@syntropize/util-pass';
import aa from 'handle-async-await';

function bulkCardOperations(writer) {
  async function $1argsCards(op, list, ...args) {
    const results = await Promise.allSettled(list.map(async item => (
      aa(
        writer.card[op](item, ...args),
        `Failed to ${op} card file.`,
      )
    )));
    return segregate(results);
  }

  async function $1argsBoard(op, list, ...args) {
    const results = await Promise.all(list.map(async (item) => {
      const cardResult = aa(
        writer.card[op](item, ...args),
        `Failed to ${op} card file.`,
      );
      const metaResult = aa(
        writer.metadata[op](item, ...args),
        `Failed to ${op} metadata file.`,
      );
      const result = await Promise.allSettled([cardResult, metaResult]);
      return segregate(result);
    }));
    return segregate(results);
  }

  async function $1argsAll(op, list, ...args) {
    const results = await Promise.allSettled(list.map(async item => (
      aa(
        writer.file[op]('.', item, ...args),
        `Failed to ${op} directory.`,
      )
    )));
    return segregate(results);
  }

  async function xcCards(op, fromLocation, fromList, toCard = '.', toList) {
    const results = await Promise.allSettled(fromList.map(async (from, i) => (
      aa(
        writer.card[op](fromLocation, from, toCard, toList[i]),
        `Failed to ${op} card file.`,
      )
    )));
    return segregate(results);
  }

  async function xcBoard(op, fromLocation, fromList, toCard = '.', toList) {
    const results = await Promise.all(fromList.map(async (from, i) => {
      const cardResult = aa(
        writer.card[op](fromLocation, from, toCard, toList[i]),
        `Failed to ${op} card file.`,
      );
      const metaResult = aa(
        writer.metadata[op](fromLocation, from, toCard, toList[i]),
        `Failed to ${op} metadata file.`,
      );
      const result = await Promise.allSettled([cardResult, metaResult]);
      return segregate(result);
    }));
    return segregate(results);
  }

  async function xcAll(op, fromLocation, fromList, toCard = '.', toList) {
    const results = await Promise.allSettled(fromList.map(async (from, i) => (
      // Pehaps we will add a check to verify if this directory is indeed a board/card
      aa(
        writer.file[op](fromLocation, from, toCard, toList[i]),
        `Failed to ${op} directory.`,
      )
    )));
    return segregate(results);
  }

  return Object.freeze({
    $1args: Object.freeze({
      CARDS: $1argsCards,
      BOARD: $1argsBoard,
      ALL: $1argsAll,
    }),
    xc: Object.freeze({
      CARDS: xcCards,
      BOARD: xcBoard,
      ALL: xcAll,
    }),
  });
}

export default bulkCardOperations;
