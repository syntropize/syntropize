/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import segregate from '@syntropize/util-pass';

function bulkFileOperations(writer) {
  async function $1args(op, card, list, ...args) {
    const results = await Promise.allSettled(list.map(async item => (
      writer.file[op](card, item, ...args)
    )));
    return segregate(results);
  }

  async function xc(op, fromLocation, fromList, toCard = '.', toList) {
    const results = await Promise.allSettled(fromList.map(async (from, i) => (
      writer.file[op](fromLocation, from, toCard, toList[i])
    )));
    return segregate(results);
  }

  return Object.freeze({
    $1args,
    xc,
  });
}

export default bulkFileOperations;
