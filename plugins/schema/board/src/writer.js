/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import aa from 'handle-async-await';
import segregate from '@syntropize/util-pass';

import bulkResults from '@syntropize/util-bulk-schema-results';
import bulkErrorMessages from './bulk/error-messages.js';

import boardCardWriter from './card-writer.js';
import boardFileWriter from './file-writer.js';
import boardMetadataWriter from './metadata-writer.js';

import bulkFileOperations from './bulk/files.js';
import bulkCardOperations from './bulk/cards.js';

import settings from './defaults.json';

const boardWriter = function boardWriter({
  location,
  reader,
  rw,
  options = {},
}) {
  const {
    metadata: metadataFile,
    card: cardFile,
  } = options.files || settings.files;

  const writer = {
    metadata: boardMetadataWriter({
      location,
      rw,
      reader,
      options: {
        file: metadataFile,
      },
    }),
    card: boardCardWriter({
      location,
      rw,
      options: {
        file: cardFile,
      },
    }),
    file: boardFileWriter({
      location,
      rw,
    }),
  };

  const {
    list: reifyList,
    xcList: reifyXCList,
  } = rw.utils.reify;

  const bFO = bulkFileOperations(writer);

  const bCO = bulkCardOperations(writer);
  const bCO$Modes = {};
  Object.keys(bCO).forEach((item) => {
    bCO$Modes[item] = Object.freeze(Object.keys(bCO[item]));
  });
  Object.freeze(bCO$Modes);
  function isValidBCO$Mode(opType, opts) {
    return bCO$Modes[opType].some(mode => mode === opts);
  }

  /* Card Operations */

  async function addCard(key, card) {
    const cardResult = await aa(
      writer.card.add(key, card),
      `Board: Unable to add card ${key}.`,
    );
    const metaResult = await aa(
      writer.metadata.addKeys([key]),
      `Board: Unable to append key to metadata after adding card ${key}.`,
    );
    return [cardResult, metaResult];
  }

  async function replaceCard(key, card) {
    return aa(
      writer.card.replace(key, card),
      `Board: Unable to replace card ${key}.`,
    );
  }


  /* Board Operations */

  function isRootKey(key) {
    return !key || key === '.' || key === '';
  }

  async function start(key) {
    return await aa(
      writer.metadata.create(key || '.', []),
      `Board: ${isRootKey(key) ? 'Not' : `"${key} not`} initialized.`,
    );
  }

  async function cease(key) {
    return await aa(
      writer.metadata.remove(key),
      `Board: ${isRootKey(key) ? 'Not' : `"${key} not`} removed.`,
    );
  }

  async function update(key, data) {
    return aa(
      writer.metadata.update(key, data),
      `Board: ${isRootKey(key) ? 'Not' : `"${key} not`} updated.`,
    );
  }


  /* Combined Operations */

  async function create(key, card) {
    const errorPrefix = `Board: Unable to create new board ${key}.`;

    const mainResults = await Promise.all([
      aa(writer.metadata.create(key, []), `${errorPrefix}: Failed to write metadata for ${key}.`),
      aa(writer.card.add(key, card), `${errorPrefix}: Failed to write card ${key}.`),
    ]);

    const metaResults = await aa(
      writer.metadata.addKeys([key]),
      `Board: Unable to update metadata after creating board ${key}.`,
    );

    return [mainResults, metaResults];
  }

  async function transclude(key, path) {
    const mainResult = await aa(
      writer.file.link('.', key, path),
      `Board: Unable to transclude card "${key}" from "${path}".`,
    );
    const metaResult = await aa(
      writer.metadata.addKeys([key]),
      `Board: Unable to append key after transcluding card "${key}".`,
    );
    return [mainResult, metaResult];
  }

  async function rename(oldKey, newKey) {
    if (oldKey === '.' || oldKey === '') {
      throw new Error('Board: Cannot rename root card.');
    }
    const mainResult = await aa(
      writer.file.rename(oldKey, '', newKey, ''),
      `Board: Unable to rename "${oldKey}" to "${newKey}".`,
    );
    const replacements = {};
    replacements[oldKey] = newKey;
    const metaResult = await aa(
      writer.metadata.replaceKeys(replacements),
      `Board: Unable to replace key "${newKey}" after renaming "${oldKey}".`,
    );
    return [mainResult, metaResult];
  }


  /* Bulk Card/Board/Directory Operations */

  async function copy(fromLocation, fromKeys, toCard = '.', toKeys, opts = 'ALL') {
    const errorPrefix = 'Board: Unable to copy:';

    if (!fromLocation) {
      throw new Error(`${errorPrefix} No source specified`);
    }

    if (fromLocation.path() === location.path(toCard || '.') && !toKeys) {
      throw new Error(`${errorPrefix} Identical source and destination.`);
    }

    const list = await aa(reifyXCList(fromKeys, toKeys), errorPrefix);

    if (!isValidBCO$Mode('xc', opts)) {
      throw new Error(`${errorPrefix} Invalid option "${opts}"`);
    }

    const cardResults = await bCO.xc[opts]('copy', fromLocation, list.from, toCard, list.to);

    const metaResults =
      cardResults.status === 'rejected' ?
        { value: [], reason: [] } :
        segregate(await Promise.allSettled([
          writer.metadata.addKeys(toCard, Object.keys(cardResults.value).map(i => list.to[i])),
        ]));

    return bulkResults(bulkErrorMessages.card('copy'), cardResults, metaResults);
  }

  async function move(fromLocation, fromKeys, toCard = '.', toKeys, opts = 'ALL') {
    const errorPrefix = 'Board: Unable to move:';

    if (fromLocation.path() === location.path(toCard) && !toKeys) {
      throw new Error(`${errorPrefix} Identical source and destination.`);
    }

    const list = await aa(reifyXCList(fromKeys, toKeys), errorPrefix);

    if (!isValidBCO$Mode('xc', opts)) {
      throw new Error(`${errorPrefix} Invalid option "${opts}"`);
    }

    const cardResults = await bCO.xc[opts]('move', fromLocation, list.from, toCard, list.to);

    const metaResults = await (async function modifyMetadata() {
      if (cardResults.status === 'rejected') {
        return { value: [], reason: [] };
      }
      else {
        const indeces = Object.keys(cardResults.value);
        const addToKeys = indeces.map(i => list.to[i]);
        const removeFromKeys = indeces.map(i => list.from[i]);
        const removeSourceKeys = boardMetadataWriter({
          location: fromLocation,
          rw,
          options: {
            file: metadataFile,
          },
        }).removeKeys;
        return segregate(await Promise.allSettled([
          writer.metadata.addKeys(toCard, addToKeys),
          removeSourceKeys(removeFromKeys),
        ]));
      }
    })();

    return bulkResults(bulkErrorMessages.card('move'), cardResults, metaResults);
  }

  async function remove(keyList, opts = 'ALL') {
    const errorPrefix = 'Board: Unable to remove:';

    const list = await aa(reifyList(keyList), errorPrefix);

    if (opts === 'ALL') {
      if (list.some(key => key === '' || key === '.')) {
        throw new Error(`${errorPrefix} List include root card with all contents;
You can delete the card from its parent board or container.`);
      }
    }

    if (!isValidBCO$Mode('$1args', opts)) {
      throw new Error(`${errorPrefix} Invalid option "${opts}"`);
    }

    const cardResults = await bCO.$1args[opts]('remove', list);

    const metaResults =
      cardResults.status === 'rejected' ?
        { value: [], reason: [] } :
        segregate(await Promise.allSettled([
          writer.metadata.removeKeys(Object.keys(cardResults.value).map(i => list[i])),
        ]));

    return bulkResults(bulkErrorMessages.card('remove'), cardResults, metaResults);
  }


  /* File Operations */

  async function copyFiles(fromLocation, fromItems, toCard = '.', toItems) {
    const errorPrefix = 'Board: Unable to copy items:';

    if (!fromLocation) {
      throw new Error(`${errorPrefix} No source specified`);
    }

    if (fromLocation.path() === location.path(toCard) && !toItems) {
      throw new Error(`${errorPrefix} Identical source and destination.`);
    }

    const list = await aa(reifyXCList(fromItems, toItems, errorPrefix));

    const results = await bFO.xc('copy', fromLocation, list.from, toCard, list.to);

    return bulkResults(bulkErrorMessages.file('copy'), results);
  }

  async function moveFiles(fromLocation, fromItems, toCard = '.', toItems) {
    const errorPrefix = 'Board: Unable to move items:';

    if (!fromLocation) {
      throw new Error(`${errorPrefix} No source specified`);
    }

    if (fromLocation.path() === location.path(toCard) && !toItems) {
      throw new Error(`${errorPrefix} Identical source and destination.`);
    }

    const list = await aa(reifyXCList(fromItems, toItems), errorPrefix);

    const results = await bFO.xc('move', fromLocation, list.from, toCard, list.to);

    return bulkResults(bulkErrorMessages.file('move'), results);
  }

  async function removeFiles(card, items) {
    const errorPrefix = 'Board: Unable to move items:';

    const list = await aa(reifyList(items), errorPrefix);

    const results = await bFO.$1args('remove', card, list);

    return bulkResults(bulkErrorMessages.file('remove'), results);
  }

  return Object.freeze({
    file: {
      create: writer.file.write,
      write: writer.file.write,
      createDir: writer.file.createDir,
      link: writer.file.link,
      rename: writer.file.rename,
      copy: copyFiles,
      move: moveFiles,
      remove: removeFiles,
    },

    addCard,
    replaceCard,

    start,
    cease,
    update,

    create,
    transclude,
    rename,

    copy,
    move,
    remove,
  });
};

export default boardWriter;
