/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { readFileSync } from 'fs';
import config from '@syntropize/steamroll';

const pkg = JSON.parse(readFileSync('./package.json'));

const options = {
  input: pkg.source,
  output: {
    file: pkg.main,
  },
};

export default config(options);
