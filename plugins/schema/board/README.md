# Board Schema

**Board Schema** tracks ordered cards in a resource and linked resources.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
