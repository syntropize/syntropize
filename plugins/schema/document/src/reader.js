/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import EventEmitter from 'events';
import libPath from 'path';
import settings from './defaults.json';

const documentReader = function documentReader({
  location,
  rw,
  digest,
  options = {},
}) {
  const self = {
    section: {},
    metadata: {},
  };

  const files = options.files || settings.files;

  const formats = {
    section: 'text',
    metadata: 'json',
  };

  const watcher = new EventEmitter();

  const parseItem = {
    text: function parseText(result) {
      return result.toString();
    },
    json: function parseJSON(result) {
      return JSON.parse(result);
    },
  };

  async function readItem(item, type) {
    try {
      const result = await rw.read(location, item);
      return parseItem[formats[type]](result);
    }
    catch (error) {
      if (error instanceof SyntaxError) {
        error.message = `Document: Unable to parse "${item}"
${error.message}`;
      }
      else {
        error.message = `Document: Unable to access "${item}"
${error.message}`;
      }
      throw error;
    }
  }

  async function destructurePath(item) {
    // To be activated once proper 'ready' logic is implemented
    // const libPath = await rw.path(location);
    return [libPath.dirname(item), libPath.basename(item)];
  }

  async function addItem(type, parent, item) {
    if (!Object.prototype.hasOwnProperty.call(self[type], parent)) {
      digest(function applyItemAddStart() {
        self[type][parent] = undefined;
      });
      watcher.emit(`${type} add`, parent);
    }
    else {
      watcher.emit(`${type} edit`, parent);
    }

    try {
      const result = await readItem(item, type);
      digest(function applyRecordItemAddSuccess() {
        self[type][parent] = result;
      });
      watcher.emit(`${type} readSuccess`, parent);
    }
    catch (error) {
      digest(function applyRecordItemAddFailure() {
        self[type][parent] = error;
      });
      watcher.emit(`${type} readError`, parent);
    }
  }

  const add = async function onDocumentItemAdd(item) {
    const [parent, base] = await destructurePath(item);

    Object.keys(self).some((type) => {
      if (files[type] === base) {
        addItem(type, parent, item);
        return true;
      }
      return false;
    });
  };

  async function unlinkItem(type, parent) {
    if (Object.prototype.hasOwnProperty.call(self[type], parent)) {
      digest(function applyRecordItemDelete() {
        delete self[type][parent];
      });
      watcher.emit(`${type} delete`, parent);
    }
  }

  const unlink = async function onDocumentItemUnlink(item) {
    const [parent, base] = await destructurePath(item);

    Object.keys(self).some((type) => {
      if (files[type] === base) {
        unlinkItem(type, parent);
        return true;
      }
      return false;
    });
  };

  const ready = function onDocumentReady() {
    watcher.emit('ready');
  };

  const clear = function clearDocument() {
    digest(function applyClearDocumentObjects() {
      Object.values(self).forEach(obj => (
        // eslint-disable-next-line no-param-reassign
        Object.keys(obj).forEach(key => delete obj[key])
      ));
    });
  };

  return {
    get sections() {
      return self.section;
    },
    get metadata() {
      return self.metadata;
    },
    on: Object.freeze({
      add,
      change: add,
      unlink,
      addDir: add,
      unlinkDir: unlink,
      ready,
    }),
    event: watcher,
    clear,
    options: {
      ...options,
      files,
    },
  };
};

export default documentReader;
