/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentMetadataWriter = function documentMetadataWriter({
  location,
  reader,
  rw,
  options = {},
}) {
  const metadataFile = options.file;

  // Helpers

  const {
    arrifyPath: arrify,
  } = rw.utils;

  function getWriteArguments(...args) {
    return args[1] ? args : ['.', args[0] || []];
  }

  const write = async function writeMetadata(key, metadata, flag) {
    return await rw.write(
      location,
      arrify(key, metadataFile),
      JSON.stringify(metadata, null, 2),
      { flag },
    );
  };

  const read = async function writeMetadata(key) {
    const result = await rw.read(
      location,
      arrify(key, metadataFile),
    );
    return JSON.parse(result);
  };

  const getKeys = async function getKeys(key) {
    if (reader && reader.metadata && Array.isArray(reader.metadata[key])) {
      return reader.metadata[key].slice();
    }
    else {
      return await read(key);
    }
  };


  // Document Methods

  const create = async function createMetadata(...args) {
    const [key, metadata] = getWriteArguments(...args);
    return await write(key, metadata, 'wx');
  };

  const update = async function updateMetadata(...args) {
    const [key, metadata] = getWriteArguments(...args);
    return await write(key, metadata, 'w');
  };

  const remove = async function removeMetadata(key = '.') {
    return await rw.remove(
      location,
      arrify(key, metadataFile),
    );
  };

  // Key Methods

  const addKeys = async function addDocumentMetadataKeys(...args) {
    const [section, keys] = getWriteArguments(...args);
    const modifiedKeys = await getKeys(section);
    keys.forEach((key) => {
      if (key !== '.' && !modifiedKeys.includes(key)) {
        modifiedKeys.push(key);
      }
    });
    return await update(section, modifiedKeys);
  };

  const removeKeys = async function removeDocumentMetadataKeys(...args) {
    const [section, keys] = getWriteArguments(...args);
    const modifiedKeys = await getKeys(section);
    keys.forEach((key) => {
      const index = modifiedKeys.indexOf(key);
      if (index >= 0) {
        modifiedKeys.splice(index, 1);
      }
    });
    return await update(section, modifiedKeys);
  };

  const replaceKeys = async function replaceDocumentMetadataKeys(...args) {
    const [section, keys] = getWriteArguments(...args);
    const modifiedKeys = await getKeys(section);
    Object.entries(keys).forEach(([oldkey, newKey]) => {
      const index = modifiedKeys.indexOf(oldkey);
      if (index >= 0) {
        modifiedKeys.splice(index, 1, newKey);
      }
      else {
        modifiedKeys.push(newKey);
      }
    });
    return await update(section, modifiedKeys);
  };


  return Object.freeze({
    create,
    update,
    remove,
    addKeys,
    removeKeys,
    replaceKeys,
  });
};

export default documentMetadataWriter;
