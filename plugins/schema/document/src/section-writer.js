/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
function documentSectionWriter({
  location,
  rw,
  options = {},
}) {
  const sectionFile = options.file;

  const {
    arrifyPath: arrify,
  } = rw.utils;

  const add = async function addSection(key, section) {
    return await rw.write(location, arrify(key, sectionFile), section, { flag: 'wx' });
  };

  const replace = async function replaceSection(key, section) { // TODO: Create a proper rw.replace
    return await rw.write(location, arrify(key, sectionFile), section, { flag: 'w' });
  };

  const remove = async function removeSection(section) {
    return await rw.remove(
      location,
      arrify(section, sectionFile),
    );
  };

  const copy = async function copySection(fromLocation, from, toSection = '.', to) {
    return await rw.copy(
      fromLocation,
      arrify(from, sectionFile),
      location,
      arrify(toSection, to, sectionFile),
    );
  };

  const move = async function moveSection(fromLocation, from, toSection = '.', to) {
    return await rw.move(
      fromLocation,
      arrify(from, sectionFile),
      location,
      arrify(toSection, to, sectionFile),
    );
  };


  return Object.freeze({
    add,
    replace,
    remove,
    copy,
    move,
  });
}

export default documentSectionWriter;
