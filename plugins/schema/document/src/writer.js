/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import aa from 'handle-async-await';

import documentSectionWriter from './section-writer.js';
import documentMetadataWriter from './metadata-writer.js';

import settings from './defaults.json';

function documentWriter({
  location,
  reader,
  rw,
  options = {},
}) {
  const {
    metadata: metadataFile,
    section: sectionFile,
  } = options.files || settings.files;

  const writer = {
    metadata: documentMetadataWriter({
      location,
      rw,
      reader,
      options: {
        file: metadataFile,
      },
    }),
    section: documentSectionWriter({
      location,
      rw,
      options: {
        file: sectionFile,
      },
    }),
  };

  /* Section Operations */

  async function addSection(key, section) {
    const sectionResult = await aa(
      writer.section.add(key, section),
      `Document: Unable to add section ${key}.`,
    );
    const metaResult = await aa(
      writer.metadata.addKeys([key]),
      `Document: Unable to append key to metadata after adding section ${key}.`,
    );
    return [sectionResult, metaResult];
  }

  async function replaceSection(key, section) {
    return aa(
      writer.section.replace(key, section),
      `Document: Unable to replace section ${key}.`,
    );
  }

  /* Document Operations */

  function isRootKey(key) {
    return !key || key === '.' || key === '';
  }

  async function update(key, data) {
    return aa(
      writer.metadata.update(key, data),
      `Document: ${isRootKey(key) ? 'Keys not' : `Section "${key} not`} updated.`,
    );
  }

  /* Combined Operations */

  async function create(key, section) {
    const errorPrefix = `Document: Unable to create new section ${key}.`;

    const mainResults = await Promise.all([
      aa(writer.metadata.create(key, []), `${errorPrefix}: Failed to write metadata for ${key}.`),
      aa(writer.section.add(key, section), `${errorPrefix}: Failed to write section ${key}.`),
    ]);

    const metaResults = await aa(
      writer.metadata.addKeys([key]),
      `Document: Unable to update metadata after creating section ${key}.`,
    );

    return [mainResults, metaResults];
  }

  async function transclude(key, path) {
    const mainResult = await aa(
      rw.link(location, [key], path),
      `Document: Unable to transclude section "${key}" from "${path}".`,
    );
    const metaResult = await aa(
      writer.metadata.addKeys([key]),
      `Document: Unable to append key after transcluding section "${key}".`,
    );
    return [mainResult, metaResult];
  }

  async function rename(oldKey, newKey) {
    if (oldKey === '.' || oldKey === '') {
      throw new Error('Document: Cannot rename root section.');
    }
    const mainResult = await aa(
      rw.rename(location, [oldKey], location, newKey),
      `Document: Unable to rename "${oldKey}" to "${newKey}".`,
    );
    const replacements = {};
    replacements[oldKey] = newKey;
    const metaResult = await aa(
      writer.metadata.replaceKeys(replacements),
      `Document: Unable to replace key "${newKey}" after renaming "${oldKey}".`,
    );
    return [mainResult, metaResult];
  }

  async function remove(key, opts = 'SECTION') {
    if (opts !== 'ALL' && opts !== 'SECTION') {
      throw new Error(`Document: Invalid Delete Options ${opts}`);
    }

    if (opts === 'ALL') {
      if (key === '.' || key === '') {
        throw new Error(`Document: Cannot remove root section.
        You can delete the section from its parent document or container.`);
      }
      const mainResult = await aa(
        rw.remove(location, key),
        `Document: Unable to remove section "${key}".`,
      );
      const metaResult = await aa(
        writer.metadata.removeKeys([key]),
        `Document: Unable to remove key "${key}" after removing section.`,
      );
      return [mainResult, metaResult];
    }

    const mainResult = await aa(
      writer.section.remove(key),
      `Document: Unable to remove section "${key}".`,
    );

    return [mainResult, undefined];
  }

  return Object.freeze({
    addSection,
    replaceSection,

    update,

    create,
    transclude,
    rename,
    remove,
  });
}

export default documentWriter;
