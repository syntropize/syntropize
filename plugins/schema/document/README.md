# Document Schema

**Document Schema** tracks markdown documents at a resource and linked resources.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
