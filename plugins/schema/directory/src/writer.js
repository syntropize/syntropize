/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import segregate from '@syntropize/util-pass';
import bulkResults from '@syntropize/util-bulk-schema-results';

import aa from 'handle-async-await';

const directoryWriter = function directoryWriter({
  location,
  rw,
}) {
  const {
    list: reifyList,
    xcList: reifyXCList,
  } = rw.utils.reify;

  function bulkErrorMessages(ops) {
    const errors = Object.freeze({
      ZERO: undefined,
      ONE: `Directory: Failed to ${ops} an item`,
      MANY: `Directory: Failed to ${ops} some items`,
      ALL: `Directory: Failed to ${ops} all items`,
    });

    return Object.freeze({
      ZERO: errors,
    });
  }

  const write = async function directoryWrite(file, contents, options) {
    return await rw.write(location, file, contents, options);
  };

  const createDir = async function directoryCreateDir(dir) {
    return await rw.createDir(location, dir);
  };

  const link = async function linkBoardFile(file, path) {
    return await rw.link(location, file, path);
  };

  const rename = async function directoryRename(oldItem, newItem) {
    return await rw.rename(location, oldItem, location, newItem);
  };

  const copy = async function directoryCopy(fromLocation, fromItems, toItems) {
    const errorPrefix = 'Directory: Unable to move items:';

    if (!fromLocation) {
      throw new Error(`${errorPrefix} No source specified.`);
    }
    if (fromLocation.path() === location.path() && !toItems) {
      throw new Error(`${errorPrefix} Identical source and destination.`);
    }

    const list = await aa(reifyXCList(fromItems, toItems), errorPrefix);

    const results = await Promise.allSettled(list.from.map(async (from, i) => (
      rw.copy(fromLocation, from, location, list.to[i])
    )));

    return bulkResults(bulkErrorMessages('copy'), segregate(results));
  };

  const move = async function directoryMove(fromLocation, fromItems, toItems) {
    const errorPrefix = 'Directory: Unable to move items:';

    if (!fromLocation) {
      throw new Error(`${errorPrefix} No source specified.`);
    }
    if (fromLocation.path() === location.path() && !toItems) {
      throw new Error(`${errorPrefix} Identical source and destination.`);
    }

    const list = await aa(reifyXCList(fromItems, toItems), errorPrefix);

    const results = await Promise.allSettled(list.from.map(async (from, i) => (
      rw.move(fromLocation, from, location, list.to[i])
    )));

    return bulkResults(bulkErrorMessages('move'), segregate(results));
  };


  const remove = async function directoryRemove(items) {
    const errorPrefix = 'Board: Unable to remove items';

    const list = await aa(reifyList(items), errorPrefix);

    const results = await Promise.allSettled(list.map(async item => rw.remove(location, item)));

    return bulkResults(bulkErrorMessages('remove'), segregate(results));
  };


  return Object.freeze({
    write,
    create: write,
    createDir,
    link,
    remove,
    rename,
    move,
    copy,
  });
};

export default directoryWriter;
