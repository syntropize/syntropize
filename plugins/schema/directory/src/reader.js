/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import EventEmitter from 'events';
import fs from 'fs';
import Mode from 'stat-mode';

const directoryReader = function directoryReader({
  location,
  rw,
  digest,
  options = {
    readOnly: false,
    files: {},
  },
}) {
  let files = { '.': {} };
  const directoryWatcher = new EventEmitter();

  const add = async function onDirectoryAdd(item, stat) {
    const libPath = await rw.path(location);
    const loc = libPath.dirname(item);
    const base = libPath.basename(item);
    const statObj = Object.assign(new fs.Stats(), stat);
    statObj.mode2 = new Mode(stat);

    digest(function applyDirectoryAdd() {
      if (statObj.isDirectory() && loc === '.' && base !== '') { files[base] = {}; }
      files[loc][base] = statObj;
    });
    directoryWatcher.emit('add', loc, base);
  };

  const unlink = async function onDirectoryUnlink(item) {
    const libPath = await rw.path(location);
    const loc = libPath.dirname(item);
    const base = libPath.basename(item);
    digest(function applyDirectoryDelete() {
      if (loc === '.' && Object.prototype.hasOwnProperty.call(files, base)) {
        delete files[base];
        directoryWatcher.emit('delete', loc, base);
      }
      else if (Object.prototype.hasOwnProperty.call(files, loc) &&
        Object.prototype.hasOwnProperty.call(files[loc], base)) {
        delete files[loc][base];
        directoryWatcher.emit('delete', loc, base);
      }
    });
  };

  const addDir = function onDirectoryAddDir(item, stat) {
    add(item, stat);
  };

  const unlinkDir = function onDirectoryUnlinkDir(item) {
    unlink(item);
  };

  const ready = function onDirectoryReady() {
    directoryWatcher.emit('ready');
  };

  const clear = function clearDirectory() {
    digest(function applyClearDirectoryObject() {
      files = {};
    });
  };

  return {
    get files() {
      return files;
    },
    on: Object.freeze({
      add,
      change: add,
      unlink,
      addDir,
      unlinkDir,
      ready,
    }),
    event: directoryWatcher,
    clear,
    options,
  };
};

export default directoryReader;
