# Schema Plugins

This directory contains **Schema Plugins**, that construct partial data and interaction model of a resource for consumption by a view. They serve as an abstraction between the resource representation and user representation. Each schema focuses on a paticular type of representation of linked resources; data and events from multiple schema are brought together to create a complex representation tailored to a user's requirements.
