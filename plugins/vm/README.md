# View Model Plugins

This directory contains **View Model Plugins**, that keep track of the data presentation and the state of user interaction within views. A view-model exists corresponding to every view and has access to all the schema for the given view. However, different views can reuse the same view-model.
