/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const fileEvents = function fileEvents(
  FileEventHandlerService,
) {
  const link = function linkFileEvents(Resource) {
    Resource.directory.event
      .on('add', FileEventHandlerService.add)
      .on('delete', FileEventHandlerService.del)
    ;
  };

  const unlink = function unlinkFileEvents(Resource) {
    Resource.directory.event
      .removeListener('add', FileEventHandlerService.add)
      .removeListener('delete', FileEventHandlerService.del)
    ;
  };

  return Object.freeze({
    link,
    unlink,
  });
};

fileEvents.$inject = [
  'FileEventHandlerService',
];

export default fileEvents;
