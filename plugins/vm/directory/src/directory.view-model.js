/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const directorySetup = function directorySetup(
  DirectorySelectionStructor,
  FileEvents,
) {
  const setup = function boardConstructor() {
    DirectorySelectionStructor.setup();
  };

  const initialize = function initializeBoardOnReady(Resource) {
    FileEvents.link(Resource);
  };

  const teardown = function boardDestructor(Resource) {
    DirectorySelectionStructor.teardown();

    FileEvents.unlink(Resource);
  };

  return Object.freeze({
    setup,
    initialize,
    teardown,
  });
};

directorySetup.$inject = [
  'DirectorySelectionStructor',
  'FileEvents',
];

export default directorySetup;
