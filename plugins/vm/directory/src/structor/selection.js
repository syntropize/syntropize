/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const directorySelectionStructor = function directorySelectionStructorService(
  SelectionService,
  DirectorySelection,
) {
  const setup = function setupDirectorySelection() {
    DirectorySelection.files = SelectionService.init();
  };

  const teardown = function teardownDirectorySelection() {
    DirectorySelection.files = undefined;
  };

  return Object.freeze({
    setup,
    teardown,
  });
};

directorySelectionStructor.$inject = [
  'SelectionService',
  'DirectorySelection',
];

export default directorySelectionStructor;
