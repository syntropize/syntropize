/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import directorySelectionStructor  from './structor/selection.js';

import fileEventHandlerService from './event-handlers/files.js';
import fileEvents from './events/files.js';

import directoryViewModel from './directory.view-model.js';

export default angular.module('vm.directory', [])
  .value('DirectorySelection', {})
  .factory('DirectorySelectionStructor', directorySelectionStructor)

  .factory('FileEventHandlerService', fileEventHandlerService)
  .factory('FileEvents', fileEvents)

  .factory('VM$directory', directoryViewModel)
;
