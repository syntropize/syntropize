# Directory View-Model

**Directory View-Model** tracks the view state when users interact with the contents at a resource.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
