/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentDetectService = function documentDetectService(
  $q,
  $log,
  $rootScope,
) {
  const promiseMessage = function promiseStateMessage(readyState) {
    switch (readyState) {
      case -1:
        return 'before Resource is ready';
      case 1:
        return 'Resource is already ready';
      default:
        return 'Resource ready event';
    }
  };

  return function documentDetection(Resource) {
    return $q(function documentDetectionPromise(resolve, reject) {
      let removeListeners = () => {};

      const waitForMetadata = function resolveIfRootDirectoryMetadataExists(key) {
        if (key === '.') {
          removeListeners();
          $log.debug('Metadata set - Metadata event before ready');
          return resolve();
        }
        return undefined;
      };

      const testForMetadata = function testForMetadataInDataModel(readyState) {
        if (Object.prototype.hasOwnProperty.call(Resource.document.metadata, '.')) {
          $log.debug(`Metadata set - ${promiseMessage(readyState)}`);
          return resolve();
        }
        return false;
      };

      removeListeners = function removeDetectEventListeners() {
        Resource.document.event.removeListener('metadata new', waitForMetadata);
        Resource.document.event.removeListener('ready', testForMetadata);
      };

      const finalTestForMetadata = function finalTestForMetadataAfterReady(readyState) {
        if (!readyState) removeListeners();
        $rootScope.$evalAsync(function _finalTestForMetadataAfterReadyConfirm() {
          if (!testForMetadata(readyState)) {
            const error = new Error(`No Metadata - ${promiseMessage(readyState)}`);
            return reject(error);
          }
          return undefined;
        });
      };

      if (Resource.socket.ready) {
        finalTestForMetadata(1);
      }
      else {
        testForMetadata(-1);
        Resource.document.event.on('metadata new', waitForMetadata);
        Resource.document.event.on('ready', finalTestForMetadata);
      }
    });
  };
};

documentDetectService.$inject = [
  '$q',
  '$log',
  '$rootScope',
];

export default documentDetectService;
