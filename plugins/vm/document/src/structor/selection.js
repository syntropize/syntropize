/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentSelectionStructor = function DocumentSelectionStructorService(
  SelectionService,
  DocumentSelection,
) {
  const setup = function setupDocumentSelection() {
    DocumentSelection.sections = SelectionService.init();
  };

  const teardown = function teardownDocumentSelection() {
    DocumentSelection.sections = undefined;
  };

  return Object.freeze({
    setup,
    teardown,
  });
};

documentSelectionStructor.$inject = [
  'SelectionService',
  'DocumentSelection',
];

export default documentSelectionStructor;
