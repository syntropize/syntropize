/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentMetadataStatusStructor = function documentMetadataStatusStructorService(
  DocumentMetadataStatus,
) {
  const setup = function setupDocumentMetadataStatus() {
    DocumentMetadataStatus.readStatus = new Map();
  };

  const teardown = function teardownDocumentMetadataStatus() {
    DocumentMetadataStatus.readStatus = undefined;
  };

  return Object.freeze({
    setup,
    teardown,
  });
};

documentMetadataStatusStructor.$inject = [
  'DocumentMetadataStatus',
];

export default documentMetadataStatusStructor;
