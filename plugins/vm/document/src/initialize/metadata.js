/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentMetadataInitialize = function documentMetadataInitialize(
  DocumentMetadataEventHandlerService,
) {
  return function documentMetadataInitializeOnReady(Resource) {
    // Adding all section keys upon ready event
    Object.entries(Resource.document.metadata)
      .forEach(([key, value]) => {
        DocumentMetadataEventHandlerService.add(key);
        if (value) {
          if (value instanceof Error) {
            DocumentMetadataEventHandlerService.error(key);
          }
          else {
            DocumentMetadataEventHandlerService.success(key);
          }
        }
      });
  };
};

documentMetadataInitialize.$inject = [
  'DocumentMetadataEventHandlerService',
];

export default documentMetadataInitialize;
