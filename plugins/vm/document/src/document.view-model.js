/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentViewModel = function documentViewModel(
  DocumentDetectService,
  DocumentSelectionStructor,
  // DocumentSearchStructor,
  // DocumentSortStructor,
  SectionDisplayStructor,
  DocumentSectionStatusStructor,
  DocumentMetadataStatusStructor,
  DocumentSectionEvents,
  DocumentMetadataEvents,
  DocumentSectionInitialize,
  DocumentMetadataInitialize,
) {
  const detect = DocumentDetectService;

  const setup = function documentConstructor() {
    DocumentSelectionStructor.setup();
    SectionDisplayStructor.setup();
    DocumentSectionStatusStructor.setup();
    DocumentMetadataStatusStructor.setup();

    // DocumentSearchStructor.setup();
    // DocumentSortStructor.setup();
  };

  const teardown = function documentDestructor(Resource) {
    DocumentSelectionStructor.teardown();
    SectionDisplayStructor.teardown();
    DocumentSectionStatusStructor.teardown();
    DocumentMetadataStatusStructor.teardown();

    DocumentSectionEvents.unlink(Resource);
    DocumentMetadataEvents.unlink(Resource);
  };

  const initialize = function initializeDocumentOnReady(Resource) {
    DocumentSectionEvents.link(Resource);
    DocumentMetadataEvents.link(Resource);
    DocumentSectionInitialize(Resource);
    DocumentMetadataInitialize(Resource);
  };

  return Object.freeze({
    detect,
    setup,
    initialize,
    teardown,
  });
};

documentViewModel.$inject = [
  'DocumentDetectService',
  'DocumentSelectionStructor',
  // 'DocumentSearchStructor',
  // 'DocumentSortStructor',
  'SectionDisplayStructor',
  'DocumentSectionStatusStructor',
  'DocumentMetadataStatusStructor',
  'DocumentSectionEvents',
  'DocumentMetadataEvents',
  'DocumentSectionInitialize',
  'DocumentMetadataInitialize',
];

export default documentViewModel;
