/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentSectionEventHandlerService = function documentSectionEventHandlerService(
  DocumentSectionStatus,
  $rootScope,
) {
  const add = function addCardProperties(key) {
    $rootScope.$applyAsync(function _addCardProperties() {
      DocumentSectionStatus.readStatus.set(key, 'loading');
    });
  };

  const edit = function editCardProperties(key) {
    $rootScope.$applyAsync(function _editCardProperties() {
      DocumentSectionStatus.readStatus.set(key, 'updating');
    });
  };

  const del = function delCardProperties(key) {
    $rootScope.$applyAsync(function _delCardProperties() {
      DocumentSectionStatus.readStatus.delete(key);
    });
  };

  const success = function readCardSuccess(key) {
    $rootScope.$applyAsync(function _readCardSuccess() {
      DocumentSectionStatus.readStatus.set(key, 'success');
    });
  };

  const error = function notifyCardError(key) {
    $rootScope.$applyAsync(function _notifyCardError() {
      DocumentSectionStatus.readStatus.set(key, 'error');
    });
  };

  return Object.freeze({
    add,
    edit,
    del,
    success,
    error,
  });
};

documentSectionEventHandlerService.$inject = [
  'DocumentSectionStatus',
  '$rootScope',
];

export default documentSectionEventHandlerService;
