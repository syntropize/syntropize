/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentMetadataEventHandlerService = function documentMetadataEventHandlerService(
  DocumentMetadataStatus,
  $rootScope,
) {
  const add = function addDocumentMetadataProperties(key) {
    $rootScope.$applyAsync(function _notifyDocumentMetadataProperties() {
      DocumentMetadataStatus.readStatus.set(key, 'loading');
    });
  };

  const edit = function editDocumentMetadataProperties(key) {
    $rootScope.$applyAsync(function _editDocumentMetadataProperties() {
      DocumentMetadataStatus.readStatus.set(key, 'updating');
    });
  };

  const del = function delDocumentMetadataProperties(key) {
    $rootScope.$applyAsync(function _delDocumentMetadataProperties() {
      DocumentMetadataStatus.readStatus.delete(key);
    });
  };

  const success = function readDocumentMetadataSuccess(key) {
    $rootScope.$applyAsync(function _readDocumentMetadataSuccess() {
      DocumentMetadataStatus.readStatus.set(key, 'success');
    });
  };

  const error = function notifyDocumentMetadataError(key) {
    $rootScope.$applyAsync(function _notifyDocumentMetadataError() {
      DocumentMetadataStatus.readStatus.set(key, 'error');
    });
  };

  return Object.freeze({
    add,
    edit,
    del,
    success,
    error,
  });
};

documentMetadataEventHandlerService.$inject = [
  'DocumentMetadataStatus',
  '$rootScope',
];

export default documentMetadataEventHandlerService;
