/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentSectionEvents = function documentSectionEvents(
  DocumentSectionEventHandlerService,
) {
  const link = function linkDocumentSectionEvents(Resource) {
    Resource.document.event
      .on('section add', DocumentSectionEventHandlerService.add)
      .on('section edit', DocumentSectionEventHandlerService.edit)
      .on('section delete', DocumentSectionEventHandlerService.del)

      .on('section readSuccess', DocumentSectionEventHandlerService.success)
      .on('section readError', DocumentSectionEventHandlerService.error)
    ;
  };

  const unlink = function unlinkDocumentSectionEvents(Resource) {
    Resource.document.event
      .removeListener('section add', DocumentSectionEventHandlerService.add)
      .removeListener('section edit', DocumentSectionEventHandlerService.edit)
      .removeListener('section delete', DocumentSectionEventHandlerService.del)

      .removeListener('section readSuccess', DocumentSectionEventHandlerService.success)
      .removeListener('section readError', DocumentSectionEventHandlerService.error)
    ;
  };

  return Object.freeze({
    link,
    unlink,
  });
};

documentSectionEvents.$inject = [
  'DocumentSectionEventHandlerService',
];

export default documentSectionEvents;
