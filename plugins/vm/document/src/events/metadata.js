/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const DocumentMetadataEvents = function DocumentMetadataEvents(
  DocumentMetadataEventHandlerService,
) {
  const link = function linkDocumentMetadataEvents(Resource) {
    Resource.document.event
      .on('metadata add', DocumentMetadataEventHandlerService.add)
      .on('metadata edit', DocumentMetadataEventHandlerService.edit)
      .on('metadata delete', DocumentMetadataEventHandlerService.del)

      .on('metadata readSuccess', DocumentMetadataEventHandlerService.success)
      .on('metadata readError', DocumentMetadataEventHandlerService.error)
    ;
  };

  const unlink = function unlinkDocumentMetadataEvents(Resource) {
    Resource.document.event
      .removeListener('metadata add', DocumentMetadataEventHandlerService.add)
      .removeListener('metadata edit', DocumentMetadataEventHandlerService.edit)
      .removeListener('metadata delete', DocumentMetadataEventHandlerService.del)

      .removeListener('metadata readSuccess', DocumentMetadataEventHandlerService.success)
      .removeListener('metadata readError', DocumentMetadataEventHandlerService.error)
    ;
  };

  return Object.freeze({
    link,
    unlink,
  });
};

DocumentMetadataEvents.$inject = [
  'DocumentMetadataEventHandlerService',
];

export default DocumentMetadataEvents;
