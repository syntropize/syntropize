/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import sectionDisplay from '@syntropize/ui-section-display';
// import documentSearch from '@syntropize/ui-document-search';
// import documentSort from '@syntropize/ui-document-sort';

import documentSelectionStructor      from './structor/selection.js';
import documentSectionStatusStructor  from './structor/section-status.js';
import documentMetadataStatusStructor from './structor/metadata-status.js';

import documentDetectService  from './detect/document.js';

import documentSectionEventHandlerService   from './event-handlers/sections.js';
import documentMetadataEventHandlerService  from './event-handlers/metadata.js';

import documentSectionEvents                from './events/sections.js';
import documentMetadataEvents               from './events/metadata.js';

import documentSectionInitialize            from './initialize/sections.js';
import documentMetadataInitialize           from './initialize/metadata.js';

import documentViewModel from './document.view-model.js';

const dependencies = [
  // documentSearch,
  // documentSort,
  sectionDisplay,
];

export default angular.module('vm.document', dependencies.map(module => module.name))
  .value('DocumentSelection', {})
  .value('DocumentSectionStatus', {})
  .value('DocumentMetadataStatus', {})

  .factory('DocumentSelectionStructor', documentSelectionStructor)
  .factory('DocumentSectionStatusStructor', documentSectionStatusStructor)
  .factory('DocumentMetadataStatusStructor', documentMetadataStatusStructor)

  .factory('DocumentDetectService', documentDetectService)

  .factory('DocumentSectionEventHandlerService', documentSectionEventHandlerService)
  .factory('DocumentMetadataEventHandlerService', documentMetadataEventHandlerService)

  .factory('DocumentSectionEvents', documentSectionEvents)
  .factory('DocumentMetadataEvents', documentMetadataEvents)

  .factory('DocumentSectionInitialize', documentSectionInitialize)
  .factory('DocumentMetadataInitialize', documentMetadataInitialize)

  .factory('VM$document', documentViewModel)
;
