/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import boardSearch from '@syntropize/ui-board-search';
import boardSort from '@syntropize/ui-board-sort';
import cardDisplay from '@syntropize/ui-card-display';

import boardSelectionStructor  from './structor/selection.js';
import cardStatusStructor      from './structor/card-status.js';
import metadataStatusStructor  from './structor/metadata-status.js';

import boardDetectService  from './detect/board.js';

import cardFileEventHandlerService  from './event-handlers/files.js';
import cardEventHandlerService      from './event-handlers/cards.js';
import metadataEventHandlerService  from './event-handlers/metadata.js';

import cardFileEvents               from './events/files.js';
import cardEvents                   from './events/cards.js';
import metadataEvents               from './events/metadata.js';

import cardInitialize               from './initialize/cards.js';
import metadataInitialize           from './initialize/metadata.js';

import boardViewModel from './board.view-model.js';

const dependencies = [
  boardSearch,
  boardSort,
  cardDisplay,
];

export default angular.module('vm.board', dependencies.map(module => module.name))
  .value('BoardSelection', {})
  .value('CardStatus', {})
  .value('MetadataStatus', {})

  .factory('BoardSelectionStructor', boardSelectionStructor)
  .factory('CardStatusStructor', cardStatusStructor)
  .factory('MetadataStatusStructor', metadataStatusStructor)

  .factory('BoardDetectService', boardDetectService)

  .factory('CardFileEventHandlerService', cardFileEventHandlerService)
  .factory('CardEventHandlerService', cardEventHandlerService)
  .factory('MetadataEventHandlerService', metadataEventHandlerService)

  .factory('CardFileEvents', cardFileEvents)
  .factory('CardEvents', cardEvents)
  .factory('MetadataEvents', metadataEvents)

  .factory('CardInitialize', cardInitialize)
  .factory('MetadataInitialize', metadataInitialize)

  .factory('VM$board', boardViewModel)
;
