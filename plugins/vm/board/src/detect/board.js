/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardDetectService = function boardDetectService(
  $q,
  $log,
  $rootScope,
) {
  const promiseMessage = function promiseStateMessage(readyState) {
    switch (readyState) {
      case -1:
        return 'before Resource is ready';
      case 1:
        return 'Resource is already ready';
      default:
        return 'Resource ready event';
    }
  };

  return function boardDetection(Resource) {
    return $q(function boardDetectionPromise(resolve, reject) {
      let removeListeners;

      const waitForMetadata = function resolveIfRootDirectoryMetadataExists(key) {
        if (key === '.') {
          removeListeners();
          $log.debug('Metadata set - Metadata event before ready');
          return resolve();
        }
        return undefined;
      };

      const testForMetadata = function testForMetadataInDataModel(readyState) {
        if (Object.prototype.hasOwnProperty.call(Resource.board.metadata, '.')) {
          $log.debug(`Metadata set - ${promiseMessage(readyState)}`);
          return resolve();
        }
        return false;
      };

      removeListeners = function removeDetectEventListeners() {
        Resource.board.event.removeListener('metadata new', waitForMetadata);
        Resource.board.event.removeListener('ready', testForMetadata);
      };

      const finalTestForMetadata = function finalTestForMetadataAfterReady(readyState) {
        if (!readyState) removeListeners();
        $rootScope.$evalAsync(function _finalTestForMetadataAfterReadyConfirm() {
          if (!testForMetadata(readyState)) {
            const error = new Error(`No Metadata - ${promiseMessage(readyState)}`);
            return reject(error);
          }
          return undefined;
        });
      };

      if (Resource.socket.ready) {
        finalTestForMetadata(1);
      }
      else {
        testForMetadata(-1);
        Resource.board.event.on('metadata new', waitForMetadata);
        Resource.board.event.on('ready', finalTestForMetadata);
      }
    });
  };
};

boardDetectService.$inject = [
  '$q',
  '$log',
  '$rootScope',
];

export default boardDetectService;
