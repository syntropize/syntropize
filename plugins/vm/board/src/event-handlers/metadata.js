/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const metadataEventHandlerService = function metadataEventHandlerService(
  MetadataStatus,
  $rootScope,
) {
  const add = function addMetadataProperties(key) {
    $rootScope.$applyAsync(function _notifyMetadataProperties() {
      MetadataStatus.readStatus.set(key, 'loading');
    });
  };

  const edit = function editMetadataProperties(key) {
    $rootScope.$applyAsync(function _editMetadataProperties() {
      MetadataStatus.readStatus.set(key, 'updating');
    });
  };

  const del = function delMetadataProperties(key) {
    $rootScope.$applyAsync(function _delMetadataProperties() {
      MetadataStatus.readStatus.delete(key);
    });
  };

  const success = function readMetadataSuccess(key) {
    $rootScope.$applyAsync(function _readMetadataSuccess() {
      MetadataStatus.readStatus.set(key, 'success');
    });
  };

  const error = function notifyMetadataError(key) {
    $rootScope.$applyAsync(function _notifyMetadataError() {
      MetadataStatus.readStatus.set(key, 'error');
    });
  };

  return Object.freeze({
    add,
    edit,
    del,
    success,
    error,
  });
};

metadataEventHandlerService.$inject = [
  'MetadataStatus',
  '$rootScope',
];

export default metadataEventHandlerService;
