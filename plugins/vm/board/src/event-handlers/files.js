/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardFileEventHandlerService = function cardFileEventHandlerService(
  BoardSelection,
  $rootScope,
) {
  const add = function addFileProperties() {};

  const del = function delFileProperties(loc, name) {
    $rootScope.$applyAsync(function _delFileProperties() {
      if (BoardSelection.files[loc]) { BoardSelection.files[loc].unselectItem(name); }
    });
  };

  return Object.freeze({
    add,
    del,
  });
};

cardFileEventHandlerService.$inject = [
  'BoardSelection',
  '$rootScope',
];

export default cardFileEventHandlerService;
