/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardEventHandlerService = function cardEventHandlerService(
  CardDisplayStructor,
  BoardSelectionStructor,
  BoardSelection,
  CardStatus,
  $rootScope,
) {
  const add = function addCardProperties(key) {
    $rootScope.$applyAsync(function _addCardProperties() {
      CardDisplayStructor.add(key);
      BoardSelectionStructor.add(key);
      CardStatus.readStatus.set(key, 'loading');
    });
  };

  const edit = function editCardProperties(key) {
    $rootScope.$applyAsync(function _editCardProperties() {
      CardStatus.readStatus.set(key, 'updating');
    });
  };

  const del = function delCardProperties(key) {
    $rootScope.$applyAsync(function _delCardProperties() {
      CardDisplayStructor.remove(key);
      BoardSelection.cards.unselectItem(key);
      BoardSelectionStructor.remove(key);
      CardStatus.readStatus.delete(key);
    });
  };

  const success = function readCardSuccess(key) {
    $rootScope.$applyAsync(function _readCardSuccess() {
      CardStatus.readStatus.set(key, 'success');
    });
  };

  const error = function notifyCardError(key) {
    $rootScope.$applyAsync(function _notifyCardError() {
      CardStatus.readStatus.set(key, 'error');
    });
  };

  return Object.freeze({
    add,
    edit,
    del,
    success,
    error,
  });
};

cardEventHandlerService.$inject = [
  'CardDisplayStructor',
  'BoardSelectionStructor',
  'BoardSelection',
  'CardStatus',
  '$rootScope',
];

export default cardEventHandlerService;
