/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardEvents = function cardEvents(
  CardEventHandlerService,
) {
  const link = function linkCardEvents(Resource) {
    Resource.board.event
      .on('card add', CardEventHandlerService.add)
      .on('card edit', CardEventHandlerService.edit)
      .on('card delete', CardEventHandlerService.del)

      .on('card readSuccess', CardEventHandlerService.success)
      .on('card readError', CardEventHandlerService.error)
    ;
  };

  const unlink = function unlinkCardEvents(Resource) {
    Resource.board.event
      .removeListener('card add', CardEventHandlerService.add)
      .removeListener('card edit', CardEventHandlerService.edit)
      .removeListener('card delete', CardEventHandlerService.del)

      .removeListener('card readSuccess', CardEventHandlerService.success)
      .removeListener('card readError', CardEventHandlerService.error)
    ;
  };

  return Object.freeze({
    link,
    unlink,
  });
};

cardEvents.$inject = [
  'CardEventHandlerService',
];

export default cardEvents;
