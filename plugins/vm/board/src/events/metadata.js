/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const MetadataEvents = function MetadataEvents(
  MetadataEventHandlerService,
) {
  const link = function linkMetadataEvents(Resource) {
    Resource.board.event
      .on('metadata add', MetadataEventHandlerService.add)
      .on('metadata edit', MetadataEventHandlerService.edit)
      .on('metadata delete', MetadataEventHandlerService.del)

      .on('metadata readSuccess', MetadataEventHandlerService.success)
      .on('metadata readError', MetadataEventHandlerService.error)
    ;
  };

  const unlink = function unlinkMetadataEvents(Resource) {
    Resource.board.event
      .removeListener('metadata add', MetadataEventHandlerService.add)
      .removeListener('metadata edit', MetadataEventHandlerService.edit)
      .removeListener('metadata delete', MetadataEventHandlerService.del)

      .removeListener('metadata readSuccess', MetadataEventHandlerService.success)
      .removeListener('metadata readError', MetadataEventHandlerService.error)
    ;
  };

  return Object.freeze({
    link,
    unlink,
  });
};

MetadataEvents.$inject = [
  'MetadataEventHandlerService',
];

export default MetadataEvents;
