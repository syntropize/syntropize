/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardViewModel = function boardViewModel(
  BoardDetectService,
  CardDisplayStructor,
  BoardSelectionStructor,
  BoardSearchStructor,
  BoardSortStructor,
  CardStatusStructor,
  MetadataStatusStructor,
  CardFileEvents,
  CardEvents,
  MetadataEvents,
  CardInitialize,
  MetadataInitialize,
) {
  const detect = BoardDetectService;

  const setup = function boardConstructor() {
    CardDisplayStructor.setup();
    BoardSelectionStructor.setup();
    CardStatusStructor.setup();
    MetadataStatusStructor.setup();

    BoardSearchStructor.setup();
    BoardSortStructor.setup();
  };

  const teardown = function boardDestructor(Resource) {
    CardDisplayStructor.teardown();
    BoardSelectionStructor.teardown();
    CardStatusStructor.teardown();
    MetadataStatusStructor.teardown();

    CardFileEvents.unlink(Resource);
    CardEvents.unlink(Resource);
    MetadataEvents.unlink(Resource);
  };

  const initialize = function initializeBoardOnReady(Resource) {
    CardFileEvents.link(Resource);
    CardEvents.link(Resource);
    MetadataEvents.link(Resource);
    CardInitialize(Resource);
    MetadataInitialize(Resource);
  };

  return Object.freeze({
    detect,
    setup,
    initialize,
    teardown,
  });
};

boardViewModel.$inject = [
  'BoardDetectService',
  'CardDisplayStructor',
  'BoardSelectionStructor',
  'BoardSearchStructor',
  'BoardSortStructor',
  'CardStatusStructor',
  'MetadataStatusStructor',
  'CardFileEvents',
  'CardEvents',
  'MetadataEvents',
  'CardInitialize',
  'MetadataInitialize',
];

export default boardViewModel;
