/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardInitialize = function cardInitialize(
  CardEventHandlerService,
) {
  return function cardInitializeOnReady(Resource) {
    // Adding all card keys upon ready event
    Object.entries(Resource.board.cards)
      .forEach(([key, value]) => {
        CardEventHandlerService.add(key);
        if (value) {
          if (value instanceof Error) {
            CardEventHandlerService.error(key);
          }
          else {
            CardEventHandlerService.success(key);
          }
        }
      });
  };
};

cardInitialize.$inject = [
  'CardEventHandlerService',
];

export default cardInitialize;
