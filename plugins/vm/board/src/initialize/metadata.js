/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const metadataInitialize = function metadataInitialize(
  MetadataEventHandlerService,
) {
  return function metadataInitializeOnReady(Resource) {
    // Adding all card keys upon ready event
    Object.entries(Resource.board.metadata)
      .forEach(([key, value]) => {
        MetadataEventHandlerService.add(key);
        if (value) {
          if (value instanceof Error) {
            MetadataEventHandlerService.error(key);
          }
          else {
            MetadataEventHandlerService.success(key);
          }
        }
      });
  };
};

metadataInitialize.$inject = [
  'MetadataEventHandlerService',
];

export default metadataInitialize;
