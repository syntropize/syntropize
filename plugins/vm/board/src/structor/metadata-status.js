/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const metadataStatusStructor = function metadataStatusStructorService(
  MetadataStatus,
) {
  const setup = function setupMetadataStatus() {
    MetadataStatus.readStatus = new Map();
  };

  const teardown = function teardownMetadataStatus() {
    MetadataStatus.readStatus = undefined;
  };

  return Object.freeze({
    setup,
    teardown,
  });
};

metadataStatusStructor.$inject = [
  'MetadataStatus',
];

export default metadataStatusStructor;
