/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardSelectionStructor = function BoardSelectionStructorService(
  SelectionService,
  BoardSelection,
) {
  const setup = function setupBoardSelection() {
    BoardSelection.files = {};
    BoardSelection.cards = SelectionService.init();
  };

  const add = function addItemToBoardSelection(key) {
    BoardSelection.files[key] = SelectionService.init();
  };

  const remove = function removeItemFromBoardSelection(key) {
    delete BoardSelection.files[key];
  };

  const teardown = function teardownBoardSelection() {
    BoardSelection.files = undefined;
    BoardSelection.cards = undefined;
  };

  return Object.freeze({
    setup,
    teardown,
    add,
    remove,
  });
};

boardSelectionStructor.$inject = [
  'SelectionService',
  'BoardSelection',
];

export default boardSelectionStructor;
