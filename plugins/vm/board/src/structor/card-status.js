/*!
 *  Copyright (c) 2016, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardStatusStructor = function cardStatusStructorService(
  CardStatus,
) {
  const setup = function setupCardStatus() {
    CardStatus.readStatus = new Map();
  };

  const teardown = function teardownCardStatus() {
    CardStatus.readStatus = undefined;
  };

  return Object.freeze({
    setup,
    teardown,
  });
};

cardStatusStructor.$inject = [
  'CardStatus',
];

export default cardStatusStructor;
