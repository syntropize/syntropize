# Board View-Model

**Board View-Model** tracks the view state when users interact with cards at a resource.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
