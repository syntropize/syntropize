# File Origin

The **File Origin** plugin allows you to access resources on your file-system.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
