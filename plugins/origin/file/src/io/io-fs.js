/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import fs from 'fs';
import util from 'util';
import EventEmitter from 'events';

import watch from './watcher.js';

const Socket = function Socket(location) {
  this.location = location;
  this.connected = false;
  this.watcher = undefined;
  this.io = {
    skipReconnect: false,
  };
  EventEmitter.call(this);
};

util.inherits(Socket, EventEmitter);

const connectSocket = function connectSocket() {
  const socket = this;
  if (socket.connected) return;

  const connection = function connection() {
    try {
      const result = fs.lstatSync(socket.location);
      if (result.isDirectory() || result.isSymbolicLink()) {
        socket.watcher = watch(socket);
        socket.emit('connect');
        socket.emit('connection');
        socket.on('forceDisconnect', function onForceDisconnect() {
          socket.watcher.close();
        });
      }
      else if (result.isFile()) {
        socket.emit('connect_error', new Error('Resource is a file'));
      }
      else {
        socket.emit('connect_error', new TypeError('Invalid resource type'));
      }
    }
    catch (error) {
      if (error.code === 'ENOENT') {
        error.message = `Resource does not exist: ${error.message}`;
      }
      if (error.code === 'EPERM' || error.code === 'EACCES') {
        error.message = `Access not authorized: ${error.message}`;
      }
      socket.emit('connect_error', error);
    }
  };

  process.nextTick(connection);
};

const disconnectSocket = function disconnectSocket() {
  const socket = this;
  if (socket.watcher) { socket.watcher.close(); }
  socket.connected = false;
  socket.emit('close');
  socket.emit('disconnect');
};

Socket.prototype.open = connectSocket;
Socket.prototype.connect = connectSocket;

Socket.prototype.close = disconnectSocket;
Socket.prototype.disconnect = disconnectSocket;

const io = function io(location) {
  return new Socket(location);
};

export default io;
export { io as connect };
