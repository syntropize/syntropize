/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import libPath from 'path';
import libUrl from 'url';

// Meant for local paths on Windows
const osPath = function osPath(path) {
  const newPath =
    libPath.sep === '\\' && /^\/\w:/.test(path) ?
      path.slice(1) :
      path;
  return libPath.normalize(newPath);
};

const parse = function parseUrl(address, options = {}) {
  const urlObj = libUrl.parse(address);

  Object.assign(urlObj, options);

  return Object.freeze({
    scheme: urlObj.protocol.split(':', 1)[0],
    origin: 'file://',
    $pathname: osPath(decodeURI(urlObj.pathname)),
    $root: '',
    ...urlObj,
  });
};

const resolve = function resolvePath(root, ...paths) {
  return libPath.join(root, ...paths);
};

export default {
  parse,
  resolve,
};
