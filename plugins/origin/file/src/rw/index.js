/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import libPath from 'path';
import fs from 'fs-extra';

const path = libPath.sep === '/' ? libPath.posix : libPath.win32;
path.combine = path.join;

async function link(tgt, src) {
  const stats = await fs.stat(src);
  if (stats.isDirectory()) {
    return await fs.ensureSymlink(src, tgt, 'junction');
  }
  else if (stats.isFile()) {
    return await fs.ensureLink(src, tgt);
  }
  else {
    throw new Error(`Cannot link ${src}`);
  }
}

async function move(src, des, options = {}) {
  return fs.move(src, des, options);
}

async function copy(src, des, options = {}) {
  return fs.copy(src, des, options);
}

const rw = Object.freeze({
  path,
  read: fs.readFile,
  readDir: fs.readdir,
  write: fs.outputFile,
  link,
  createDir: fs.ensureDir,

  remove: fs.remove,
  delete: fs.remove,

  rename: move,

  move,
  copy,
});

export default rw;
