# Solid Origin

The **Solid Origin** plugin allow you to access resources on a [Solid](https://solidproject.org/) server over HTTPS.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
