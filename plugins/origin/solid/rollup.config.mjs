/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import { readFileSync } from 'fs';
import config from '@syntropize/steamroll';

const pkg = JSON.parse(readFileSync('./package.json'));

const onwarn = (warning, rollupWarn) => {
  const circularErrorPackages = [
    '@solid/oidc-rp/src/RelyingParty.js',
  ];

  // supress circular dependency error for known modules
  if (warning.code === 'CIRCULAR_DEPENDENCY') {
    if (circularErrorPackages.some(p => !warning.importer.includes(p))) {
      return;
    }
  }
  rollupWarn(warning);
};

const defaultRequireRe = /solid-rest[\/|\\]src[\/|\\]localStorage\.js$/;

const options = {
  input: pkg.source,
  onwarn,
  pluginOptions: {
    modify: {
      // Fixes in libs
      'queryForFirstResult = ': 'var queryForFirstResult = ',
      // Fixes in lib: solid-rest
      'alert = (msg) =>': 'global.alert = (msg) =>',
      'localStorage = {': 'global.localStorage = {',
      // https://github.com/rollup/rollup/issues/1507 // TODO
      'readable-stream': 'stream',
    },
    shim: {
      'whatwg-url': 'const { URL } = window; export { URL }; export default { URL };',
      '@sinonjs/text-encoding': 'const { TextEncoder } = window; export { TextEncoder }; export default { TextEncoder };',
    },
    alias: {
      entries: [
        {
          find: /^isomorphic-webcrypto$/,
          replacement: 'isomorphic-webcrypto/src/browser.js',
        },
        {
          find: /^node-fetch$/,
          replacement: 'node-fetch/lib/index.js',
        },
      ],
    },
    legacy: {
      'node_modules/.pnpm/solid-rest@1.2.9/node_modules/solid-rest/src/localStorage.js': 'SolidLocalStorage',
    },
    externalGlobals: {
      'isomorphic-ws': 'WebSocket',
      // 'node-fetch': 'fetch',
      // 'cross-fetch': 'fetch',
    },
    commonjs: {
      ignore: ['supports-color', 'encoding'],
      ignoreTryCatch: 'remove',
      requireReturnsDefault(id) {
        return defaultRequireRe.test(id);
      },
    },
  },
  output: {
    file: pkg.main,
  },
};

if (process.env.NODE_ENV === 'production') {
  options.pluginOptions.strip = {
    // Solid file client unfortunately defines debug as devDependency
    include: 'solid-file-client/**',
    functions: ['debug', 'debug.*'],
  };
}

export default config(options);
