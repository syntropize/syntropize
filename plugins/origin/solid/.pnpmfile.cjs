/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */

/* eslint-disable no-param-reassign */
function readPackage(pkg) {
  if (pkg.name === 'solid-file-client') {
    pkg.dependencies.debug = '^4.1.1';
    pkg.main = pkg.module;
    pkg.browser = pkg.module;
  }
  if (pkg.name === 'solid-auth-cli') {
    pkg.dependencies['solid-rest'] = '~1.2.0';
  }
  if (pkg.name === 'react-native-securerandom' && pkg.peerDependencies) {
    delete pkg.peerDependencies['react-native'];
  }
  if (pkg.name === '@unimodules/react-native-adapter' && pkg.peerDependencies) {
    delete pkg.peerDependencies['react-native'];
    delete pkg.peerDependencies['react-native-web'];
  }
  return pkg;
}

module.exports = {
  hooks: {
    readPackage,
  },
};
