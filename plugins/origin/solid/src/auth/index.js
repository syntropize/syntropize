/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import auth from './auth.js';

async function login(credentials = {}) {
  try {
    if (!await auth.currentSession()) {
      await auth.login(credentials);
    }
  }
  catch (error) {
    error.message = `Failed to log in: ${error.message}`;
    throw error;
  }
}

async function logout() {
  try {
    if (await auth.currentSession()) {
      await auth.logout();
    }
  }
  catch (error) {
    error.message = `Failed to log out: ${error.message}`;
    throw error;
  }
}

export default Object.freeze({
  login,
  logout,
});
