/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import libPath from 'path';
import fc from './fc.js';

const path = libPath.posix;

async function read(...args) {
  return fc.readFile(...args);
}

async function readDir(...args) {
  return fc.readFolder(...args);
}

async function write(...args) {
  return fc.createFile(...args);
}

async function createDir(...args) {
  return fc.createFolder(...args);
}

// link: function link(tgt, src) {
//     if (src.slice(-1) === '/' || src.slice(-1) === '\\') {
//       return fs.ensureSymlinkAsync(src, tgt, 'junction');
//     }
//     else {
//       return fs.ensureLinkAsync(src, tgt);
//     }
//   },

async function remove(...args) {
  return fc.remove(...args);
}

async function move(...args) {
  return fc.move(...args);
}

function copy(...args) {
  return fc.copy(...args);
}

export default {
  path,
  read,
  readDir,
  write,
  // link,
  createDir,

  remove,
  delete: remove,

  rename: move,

  move,
  copy,
};
