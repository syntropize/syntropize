/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */

// We resolve sfc using the module property with Rollup.
// (dist folder is not available as this is a unbuilt install from a git repo)
import FC from 'solid-file-client'; // eslint-disable-line import/no-unresolved
import auth from '../auth/auth.js';

const fc = new FC(auth);

export default fc;
