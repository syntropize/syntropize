/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
function filterMessage(msg) {
  const tokenList = ['ack', 'pub'];
  const token = msg.substring(0, 3);
  const url = msg.substring(4);
  const isValid = tokenList.some(t => t === token) && msg[3] === ' ';
  return {
    token: isValid ? token : undefined,
    url: isValid ? url : undefined,
  };
}

export default filterMessage;
