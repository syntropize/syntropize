/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import WebSocket from 'isomorphic-ws';
import debug from 'debug';
import Emit from './emit.js';
import filterMessage from './filter-message.js';
import fc from '../rw/fc.js';

const log = debug('Plugin:Solid:IO:Watcher');

function watcher(socketAddress, socket) {
  const location = socket.location.endsWith('/') ?
    socket.location : `${socket.location}/`;
  const { depth = 1 } = socket.options;
  const ws = new WebSocket(socketAddress);
  const emit = Emit(socket);
  const contents = new Map();
  const initial = new Set();
  let history;
  let ready = false;
  log('Initialize');

  function track(item, parentId) {
    log('Track', item, parentId);
    // eslint-disable-next-line no-param-reassign
    item.id = [...parentId, item.name];
    contents.set(item.url, item);
    initial.add(item.url);
    ws.send(`sub ${item.url}`);
  }

  function trackFolderContents(item) {
    const { id } = contents.get(item.url);
    const depthLevel = depth - id.length;
    log(`Track folder: ${id} @ ${item.url}`);
    if (depthLevel > 1) {
      item.files.forEach(file => track(file, id));
      item.folders.forEach(folder => track(folder, id));
    }
    if (depthLevel === 1) {
      item.files.forEach(file => track(file, id));
      item.folders.forEach((folder) => {
        // eslint-disable-next-line no-param-reassign
        folder.url = folder.url.substring(0, -1);
        track(folder, id);
      });
    }
  }

  async function handleAck(url) {
    let item;
    if (url.endsWith('/')) {
      try {
        item = await fc.readFolder(url);
      }
      catch (error) {
        error.message = `Solid: ack: Unable to read ${url}
${error.message}`;
        emit.fsError(error);
        log(`Acknowlege error ${error}`);
        item = { url };
        if (error.status === '404') { return; }
      }
      if (url === location) {
        item.id = [];
        log(`Acknowlege success root @ ${url}`);
      }
      else {
        item.id = contents.get(url).id;
        log(`Acknowlege success sub-item: ${item.id} @ ${url}`);
      }
      contents.set(url, item); // set contents again to get children: potentially for deleting
      trackFolderContents(item);
    }
    else {
      item = contents.get(url);
    }

    if (item) {
      emit.add(item);

      initial.delete(url);
      if (initial.size === 0 && !ready) {
        emit.ready();
        ready = true;
      }
    }
    else {
      const error = new Error(`Solid: ack not handled: Item at ${url} missing`);
      emit.fsError(error);
    }
  }

  async function handlePubFile(url) {
    const item = contents.get(url);
    log(`Handle publish file ${item} @ ${url}`);

    // new item that was previously tracked; let this be handled by parent folder
    // this happens because there is no unsubscribe call in the standard
    if (!item) {
      history = undefined;
      return;
    }

    let parent;
    try {
      parent = await fc.readFolder(item.parent);
      log('Handle publish file success', parent);
    }
    catch (error) {
      error.message = `Solid: pub: Unable to read parent ${item.parent} for file ${item.name}
${error.message}`;
      emit.fsError(error);
      log('Handle publish file error', error);
      return;
    }
    let newItem;
    const isChanged = [...parent.files, ...parent.folders].some((subItem) => {
      if (subItem.url === url) {
        newItem = subItem;
        return true;
      }
      return false;
    });

    if (isChanged) {
      newItem.id = item.id;
      log('Handle Publish File: Change: ', newItem);
      emit.change(newItem);
      contents.set(newItem.url, newItem);
    }
    else {
      log('Handle publish file: Delete: ', item);
      emit.unlink(item);
      contents.delete(item.url);
    }
    parent.id = item.id.slice(0, -1);
    contents.set(parent.url, parent);
  }

  function unlinkFolder(url) {
    const item = contents.get(url);
    item.folders.forEach((i) => {
      unlinkFolder(i.url); // correct for '/' during testing;
    });
    item.files.forEach((i) => {
      contents.delete(i.url);
      emit.unlink(i);
    });
    contents.delete(item.url);
    emit.unlink(item);
  }

  function findAndTrackNewItem(parent) {
    function checkNewAndTrack(item) {
      log(`Track new item ${item}`);
      if (!contents.has(item.url)) {
        track(item, parent.id);
        return true;
      }
      return false;
    }
    return parent.files.some(checkNewAndTrack) || parent.folders.some(checkNewAndTrack);
  }

  async function handlePubFolder(url) {
    const old = contents.get(url);

    // new item that was previously tracked; let this be handled by parent folder
    // this happens because there is no unsubscribe call in the standard
    if (!old) {
      history = undefined;
      return;
    }
    try {
      // Success means folder changed, that is:
      // 1. file:
      //   a. created (findAndTrack)
      //   b. changed (ignore: covered in handlePubFile)
      //   c. deleted (ignore: covered in handlePubFile)
      // 2. folder
      //   a. created inside (findAndTrack)
      //   b. deleted inside (ignore: handled as failure for child)
      const item = await fc.readFolder(url);
      item.id = old.id;
      log('Track new item: Folder Changed', old);
      findAndTrackNewItem(item);
      contents.set(url, item); // Use Latest
    }
    catch (error) {
      // Failure means folder itself was deleted
      if (error.status === 404) {
        unlinkFolder(url);
        log('Track new item: Folder Delete', old);
      }
      else {
        error.message = `Solid: pub: Unable to read folder ${url}
${error.message}`;
        emit.fsError(error);
        log('Track new item: error', error);
      }
    }
  }

  ws.onerror = (error) => {
    log('Error', error);
    emit.fsError(error);
  };

  ws.onopen = function socketOpen() {
    log('Open Connection');
    initial.add(location);
    ws.send(`sub ${location}`);
    emit.connect();
    socket.on('forceDisconnect', function onForceDisconnect() {
      ws.close();
    });
    // contents for the start is assigned after first read folder
  };

  ws.onmessage = function socketMessage(msg) {
    log(`Message: ${msg.data}`);
    const { token, url } = filterMessage(msg.data);
    if (token === 'ack') {
      emit.connection(url);
      handleAck(url);
    }
    if (token === 'pub') {
      if (url.endsWith('/')) {
        if (history && history.startsWith(url)) {
          history = undefined;
        }
        else {
          history = url;
          handlePubFolder(url);
        }
      }
      else {
        history = url;
        handlePubFile(url);
      }
    }
  };

  ws.onclose = function socketClose() {
    log('Close connection');
    emit.disconnect();
  };

  return ws;
}

export default watcher;
