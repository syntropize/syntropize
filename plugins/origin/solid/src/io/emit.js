/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
function type2Mode(type) {
  if (type === 'Container') {
    return 0o40777;
  }
  else {
    return 0o100777;
  }
}

function type2Emit(type) {
  if (type === 'Container') {
    return 'Dir';
  }
  else {
    return '';
  }
}

function Emit(socket) {
  function getStat(item) {
    const {
      itemType,
      modified,
      mtime,
      size,
    } = item;

    return Object.freeze({
      size,
      mtime,
      modified,
      mode: type2Mode(itemType),
    });
  }

  function add(item) {
    socket.emit(
      `add${type2Emit(item.type)}`,
      item.id.join('/'),
      getStat(item),
    );
  }

  function change(item) {
    socket.emit(
      'change',
      item.id.join('/'),
      getStat(item),
    );
  }

  function unlink(item) {
    socket.emit(
      `unlink${type2Emit(item.type)}`,
      item.id.join('/'),
    );
  }

  function fsError(error) {
    socket.emit('fs_error', error);
  }

  function ready() {
    socket.emit('ready');
  }

  function connection(url) {
    socket.emit('connection', url);
  }

  function connect() {
    socket.emit('connect');
  }

  function disconnect() {
    socket.emit('disconnect');
    socket.emit('close');
  }

  return {
    add,
    ready,
    fsError,
    unlink,
    change,
    connection,
    connect,
    disconnect,
  };
}

export default Emit;
