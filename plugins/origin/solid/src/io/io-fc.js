/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import util from 'util';
import EventEmitter from 'events';

import fc from '../rw/fc.js';
import watcher from './watcher.js';

const Socket = function Socket(location, options) {
  this.location = location;
  this.connected = false;
  this.io = {
    skipReconnect: false,
  };
  this.watcher = undefined;
  this.options = options || {};
  EventEmitter.call(this);
};

util.inherits(Socket, EventEmitter);

const connectSocket = function connectSocket() {
  const socket = this;
  if (socket.connected) return;

  const connection = async function connection() {
    try {
      const response = await fc.head(socket.location);

      const location = response.headers.get('updates-via');
      const poweredByHeader = response.headers.get('x-powered-by');
      const linkHeader = response.headers.get('link');
      const isSolid = typeof poweredByHeader === 'string' && poweredByHeader.includes('solid');
      const isResource = typeof linkHeader === 'string' && linkHeader.split(', ').some(link => (
        link.includes('#Resource') && link.includes('rel="type"')));

      if (response.status === 200) {
        if (!isSolid) {
          throw new Error('Invalid Syntropize resource');
        }
        if (isResource) {
          throw new Error('Resource is a file');
        }

        if (location) {
          socket.watcher = watcher(location, socket);
          return;
        }
        else {
          throw new Error('Updates not avaiable');
        }
      }
      else {
        const error = new Error(response.statusText);
        throw Object.assign(
          error,
          {
            url: response.url,
            status: response.status,
          },
        );
      }
    }
    catch (error) {
      if (error.status === 404) {
        error.message = `Resource does not exist: ${error.message}`;
      }
      else if (error.status === 401) {
        error.message = `Access not authorized: ${error.message}`;
      }
      else if (error.status >= 500) {
        error.message = `Resource Error: ${error.message}`;
      }
      else if (error.status >= 400) {
        error.message = `Request Error: ${error.message}`;
      }
      socket.emit('connect_error', error);
    }
  };

  process.nextTick(connection);
};

const disconnectSocket = function disconnectSocket() {
  const socket = this;
  if (socket.watcher) {
    socket.watcher.close();
  }
  else {
    socket.emit('close');
    socket.emit('disconnect');
  }
  socket.connected = false;
};

Socket.prototype.open = connectSocket;
Socket.prototype.connect = connectSocket;

Socket.prototype.close = disconnectSocket;
Socket.prototype.disconnect = disconnectSocket;

const io = function io(location, options) {
  return new Socket(location, options);
};

export default io;
export { io as connect };
