/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import libUrl from 'url';
import libPath from 'path';
import settings from '../defaults.json';

function parse(address, options = {}) {
  const urlObj = libUrl.parse(address);

  const [scheme] = urlObj.protocol.split(':', 1);

  Object.assign(urlObj, options);
  urlObj.port = urlObj.port || settings.port[scheme];

  const originUrlObj = {
    protocol: urlObj.protocol,
    slashes: urlObj.slashes,
    hostname: urlObj.hostname,
  };
  if (urlObj.port !== 443) {
    originUrlObj.port = urlObj.port;
  }
  const origin = libUrl.format(originUrlObj);

  return Object.freeze({
    scheme,
    origin,
    $pathname: decodeURI(urlObj.pathname),
    $root: origin,
    ...urlObj,
  });
}

function resolve(root, ...paths) {
  return libUrl.resolve(root, libPath.posix.join(...paths));
}

export default {
  parse,
  resolve,
};
