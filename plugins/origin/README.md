# Origin Plugins

This directory contains **Origin Plugins**, that allow Syntropize to connect to different data sources. They abstract away the differences between these data sources to present a uniform interface to downstream components.
