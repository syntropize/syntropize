# Lyx Compile

The **Lyx Compile** plugin allow you to combine Lyx documents linked within a resource into a single Lyx document.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
