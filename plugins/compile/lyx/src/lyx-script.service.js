/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const lyxScriptService = function lyxScriptService() {
  const args = function lyxScriptArgs(target, files) {
    const argPrefix = `command-sequence buffer-new ${target};`;
    const argSuffix = ' buffer-write; lyx-quit;';
    const argFileList = files
      .filter(file => file.match(/\.lyx$/))
      .map(file => ` file-insert ${file}; newline-insert;`)
      .join('')
    ;

    return ['--execute', argPrefix + argFileList + argSuffix];
  };

  return {
    args,
  };
};

export default lyxScriptService;
