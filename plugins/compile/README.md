# Compile Plugins

This directory contains **Compile plugins**, which provide users to generate compilations from selected contents of a resource.

These plugins are invoked from the *Run* menu of view, with Syntropize automatically providing the plugin instance with the list of objects selected by the user within that view.
