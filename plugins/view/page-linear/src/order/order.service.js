/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentSectionOrderService = function documentSectionOrderService(
  ResourceService,
  DocumentMetadataStatus,
  $log,
) {
  let list = []; // Holds the unfiltered List
  let displaySections = []; // Cards filtered and sorted by list

  function reorderSections() {
    const { sections } = ResourceService.resource.document;
    displaySections.length = 0;
    // First Section is always "."
    displaySections.push({
      key: '.',
      value: sections['.'],
    });
    // Add metadata list sections except '.'
    list.filter(key => key !== '.').forEach((key) => {
      displaySections.push({
        key,
        value: sections[key],
      });
    });
    // Add leftover sections
    Object.keys(sections)
      .filter(key => key !== '.')
      .filter(key => !list.includes(key))
      .forEach((key) => {
        displaySections.push({
          key,
          value: sections[key],
        });
      })
    ;
  }


  const update = function updateCardList() {
    list = displaySections.map(item => item.key);
    // list.splice(list.indexOf('.'), 1);
  };

  const resetList = function resetCardOrder() {
    if (angular.isArray(ResourceService.resource.board.metadata['.'])) {
      list = ResourceService.resource.board.metadata['.'].slice();
    }
    else {
      list = [];
    }
  };

  const init = function initializeNewCardOrder() {
    list = [];
    displaySections = [];
  };

  const status = function getMetadataStatus() {
    return DocumentMetadataStatus.readStatus.get('.');
  };

  const revert = function revertCardOrder() {
    const myStatus = status();
    if (!myStatus || myStatus === 'error' || myStatus === 'loading') {
      $log.warn(`Section Order: Did not revert because board status is ${status}`);
    }
    else {
      resetList();
      reorderSections();
    }
  };

  function updateDisplaySectionsPreservingOrder() {
    const delIndex = [];
    // Remove sections not in list from displaySections
    // ensures that any deleted sections are removed but also
    // removes existing sections not in list
    displaySections.map(s => s.key).forEach((key, index) => {
      if (!list.includes(key) && key !== '.') {
        delIndex.push(index);
      }
    });
    delIndex.reverse().forEach((i) => {
      displaySections.splice(i, 1);
    });
    // Add back existing sections
    const { sections } = ResourceService.resource.document;
    const displayKeys = displaySections.map(section => section.key);
    const extraListKeys = list.filter(key => !displayKeys.includes(key));
    const extraSectionKeys = Object.keys(sections)
      .filter(key => ![...displayKeys, ...extraListKeys].includes(key))
    ;
    displaySections.push(...[...extraListKeys, ...extraSectionKeys].map(key => ({
      key,
      value: sections[key],
    })));
  }

  function relist() {
    const myStatus = status();
    if (!myStatus || myStatus === 'error' || myStatus === 'loading') {
      $log.warn(`Section Order: Did not revert because board status is ${status}`);
    }
    else {
      resetList();
      updateDisplaySectionsPreservingOrder();
    }
  }

  return {
    get displaySections() {
      return displaySections;
    },
    get list() {
      return list;
    },
    init,
    status,
    update,
    reorder: reorderSections,
    revert,
    relist,
  };
};

documentSectionOrderService.$inject = [
  'ResourceService',
  'DocumentMetadataStatus',
  '$log',
];

export default documentSectionOrderService;
