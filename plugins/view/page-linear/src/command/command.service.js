/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const documentCommandService = function documentCommandService(
  ResourceService,
  DocumentSelection,

  ErrorToastService,
  SuccessToastService,
  ConfirmDialogService,
  InputDialogService,
  FileSelectDialogService,
  $log,
) {
  const filenameRE = /^\w{1}[ \w-.,;]*$/;
  const filepathRE = undefined;

  const document = {
    section: {
      add: function addDocumentSection() {
        return InputDialogService({
          text: 'Enter Section name',
          title: 'Add Section',
          cancel: 'Skip',
          continue: 'Create',
          value: undefined,
          legal: filenameRE,
          blacklist: ResourceService.resource.directory.files['.'],
        })
          .then(function onValidNewDocumentSectionName(key) {
            return ResourceService.resource.document.writer.create(key, `# ${key}\n`);
          })
          .then(function onAddDocumentSectionSuccess() {
            SuccessToastService({ message: 'Success: Section Added' });
          })
          .catch(function onAddDocumentSectionError(error) {
            if (error) {
              ErrorToastService(error);
              $log.error(error);
            }
            return error;
          })
        ;
      },

      start: function startDocumentSection(...args) {
        return ResourceService.resource.document.writer.addSection(...args)
          .then(function onStartDocumentSectionSuccess() {
            SuccessToastService({ message: 'Success: Section Started' });
          })
          .catch(function onStartDocumentSectionError(error) {
            ErrorToastService(error);
            $log.error(error);
            return error;
          })
        ;
      },

      transclude: function transcludeDocumentSection() {
        return FileSelectDialogService({
          title: 'Transclude Section',
          text: 'Cross-domain Transclusion shall be implemented in a later version!',
          cancel: 'Skip',
          continue: 'Transclude',
          type: 'folder',
          properties: {
            name: {
              title: 'Section Name',
              value: undefined,
              legal: filenameRE,
              blacklist: ResourceService.resource.directory.files['.'],
            },
            path: {
              title: 'Path to Folder',
              value: undefined,
              legal: filepathRE,
              blacklist: [ResourceService.resource.location.path()],
            },
          },
        })
          .then(function onValidPathForDocumentSection({ name, path }) {
            return ResourceService.resource.document.writer.transclude(name, path);
          })
          .then(function onTranscludeDocumentSectionSuccess() {
            SuccessToastService({ message: 'Success: Section Transcluded' });
          })
          .catch(function onTranscludeDocumentSectionError(error) {
            if (error) {
              ErrorToastService(error);
              $log.error(error);
            }
            return error;
          })
        ;
      },

      remove: function removeDocumentSection(key) {
        return ConfirmDialogService({
          text: 'Are you sure?',
          title: 'Delete Sections',
          cancel: 'Skip',
          continue: 'Delete',
        })
          .then(function removeOnDocumentSectionConfirm() {
            return ResourceService.resource.document.writer.remove(key);
          })
          .then(function onRemoveDocumentSectionSuccess() {
            SuccessToastService({
              message: 'Success: Section Deleted',
            });
          })
          .catch(function onRemoveDocumentSectionError(error) {
            if (error) {
              ErrorToastService(error);
              $log.error(error);
            }
            throw error;
          })
        ;
      },

      removeAll: function removeDocumentSectionAndFiles(key) {
        return ConfirmDialogService({
          text: 'Are you sure?',
          title: 'Delete Sections with Contents',
          cancel: 'Skip',
          continue: 'Delete',
        })
          .then(function removeOnDocumentSectionAndFilesConfirm() {
            return ResourceService.resource.document.writer.remove(key, 'ALL');
          })
          .then(function onRemoveDocumentSectionAndFilesSuccess() {
            SuccessToastService({
              message: 'Success: Section with Folder Deleted',
            });
            // DocumentSelection.sections.clear();
          })
          .catch(function onRemoveDocumentSectionAndFilesError(error) {
            if (error) {
              // DocumentSelection.sections.clear();
              ErrorToastService(error);
              $log.error(error);
            }
            return error;
          })
        ;
      },

      rename: function renameDocumentSection(...args) {
        return ResourceService.resource.document.writer.rename(...args)
          .then(function onRenameDocumentSectionSuccess() {
            // DocumentSelection.sections.toggle(args[0]);
            // DocumentSelection.sections.toggle(args[1]);
            SuccessToastService({ message: 'Success: Section Renamed' });
          })
          .catch(function onRenameDocumentSectionError(error) {
            ErrorToastService(error);
            $log.error(error);
            return error;
          })
        ;
      },

      replace: function replaceDocumentSection(...args) {
        return ResourceService.resource.document.writer.replaceSection(...args)
          .then(function onReplaceDocumentSectionSuccess() {
            SuccessToastService({ message: 'Success: Section Replaced' });
          })
          .catch(function onReplaceDocumentSectionError(error) {
            ErrorToastService(error);
            $log.error(error);
            return error;
          })
        ;
      },

    },

    reorder: function reorderDocumentItems(list) {
      return ResourceService.resource.document.writer.update('.', list)
        .then(function onReorderDocumentItemsSuccess() {
          SuccessToastService({ message: 'Success: New Section Order Saved' });
        })
        .catch(function onReorderDocumentItemsError(error) {
          ErrorToastService(error);
          $log.error(error);
          return error;
        })
      ;
    },
  };

  return Object.freeze(document);
};

documentCommandService.$inject = [
  'ResourceService',
  'DocumentSelection',

  'ErrorToastService',
  'SuccessToastService',
  'ConfirmDialogService',
  'InputDialogService',
  'FileSelectDialogService',
  '$log',
];

export default documentCommandService;
