/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import './tufte.css';
import './latex.css';
import './page-linear.css';
import './page-list/page-list.css';
import './page-contents/page-contents.css';

import documentCommandService   from './command/command.service.js';

import documentSectionOrderService from './order/order.service.js';

import pageContentsDirective from './page-contents/page-contents.directive.js';
import pageContentsController from './page-contents/page-contents.controller.js';

import pageListDirective from './page-list/page-list.directive.js';
import pageListController from './page-list/page-list.controller.js';
import pageListService from './page-list/page-list.service.js';

import documentRibbonDirective  from './ribbon/ribbon.directive.js';
import documentRibbonController from './ribbon/ribbon.controller.js';

import pageLinearDirective from './page-linear.directive.js';
import pageLinearController from './page-linear.controller.js';

// const dependencies = [
//   // 'markdown',
// ];

export default angular.module('view.pageLinear', [])

  // Commands
  .factory('DocumentCommandService', documentCommandService)

  // Section Order
  .factory('DocumentSectionOrderService', documentSectionOrderService)
  .value('DocumentSectionOrder', {})

  // Page Contents
  .directive('viewPageLinearContents', pageContentsDirective)
  .controller('PageContentsController', pageContentsController)

  // List of Sub-Pages
  .directive('viewPageLinearList', pageListDirective)
  .controller('PageListController', pageListController)
  .factory('PageListService', pageListService)

  // Ribbon
  .directive('viewPageLinearRibbon', documentRibbonDirective)
  .controller('DocumentRibbonController', documentRibbonController)

  .directive('viewPageLinear', pageLinearDirective)
  .controller('PageLinearController', pageLinearController)
;
