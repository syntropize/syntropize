/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const ribbonController = function ribbonController(
  ResourceService,
  // DocumentSelection,
  SectionDisplay,
  PageListService,
  DocumentCommandService,

  ReadOnlyService,
  HelpService,
  LaunchService,
  OpenService,
) {
  const doc = ResourceService.resource.document;
  const { sections } = doc;

  const open = function open() {
    OpenService(ResourceService.resource.location.path(
      SectionDisplay.top,
      doc.options.files.section,
    ));
  };

  const openFolder = function openFolder() {
    OpenService(ResourceService.resource.location.path(SectionDisplay.top));
  };

  const create = function create() {
    DocumentCommandService.section.start(SectionDisplay.top, `# ${SectionDisplay.top}\n\n`);
  };

  const add = function add() {
    DocumentCommandService.section.add();
  };

  const transclude = function transclude() {
    DocumentCommandService.section.transclude();
  };

  const launchState = function launchState(state) {
    LaunchService.state(state);
  };

  const remove = function remove() {
    DocumentCommandService.section.remove(SectionDisplay.top);
  };

  const removeAll = function removeAll() {
    DocumentCommandService.section.removeAll(SectionDisplay.top);
  };

  return {
    get isActiveSectionEnabled() {
      return Object.prototype.hasOwnProperty.call(sections, SectionDisplay.top || '.');
    },
    list: PageListService,
    // selection: DocumentSelection,

    get readOnly() {
      return ReadOnlyService.status;
    },

    help: HelpService,

    create,
    add,
    transclude,
    remove,
    removeAll,
    launchState,
    open,
    openFolder,
  };
};

ribbonController.$inject = [
  'ResourceService',
  // 'DocumentSelection',
  'SectionDisplay',
  'PageListService',
  'DocumentCommandService',

  'ReadOnlyService',
  'HelpService',
  'LaunchService',
  'OpenService',
];

export default ribbonController;
