/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const pageLinearController = function pageLinearController(
  ResourceService,
  DocumentSectionOrder,
  DocumentSelection,
  SectionDisplay,
  PageListService,
  // DocumentSort,
  // DocumentSearch,
  DocumentSectionOrderService,
  DocumentCommandService,

  LaunchService,
  OpenService,

  $anchorScroll,
  $scope,
  $location,
  $timeout,
) {
  const doc = ResourceService.resource.document;

  DocumentSectionOrderService.init();

  $scope.$watchCollection(
    function returnResourceDocumentSections() {
      return doc.sections;
    },
    DocumentSectionOrderService.reorder,
  );

  function relistOnOrderChange() {
    $scope.$watch(
      function getOrderStatus() {
        return DocumentSectionOrderService.status();
      },
      function initializeDocumentSectionOrder(status) {
        if (status === 'error' || status === 'success') {
          DocumentSectionOrderService.relist();
        }
      },
    );
  }

  const initializeStop = $scope.$watch(
    function getOrderStatus() {
      return DocumentSectionOrderService.status();
    },
    function initializeDocumentSectionOrder(status) {
      if (status === 'error' || status === 'success') {
        initializeStop();
        DocumentSectionOrderService.revert();
        relistOnOrderChange();
      }
    },
  );

  const keySet = new Set();
  function autoScrollOnSectionLoad(key) {
    keySet.add(key);
    if (Object.keys(doc.sections).every(s => keySet.has(s))) {
      doc.event.removeListener('section readSuccess', autoScrollOnSectionLoad);
      doc.event.removeListener('section readError', autoScrollOnSectionLoad);
    }
    $timeout(() => { $anchorScroll(); }, 1, false);
  }
  $anchorScroll();
  SectionDisplay.top = $location.hash().substring(8).replace('__', ' ');
  doc.event.on('section readSuccess', autoScrollOnSectionLoad);
  doc.event.on('section readError', autoScrollOnSectionLoad);

  return {
    selection: DocumentSelection,
    command: DocumentCommandService,
    get name() {
      const loc = ResourceService.resource.location.url.href;
      return loc.substring(loc.lastIndexOf('/') + 1, loc.length);
    },
    get address() {
      return ResourceService.resource.location.url.href;
    },
    get isMainSection() {
      return (Object.prototype.hasOwnProperty.call(doc.sections, '.'));
    },
    get metadata() {
      return doc.metadata;
    },
    get displaySections() {
      return DocumentSectionOrderService.displaySections;
    },
    get activeSection() {
      return SectionDisplay.top;
    },
    set activeSection(section) {
      SectionDisplay.top = section;
    },
    list: PageListService,
  };
};

pageLinearController.$inject = [
  'ResourceService',
  'DocumentSectionOrder',
  'DocumentSelection',
  'SectionDisplay',
  'PageListService',
  // 'DocumentSort',
  // 'DocumentSearch',
  'DocumentSectionOrderService',
  'DocumentCommandService',

  'LaunchService',
  'OpenService',

  '$anchorScroll',
  '$scope',
  '$location',
  '$timeout',
];

export default pageLinearController;
