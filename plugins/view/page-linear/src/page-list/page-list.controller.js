/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const pageListController = function pageListController(
  ResourceService,
  DocumentSectionOrder,
  DocumentSelection,
  SectionDisplay,
  PageListService,
  ReadOnlyService,
  // DocumentSort,
  // DocumentSearch,
  DocumentSectionOrderService,
  DocumentCommandService,

  dragulaService,
  LaunchService,
  OpenService,

  $anchorScroll,
  $location,
  $scope,
) {
  const loc = ResourceService.resource.location;

  const doc = ResourceService.resource.document;
  const { sections } = doc;
  const { metadata } = doc;

  const { files } = ResourceService.resource.directory;

  const open = function open(key) {
    OpenService(loc.path(key, doc.options.files.section));
  };

  const openFolder = function openFolder(key) {
    if (key === '.' || (files['.'][key] && !files['.'][key].isFile())) {
      OpenService(loc.path(key));
    }
  };

  const start = function start(key) {
    DocumentCommandService.section.start(key, key === '.' ? '' : `# ${key}\n\n`);
  };

  const launch = function launch(key) {
    if (key !== '.' && Object.prototype.hasOwnProperty.call(metadata, key)) {
      LaunchService.item(key);
    }
  };

  dragulaService.options($scope, 'section-bag', {
    moves: function preventParentSectionDrag(el) {
      return (el.id !== 'document-section-.');
    },
    accepts: function preventParentSectionDrop(el, target, source, sibling) {
      return (sibling === null || sibling.id !== 'document-section-.');
    },
  });

  $scope.$on('section-bag.drop-model', function sectionBagDropModel() {
    DocumentSectionOrderService.update();
  });

  function goto(key) {
    const newHash = `section-${key.replace(' ', '__')}`;
    $anchorScroll(newHash);
    // Disabled because upon navigation to another page, $location.hash() is not being reset and the
    // address still carries it, despite no "#" parameter being specified to $state.go(). Thus at
    // the new location $anchorScroll is still looking for the old # and failing.
    // Correctly, the # component should change everytime we jump to a new topic.
    // if ($location.hash() !== newHash) {
    //   // set the $location.hash to `newHash` and
    //   // $anchorScroll will automatically scroll to it
    //   $location.hash(newHash);
    // }
    // else {
    //   // call $anchorScroll() explicitly,
    //   // since $location.hash hasn't changed
    //   $anchorScroll();
    // }
    $scope.$applyAsync(() => { SectionDisplay.top = key; });
  }

  return {
    goto,
    selection: DocumentSelection,
    get name() {
      const { href } = loc.url;
      return href.substring(href.lastIndexOf('/') + 1, loc.length);
    },
    getTitle(key) {
      return key === '.' ? decodeURI(this.name) : key;
    },
    get isMainSection() {
      return (Object.prototype.hasOwnProperty.call(sections, '.'));
    },
    get metadata() {
      return metadata;
    },
    reorder() {
      const list = DocumentSectionOrderService.displaySections.map(i => i.key);
      if (list[0] === '.') { list.shift(); }
      DocumentCommandService.reorder(list);
    },
    revertOrder() {
      DocumentSectionOrderService.revert();
    },
    get readOnly() {
      return ReadOnlyService.status;
    },
    get displaySections() {
      return DocumentSectionOrderService.displaySections;
    },
    get activeSection() {
      return SectionDisplay.top;
    },
    isSectionEnabled(key) {
      return Object.prototype.hasOwnProperty.call(sections, key);
    },
    set activeSection(section) {
      SectionDisplay.top = section;
    },
    isActiveSection(section) {
      return (section === SectionDisplay.top);
    },
    close() {
      return PageListService.close();
    },
    start,
    launch,
    open,
    openFolder,
  };
};

pageListController.$inject = [
  'ResourceService',
  'DocumentSectionOrder',
  'DocumentSelection',
  'SectionDisplay',
  'PageListService',
  'ReadOnlyService',
  // 'DocumentSort',
  // 'DocumentSearch',
  'DocumentSectionOrderService',
  'DocumentCommandService',

  'dragulaService',
  'LaunchService',
  'OpenService',

  '$anchorScroll',
  '$location',
  '$scope',
];

export default pageListController;
