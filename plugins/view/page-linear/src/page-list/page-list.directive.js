/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import pageListTemplate from './page-list.html';

const pageListDirective = function pageListDirective() {
  return {
    template: pageListTemplate,
    restrict: 'E',
    scope: {},
    controller: 'PageListController',
    controllerAs: 'pageList',
  };
};

export default pageListDirective;
