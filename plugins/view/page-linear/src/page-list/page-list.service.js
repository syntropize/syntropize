/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const pageListService = function pageListService(
  $mdMedia,
  $mdSidenav,
) {
  return {
    open() {
      return $mdSidenav('section-list').open();
    },
    close() {
      return $mdSidenav('section-list').close();
    },
    get isOpen() {
      return $mdSidenav('section-list').isOpen();
    },
    toggle() {
      return $mdSidenav('section-list').toggle();
    },
    get isLockedOpen() {
      return $mdSidenav('section-list').isOpen() && $mdMedia('gt-sm');
    },
  };
};

pageListService.$inject = [
  '$mdMedia',
  '$mdSidenav',
];

export default pageListService;
