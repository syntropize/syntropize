# Page-Linear View

The **Page-Linear View** plugin presents sections of text at the resource and its linked resources as a document.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
