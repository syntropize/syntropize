/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardSortController = function cardSortController(
  BoardSort,
  BoardSortService,
) {
  const sort = BoardSort;

  const {
    changeDirection,
    toggleField,
    toggleDirection,
    clear,
  } = BoardSortService;

  const fieldList = [
    { id: 'value.contents.title', name: 'Title' },
    { id: 'value.core.color', name: 'Color' },
  ];

  return {
    fieldList,
    changeDirection,
    toggleField,
    toggleDirection,
    clear,

    get global() {
      return sort.global;
    },
    set global(val) {
      sort.global = val;
    },
    get field() {
      return sort.field;
    },
    get direction() {
      return sort.direction;
    },
    get indexDirection() {
      return sort.index.direction;
    },
    set indexDirection(value) {
      sort.index.direction = value;
    },
  };
};

cardSortController.$inject = [
  'BoardSort',
  'BoardSortService',
];

export default cardSortController;
