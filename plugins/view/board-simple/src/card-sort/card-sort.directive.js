/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import cardSortTemplate from './card-sort.html';

const cardSortDirective = function cardSortDirective() {
  return {
    template: cardSortTemplate,
    restrict: 'E',
    scope: {},
    controller: 'CardSortController',
    controllerAs: 'sort',
  };
};

export default cardSortDirective;
