/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardOrderService = function cardOrderService(
  ResourceService,
  MetadataStatus,
  $rootScope,
  $filter,
  $log,
) {
  let list = []; // Holds the unfiltered List
  let cards = []; // Cards as Array
  let displayCards = []; // Cards filtered and sorted by list

  const index = function cardIndex(item) {
    const { key } = item;
    if (key === '.') { return -1; }
    const idx = list.indexOf(key);
    return (idx !== -1) ? idx : list.push(item.key) - 1;
  };

  const reorder = function updateCardOrder() {
    cards = $filter('toArray')(ResourceService.resource.board.cards);
    displayCards = $filter('orderBy')(cards, index);
  };

  const update = function updateCardList() {
    list = displayCards.map(card => card.key);
    // list.splice(list.indexOf('.'), 1);
  };

  const reset = function resetCardOrder() {
    if (angular.isArray(ResourceService.resource.board.metadata['.'])) {
      list = ResourceService.resource.board.metadata['.'].slice();
    }
    else {
      list = [];
    }
  };

  const init = function initializeNewCardOrder() {
    list = [];
    cards = [];
    displayCards = [];
  };

  const status = function getMetadataStatus() {
    return MetadataStatus.readStatus.get('.');
  };

  const revert = function revertCardOrder() {
    if (!status || status === 'error' || status === 'loading') {
      $log.warn(`Card Order: Did not revert because board status is ${status}`);
    }
    else {
      reset();
      reorder();
    }
  };

  return {
    get displayCards() {
      return displayCards;
    },
    get list() {
      return list;
    },
    init,
    status,
    update,
    reorder,
    revert,
  };
};

cardOrderService.$inject = [
  'ResourceService',
  'MetadataStatus',
  '$rootScope',
  '$filter',
  '$log',
];

export default cardOrderService;
