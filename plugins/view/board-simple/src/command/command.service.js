/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardCommandService = function boardCommandService(
  ResourceService,
  BoardSelection,

  ErrorToastService,
  SuccessToastService,
  ConfirmDialogService,
  InputDialogService,
  FileSelectDialogService,
  $log,
) {
  const filenameRE = /^\w{1}[ \w-.,;]*$/;
  const filepathRE = undefined;

  function renameDuplicate(exists, item) {
    if (exists(item)) {
      const pathSplitPoint = item.lastIndexOf('/');
      const parent = item.substring(0, pathSplitPoint + 1); // include slash in parent
      const base = item.substring(pathSplitPoint + 1);
      const [name, ...extn] = base.split('.');
      const parts = name.split('_');
      const last = parts.pop();
      let count = Number(last);

      if (Number.isNaN(count)) {
        parts.push(last);
        count = 1;
      }

      do {
        const value = [`${parent}${parts.join('_')}_${count}`, ...extn].join('.');
        if (exists(value)) { count += 1; }
        else { return value; }
      } while (count);
    }

    return item;
  }

  // FILE COMMANDS
  function moveBoardItems(...args) {
    return ResourceService.resource.board.writer.file.move(...args)
      .then(function onMoveBoardItemsSuccess() {
        SuccessToastService({ message: 'Success: Files Moved' });
      })
      .catch(function onMoveBoardItemError(error) {
        ErrorToastService(error);
        $log.error(error);
        return error;
      })
    ;
  }

  function copyBoardItems(...args) {
    return ResourceService.resource.board.writer.file.copy(...args)
      .then(function onCopyBoardItemsSuccess() {
        SuccessToastService({ message: 'Success: Files Copied' });
      })
      .catch(function onCopyBoardItemsError(error) {
        ErrorToastService(error);
        $log.error(error);
        return error;
      })
    ;
  }

  function createBoardFile(card) {
    return InputDialogService({
      text: 'Enter new name',
      title: 'Create File',
      cancel: 'Skip',
      continue: 'Create',
      value: undefined,
      legal: filenameRE,
      blacklist: ResourceService.resource.directory.files[card],
    })
      .then(function onValidNewBoardDirectoryName(value) {
        return ResourceService.resource.board.writer.file.create(card, value, '');
      })
      .then(function onCreateBoardFileSuccess() {
        SuccessToastService({ message: 'Success: New File Created' });
      })
      .catch(function onCreateBoardFileError(error) {
        if (error) {
          ErrorToastService(error);
          $log.error(error);
        }
        return error;
      })
    ;
  }

  function createBoardDirectory(...args) {
    return InputDialogService({
      text: 'Enter new name',
      title: 'Create Folder',
      cancel: 'Skip',
      continue: 'Create',
      value: undefined,
      legal: filenameRE,
      blacklist: ResourceService.resource.directory.files[args[0]],
    })
      .then(function onValidNewBoardDirectoryName(value) {
        return ResourceService.resource.board.writer.file.createDir(args[0], value);
      })
      .then(function onCreateBoardDirectorySuccess() {
        SuccessToastService({ message: 'Success: New Directory Created' });
      })
      .catch(function onCreateBoardDirectoryError(error) {
        if (error) {
          ErrorToastService(error);
          $log.error(error);
        }
        return error;
      })
    ;
  }

  function linkBoardFile(...args) {
    return FileSelectDialogService({
      title: 'Link',
      text: 'Cross-domain Linking shall be implemented in a later version!',
      cancel: 'Skip',
      continue: 'Link',
      type: 'file',
      properties: {
        name: {
          title: 'Link Name',
          value: undefined,
          legal: filenameRE,
          blacklist: ResourceService.resource.directory.files[args[0]],
        },
        path: {
          title: 'Path to File/Folder',
          value: undefined,
          legal: filepathRE,
          blacklist: [ResourceService.resource.location.path(args[0])],
        },
      },
    })
      .then(function onValidSymlinkBoardFile({ name, path }) {
        return ResourceService.resource.board.writer.file.link(args[0], name, path);
      })
      .then(function onSymlinkBoardFileSuccess() {
        SuccessToastService({ message: 'Success: File Linked' });
      })
      .catch(function onSymlinkBoardFileError(error) {
        if (error) {
          ErrorToastService(error);
          $log.error(error);
        }
        return error;
      })
    ;
  }

  function removeBoardItems(card, itemList) {
    if (itemList.length === 0) {
      throw new Error('No item selected for deletion!');
    }
    return ConfirmDialogService({
      text: 'Are you sure?',
      title: 'Delete Files',
      cancel: 'Skip',
      continue: 'Delete',
    })
      .then(function onRemoveBoardItemsConfirm() {
        return ResourceService.resource.board.writer.file.remove(card, itemList);
      })
      .then(function onRemoveBoardItemsSuccess() {
        SuccessToastService({
          message: (itemList.length === 1) ?
            'Success: Item Deleted' :
            'Success: Items Deleted',
        });
        // BoardSelection.files[args[0]].clear();
      })
      .catch(function onRemoveBoardItemsError(error) {
        if (error) {
          if (error.status === 'partial') {
            // temp fix for improper deletes with no real-time server updates
            // TODO: BoardSelection.files[itemList].clear();
          }
          ErrorToastService(error);
          $log.error(error);
        }
        return error;
      })
    ;
  }

  function renameBoardItem(...args) {
    let newName;
    return InputDialogService({
      text: 'Enter new name',
      title: 'Rename File',
      cancel: 'Skip',
      continue: 'Rename',
      value: args[1],
      legal: filenameRE,
      blacklist: ResourceService.resource.directory.files[args[0]],
    })
      .then(function renameOnValidBoardItemName(value) {
        newName = value;
        return ResourceService.resource.board.writer.file.rename(...args, args[0], value);
      })
      .then(function onRenameBoardItemSuccess() {
        BoardSelection.files[args[0]].toggle(newName);
        SuccessToastService({ message: 'Success: File Renamed' });
      })
      .catch(function onRenameBoardItemError(error) {
        if (error) {
          ErrorToastService(error);
          $log.error(error);
        }
        return error;
      })
    ;
  }

  function pasteDirectoryItems({ mode, location, items }, toCard) {
    const isSameLocation =
      ResourceService.resource.location.address(toCard) === location.address();
    const list = [...items.values()];

    const { files } = ResourceService.resource.directory;
    const newNames = [];
    const check = function checkIfFileExists(value) {
      const parts = value.split('/');
      if (newNames.includes(value)) { return true; }
      let status = false; // Assume name not taken
      if (parts.length === 2 && toCard === '.') {
        status = !!(files[parts[0]] && files[parts[0]][parts[1]]);
      }
      else if (parts.length === 1) {
        status = !!files[toCard][parts[0]];
      }
      if (status === false) {
        newNames.push(value);
      }
      return status;
    };

    if (mode === 'X') {
      if (isSameLocation) {
        const error = new Error('Cannot move items to same location');
        ErrorToastService(error);
        $log.error(error);
        return undefined;
      }
      return moveBoardItems(
        location,
        list,
        toCard,
      );
    }
    else if (mode === 'C') {
      const toList = list.map(renameDuplicate.bind(null, check));
      return copyBoardItems(
        location,
        list,
        toCard,
        isSameLocation ? toList : undefined,
      );
    }
    else {
      const error = new Error('No Files Selected');
      ErrorToastService(error);
      $log.error(error);
      return undefined;
    }
  }

  // CARD COMMANDS

  function moveBoardCards(...args) {
    return ResourceService.resource.board.writer.move(...args)
      .then(function onMoveBoardItemsSuccess() {
        SuccessToastService({ message: 'Success: Files Moved' });
      })
      .catch(function onMoveBoardItemError(error) {
        ErrorToastService(error);
        $log.error(error);
        return error;
      })
    ;
  }

  function copyBoardCards(...args) {
    return ResourceService.resource.board.writer.copy(...args)
      .then(function onCopyBoardItemsSuccess() {
        SuccessToastService({ message: 'Success: File Copied' });
      })
      .catch(function onCopyBoardItemsError(error) {
        ErrorToastService(error);
        $log.error(error);
        return error;
      })
    ;
  }

  function addBoardCard(...args) {
    return ResourceService.resource.board.writer.addCard(...args)
      .then(function onAddBoardCardSuccess() {
        SuccessToastService({ message: 'Success: Card Added' });
      })
      .catch(function onAddBoardCardError(error) {
        ErrorToastService(error);
        $log.error(error);
        return error;
      })
    ;
  }

  function replaceBoardCard(...args) {
    return ResourceService.resource.board.writer.replaceCard(...args)
      .then(function onReplaceBoardCardSuccess() {
        SuccessToastService({ message: 'Success: Card Replaced' });
      })
      .catch(function onReplaceBoardCardError(error) {
        ErrorToastService(error);
        $log.error(error);
        return error;
      })
    ;
  }

  function transcludeBoardCard() {
    return FileSelectDialogService({
      title: 'Transclude Card',
      text: 'Cross-domain Transclusion shall be implemented in a later version!',
      cancel: 'Skip',
      continue: 'Transclude',
      type: 'folder',
      properties: {
        name: {
          title: 'Card Name',
          value: undefined,
          legal: filenameRE,
          blacklist: ResourceService.resource.directory.files['.'],
        },
        path: {
          title: 'Path to Folder',
          value: undefined,
          legal: filepathRE,
          blacklist: [ResourceService.resource.location.path()],
        },
      },
    })
      .then(function onValidPathForBoardCard({ name, path }) {
        return ResourceService.resource.board.writer.transclude(name, path);
      })
      .then(function onTranscludeBoardCardSuccess() {
        SuccessToastService({ message: 'Success: Card Transcluded' });
      })
      .catch(function onTranscludeBoardCardError(error) {
        if (error) {
          ErrorToastService(error);
          $log.error(error);
        }
        return error;
      })
    ;
  }

  function renameBoardCard(...args) {
    return ResourceService.resource.board.writer.rename(...args)
      .then(function onRenameBoardCardSuccess() {
        BoardSelection.cards.toggle(args[0]);
        BoardSelection.cards.toggle(args[1]);
        SuccessToastService({ message: 'Success: Card Renamed' });
      })
      .catch(function onRenameBoardCardError(error) {
        ErrorToastService(error);
        $log.error(error);
        return error;
      })
    ;
  }

  function reorderBoardItems(...args) {
    return ResourceService.resource.board.writer.update('.', ...args)
      .then(function onReorderBoardItemsSuccess() {
        SuccessToastService({ message: 'Success: New Card Order Saved' });
      })
      .catch(function onReorderBoardItemsError(error) {
        ErrorToastService(error);
        $log.error(error);
        return error;
      })
    ;
  }

  function removeBoardCard(cards, opts) {
    const text = {
      CARDS: 'Delete Cards',
      BOARD: 'Delete Cards with Boards',
      ALL: 'Delete All Contents',
    };
    const count = cards.length;
    let runStatus = true;

    return ConfirmDialogService({
      text: 'Are you sure?',
      title: text[opts],
      cancel: 'Skip',
      continue: 'Delete',
    })
      .then(function removeOnBoardCardConfirm() {
        if (cards.some(card => card === '' || card === '.') && opts !== 'CARDS') {
          runStatus = false;
          throw new Error(`Cannot delete root card.
You can remove root card from its parent`);
        }
        return ResourceService.resource.board.writer.remove(cards, opts);
      })
      .then(function onRemoveBoardCardSuccess() {
        SuccessToastService({
          message: (count === 1) ?
            'Success: Card Deleted' : 'Success: Cards Deleted',
        });
        // BoardSelection.cards.clear();
      })
      .catch(function onRemoveBoardCardError(error) {
        if (error) {
          if (runStatus) { BoardSelection.cards.clear(); }
          ErrorToastService(error);
          $log.error(error);
        }
        return error;
      })
    ;
  }

  function pasteBoardCards({ mode, location, items }, toCard = '.', opts) {
    const isLocationSame =
      ResourceService.resource.location.address(toCard) === location.address();
    const list = [...items.values()];

    const cards = ResourceService.resource.board.metadata[toCard].slice();
    const check = function checkIfCardExists(value) {
      if (cards.includes(value)) { return true; }
      cards.push(value);
      return false;
    };
    if (mode === 'X') {
      if (isLocationSame) {
        const error = new Error('Duh! Cannot move cards to same location');
        ErrorToastService(error);
        $log.error(error);
        return undefined;
      }
      return moveBoardCards(
        location,
        list,
        toCard,
        // We are not checking for overwrites for now (overwriting silently)
        // Properly this has to be an annoying dialog box
        undefined,
        opts,
      );
    }
    else if (mode === 'C') {
      const toList = list.map(renameDuplicate.bind(null, check));
      return copyBoardCards(
        location,
        list,
        toCard,
        // Not allowing overwrites only for self (which would be annoying)
        isLocationSame ? toList : undefined,
        opts,
      );
    }
    else {
      const error = new Error('No Cards Selected');
      ErrorToastService(error);
      $log.error(error);
      return undefined;
    }
  }

  return Object.freeze({
    file: {
      create: createBoardFile,
      createDir: createBoardDirectory,
      link: linkBoardFile,
      remove: removeBoardItems,
      rename: renameBoardItem,
      copy: copyBoardItems,
      move: moveBoardItems,
      paste: pasteDirectoryItems,
    },

    card: {
      add: addBoardCard,
      replace: replaceBoardCard,
      transclude: transcludeBoardCard,
      rename: renameBoardCard,
    },

    start(...args) {
      return ResourceService.resource.board.writer.start(...args);
    },
    cease(...args) {
      return ResourceService.resource.board.writer.cease(...args);
    },
    reorder: reorderBoardItems,
    remove: removeBoardCard,

    copy: copyBoardCards,
    move: moveBoardCards,

    paste: pasteBoardCards,
  });
};

boardCommandService.$inject = [
  'ResourceService',
  'BoardSelection',

  'ErrorToastService',
  'SuccessToastService',
  'ConfirmDialogService',
  'InputDialogService',
  'FileSelectDialogService',
  '$log',
];

export default boardCommandService;
