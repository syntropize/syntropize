/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const selectionAggregationBoardService = function selectionAggregationBoardService(
  BoardSelection,
) {
  const selection = BoardSelection;

  const cardsAggregation = function cardsAggregation() {
    const fileList = [];
    selection.cards.list.forEach((card) => {
      selection.files[card].list.forEach((file) => {
        fileList.push(card === '.' ? file : `${card}/${file}`);
      });
    });
    return fileList;
  };

  const filesAggregation = function filesAggregation() {
    const fileList = [];
    Object.keys(selection.files).forEach((card) => {
      selection.files[card].list.forEach((file) => {
        fileList.push(card === '.' ? file : `${card}/${file}`);
      });
    });
    return fileList;
  };

  return {
    allSelectedFiles: filesAggregation,
    cardSelectedFiles: cardsAggregation,
  };
};

selectionAggregationBoardService.$inject = [
  'BoardSelection',
];

export default selectionAggregationBoardService;
