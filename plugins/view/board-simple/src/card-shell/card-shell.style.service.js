/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardShellStyleService = function cardShellStyleService(
  BoardSelection,
  ClipboardService,
  ResourceService,
) {
  const cardShellStyle = function cardShellStyle(key, index) {
    const { location } = ResourceService.resource;

    let clipboardBuffer;
    let exists;

    const updateClip = function updateClipboardStatusIfNeeded() {
      const newValue = ClipboardService.get('card');
      if (clipboardBuffer !== newValue) {
        clipboardBuffer = newValue;
        if (!clipboardBuffer) {
          exists = false;
        }
        else {
          const { url } = clipboardBuffer.location;
          exists =
            url.scheme === location.url.scheme &&
            url.$root === location.url.$root &&
            url.$pathname === location.url.$pathname;
        }
      }
    };

    const clipboard = Object.freeze({
      get has() {
        updateClip();
        return exists && clipboardBuffer.items.has(key);
      },
      get mode() {
        updateClip();
        return exists && clipboardBuffer.mode;
      },
    });

    const height = function cardShellHeight() {
      return '236px';
    };

    const width = function cardShellWidth() {
      return '236px';
    };

    const borderColor = function cardShellBorderColor() {
      let shellBorderColor = 'transparent';

      if (clipboard.has) {
        shellBorderColor = clipboard.mode === 'X' ? 'darkgrey' : 'dimgrey';
      }
      else if (BoardSelection.cards.isLastSelected(key)) {
        shellBorderColor = '#E91E63';
      }
      else if (BoardSelection.cards.isSelected(key)) {
        shellBorderColor = '#3F51B5';
      }
      return shellBorderColor;
    };

    const borderStyle = function cardShellBorderStyle() {
      let shellBorderStyle = '';

      if (clipboard.has) {
        shellBorderStyle += 'dashed ';
      }
      if (shellBorderStyle === '') {
        if (BoardSelection.cards.isSelected(key)) {
          shellBorderStyle = 'solid';
        }
        else {
          shellBorderStyle = 'transparent';
        }
      }
      return shellBorderStyle;
    };

    const highlight = function cardShellHighlight() {
      return (
        // view.xcCards.isXCed(key) ||
        // view.sc.key === key ||
        BoardSelection.cards.isSelected(key)
      );
    };

    const cssClass = function cardShellCssClass() {
      return {
        selected: BoardSelection.cards.isSelected(index),
        lastSelected: BoardSelection.cards.isLastSelected(index),
        'md-whiteframe-z5': BoardSelection.cards.isSelected(index),
      };
    };

    return {
      border: {
        get borderColor() {
          return borderColor();
        },
        get borderStyle() {
          return borderStyle();
        },
      },
      get highlight() {
        return highlight();
      },
      get height() {
        return height();
      },
      get width() {
        return width();
      },
      get cssClass() {
        return cssClass();
      },
    };
  };

  return cardShellStyle;
};

cardShellStyleService.$inject = [
  'BoardSelection',
  'ClipboardService',
  'ResourceService',
];

export default cardShellStyleService;
