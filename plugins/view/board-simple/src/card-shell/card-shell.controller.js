/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardShellController = function cardShellController(
  ResourceService,
  BoardSelection,
  CardStatus,

  CardDisplayService,

  LaunchService,
  OpenService,

  Registry,

  CardShellStyleService,

  $scope,
  $log,
) {
  const { key } = this;
  const { cards } = ResourceService.resource.board;
  const { metadata } = ResourceService.resource.board;
  const cardRegistry = Registry.card;

  let type;

  $scope.$watch(
    function watchTemplateType() {
      try {
        return cards[key].core.type;
      }
      catch (e) {
        return undefined;
      }
    },
    function loadTemplate(templateType) {
      type = templateType;
      if (type) {
        cardRegistry.load(templateType)
          .catch(error => $log.error(error))
        ;
      }
    },
  );

  const launch = function launchItem() {
    if (key !== '.') {
      if (Object.prototype.hasOwnProperty.call(metadata, key)) {
        LaunchService.item(key);
      }
    }
  };

  const open = function open() {
    OpenService(ResourceService.resource.location.path(key));
  };

  const icon = function cardShellIcon() {
    return Object.prototype.hasOwnProperty.call(metadata, key) ?
      'action:view_quilt' : 'navigation:more_vert';
  };

  return {
    get value() {
      return cards[key];
    },
    get metadata() {
      return metadata[key];
    },
    get isValidStatus() {
      const myStatus = CardStatus.readStatus.get(key);
      return myStatus === 'updating' || myStatus === 'success';
    },
    get status() {
      return CardStatus.readStatus.get(key);
    },
    get regStatus() {
      return type && cardRegistry.status[type] === 'Success';
    },
    get displayMode() {
      return CardDisplayService.mode(key);
    },

    style: CardShellStyleService(key),

    toggleSelection() {
      BoardSelection.cards.toggle(key);
    },
    get selectionNumber() {
      return BoardSelection.cards.selectionNumber(key);
    },

    get color() {
      if (key !== '.') {
        try {
          const myColor = cards[key].core.color;
          return myColor || '#ccff90';
        }
        catch (e) {
          return 'darkgrey';
        }
      }
      return 'white';
    },
    get type() {
      return type;
    },

    launch,
    open,
    icon,
  };
};

cardShellController.$inject = [
  'ResourceService',
  'BoardSelection',
  'CardStatus',

  'CardDisplayService',

  'LaunchService',
  'OpenService',

  'Registry',

  'CardShellStyleService',

  '$scope',
  '$log',
];

export default cardShellController;
