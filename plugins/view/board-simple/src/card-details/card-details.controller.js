/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardDetailsController = function cardDetailsController(
  ResourceService,
  BoardSelection,

  $scope,
) {
  let key;
  const { cards } = ResourceService.resource.board;
  const { files } = ResourceService.resource.directory;

  const selection = BoardSelection.cards;

  const updateKey = function updateKey(newVal) {
    key = newVal;
  };

  const getLastSelection = function getLastSelection() {
    return selection.lastSelection();
  };

  $scope.$watch(getLastSelection, updateKey);

  return {
    get key() {
      return key;
    },
    get card() {
      return key && cards[key];
    },
    get file() {
      return key && files[key]['_card.json'];
    },
    get selection() {
      return selection.selectionNumber(key);
    },
  };
};

cardDetailsController.$inject = [
  'ResourceService',
  'BoardSelection',

  '$scope',
];

export default cardDetailsController;
