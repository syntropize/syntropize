/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const boardSimpleController = function boardSimpleController(
  ResourceService,
  BoardSelection,

  BoardSort,
  BoardSearch,
  CardOrderService,
  dragulaService,

  $scope,
) {
  const { board } = ResourceService.resource;

  CardOrderService.init();

  $scope.$watchCollection(
    function returnResourceBoardCards() {
      return board.cards;
    },
    CardOrderService.reorder,
  );

  const initializeStop = $scope.$watch(
    function getOrderStatus() {
      return CardOrderService.status();
    },
    function initializeCardOrder(status) {
      if (status === 'error' || status === 'success') {
        initializeStop();
        CardOrderService.revert();
      }
    },
  );

  dragulaService.options($scope, 'card-bag', {
    direction: 'horizontal',
    moves: function setDragHandleOnCard(el, container, handle) {
      return handle.className === 'card-title-overlay';
    },
  });

  $scope.$on('card-bag.drop-model', function cardBagDropModel() {
    CardOrderService.update();
  });

  return {
    sort: BoardSort,
    search: BoardSearch,
    selection: BoardSelection,
    get isMainCard() {
      return (Object.prototype.hasOwnProperty.call(board.cards, '.'));
    },
    get metadata() {
      return board.metadata;
    },
    get displayCards() {
      return CardOrderService.displayCards;
    },
  };
};

boardSimpleController.$inject = [
  'ResourceService',
  'BoardSelection',

  'BoardSort',
  'BoardSearch',
  'CardOrderService',
  'dragulaService',

  '$scope',
];

export default boardSimpleController;
