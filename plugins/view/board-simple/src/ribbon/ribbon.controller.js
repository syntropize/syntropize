/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const ribbonController = function ribbonController(
  ResourceService,
  BoardSelection,
  ClipboardService,
  $panel,

  CardDisplayService,
  CardOrderService,
  CommandService,
  BoardCommandService,
  SelectionAggregationBoardService,
  ReadOnlyService,
  HelpService,
  LaunchService,
  Registry,
) {
  const cardRegistry = Registry.card;
  const compileRegistry = Registry.compile;

  return {
    get cards() {
      return ResourceService.resource.board.cards;
    },
    get files() {
      return ResourceService.resource.directory.files;
    },

    panel(...args) {
      $panel.start(...args);
    },
    runIcon(item) {
      return `${compileRegistry.location}-${item}/${compileRegistry.fileMap[item]['./icon_svg']}`;
    },

    get cardTypes() {
      return Object.keys(cardRegistry.items);
    },
    get compileTypes() {
      return Object.keys(compileRegistry.items);
    },

    display: CardDisplayService,
    selection: BoardSelection,

    selectionClear() {
      Object.values(BoardSelection.files).forEach(fileSelection => fileSelection.clear());
      BoardSelection.cards.clear();
    },
    saveOrder() {
      const order = CardOrderService.list.slice().filter(i => (i !== '.' && i !== ''));
      BoardCommandService.reorder(order);
    },
    revertOrder() {
      CardOrderService.revert();
    },
    card: {
      cut() {
        ClipboardService.cut('card', ResourceService.resource.location, BoardSelection.cards.list);
      },
      copy() {
        ClipboardService.copy('card', ResourceService.resource.location, BoardSelection.cards.list);
      },
      clearXC() {
        ClipboardService.clear('card');
      },
      paste(opts) {
        BoardCommandService.paste(ClipboardService.get('card'), '.', opts);
      },
      get pasteActive() {
        return ClipboardService.active('card');
      },
      remove(opts) {
        return BoardCommandService.remove(BoardSelection.cards.list.slice(), opts);
      },
      transclude() {
        BoardCommandService.card.transclude();
      },
      // Need some way to deal with missing meta files
      // get pasteBoardActive() {
      //   return ClipboardService.active('card') // &&;
      // },
    },
    file: {
      cut() {
        ClipboardService.cut('file', ResourceService.resource.location, SelectionAggregationBoardService.allSelectedFiles());
      },
      copy() {
        ClipboardService.copy('file', ResourceService.resource.location, SelectionAggregationBoardService.allSelectedFiles());
      },
      paste() {
        BoardCommandService.file.paste(ClipboardService.get('file'), '.');
      },
      clearXC() {
        ClipboardService.clear('file');
      },
      get pasteActive() {
        return ClipboardService.active('file');
      },
      create() {
        BoardCommandService.file.create('.');
      },
      createDir() {
        BoardCommandService.file.createDir('.');
      },
      add(event) {
        const { files } = event.target;
        const list = Object.values(files).map(({ name }) => name);
        const location = files[0].path.replace(list[0], '');
        BoardCommandService.file.copy(location, list);
      },
      addDir(event) {
        const { files } = event.target;
        const location = files[0].path.replace(files[0].name, '');
        BoardCommandService.file.copy(location, [files[0].name]);
      },
      remove() {
        BoardCommandService.file.remove('.', SelectionAggregationBoardService.allSelectedFiles());
      },
    },

    run: CommandService.run,
    get readOnly() {
      return ReadOnlyService.status;
    },
    help: HelpService,
    launch: LaunchService,
  };
};

ribbonController.$inject = [
  'ResourceService',
  'BoardSelection',
  'ClipboardService',
  '$panel',

  'CardDisplayService',
  'CardOrderService',
  'CommandService',
  'BoardCommandService',
  'SelectionAggregationBoardService',
  'ReadOnlyService',
  'HelpService',
  'LaunchService',
  'Registry',
];

export default ribbonController;
