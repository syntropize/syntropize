/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import './card-shell/card-shell.css';
import './card-sort/card-sort.css';
import './system-cards/file/file.css';
import './system-cards/status/status.css';
import './system-cards/template/template.css';
import './board-simple.css';

import cardDetailsController  from './card-details/card-details.controller.js';
import cardDetailsDirective   from './card-details/card-details.directive.js';

import cardInputController    from './card-input/card-input.controller.js';
import cardInputDirective     from './card-input/card-input.directive.js';
import cardInputService       from './card-input/card-input.service.js';
import cardAddAPI             from './card-input/card-add.api.service.js';
import cardEditAPI            from './card-input/card-edit.api.service.js';

import cardShellDirective     from './card-shell/card-shell.directive.js';
import cardShellController    from './card-shell/card-shell.controller.js';
import cardShellStyleService  from './card-shell/card-shell.style.service.js';

import cardOrderService       from './card-order/card-order.service.js';

import cardSearchController   from './card-search/card-search.controller.js';
import cardSearchDirective    from './card-search/card-search.directive.js';

import cardSortController     from './card-sort/card-sort.controller.js';
import cardSortDirective      from './card-sort/card-sort.directive.js';

import boardCommandService    from './command/command.service.js';

import boardRibbonController  from './ribbon/ribbon.controller.js';
import boardRibbonDirective   from './ribbon/ribbon.directive.js';

import selectionAggregationBoardService from './selection-aggregation/selection-aggregation.service.js';

import fileSystemCardDirective      from './system-cards/file/file.directive.js';
import fileSystemCardController     from './system-cards/file/file.controller.js';
import statusSystemCardDirective    from './system-cards/status/status.directive.js';
import statusSystemCardController   from './system-cards/status/status.controller.js';
import templateSystemCardDirective  from './system-cards/template/template.directive.js';
import templateSystemCardController from './system-cards/template/template.controller.js';

import boardSimpleController  from './board-simple.controller.js';
import boardSimpleDirective   from './board-simple.directive.js';

// export default angular.module('view.boardSimple.cardDetails', [
//   // 'ngFilesizeFilter',
// ])
// export default angular.module('view.boardSimple.cardShell', [
//   // 'ngSglclick',
//   // 'sCardHost',
//   // 'sCardFactory',
// ])
// export default angular.module('view.boardSimple.ribbon', [
//   // 'ngFileChange',
// ])
// export default angular.module('view.boardSimple.cardOrder', [
//   // 'filter.toArray',
// ])
// export default angular.module('view.boardSimple.cardInput', [
//   // 'sCardFactory',
//   // 'sCardHost',
// ])
// export default angular.module('systemCard.file', [
//   // 'angular-perfect-scrollbar-2',
//   // 'sFileList',
// ])
// export default angular.module('systemCard.status', [
//   // 'angular-perfect-scrollbar-2',
// ])
// export default angular.module('systemCard.template', [
//   // 'registry',
// ])
//
// const dependencies = [
//   // angularDragula(angular),
// ];

export default angular.module('view.boardSimple', [])

  // Card Details
  .controller('CardDetailsController', cardDetailsController)
  .directive('boardSimpleCardDetails', cardDetailsDirective)
  .constant('BoardSimpleCardDetailsAPI', {
    name: 'Card Details',
    tag: 'board-simple-card-details',
  })

  // Card Input
  .controller('CardInputController', cardInputController)
  .directive('boardSimpleCardInput', cardInputDirective)
  .factory('CardInputService', cardInputService)
  .factory('BoardSimpleCardAddAPI', cardAddAPI)
  .factory('BoardSimpleCardEditAPI', cardEditAPI)

  // Card Shell
  .directive('cardShell', cardShellDirective)
  .controller('CardShellController', cardShellController)
  .factory('CardShellStyleService', cardShellStyleService)

  // Card Order
  .value('CardOrder', {})
  .factory('CardOrderService', cardOrderService)

  // Card Search
  .controller('CardSearchController', cardSearchController)
  .directive('boardSimpleCardSearch', cardSearchDirective)
  .constant('BoardSimpleCardSearchAPI', {
    name: 'Card Search',
    tag: 'board-simple-card-search',
  })

  // Card Sort
  .controller('CardSortController', cardSortController)
  .directive('boardSimpleCardSort', cardSortDirective)
  .constant('BoardSimpleCardSortAPI', {
    name: 'Card Sort',
    tag: 'board-simple-card-sort',
  })

  // Command
  .factory('BoardCommandService', boardCommandService)

  // Ribbon
  .controller('BoardRibbonController', boardRibbonController)
  .directive('viewBoardSimpleRibbon', boardRibbonDirective)

  // Selection Aggregation
  .factory('SelectionAggregationBoardService', selectionAggregationBoardService)

  // System Cards
  .directive('fileSystemCard', fileSystemCardDirective)
  .controller('FileSystemCardController', fileSystemCardController)
  .directive('statusSystemCard', statusSystemCardDirective)
  .controller('StatusSystemCardController', statusSystemCardController)
  .directive('templateSystemCard', templateSystemCardDirective)
  .controller('TemplateSystemCardController', templateSystemCardController)

  .controller('BoardSimpleController', boardSimpleController)
  .directive('viewBoardSimple', boardSimpleDirective)
;
