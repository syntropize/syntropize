/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const fileSystemCardController = function fileSystemCardController(
  ResourceService,
  BoardSelection,
  BoardCommandService,
  ClipboardService,
  ReadOnlyService,
) {
  const card = this;
  const { files } = ResourceService.resource.directory;
  const fileSelection = BoardSelection.files;

  return {
    get items() {
      return files[card.key];
    },
    get selection() {
      return fileSelection[card.key];
    },
    get xc() {
      return ClipboardService.get('file');
    },
    get readOnly() {
      return ReadOnlyService.status;
    },
    remove() {
      BoardCommandService.file.remove(card.key, fileSelection[card.key].list.slice());
    },
    rename() {
      BoardCommandService.file.rename(card.key, fileSelection[card.key].lastSelection());
    },
    create() {
      BoardCommandService.file.create(card.key);
    },
    createDir() {
      BoardCommandService.file.createDir(card.key);
    },
    link() {
      BoardCommandService.file.link(card.key);
    },
    paste() {
      BoardCommandService.file.paste(card.file.xc, card.key);
    },
    // folderToBoard() {
    //   BoardCommandService.file.create(
    //     [card.key, fileSelection[card.key].lastSelection],
    //     "[]",
    //   );
    // },
    // BoardToFolder() {
    //   BoardCommandService.file.remove([card.key, fileSelection[card.key].lastSelection]);
    // },
    get xcActive() {
      return ClipboardService.active('file');
    },
    get isSelection() {
      return fileSelection[card.key] && fileSelection[card.key].selection();
    },
  };
};

fileSystemCardController.$inject = [
  'ResourceService',
  'BoardSelection',
  'BoardCommandService',
  'ClipboardService',
  'ReadOnlyService',
];

export default fileSystemCardController;
