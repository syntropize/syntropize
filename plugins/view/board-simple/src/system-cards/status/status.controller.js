/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const statusSystemCardController = function statusSystemCardController(
  ResourceService,
  CardStatus,
) {
  const card = this;
  return {
    get status() {
      return CardStatus.readStatus.get(card.key);
    },
    get error() {
      if (CardStatus.readStatus.get(card.key) === 'error') {
        return ResourceService.resource.board.cards[card.key];
      }
      if (!CardStatus.readStatus.get(card.key)) {
        return new Error('Status Unknown');
      }
      return undefined;
    },
  };
};

statusSystemCardController.$inject = [
  'ResourceService',
  'CardStatus',
];

export default statusSystemCardController;
