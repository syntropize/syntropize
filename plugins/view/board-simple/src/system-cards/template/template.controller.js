/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const templateSystemCardController = function templateSystemCardController(
  Registry,
) {
  const card = this;
  const cardRegister = Registry.card;

  return {
    template: {
      get status() {
        return cardRegister.status[card.type];
      },
      load: function loadTemplate() {
        return cardRegister.load(card.type);
      },
      refresh: function refreshTemplate() {
        return cardRegister.refresh(card.type);
      },
    },
  };
};

templateSystemCardController.$inject = [
  'Registry',
];

export default templateSystemCardController;
