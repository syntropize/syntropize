/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardInputService = function cardInputService(
  ResourceService,

  BoardCommandService,
  CardFactory,
  Registry,

  $timeout,
  $mdDialog,
  $log,
  $q,
) {
  const cardRegistry = Registry.card;

  let form = {};
  let key;

  const confirmDialog = $mdDialog.confirm()
    .title('Discard Card')
    .textContent('Are you sure you want to discard all changes to the card?')
    .ariaLabel('discard-card-changes')
    .ok('Discard!')
    .cancel('Not Yet!');

  const confirm = function confirmDiscard() {
    if (!form) {
      return $q.when();
    }
    else {
      return $mdDialog.show(confirmDialog);
    }
  };

  const reset = function resetCard() {
    form = {};
    key = undefined;
  };

  const add = function addCardStart(type, master) {
    return cardRegistry.load(type)
      .then(function cardLoadForAddSuccess() {
        // Timeout forces the form to reset with the ng-if
        $timeout(function setNewForm() {
          angular.copy({
            key: master ? '.' : undefined,
            name: undefined,
            board: true,
            card: CardFactory({ core: { type } }),
          }, form);
          ({ key } = form);
        }, 10);
      });
  };

  const edit = function editCardStart(cardKey) {
    return $q(function initEditCardForm(resolve) {
      // Timeout forces the form to reset with the ng-if
      $timeout(function setFormFromCard() {
        angular.copy({
          key: cardKey,
          name: undefined,
          card: CardFactory(angular.copy(ResourceService.resource.board.cards[cardKey])),
          board: Object.prototype.hasOwnProperty.call(
            ResourceService.resource.board.metadata,
            cardKey,
          ),
        }, form);
        key = cardKey;
      }, 10);
      resolve();
    });
  };

  const canSubmit = function isFormValid() {
    return form.name && (form.name.$dirty && form.name.$valid);
  };

  const submit = function submitCard() {
    let submitKey;
    let submitPromise;
    let step = 0;

    if (angular.isUndefined(key)) {
      submitKey = form.key;
      submitPromise = BoardCommandService.card.add(form.key, form.card);
    }
    else {
      submitKey = key;
      submitPromise = BoardCommandService.card.replace(key, form.card);
    }

    return submitPromise
      .then(function enableBoardIfNeeded() {
        step += 1;
        const keyExists = Object.prototype.hasOwnProperty.call(
          ResourceService.resource.board.metadata,
          submitKey,
        );

        if (form.board && !keyExists) {
          return BoardCommandService.start(submitKey);
        }
        if (!form.board && keyExists) {
          return BoardCommandService.cease(submitKey);
        }
        return undefined;
      })
      .then(function renameCardIfNeeded() {
        step += 1;
        if (key && form.key !== key) {
          return BoardCommandService.card.rename(key, form.key);
        }
        else {
          step += 1;
        }
        return undefined;
      })
      .then(function clearCard() {
        reset();
      })
      .catch(function cardSubmitFailure(err) {
        switch (step) {
          case 0:
            break;
          case 1:
          case 2:
          case 3:
          default:
            $log.error('Card Submit Failure at Step: ', step, err);
        }
      })
    ;
  };

  const cancel = function cancelCard() {
    if (!form.name || (form.name && form.name.$pristine)) {
      reset();
      return $q.when();
    }
    else {
      return confirm()
        .then(function discardCard() {
          $log.debug('Discarding Card!');
          reset();
          key = undefined;
        })
        .catch(function noAction() {
          $log.debug('Close Cancelled!');
          return $q.reject();
        })
      ;
    }
  };

  const isUnique = function isNameUnique(name) {
    if (name === key) { return true; }
    /** if the name is not the same as the original it cannot be
        same as any other name as well */
    return Object.keys(ResourceService.resource.board.cards).every(cardKey => (cardKey !== name));
  };

  const isLegal = function isKeyLegal(name) {
    return /^\w{1}[ \w\s\-.,;]*$/.test(name);
  };

  return {
    get form() {
      return form;
    },
    set form(value) {
      form = value;
    },
    add,
    edit,
    confirm,
    submit,
    canSubmit,
    cancel,
    valid: {
      isUnique,
      isLegal,
    },
  };
};

cardInputService.$inject = [
  'ResourceService',

  'BoardCommandService',
  'CardFactory',
  'Registry',

  '$timeout',
  '$mdDialog',
  '$log',
  '$q',
];

export default cardInputService;
