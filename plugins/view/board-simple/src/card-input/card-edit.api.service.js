/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const cardEditAPI = function cardEditAPI(
  CardInputService,
) {
  return {
    name: 'Card Edit',
    tag: 'board-simple-card-input',
    start: CardInputService.edit,
    submit: CardInputService.submit,
    canSubmit: CardInputService.canSubmit,
    cancel: CardInputService.cancel,
  };
};

cardEditAPI.$inject = [
  'CardInputService',
];

export default cardEditAPI;
