# Board-Simple View

The **Board-Simple View** plugin represents a resource as tiled cards placed on a virtual corkboard, a bit like applications such as Evernote and Scriviner. The view can dynamically load cards of different media as needed to represent the content.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
