# File-List View

The **File-List View** plugin presents the contents of a resource.

## Documentation

To learn more, please visit the Syntropize website at https://syntropize.com.
Documentation can also be accessed from the Syntropize application through the home screen.
