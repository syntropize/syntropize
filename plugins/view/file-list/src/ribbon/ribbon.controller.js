/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const directoryRibbonController = function directoryRibbonController(
  ResourceService,
  $panel,

  DirectorySelection,
  ClipboardService,

  CommandService,
  DirectoryCommandService,
  ReadOnlyService,
  HelpService,
  LaunchService,
  Registry,
) {
  const compileRegistry = Registry.compile;
  const cardRegistry = Registry.card;
  return {
    get files() {
      return ResourceService.resource.directory.files['.'];
    },
    panel: $panel,
    runIcon(item) {
      return `${compileRegistry.location}-${item}/${compileRegistry.fileMap[item]['./icon_svg']}`;
    },

    get cardTypes() {
      return Object.keys(cardRegistry.items);
    },
    get compileTypes() {
      return Object.keys(compileRegistry.items);
    },
    get readOnly() {
      return ReadOnlyService.status;
    },
    selection: DirectorySelection,
    selectionAggr() {
      return DirectorySelection.files.list.slice();
    },
    selectionClear() {
      return DirectorySelection.files.clear();
    },
    cut() {
      ClipboardService.cut('file', ResourceService.resource.location, DirectorySelection.files.list);
    },
    copy() {
      ClipboardService.copy('file', ResourceService.resource.location, DirectorySelection.files.list);
    },
    paste() {
      DirectoryCommandService.paste(ClipboardService.get('file'));
    },
    clearXC() {
      ClipboardService.clear('file');
    },
    get pasteActive() {
      return ClipboardService.active('file');
    },
    remove() {
      DirectoryCommandService.remove(DirectorySelection.files.list.slice());
    },
    command: DirectoryCommandService,
    run: CommandService.run,
    help: HelpService,
    launch: LaunchService,
  };
};

directoryRibbonController.$inject = [
  'ResourceService',
  '$panel',

  'DirectorySelection',
  'ClipboardService',

  'CommandService',
  'DirectoryCommandService',
  'ReadOnlyService',
  'HelpService',
  'LaunchService',
  'Registry',
];

export default directoryRibbonController;
