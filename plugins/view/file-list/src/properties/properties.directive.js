/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import filePropertiesTemplate from './properties.html';

const filePropertiesDirective = function filePropertiesDirective() {
  return {
    template: filePropertiesTemplate,
    restrict: 'E',
    scope: {},
    controller: 'FilePropertiesController',
    controllerAs: 'properties',
  };
};

export default filePropertiesDirective;
