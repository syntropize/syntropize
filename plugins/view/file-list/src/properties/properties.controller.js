/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const filePropertiesController = function filePropertiesController(
  ResourceService,
  DirectorySelection,

  $scope,
) {
  let name;
  const { files } = ResourceService.resource.directory;

  const selection = DirectorySelection.files;

  const updateName = function updateName(newVal) {
    name = newVal;
  };

  const getLastSelection = function getLastSelection() {
    return selection.lastSelection();
  };

  $scope.$watch(getLastSelection, updateName);

  return {
    get name() {
      return DirectorySelection.files.lastSelection();
    },
    get files() {
      return files['.'][name];
    },
    get selection() {
      return selection.selectionNumber(name);
    },
  };
};

filePropertiesController.$inject = [
  'ResourceService',
  'DirectorySelection',

  '$scope',
];

export default filePropertiesController;
