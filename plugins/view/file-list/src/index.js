/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import './properties/properties.css';
import './file-list.css';

import directoryCommandService   from './command/command.service.js';

import filePropertiesDirective  from './properties/properties.directive.js';
import filePropertiesController from './properties/properties.controller.js';

import directoryRibbonDirective  from './ribbon/ribbon.directive.js';
import directoryRibbonController from './ribbon/ribbon.controller.js';

// import sort from 'sort';
// import search from 'search';

import fileListDirective  from './file-list.directive.js';
import fileListController from './file-list.controller.js';

// export default angular.module('view.fileList.properties', [
//   // ngFilesizeFilter
// ])
// const dependencies = [
//   // sort,
//   // search,
// ];

export default angular.module('view.fileList', [])

  // Commands
  .factory('DirectoryCommandService', directoryCommandService)

  // Properties Panel
  .directive('fileListProperties', filePropertiesDirective)
  .controller('FilePropertiesController', filePropertiesController)
  .constant('FileListPropertiesAPI', {
    tag: 'file-list-properties',
    name: 'File Properties',
  })

  // Ribbon
  .directive('viewFileListRibbon', directoryRibbonDirective)
  .controller('DirectoryRibbonController', directoryRibbonController)

  .directive('viewFileList', fileListDirective)
  .controller('ViewFileListController', fileListController)
;
