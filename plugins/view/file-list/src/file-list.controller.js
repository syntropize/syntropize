/*!
 *  Copyright (c) 2016, 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const fileListController = function fileListController(
  ResourceService,

  DirectorySelection,
  ClipboardService,
  // FileSort,
  // FileSearch,
) {
  const { directory } = ResourceService.resource;

  return {
    get selection() {
      return DirectorySelection.files;
    },
    get xc() {
      return ClipboardService.get('file');
    },
    get files() {
      return directory.files['.'];
    },
    // sort:FileSort,
    // search:FileSearch,
  };
};

fileListController.$inject = [
  'ResourceService',

  'DirectorySelection',
  'ClipboardService',
  // 'FileSort',
  // 'FileSearch',
];

export default fileListController;
