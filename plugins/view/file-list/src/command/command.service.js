/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const commandService = function commandService(
  ResourceService,
  DirectorySelection,

  ErrorToastService,
  SuccessToastService,
  ConfirmDialogService,
  InputDialogService,
  $log,
) {
  const filenameRE = /^\w{1}[ \w-.,;]*$/;

  function renameDuplicate(exists, item) {
    if (exists(item)) {
      const [name, ...extn] = item.split('.');
      const parts = name.split('_');
      const last = parts.pop();
      let count = Number(last);

      if (Number.isNaN(count)) {
        parts.push(last);
        count = 1;
      }

      do {
        const value = [`${parts.join('_')}_${count}`, ...extn].join('.');
        if (exists(value)) { count += 1; }
        else { return value; }
      } while (count);
    }

    return item;
  }

  const move = function moveDirectoryItems(...args) {
    return ResourceService.resource.directory.writer.move(...args)
      .then(function onMoveDirectoryItemsSuccess() {
        SuccessToastService({ message: 'Success: Files Moved' });
      })
      .catch(function onMoveDirectoryItemsError(error) {
        ErrorToastService(error);
        return error;
      })
    ;
  };

  const copy = function copyDirectoryItems(...args) {
    return ResourceService.resource.directory.writer.copy(...args)
      .then(function onCopyDirectoryItemsSuccess() {
        SuccessToastService({ message: 'Success: Files Copied' });
      })
      .catch(function onCopyDirectoryItemsError(error) {
        ErrorToastService(error);
        return error;
      })
    ;
  };

  const directory = {
    create: function createDirectoryItem() {
      return InputDialogService({
        text: 'Enter new name',
        title: 'Create File',
        cancel: 'Skip',
        continue: 'Create',
        value: undefined,
        legal: filenameRE,
        blacklist: ResourceService.resource.directory.files['.'],
      })
        .then(function onValidNewDirectoryName(value) {
          return ResourceService.resource.directory.writer.write(value, '');
        })
        .then(function onCreateDirectoryItemSuccess() {
          SuccessToastService({ message: 'Success: File Created' });
        })
        .catch(function onCreateDirectoryItemError(error) {
          ErrorToastService(error);
          return error;
        })
      ;
    },

    createDir: function createNewDirectory() {
      let newName;
      return InputDialogService({
        text: 'Enter new name',
        title: 'New Directory',
        cancel: 'Skip',
        continue: 'Create',
        value: undefined,
        legal: filenameRE,
        blacklist: ResourceService.resource.directory.files['.'],
      })
        .then(function createNewDirectoryOnValidName(value) {
          newName = value;
          return ResourceService.resource.directory.writer.createDir(value);
        })
        .then(function onCreateNewDirectorySuccess() {
          return SuccessToastService({ message: `Success: New Directory '${newName}' Created` });
        })
        .catch(function onCreateNewDirectoryError(error) {
          if (error) {
            ErrorToastService(error);
          }
          return error;
        })
      ;
    },

    symlink: function symlinkDirectoryItem(...args) {
      return ResourceService.resource.directory.writer.symlink(...args)
        .then(function onSymlinkDirectoryItemSuccess() {
          SuccessToastService({ message: 'Success: File Written' });
        })
        .catch(function onSymlinkDirectoryItemError(error) {
          ErrorToastService(error);
          return error;
        })
      ;
    },

    remove: function removeDirectoryItem(itemList) {
      if (itemList.length === 0) {
        throw new Error('No item selected for deletion!');
      }
      return ConfirmDialogService({
        text: 'Are you sure?',
        title: 'Delete Files',
        cancel: 'Skip',
        continue: 'Delete',
      })
        .then(function onRemoveDirectoryItemConfirm() {
          return ResourceService.resource.directory.writer.remove(itemList);
        })
        .then(function onRemoveDirectoryItemSuccess() {
          SuccessToastService({
            message: (itemList.length === 1) ?
              'Success: File Deleted' :
              'Success: Files Deleted',
          });
          DirectorySelection.files.clear();
        })
        .catch(function onRemoveDirectoryItemError(error) {
          if (error) {
            if (error.status === 'partial') {
              // temp fix for improper deletes with no real-time server updates
              // TODO: DirectorySelection.files.clear();
            }
            // DirectorySelection.files.clear();
            ErrorToastService(error);
            $log.error(error);
          }
          return error;
        })
      ;
    },

    rename: function renameDirectoryItem(...args) {
      let newName;
      return InputDialogService({
        text: 'Enter new name: ',
        title: 'Rename File',
        cancel: 'Skip',
        continue: 'Rename',
        value: args[0],
        legal: filenameRE,
        blacklist: ResourceService.resource.directory.files['.'],
      })
        .then(function onValidDirectoryItemName(value) {
          newName = value;
          return ResourceService.resource.directory.writer.rename(args[0], value);
        })
        .then(function onRenameDirectoryItemSuccess() {
          DirectorySelection.files.toggle(args[0]);
          DirectorySelection.files.toggle(newName);
          SuccessToastService({ message: 'Success: File Renamed' });
        })
        .catch(function onRenameDirectoryItemError(error) {
          if (error) { ErrorToastService(error); }
          return error;
        })
      ;
    },

    paste: function pasteDirectoryItems({ mode, location, items }) {
      const isSameLocation =
        ResourceService.resource.location.address() === location.address();
      const list = [...items.values()];

      const { files } = ResourceService.resource.directory;
      const check = function checkIfFileExists(value) {
        return !!files['.'][value];
      };

      if (mode === 'X') {
        if (isSameLocation) {
          ErrorToastService(new Error('Duh! Cannot move items to same location'));
          return undefined;
        }
        return move(
          location,
          list,
        );
      }
      else if (mode === 'C') {
        const toList = list.map(renameDuplicate.bind(null, check));
        return copy(
          location,
          list,
          isSameLocation ? toList : undefined,
        );
      }
      else {
        const error = new Error('No Files Selected');
        ErrorToastService(error);
        $log.error(error);
        return undefined;
      }
    },
  };

  return Object.freeze(directory);
};

commandService.$inject = [
  'ResourceService',
  'DirectorySelection',

  'ErrorToastService',
  'SuccessToastService',
  'ConfirmDialogService',
  'InputDialogService',
  '$log',
];

export default commandService;
